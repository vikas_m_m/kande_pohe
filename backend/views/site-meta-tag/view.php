<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\SiteMetaTag */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Site Meta Tags', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>



<div class="box box-primary">
    <div class="box-body box-profile">
        <!--<img class="profile-user-img img-responsive img-circle" src="<?= Yii::getAlias('@web') . '/images/' ?>default.png" alt="User profile picture">-->

        <h3 class="profile-username text-center"><h1><?= Html::encode($this->title) ?></h1></h3>
    </div>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'meta_tag_id',
            'site_url:ntext',
            'type:ntext',
            'title:ntext',
            'description:ntext',
            'meta_title:ntext',
            'meta_description:ntext',
            'meta_keyword:ntext',
        ],
    ]) ?>

    <div class="row">
        <div class="col-md-6">
            <?= Html::a('Update', ['update', 'id' => $model->meta_tag_id], ['class' => 'btn btn-block btn-primary']) ?>
        </div>
        <div class="col-md-6">
            <?= Html::a('Delete', ['delete', 'id' => $model->meta_tag_id], ['class' => 'btn btn-danger btn-block',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </div>

    </div>

</div>
