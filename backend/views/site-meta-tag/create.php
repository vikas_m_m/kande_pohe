<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SiteMetaTag */

$this->title = 'Create Site Meta Tag';
$this->params['breadcrumbs'][] = ['label' => 'Site Meta Tags', 'url' => [strtolower($meta_tag_section)]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary">
    <div class="box-header with-border">

    </div>
    <!-- /.box-header -->
    <!-- form start -->

    <div class="box-body">

        <div class="blood-group-create">

            <!--<h1><? /*= Html::encode($this->title) */ ?></h1>-->

            <?= $this->render('_form', [
                'model' => $model,
                'type' => $type
            ]) ?>

        </div>
    </div>
</div>