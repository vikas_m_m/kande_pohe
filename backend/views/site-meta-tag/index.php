<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\components\CommonHelper;
/* @var $this yii\web\View */
/* @var $searchModel common\models\SiteMetaTagSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $title;//'Site Meta Tags';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="email-format-index">
    <div class="blood-group-index">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?= $this->title ?></h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-sm-6"></div>
                                <div class="col-sm-6"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="example2" class="table table-bordered table-hover dataTable" role="grid"
                                           aria-describedby="example2_info">
                                        <thead>
                                        <tr role="row">
                                        </thead>
                                        <tbody>

                                        <p>
                                            <!--  <?= Html::a('Create ' . $type . ' Meta Tag', ['create', 'type' => $type], ['class' => 'btn btn-success pull-right']) ?> -->
                                        </p>
                                        <?= GridView::widget([
                                            'dataProvider' => $dataProvider,
                                            'filterModel' => $searchModel,
                                            'columns' => [
                                                ['class' => 'yii\grid\SerialColumn'],

                                                #'meta_tag_id',
                                                #'site_url:ntext',
                                                /* [
                                                     'attribute'=>'site_url',
                                                     'label'=>'URL Name',

                                                 ],*/
                                                //'type:ntext',
                                                'title:ntext',
                                                #'description:ntext',
                                                #'meta_title:ntext',
                                                #'meta_description:ntext',
                                                #'meta_keyword:ntext',
                                                [
                                                    'attribute' => 'description',
                                                    'format' => 'raw',
                                                    'value' => function ($model, $key, $index) {
                                                        $commonhelper = new CommonHelper();
                                                        return $commonhelper->truncate(strip_tags($model->description), 30);
                                                    },
                                                ],
                                                [
                                                    'attribute' => 'meta_title',
                                                    'format' => 'raw',
                                                    'value' => function ($model, $key, $index) {
                                                        $commonhelper = new CommonHelper();
                                                        return $commonhelper->truncate(strip_tags($model->meta_title), 30);
                                                    },
                                                ],
                                                [
                                                    'attribute' => 'meta_description',
                                                    'format' => 'raw',
                                                    'value' => function ($model, $key, $index) {
                                                        $commonhelper = new CommonHelper();
                                                        return $commonhelper->truncate(strip_tags($model->meta_description), 30);
                                                    },
                                                ],
                                                [
                                                    'attribute' => 'meta_keyword',
                                                    'format' => 'raw',
                                                    'value' => function ($model, $key, $index) {
                                                        $commonhelper = new CommonHelper();
                                                        return $commonhelper->truncate(strip_tags($model->meta_keyword), 30);
                                                    },
                                                ],


                                                ['class' => 'yii\grid\ActionColumn'],
                                            ],
                                        ]); ?>
                                        </tbody>
                                        <tfoot>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
            <!-- /.col -->
        </div>

