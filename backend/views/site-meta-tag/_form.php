<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;
use common\components\CommonHelper;
use common\components\MessageHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $model common\models\SiteMetaTag */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="site-meta-tag-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'site_url')->textInput(['readonly' => !$model->isNewRecord]) ?> <!-- Based On Type-->
    <!-- <?php if($type == \common\models\SiteMetaTag::EDUCATION) { ?>
        <?= $form->field($model, 'site_url')->dropDownList(
            ArrayHelper::map(CommonHelper::getEducationField(), 'iEducationFieldID', 'vEducationFieldName'),
            ['class' => 'multiselect select4 tag-select-box',
            ]
        )->label(false)->error(false); ?>
    <?php }?> -->
    <?php if($model->isNewRecord) { ?>
    <?= $form->field($model, 'type')->dropDownList([
            \common\models\SiteMetaTag::CITY => \common\models\SiteMetaTag::CITY ,
            \common\models\SiteMetaTag::CASTE => \common\models\SiteMetaTag::CASTE,
            \common\models\SiteMetaTag::EDUCATION => \common\models\SiteMetaTag::EDUCATION,
            \common\models\SiteMetaTag::OCCUPATION => \common\models\SiteMetaTag::OCCUPATION,
            \common\models\SiteMetaTag::PHYSICAL_STATUS => \common\models\SiteMetaTag::PHYSICAL_STATUS,
            \common\models\SiteMetaTag::MARITAL_STATUS => \common\models\SiteMetaTag::MARITAL_STATUS,
        ]) ?>
    <?php } ?>
    <?= $form->field($model, 'title')->textInput() ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'meta_title')->textInput() ?>

    <?= $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'meta_keyword')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
