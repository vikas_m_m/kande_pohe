<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\WorkingAS */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="working-as-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'vWorkingAsName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'eStatus')->dropDownList(['Active' => 'Active', 'Inactive' => 'Inactive',], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
