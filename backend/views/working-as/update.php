<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\WorkingAS */

$this->title = 'Update Working As: ' . $model->vWorkingAsName;
$this->params['breadcrumbs'][] = ['label' => 'Working As', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->vWorkingAsName, 'url' => ['view', 'id' => $model->iWorkingAsID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box box-primary">
    <div class="box-header with-border">
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="blood-group-update">

            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>