<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use common\components\CommonHelper;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

if(count($user_photos)==0){ ?>
    <div class="alert alert-info alert-dismissible">
        <h5><i class="fa fa-info"></i> Information!</h5>
        Photo not available at this moment.
    </div>
<?php    }else{ ?>
    <?php foreach ($user_photos as $K => $V) {
        ?>
        <?php $SELECTED = '';
        if ($V['Is_Profile_Photo'] == 'YES') {
            $SELECTED = "selected";
        }
        $PhotoHeading = '';
        $PhotoMessage = '';
        if ($V->eStatus == 'Approve') {
            $PhotoHeading = 'Approved';
            $PhotoMessage = Yii::$app->params['photoApprovedMode'];
        } else if ($V->eStatus == 'Pending') {
            $PhotoHeading = 'Pending';
            $PhotoMessage = Yii::$app->params['photoPendingMode'];
        } else {
            $PhotoHeading = 'Disapproved';
            $PhotoMessage = Yii::$app->params['photoDisapprovedMode'];
        }

        $data_src_and_a = str_replace("backend","frontend",CommonHelper::getPhotos('USER',  $V['iUser_ID'], $V['File_Name']));
        $display_pic = str_replace("backend","frontend",CommonHelper::getPhotos('USER', $V['iUser_ID'], $V['File_Name']));
        #$display_pic = str_replace("backend","frontend",CommonHelper::getPhotos('USER', 1127, Yii::$app->params['thumbnailPrefix'] . "110_" . $V['File_Name'], 110));

        ?>
        <div data-src="<?= $data_src_and_a ?>"
             data-sub-html="<h4><?= $PhotoHeading ?></h4><p><?= $PhotoMessage ?></p>"
             class="kp_gallery col-md-3 col-sm-3 col-xs-6">
            <div class="<?= ($V->eStatus == 'Approve') ? 'gallery1 ' : 'img-blur1' ?>">
                <a class="<?= $SELECTED ?>"
                   data-toggle="tooltip" data-placement="top"
                   href="<?= $data_src_and_a ?>"
                    <?php if ($V->eStatus == 'Approve') { ?>
                        data-original-title="Click for full view"
                    <?php } else { ?>
                        data-original-title="<?= ($V->eStatus == 'Pending') ? 'Awaiting Approval' : 'Please Remove this Photo.' ?>"
                    <?php } ?>>
                    <?= Html::img($display_pic , ['class' => 'img-responsive img-fluid mb-3' . $SELECTED, 'width' => '140', 'alt' => 'Photo' . $K]); ?>
                </a>
            </div>
            <?php if ($V->eStatus == 'Approve' || $V->eStatus == 'Pending') { ?>
                <a href="javascript:void(0)"
                   class="pull-left profile_set_kp set_profile_photo kp_not_gallery"
                   data-id="<?= $V['iPhoto_ID'] ?>"
                   data-target="#profilecrop" data-toggle="modal"
                   data-item="<?= $data_src_and_a ?>"
                   data-name="<?= $V['File_Name'] ?>">
                    Profile pic
                </a>

            <?php } else { ?>
                <a href="javascript:void(0)"
                   class="kp_not_gallery"
                   data-id="<?= $V['iPhoto_ID'] ?>"
                   data-item="<?= $data_src_and_a ?>"
                   data-name="<?= $V['File_Name'] ?>">
                    <?= ($V->eStatus == 'Pending') ? 'Pending' : 'Disapproved' ?>
                </a>
                <?php if ($V->eStatus == 'Disapprove') { ?>
                <?php } ?>
            <?php } ?>
            <a href="javascript:void(0)"
               class="pull-right profile_delete kp_not_gallery"
               data-id="<?= $V['iPhoto_ID'] ?>"
               data-target="#photodelete" data-toggle="modal">
                <i aria-hidden="true" class="fa fa-trash-o text-red"></i>
            </a>
        </div>
    <?php } ?>
<?php   }
?>