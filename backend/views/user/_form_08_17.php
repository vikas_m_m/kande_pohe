<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use common\components\CommonHelper;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */


/*$this->title = $model->First_Name." ".$model->Last_Name;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/
//echo Yii::$app->urlManagerFrontend->baseUrl;
$PROPIC = CommonHelper::getPhotosBackend('USER', $model->id, "200" . $model->propic, 200, '', 'Yes', 0);
?>
    <!--<h1><?/*= Html::encode($this->title) */?></h1>-->

    <div class="box box-primary main-section">
        <div class="box-body box-profile text-center">
            <!--<img class="profile-user-img img-responsive img-circle" src="<?/*= $PROPIC */?>" alt="User profile picture">-->
            <h2> <?=$this->title?>'s Profile</h2>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        <ul class="nav nav-tabs" id="custom-content-below-tab" role="tablist">
                            <li class="nav-item active">
                                <a class="nav-link text-bold" id="custom-content-below-home-tab" data-toggle="pill" href="#custom-content-below-home" role="tab" aria-controls="custom-content-below-home" aria-selected="false">User Profile</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link text-bold" id="custom-content-below-messages-tab" data-toggle="pill" href="#custom-content-below-messages" role="tab" aria-controls="custom-content-below-messages" aria-selected="false">User Album</a>
                            </li>

                        </ul>
                        <div class="tab-content" id="custom-content-below-tabContent">
                            <div class="tab-pane fade active in" id="custom-content-below-home" role="tabpanel" aria-labelledby="custom-content-below-home-tab">
                                <br>
                                <?php $form = ActiveForm::begin([
                                    'id' =>     'active-form',
                                    'options' => [
                                        'class' => 'form-horizontal',
                                        'validateOnChange' => true,
                                        'validateOnSubmit' => true,
                                        'enctype' => 'multipart/form-data'
                                    ],
                                ]); ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box box-warning">
                                            <div class="box-header with-border">
                                                <h3 class="box-title text-bold">Basic information</h3>
                                            </div>
                                            <div class="box-body">
                                                <div class="col-md-6">
                                                    <?= $form->field($model, 'First_Name', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->textInput(['maxlength' => true])->label("First Name<span class='text-danger'>*</span>")?>

                                                    <?= $form->field($model, 'Last_Name', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->textInput(['maxlength' => true])->label("Last Name<span class='text-danger'>*</span>")?>

                                                    <?= $form->field($model, 'Gender', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->RadioList(
                                                        ['MALE' => 'MALE', 'FEMALE' => 'FEMALE'],
                                                        [
                                                            'item' => function ($index, $label, $name, $checked, $value) {
                                                                $checked = ($checked) ? 'checked' : '';
                                                                $return = '<input type="radio" class = "genderV" id="' . $value . '" name="' . $name . '" value="' . $value . '" ' . $checked . '>';
                                                                $return .= '<label for="' . $value . '">' . ucwords($label) . '</label>';
                                                                return $return;
                                                            }
                                                        ]
                                                    )->label("Gender<span class='text-danger'>*</span>")?>
                                                    <?= $form->field($model, 'DOB', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->textInput()
                                                        ->widget(\yii\jui\DatePicker::classname(),
                                                            [
                                                                'dateFormat' => 'php:Y-m-d',
                                                                'options' => [
                                                                    'id' => 'DOB',
                                                                    'class' => 'form-control',
                                                                    'onkeyup' => ' $(".hasDatepicker").val("");',
                                                                ],
                                                                'clientOptions' => [
                                                                    'changeMonth' => true,
                                                                    'yearRange' => '-70:-21',
                                                                    'changeYear' => true,
                                                                    'maxDate' => date('Y-m-d', strtotime('-21 year')),
                                                                ]

                                                            ])->label("Date Of Birth<span class='text-danger'>*</span>")?>

                                                    <?= $form->field($model, 'Profile_created_for', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->dropDownList(
                                                        Yii::$app->params['profileFor'],
                                                        ['class' => 'demo-default select-beast clspersonalinfo', 'prompt' => 'Profile For']
                                                    )->label("Profiel Created For<span class='text-danger'>*</span>")?>

                                                    <?= $form->field($model, 'mother_tongue', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->dropDownList(
                                                        ArrayHelper::map(CommonHelper::getMotherTongue(), 'ID', 'Name'),
                                                        ['class' => 'demo-default select-beast clspersonalinfo', 'prompt' => 'Mother Tongue']
                                                    )->label("Mother Tongue<span class='text-danger'>*</span>")?>

                                                    <?= $form->field($model, 'iReligion_ID', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->dropDownList(
                                                        ArrayHelper::map(CommonHelper::getReligion(), 'iReligion_ID', 'vName'),
                                                        ['class' => 'demo-default select-beast clsbasicinfo', 'prompt' => 'Religion']
                                                    )->label("Religion<span class='text-danger'>*</span>")?>

                                                    <?= $form->field($model, 'iCommunity_ID', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->dropDownList(
                                                        ArrayHelper::map(CommonHelper::getCommunity(), 'iCommunity_ID', 'vName'),
                                                        ['class' => 'demo-default select-beast clsbasicinfo', 'prompt' => 'Community']
                                                    )->label("Community<span class='text-danger'>*</span>")?>
                                                </div>
                                                <div class="col-md-6">
                                                    <?= $form->field($model, 'iSubCommunity_ID', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->textInput()?>

                                                    <?= $form->field($model, 'iGotraID', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->dropDownList(
                                                        ArrayHelper::map(CommonHelper::getGotra(), 'iGotraID', 'vName'),
                                                        ['class' => 'demo-default select-beast clsbasicinfo', 'prompt' => 'Gotra']
                                                    )?>

                                                    <?= $form->field($model, 'iMaritalStatusID', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->dropDownList(
                                                        ArrayHelper::map(CommonHelper::getMaritalStatus(), 'iMaritalStatusID', 'vName'),
                                                        ['prompt' => 'Maritial Status',
                                                            'class' => 'demo-default select-beast clsbasicinfo',
                                                            'onchange' => '
                        var iMaritalStatusID = $(this).val();
                        if(iMaritalStatusID == "4" || iMaritalStatusID == "5"){
                          $("#noc_div").show();
                        }
                        else {
                          $("#noc_div").hide();
                          $("#noc").val("0");
                        }
                    '
                                                        ]
                                                    )->label("Marital Status<span class='text-danger'>*</span>")?>

                                                    <?php
                                                    if ($model->iMaritalStatusID == '4' || $model->iMaritalStatusID == '5') {
                                                        $style = "display:block";
                                                    } else {
                                                        $style = "display:none";
                                                    }
                                                    ?>
                                                    <div id="noc_div" style="<?= $style ?>">
                                                        <?= $form->field($model, 'noc')->input('number', ['id' => 'noc']) ?>
                                                    </div>

                                                    <?= $form->field($model, 'iCountryId', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->dropDownList(
                                                        ArrayHelper::map(CommonHelper::getCountry(), 'iCountryId', 'vCountryName'),
                                                        ['prompt' => 'Country', 'class' => 'demo-default select-beast clsbasicinfo',
                                                            'onchange' => '
                               $.post( "' . Yii::$app->urlManager->createUrl('ajax/getstatenew?id=') . '"+$(this).val(), function( data ) {
                                 var htmldata = "";
                                                            jsondata = data.state;
                                                                    var new_value_options   = "[";
                                                                    for (var key in jsondata) {
                                                                    //console.log(jsondata[key].vStateName);
                                                                        htmldata += "<option value=\'"+jsondata[key].iStateId+"\'>"+jsondata[key].vStateName+"</option>";

                                                                        var keyPlus = parseInt(key) + 1;
                                                                        if (keyPlus == jsondata.length) {
                                                                            new_value_options += "{text: \'"+jsondata[key].vStateName+"\', value: "+jsondata[key].iStateId+"}";
                                                                        } else {
                                                                            new_value_options += "{text: \'"+jsondata[key].vStateName+"\', value: "+jsondata[key].iStateId+"},";
                                                                        }
                                                                    }
                                                                    new_value_options   += "]";

                                                            new_value_options = eval("(" + new_value_options + ")");
                                                            if (new_value_options[0] != undefined) {
                                                                        // re-fill html select option field
                                                                        $("select#iStateId").html(htmldata);
                                                                        // re-fill/set the selectize values
                                                                        var selectize = $("select#iStateId")[0].selectize;
                                                                        selectize.clear();
                                                                        selectize.clearOptions();
                                                                        selectize.renderCache["option"] = {};
                                                                        selectize.renderCache["item"] = {};

                                                                        selectize.addOption(new_value_options);
                                                                        //selectize.setValue(iStateId);

                                                                        var selectize = $("select#iCityId")[0].selectize;
                                                                        selectize.clear();
                                                                        selectize.clearOptions();

                                                                         if(data.CountryId == 101){
                                                                            $(".user_idistrictid_div").show();
                                                                            $(".user_iTalukaID_div").show();
                                                                            var selectize = $("select#user-idistrictid")[0].selectize;
                                                                            selectize.clear();
                                                                            var selectize = $("select#user-italukaid")[0].selectize;
                                                                            selectize.clear();
                                                                        }else{
                                                                            $(".user_idistrictid_div").hide();
                                                                            var selectize = $("select#user-idistrictid")[0].selectize;
                                                                            selectize.clear();
                                                                            selectize.setValue(1);
                                                                            $(".user_iTalukaID_div").hide();
                                                                            var selectize = $("select#user-italukaid")[0].selectize;
                                                                            selectize.clear();
                                                                            selectize.setValue(1);

                                                                        }

                                                            }
                                });'
                                                        ]

                                                    )->label("Country<span class='text-danger'>*</span>")?>

                                                    <?php
                                                    $stateList = [];
                                                    if ($model->iCountryId != "") {
                                                        $stateList = ArrayHelper::map(CommonHelper::getState($model->iCountryId), 'iStateId', 'vStateName');
                                                    }
                                                    ?>
                                                    <?= $form->field($model, 'iStateId', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->dropDownList(
                                                        $stateList,
                                                        ['id' => 'iStateId',
                                                            'prompt' => 'State', 'class' => 'demo-default select-beast clsbasicinfo',
                                                            'onchange' => '
                               $.post( "' . Yii::$app->urlManager->createUrl('ajax/getcitynew?id=') . '"+$(this).val(), function( data ) {
                                  //$( "select#iCityId" ).html( data );
                                  //$("select#iCityId").niceSelect("update");
                                  var htmldata = "";
                                                            jsondata = data.city;
                                                                    var new_value_options   = "[";
                                                                    for (var key in jsondata) {
                                                                        htmldata += "<option value=\'"+jsondata[key].iCityId+"\'>"+jsondata[key].vCityName+"</option>";
                                                                        var keyPlus = parseInt(key) + 1;
                                                                        if (keyPlus == jsondata.length) {
                                                                            new_value_options += "{text: \'"+jsondata[key].vCityName+"\', value: "+jsondata[key].iCityId+"}";
                                                                        } else {
                                                                            new_value_options += "{text: \'"+jsondata[key].vCityName+"\', value: "+jsondata[key].iCityId+"},";
                                                                        }
                                                                    }
                                                                    new_value_options   += "]";

                                                            new_value_options = eval("(" + new_value_options + ")");
                                                            if (new_value_options[0] != undefined) {
                                                                        // re-fill html select option field
                                                                        $("select#iCityId").html(htmldata);
                                                                        // re-fill/set the selectize values
                                                                        var selectize = $("select#iCityId")[0].selectize;
                                                                        selectize.clear();
                                                                        selectize.clearOptions();
                                                                        selectize.renderCache["option"] = {};
                                                                        selectize.renderCache["item"] = {};

                                                                        selectize.addOption(new_value_options);
                                                                       // selectize.setValue(iCityId);
                                                            }
                                });'
                                                        ]

                                                    )->label("State<span class='text-danger'>*</span>")?>

                                                    <?php
                                                    $cityList = [];
                                                    if ($model->iStateId != "") {
                                                        $cityList = ArrayHelper::map(CommonHelper::getCity($model->iStateId), 'iCityId', 'vCityName');
                                                    }
                                                    ?>
                                                    <?= $form->field($model, 'iCityId', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->dropDownList(
                                                        $cityList,
                                                        ['id' => 'iCityId', 'class' => 'demo-default select-beast clsbasicinfo', 'prompt' => 'City']
                                                    )->label("City<span class='text-danger'>*</span>")?>

                                                    <?php $hide = '';
                                                    if ($model->iCountryId != 101) {
                                                        $hide = "display: none; ";
                                                    } ?>
                                                    <div class="user_idistrictid_div"
                                                         style="<?= $hide ?>">
                                                        <?= $form->field($model, 'iDistrictID', [
                                                            'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                            'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                        ])->dropDownList(
                                                            ArrayHelper::map(CommonHelper::getDistrict(), 'iDistrictID', 'vName'),
                                                            ['class' => 'demo-default select-beast clsbasicinfo', 'prompt' => 'District']
                                                        )->label("District<span class='text-danger'>*</span>")?>
                                                    </div>
                                                    <?= $form->field($model, 'vAreaName', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->textInput(['maxlength' => true])->label("Area Name<span class='text-danger'>*</span>")?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box box-info">
                                            <div class="box-header with-border">
                                                <h3 class="box-title text-bold">Education & Occupation</h3>
                                            </div>
                                            <div class="box-body">
                                                <div class="col-md-6">
                                                    <?= $form->field($model, 'iEducationLevelID', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->dropDownList(
                                                        ArrayHelper::map(CommonHelper::getEducationLevel(), 'iEducationLevelID', 'vEducationLevelName'),
                                                        ['class' => 'demo-default select-beast clseducation', 'prompt' => 'Education Level']
                                                    )->label("Education Level<span class='text-danger'>*</span>")?>

                                                    <?= $form->field($model, 'iWorkingWithID', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->dropDownList(
                                                        ArrayHelper::map(CommonHelper::getWorkingWith(), 'iWorkingWithID', 'vWorkingWithName'),
                                                        ['class' => 'demo-default select-beast clseducation', 'prompt' => 'Working with']
                                                    )->label("Working With<span class='text-danger'>*</span>")?>



                                                    <?= $form->field($model, 'iAnnualIncomeID', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->dropDownList(
                                                        ArrayHelper::map(CommonHelper::getAnnualIncome(), 'iAnnualIncomeID', 'vAnnualIncome'),
                                                        ['class' => 'demo-default select-beast clseducation', 'prompt' => 'Annual Income']
                                                    )->label("Annual Income<span class='text-danger'>*</span>")?>
                                                </div>
                                                <div class="col-md-6">
                                                    <?= $form->field($model, 'iEducationFieldID', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->dropDownList(
                                                        ArrayHelper::map(CommonHelper::getEducationField(), 'iEducationFieldID', 'vEducationFieldName'),
                                                        ['class' => 'demo-default select-beast clseducation', 'prompt' => 'Education Field']
                                                    )->label("Education Field<span class='text-danger'>*</span>")?>
                                                    <?= $form->field($model, 'iWorkingAsID', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->dropDownList(
                                                        ArrayHelper::map(CommonHelper::getWorkingAS(), 'iWorkingAsID', 'vWorkingAsName'),
                                                        ['class' => 'demo-default select-beast clseducation', 'prompt' => 'Working As']
                                                    )->label("Working As<span class='text-danger'>*</span>")?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12" >
                                        <div class="box box-primary">
                                            <div class="box-header with-border">
                                                <h3 class="box-title text-bold">Lifestyle & Appearance Detail</h3>
                                            </div>
                                            <div class="box-body">
                                                <div class="col-md-6">
                                                    <?= $form->field($model, 'iHeightID', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->dropDownList(
                                                        ArrayHelper::map(CommonHelper::getHeight(), 'iHeightID', 'vName'),
                                                        ['class' => 'demo-default select-beast clslifestyle', 'prompt' => 'Height']
                                                    )->label("Height<span class='text-danger'>*</span>")?>

                                                    <?= $form->field($model, 'vSkinTone', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->RadioList(
                                                        ArrayHelper::map(CommonHelper::getSkinTone(), 'ID', 'Name'),
                                                        ['item' => function ($index, $label, $name, $checked, $value) {
                                                            $checked = ($checked) ? 'checked' : '';
                                                            $return = '<input type="radio" id="vSkinTone_' . $value . '" name="' . $name . '" value="' . $value . '" ' . $checked . '>';
                                                            $return .= '<label for="vSkinTone_' . $value . '">' . ucwords($label) . '</label>';
                                                            return $return;
                                                        }
                                                        ]
                                                    )->label("Skin Tone<span class='text-danger'>*</span>")?>

                                                    <?= $form->field($model, 'vBodyType', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->RadioList(
                                                        ArrayHelper::map(CommonHelper::getBodyType(), 'ID', 'Name'),
                                                        ['item' => function ($index, $label, $name, $checked, $value) {
                                                            $checked = ($checked) ? 'checked' : '';
                                                            $return = '<input type="radio" id="vBodyType_' . $value . '" name="' . $name . '" value="' . $value . '" ' . $checked . '>';
                                                            $return .= '<label for="vBodyType_' . $value . '">' . ucwords($label) . '</label>';
                                                            return $return;
                                                        }
                                                        ]
                                                    )->label("Body Type<span class='text-danger'>*</span>")?>
                                                    <?= $form->field($model, 'vSmoke', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->RadioList(
                                                        Yii::$app->params['smokeArray'],
                                                        [
                                                            'item' => function ($index, $label, $name, $checked, $value) {
                                                                $checked = ($checked) ? 'checked' : '';
                                                                $return = '<input type="radio" id="' . $value . '" name="' . $name . '" value="' . ucwords($value) . '" ' . $checked . '>';
                                                                $return .= '<label for="' . $value . '">' . ucwords($value) . '</label>';
                                                                return $return;
                                                            }

                                                        ]
                                                    )->label("Smoke<span class='text-danger'>*</span>")?>
                                                </div>
                                                <div class="col-md-6">
                                                    <?= $form->field($model, 'vDrink', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->RadioList(
                                                        Yii::$app->params['drinkArray'],
                                                        [
                                                            'item' => function ($index, $label, $name, $checked, $value) {
                                                                $checked = ($checked) ? 'checked' : '';
                                                                $return = '<input type="radio" id="' . $label . '" name="' . $name . '" value="' . ucwords($value) . '" ' . $checked . '>';
                                                                $return .= '<label for="' . $label . '">' . ucwords($value) . '</label>';
                                                                return $return;
                                                            }

                                                        ]
                                                    )->label("Drink<span class='text-danger'>*</span>")?>

                                                    <?= $form->field($model, 'vSpectaclesLens', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->RadioList(
                                                        Yii::$app->params['eyesArray'],
                                                        [
                                                            'item' => function ($index, $label, $name, $checked, $value) {
                                                                $checked = ($checked) ? 'checked' : '';
                                                                $return = '<input type="radio" id="' . $label . '" name="' . $name . '" value="' . ucwords($value) . '" ' . $checked . '>';
                                                                $return .= '<label for="' . $label . '">' . ucwords($value) . '</label>';
                                                                return $return;
                                                            }

                                                        ]
                                                    ); ?>

                                                    <?= $form->field($model, 'vDiet', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->dropDownList(
                                                        ArrayHelper::map(CommonHelper::getDiet(), 'iDietID', 'vName'),
                                                        ['class' => 'demo-default select-beast clslifestyle', 'prompt' => 'Diet']
                                                    )->label("Diet<span class='text-danger'>*</span>")?>

                                                    <?= $form->field($model, 'weight', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->input('number')->label("Weight<span class='text-danger'>*</span>")?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box box-success">
                                            <div class="box-header with-border">
                                                <h3 class="box-title text-bold">Family Detail</h3>
                                            </div>
                                            <div class="box-body">
                                                <div class="col-md-6">
                                                    <?= $form->field($model, 'iFatherStatusID', [
                                                        'template' => '<label class="control-label col-sm-4 col-xs-4" for="user-last_name">{label}<span class="text-danger">*</span></label>
                                <div class="col-sm-8 col-xs-8">{input}</div>',
                                                        'labelOptions' => ['class' => ''],
                                                    ])->dropDownList(
                                                        ArrayHelper::map(CommonHelper::getFmstatus(), 'iFMStatusID', 'vName'),
                                                        ['class' => 'demo-default select-beast clsfamily', 'prompt' => 'Father Status',
                                                            'onchange' => '
                                                    var wArray = ' . json_encode(Yii::$app->params['fatherWorkingAsNot']) . ';
                                                                if(wArray.indexOf($(this).val())!=-1){ //.trim()
                                                                       $(".user_ifatherworkingasid_div").hide();
                                                                            var selectize = $("select#user-ifatherworkingasid")[0].selectize;
                                                                            selectize.clear();
                                                                            selectize.setValue(0);
                                                                }else{
                                                                    $(".user_ifatherworkingasid_div").show();
                                                                }
                                                    '
                                                        ]
                                                    ); ?>
                                                    <?php
                                                    if (in_array($model->iFatherStatusID, Yii::$app->params['fatherWorkingAsNot'])) {
                                                        $Ft = 'style="display:none;"';
                                                    }
                                                    ?>
                                                    <div class="user_ifatherworkingasid_div" <?= $Ft ?>>
                                                        <?= $form->field($model, 'iFatherWorkingAsID', [
                                                            'template' => '<label class="control-label col-sm-4 col-xs-4" for="user-last_name">{label}<span class="text-danger">&nbsp;</span></label>
                                <div class="col-sm-8 col-xs-8">{input}</div>',
                                                            'labelOptions' => ['class' => ''],
                                                        ])->dropDownList(
                                                            ArrayHelper::map(CommonHelper::getWorkingas(), 'iWorkingAsID', 'vWorkingAsName'),
                                                            ['class' => 'demo-default select-beast clsfamily', 'prompt' => 'Father Working AS']
                                                        ); ?>
                                                    </div>
                                                    <?= $form->field($model, 'iMotherStatusID', [
                                                        'template' => '<label class="control-label col-sm-4 col-xs-4" for="user-last_name">{label}<span class="text-danger">*</span></label>
                                <div class="col-sm-8 col-xs-8">{input}</div>',
                                                        'labelOptions' => ['class' => ''],
                                                    ])->dropDownList(
                                                        ArrayHelper::map(CommonHelper::getFmstatus(), 'iFMStatusID', 'vName'),
                                                        ['class' => 'demo-default select-beast clsfamily', 'prompt' => 'Mother Status',
                                                            'onchange' => '
                                                      var wArray = ' . json_encode(Yii::$app->params['motherWorkingAsNot']) . ';
                                                                if(wArray.indexOf($(this).val().trim())!=-1){
                                                                       $(".user_imotherworkingasid_div").hide();
                                                                            var selectize = $("select#user-imotherworkingasid")[0].selectize;
                                                                            selectize.clear();
                                                                            selectize.setValue(0);
                                                                }else{
                                                                    $(".user_imotherworkingasid_div").show();
                                                                }
                                                    '
                                                        ]
                                                    ); ?>
                                                    <?php
                                                    if (in_array($model->iMotherStatusID, Yii::$app->params['motherWorkingAsNot'])) {
                                                        $Mt = 'style="display:none;"';
                                                    }
                                                    ?>
                                                    <div class="user_imotherworkingasid_div" <?= $Mt ?>>
                                                        <?= $form->field($model, 'iMotherWorkingAsID', [
                                                            'template' => '<label class="control-label col-sm-4 col-xs-4" for="user-last_name">{label}<span class="text-danger">&nbsp;</span></label>
                                <div class="col-sm-8 col-xs-8">{input}</div>',
                                                            'labelOptions' => ['class' => ''],
                                                        ])->dropDownList(
                                                            ArrayHelper::map(CommonHelper::getWorkingAS(), 'iWorkingAsID', 'vWorkingAsName'),
                                                            ['class' => 'demo-default select-beast clsfamily', 'prompt' => 'Mother Working AS']
                                                        ); ?>
                                                    </div>

                                                    <?= $form->field($model, 'nob', [
                                                        'template' => '<label class="control-label col-sm-4 col-xs-4" for="user-last_name">{label}<span class="text-danger">*</span></label>
                                <div class="col-sm-8 col-xs-8">{input}</div>',
                                                        'labelOptions' => ['class' => ''],
                                                    ])->input('number', ['min' => '0',
                                                        'onkeyup' => '
                                                            var nob = $(this).val();
                                                            if(nob == 0){
                                                            $("#nobmDiv").hide();
                                                               // $("#NobM").val("0");
                                                            }
                                                            else {
                                                              $("#nobmDiv").show();
                                                            }
                                                            '
                                                    ]) ?>
                                                    <?php

                                                    if ($model->nob == '0') {
                                                        $style = "display:none";
                                                    } else {
                                                        $style = "display:block";
                                                    }
                                                    ?>
                                                    <div class="box1 NobM" id="nobmDiv" style="<?= $style ?>">
                                                        <?= $form->field($model, 'NobM', [
                                                            'template' => '<label class="control-label col-sm-4 col-xs-4" for="user-last_name"><span class="text-danger"> </span>{label}</label>
                                <div class="col-sm-8 col-xs-8">{input}</div>',
                                                            'labelOptions' => ['class' => ''],
                                                        ])->input('number') ?>
                                                    </div>

                                                    <?= $form->field($model, 'nos', [
                                                        'template' => '<label class="control-label col-sm-4 col-xs-4" for="user-last_name">{label}<span class="text-danger">*</span></label>
                                <div class="col-sm-8 col-xs-8">{input}</div>',
                                                        'labelOptions' => ['class' => ''],
                                                    ])->input('number', ['min' => '0', 'onkeyup' => '
                                                    var nos = $(this).val();
                                                    if(nos == 0){
                                                    $("#nosmDiv").hide();
                                                      $("#NosM").val("0");
                                                    }
                                                    else {
                                                      $("#nosmDiv").show();
                                                    }
                                                     '
                                                    ]) ?>

                                                    <?php
                                                    if ($model->nos == '0') {
                                                        $style = "display:none";
                                                    } else {
                                                        $style = "display:block";
                                                    }
                                                    ?>
                                                    <div class="box1 NosM" id="nosmDiv" style="<?= $style ?>">
                                                        <?= $form->field($model, 'NosM', [
                                                            'template' => '<label class="control-label col-sm-4 col-xs-4" for="user-last_name"><span class="text-danger"></span>{label}</label>
                                <div class="col-sm-8 col-xs-8">{input}</div>',
                                                            'labelOptions' => ['class' => ''],
                                                        ])->input('number') ?>
                                                    </div>
                                                    <?= $form->field($model, 'iCountryCAId', [
                                                        'template' => '<label class="control-label col-sm-4 col-xs-4" for="user-last_name">{label}<span class="text-danger">*</span></label>
                                <div class="col-sm-8 col-xs-8">{input}</div>',
                                                        'labelOptions' => ['class' => ''],
                                                    ])->dropDownList(
                                                        ArrayHelper::map(CommonHelper::getCountry(), 'iCountryId', 'vCountryName'),
                                                        ['class' => 'demo-default select-beast clsfamily',
                                                            'prompt' => 'Country',
                                                            'onchange' => '
                                $.post( "' . Yii::$app->urlManager->createUrl('ajax/getstatenew?id=') . '"+$(this).val(), function( data ) {
                                 var htmldata = "";
                                                            jsondata = data.state;
                                                                    var new_value_options   = "[";
                                                                    for (var key in jsondata) {
                                                                    //console.log(jsondata[key].vStateName);
                                                                        htmldata += "<option value=\'"+jsondata[key].iStateId+"\'>"+jsondata[key].vStateName+"</option>";

                                                                        var keyPlus = parseInt(key) + 1;
                                                                        if (keyPlus == jsondata.length) {
                                                                            new_value_options += "{text: \'"+jsondata[key].vStateName+"\', value: "+jsondata[key].iStateId+"}";
                                                                        } else {
                                                                            new_value_options += "{text: \'"+jsondata[key].vStateName+"\', value: "+jsondata[key].iStateId+"},";
                                                                        }
                                                                    }
                                                                    new_value_options   += "]";

                                                            new_value_options = eval("(" + new_value_options + ")");
                                                            if (new_value_options[0] != undefined) {
                                                                        // re-fill html select option field
                                                                        $("select#iStateCAId").html(htmldata);
                                                                        // re-fill/set the selectize values
                                                                        var selectize = $("select#iStateCAId")[0].selectize;
                                                                        selectize.clear();
                                                                        selectize.clearOptions();
                                                                        selectize.renderCache["option"] = {};
                                                                        selectize.renderCache["item"] = {};

                                                                        selectize.addOption(new_value_options);
                                                                        //selectize.setValue(iStateCAId);

                                                                        var selectize = $("select#iCityCAId")[0].selectize;
                                                                        selectize.clear();
                                                                        selectize.clearOptions();


                                                                        if(data.CountryId == 101){
                                                                           $(".user_idistrictid_div").show();
                                                                            $(".user_iTalukaID_div").show();
                                                                            var selectize = $("select#user-idistrictcaid")[0].selectize;
                                                                            selectize.clear();
                                                                            var selectize = $("select#user-italukacaid")[0].selectize;
                                                                            selectize.clear();
                                                                        }else{
                                                                            $(".user_idistrictid_div").hide();
                                                                            var selectize = $("select#user-idistrictcaid")[0].selectize;
                                                                            selectize.clear();
                                                                            selectize.setValue(1);
                                                                            $(".user_iTalukaID_div").hide();
                                                                            var selectize = $("select#user-italukacaid")[0].selectize;
                                                                            selectize.clear();
                                                                            selectize.setValue(1);
                                                                        }
                                                            }
                                });'
                                                        ]

                                                    ); ?>
                                                    <?php
                                                    $stateList = [];
                                                    if ($model->iCountryCAId != "") {
                                                        $stateList = ArrayHelper::map(CommonHelper::getState($model->iCountryCAId), 'iStateId', 'vStateName');
                                                    }
                                                    ?>
                                                    <?= $form->field($model, 'iStateCAId', [
                                                        'template' => '<label class="control-label col-sm-4 col-xs-4" for="user-last_name">{label}<span class="text-danger">*</span></label>
                                <div class="col-sm-8 col-xs-8">{input}</div>',
                                                        'labelOptions' => ['class' => ''],
                                                    ])->dropDownList(
                                                        $stateList,
                                                        ['class' => 'demo-default select-beast clsfamily',
                                                            'id' => 'iStateCAId',
                                                            'prompt' => 'State',
                                                            'onchange' => '
                                $.post( "' . Yii::$app->urlManager->createUrl('ajax/getcitynew?id=') . '"+$(this).val(), function( data ) {
                                  var htmldata = "";
                                                            jsondata = data.city;
                                                                    var new_value_options   = "[";
                                                                    for (var key in jsondata) {
                                                                        htmldata += "<option value=\'"+jsondata[key].iCityId+"\'>"+jsondata[key].vCityName+"</option>";
                                                                        var keyPlus = parseInt(key) + 1;
                                                                        if (keyPlus == jsondata.length) {
                                                                            new_value_options += "{text: \'"+jsondata[key].vCityName+"\', value: "+jsondata[key].iCityId+"}";
                                                                        } else {
                                                                            new_value_options += "{text: \'"+jsondata[key].vCityName+"\', value: "+jsondata[key].iCityId+"},";
                                                                        }
                                                                    }
                                                                    new_value_options   += "]";

                                                            new_value_options = eval("(" + new_value_options + ")");
                                                            if (new_value_options[0] != undefined) {
                                                                        // re-fill html select option field
                                                                        $("select#iCityCAId").html(htmldata);
                                                                        // re-fill/set the selectize values
                                                                        var selectize = $("select#iCityCAId")[0].selectize;
                                                                        selectize.clear();
                                                                        selectize.clearOptions();
                                                                        selectize.renderCache["option"] = {};
                                                                        selectize.renderCache["item"] = {};

                                                                        selectize.addOption(new_value_options);
                                                            }
                                });'
                                                        ]

                                                    ); ?>
                                                    <?php
                                                    $cityList = [];
                                                    if ($model->iStateCAId != "") {
                                                        $cityList = ArrayHelper::map(CommonHelper::getCity($model->iStateCAId), 'iCityId', 'vCityName');
                                                    }
                                                    ?>
                                                    <?= $form->field($model, 'iCityCAId', [
                                                        'template' => '<label class="control-label col-sm-4 col-xs-4" for="user-last_name">{label}<span class="text-danger">*</span></label>
                                <div class="col-sm-8 col-xs-8">{input}</div>',
                                                        'labelOptions' => ['class' => ''],
                                                    ])->dropDownList(
                                                        $cityList,
                                                        ['class' => 'demo-default select-beast clsfamily',
                                                            'id' => 'iCityCAId',
                                                            'prompt' => 'City'
                                                        ]

                                                    ); ?>
                                                    <?php $hide = '';
                                                    if ($model->iCountryCAId != 101) {
                                                        $hide = "display: none; ";
                                                    } ?>
                                                    <div class="user_idistrictid_div"
                                                         style="<?= $hide ?>">
                                                        <?= $form->field($model, 'iDistrictCAID', [
                                                            'template' => '<label class="control-label col-sm-4 col-xs-4" for="user-last_name">{label}<span class="text-danger">*</span></label>
                                <div class="col-sm-8 col-xs-8">{input}</div>',
                                                            'labelOptions' => ['class' => ''],
                                                        ])->dropDownList(
                                                            ArrayHelper::map(CommonHelper::getDistrict(), 'iDistrictID', 'vName'),
                                                            ['class' => 'demo-default select-beast clsfamily',
                                                                'prompt' => 'District'
                                                            ]
                                                        ); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <?= $form->field($model, 'vAreaNameCA', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->input('text', ['class' => 'form-control']) ?>

                                                    <?= $form->field($model, 'vNativePlaceCA', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->input('text', ['class' => 'form-control']) ?>
                                                    <!--
    <?= $form->field($model, 'vParentsResiding', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->RadioList(
                                                        ['Current_Address' => 'Current Address', 'Permanent_Address' => 'Permanent Address'],
                                                        [
                                                            'item' => function ($index, $label, $name, $checked, $value) {
                                                                $checked = ($checked) ? 'checked' : '';
                                                                $return = '<input type="radio" id="' . $value . '" name="' . $name . '" value="' . $value . '" ' . $checked . '>';
                                                                $return .= '<label for="' . $value . '">' . ucwords($label) . '</label>';
                                                                return $return;
                                                            }
                                                        ]
                                                    ); ?>
-->
                                                    <?= $form->field($model, 'vFamilyAffluenceLevel', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->RadioList(
                                                        ArrayHelper::map(CommonHelper::getFamilyAffulenceLevel(), 'ID', 'Name'),
                                                        [
                                                            'item' => function ($index, $label, $name, $checked, $value) {
                                                                $checked = ($checked) ? 'checked' : '';
                                                                $return = '<input type="radio" id="' . $value . '" name="' . $name . '" value="' . $value . '" ' . $checked . '>';
                                                                $return .= '<label for="' . $value . '">' . ucwords($label) . '</label>';
                                                                return $return;
                                                            }
                                                        ]
                                                    ); ?>

                                                    <?= $form->field($model, 'vFamilyType', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->RadioList(
                                                        Yii::$app->params['familyTypeArray'],
                                                        [
                                                            'item' => function ($index, $label, $name, $checked, $value) {
                                                                $checked = ($checked) ? 'checked' : '';
                                                                $return = '<input type="radio" id="' . $label . '" name="' . $name . '" value="' . $value . '" ' . $checked . '>';
                                                                $return .= '<label for="' . $label . '">' . ucwords($value) . '</label>';
                                                                return $return;
                                                            }
                                                        ]
                                                    ); ?>

                                                    <?php global $ABC;
                                                    $ABC = $model->vFamilyProperty; ?>
                                                    <?= $form->field($model, 'vFamilyProperty', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->checkboxList(
                                                        ArrayHelper::map(CommonHelper::getFamilyPropertyDetail(), 'ID', 'Name'),
                                                        [
                                                            'item' => function ($index, $label, $name, $checked, $value) {
                                                                global $ABC;
                                                                $checked = (in_array($value, explode(",", $ABC))) ? 'checked' : '';
                                                                $return = '<input type="checkbox" id="vFamilyProperty' . $label . '" name="' . $name . '" value="' . $value . '"' . $checked . '>';
                                                                $return .= '<label for="vFamilyProperty' . $label . '" class="control-label no-content">' . $label . '</label>';
                                                                return $return;
                                                            }
                                                        ]
                                                    ); ?>
                                                    <div class="form-group field-user-vDetailRelative">
                                                        <label class="control-label col-md-3" for="user-vDetailRelative">About Relative</label>
                                                        <div class="col-md-9"><textarea class="input__field input__field--akira" cols="50" rows="5" style="resize:none"
                                                                                        name="User[vDetailRelative]" placeholder="You can enter your relative surnames etc..."><?= ($model->vDetailRelative) ?></textarea>
                                                        </div>
                                                        <div class="help-block"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box box-warning">
                                            <div class="box-header with-border">
                                                <h3 class="box-title text-bold">Horoscope Details</h3>
                                            </div>
                                            <div class="box-body">
                                                <div class="col-md-6">
                                                    <?= $form->field($model, 'RaashiId', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->dropDownList(
                                                        ArrayHelper::map(CommonHelper::getRaashi(), 'ID', 'Name'),
                                                        ['class' => 'demo-default select-beast clshoroscope', 'prompt' => 'Raashi']
                                                    ); ?>
                                                    <?= $form->field($model, 'NakshtraId', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->dropDownList(
                                                        ArrayHelper::map(CommonHelper::getNaksatra(), 'ID', 'Name'),
                                                        ['class' => 'demo-default select-beast clshoroscope', 'prompt' => 'Nakshtra']
                                                    ); ?>
                                                    <?= $form->field($model, 'CharanId', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->dropDownList(
                                                        ArrayHelper::map(CommonHelper::getCharan(), 'ID', 'Name'),
                                                        ['class' => 'demo-default select-beast clshoroscope', 'prompt' => 'Charan']
                                                    ); ?>

                                                </div>
                                                <div class="col-md-6">
                                                    <?= $form->field($model, 'NadiId', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->dropDownList(
                                                        ArrayHelper::map(CommonHelper::getNadi(), 'ID', 'Name'),
                                                        ['class' => 'demo-default select-beast clshoroscope', 'prompt' => 'Nadi']
                                                    ); ?>

                                                    <?= $form->field($model, 'Mangalik', [
                                                        'template' => "{label}\n<div class='col-md-9'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
                                                    ])->RadioList(
                                                        ['Yes' => 'Yes', 'No' => 'No'],
                                                        [
                                                            'item' => function ($index, $label, $name, $checked, $value) {
                                                                $checked = ($checked) ? 'checked' : '';
                                                                $return = '<input type="radio" class = "genderV" id="' . $value . '" name="' . $name . '" value="' . $value . '" ' . $checked . '>';
                                                                $return .= '<label for="' . $value . '">' . ucwords($label) . '</label>';
                                                                return $return;
                                                            }
                                                        ]
                                                    )
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box box-info">
                                            <div class="box-header with-border">
                                                <h3 class="box-title text-bold">Hobby/Interest Details</h3>
                                            </div>
                                            <div class="box-body">
                                                <div class="col-md-6">
                                                    <div class="form-group field-user-interestid">
                                                        <label class="control-label col-sm-4 col-xs-4" for="user-interestid">Interest</label>

                                                        <div class="col-sm-8 col-xs-8">
                                                            <select id="select-state" multiple class="demo-default select-beast clhobby"
                                                                    placeholder="Select an Interest" name="User[InterestID][]" size="4">
                                                                <?php if ($model->InterestID) {
                                                                    $UserInterestArray = explode(",", CommonHelper::removeComma($model->InterestID));
                                                                }
                                                                $InterestArray = CommonHelper::getInterests();
                                                                foreach ($InterestArray as $K => $V) { ?>
                                                                    <option value="<?= $V->ID ?>" <?php if (in_array($V->ID, $UserInterestArray)) {
                                                                        echo "selected";
                                                                    } ?>><?= $V->Name ?></option>
                                                                <?php }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group field-user-favioritereadid">
                                                        <label class="control-label col-sm-4 col-xs-4" for="user-favioritereadid">Favourite Reads</label>

                                                        <div class="col-sm-8 col-xs-8">
                                                            <select id="select-state" multiple class="demo-default select-beast clhobby "
                                                                    placeholder="Select a Favourite Reads" name="User[FavioriteReadID][]" size="4">
                                                                <?php if ($model->FavioriteReadID) {
                                                                    $UserFavReadsArray = explode(",", CommonHelper::removeComma($model->FavioriteReadID));
                                                                }
                                                                $FaviouriteReadArray = CommonHelper::getFavouriteReads();
                                                                foreach ($FaviouriteReadArray as $K => $V) { ?>
                                                                    <option value="<?= $V->ID ?>" <?php if (in_array($V->ID, $UserFavReadsArray)) {
                                                                        echo "selected";
                                                                    } ?>><?= $V->Name ?></option>
                                                                <?php }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group field-user-faviouritemusicid">
                                                        <label class="control-label col-sm-4 col-xs-4" for="user-faviouritemusicid">Favourite Music</label>

                                                        <div class="col-sm-8 col-xs-8">
                                                            <select id="select-state" multiple class="demo-default select-beast clhobby"
                                                                    placeholder="Select a Favourite Music" name="User[FaviouriteMusicID][]" size="4">
                                                                <?php if ($model->FaviouriteMusicID) {
                                                                    $UserFavMusicArray = explode(",", CommonHelper::removeComma($model->FaviouriteMusicID));
                                                                }
                                                                $FaviouriteMusicArray = CommonHelper::getFavouriteMusic();
                                                                foreach ($FaviouriteMusicArray as $K => $V) { ?>
                                                                    <option value="<?= $V->ID ?>" <?php if (in_array($V->ID, $UserFavMusicArray)) {
                                                                        echo "selected";
                                                                    } ?>><?= $V->Name ?></option>
                                                                <?php }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group field-user-favouritecousinesid">
                                                        <label class="control-label col-sm-4 col-xs-4" for="user-favouritecousinesid">Favourite Cousines</label>

                                                        <div class="col-sm-8 col-xs-8">
                                                            <select id="select-state" multiple class="demo-default select-beast clhobby"
                                                                    placeholder="Select a Favourite Cousine" name="User[FavouriteCousinesID][]" size="4">
                                                                <?php if ($model->FavouriteCousinesID) {
                                                                    $UserFavCousinArray = explode(",", CommonHelper::removeComma($model->FavouriteCousinesID));
                                                                }
                                                                $FaviouriteCousinArray = CommonHelper::getFavouriteCousines();
                                                                foreach ($FaviouriteCousinArray as $K => $V) { ?>
                                                                    <option value="<?= $V->ID ?>" <?php if (in_array($V->ID, $UserFavCousinArray)) {
                                                                        echo "selected";
                                                                    } ?>><?= $V->Name ?></option>
                                                                <?php }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group field-user-sportsfittnessid">
                                                        <label class="control-label col-sm-4 col-xs-4" for="user-sportsfittnessid">Sports Fitness
                                                            Activities</label>

                                                        <div class="col-sm-8 col-xs-8">
                                                            <select id="select-state" multiple class="demo-default select-beast clhobby"
                                                                    placeholder="Select a Sports Fitness Activities" name="User[SportsFittnessID][]" size="4">
                                                                <?php if ($model->SportsFittnessID) {
                                                                    $UserSportFitnessArray = explode(",", CommonHelper::removeComma($model->SportsFittnessID));
                                                                }
                                                                $SportFitnessArray = CommonHelper::getSportsFitnActivities();
                                                                foreach ($SportFitnessArray as $K => $V) { ?>
                                                                    <option value="<?= $V->ID ?>" <?php if (in_array($V->ID, $UserSportFitnessArray)) {
                                                                        echo "selected";
                                                                    } ?>><?= $V->Name ?></option>
                                                                <?php }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group field-user-preferreddressid">
                                                        <label class="control-label col-sm-4 col-xs-4" for="user-preferreddressid">Preferred Dress Style</label>

                                                        <div class="col-sm-8 col-xs-8">
                                                            <select id="select-state" multiple class="demo-default select-beast clhobby"
                                                                    placeholder="Select a Preferred Dress Style" name="User[PreferredDressID][]" size="4">
                                                                <?php if ($model->PreferredDressID) {
                                                                    $UserPreferredDressArray = explode(",", CommonHelper::removeComma($model->PreferredDressID));
                                                                }
                                                                $PreferredDressArray = CommonHelper::getPreferredDressStyle();
                                                                foreach ($PreferredDressArray as $K => $V) { ?>
                                                                    <option value="<?= $V->ID ?>" <?php if (in_array($V->ID, $UserPreferredDressArray)) {
                                                                        echo "selected";
                                                                    } ?>><?= $V->Name ?></option>
                                                                <?php }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group field-user-preferredmovieid">
                                                        <label class="control-label col-sm-4 col-xs-4" for="user-preferredmovieid">Preferred Movie</label>

                                                        <div class="col-sm-8 col-xs-8">
                                                            <select id="select-state" multiple class="demo-default select-beast clhobby"
                                                                    placeholder="Select a Preferred Movie" name="User[PreferredMovieID][]" size="4">
                                                                <?php if ($model->PreferredMovieID) {
                                                                    $UserPreferredMovieArray = explode(",", CommonHelper::removeComma($model->PreferredMovieID));
                                                                }
                                                                $PreferredMovieArray = CommonHelper::getPreferredMovies();
                                                                foreach ($PreferredMovieArray as $K => $V) { ?>
                                                                    <option value="<?= $V->ID ?>" <?php if (in_array($V->ID, $UserPreferredMovieArray)) {
                                                                        echo "selected";
                                                                    } ?>><?= $V->Name ?></option>
                                                                <?php }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-md-offset-5">
                                        <div class="form-group">
                                            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                                        </div>
                                    </div>
                                </div>
                                <?php ActiveForm::end(); ?>
                            </div>
                            <div class="tab-pane fade" id="custom-content-below-messages" role="tabpanel" aria-labelledby="custom-content-below-messages-tab">
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box box-warning">
                                            <div class="box-header with-border">
                                                <!--<h3 class="box-title text-bold">Hobby/Interest Details</h3>-->
                                            </div>
                                            <div class="box-body lightgallery" id="photo_list">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<div class="modal fade" id="photo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <p class="text-center mrg-bt-10"><img src="<?= CommonHelper::getLogo() ?>" width="157" height="61"
                                              alt="logo"></p>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span> <span
                        class="sr-only">Close</span></button>
                <h2 class="text-center">My Photo Gallery</h2>
                <div class="profile-control photo-btn">
                    <button class="btn " type="button"> Upload Video or Photo</button>
                    <button class="btn active" type="button"> Choose from Photos</button>
                    <button class="btn" type="button"> Albums</button>
                </div>
            </div>
            <div class="modal-body photo-gallery">
                <div class="choose-photo">
                    <div class="row" id="profile_list_popup">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="photodelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <p class="text-center mrg-bt-10">
            <img src="<?= CommonHelper::getLogo() ?>" width="157" height="61" alt="logo"></p>

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span> <span
                        class="sr-only">Close</span></button>
                <h2 class="text-center" id="model_heading"></h2>
            </div>
            <div class="modal-body photo-gallery">
                <div class="choose-photo">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <a href="javascript:void(0)"
                               class="btn btn-success mrg-tp-10 col-xs-5 col-xs-5 pull-right yes"> Yes </a>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 ">
                            <a href="javascript:void(0)" class="btn btn-danger mrg-tp-10 col-xs-5 col-xs-5 pull-left"
                               data-dismiss="modal"> No </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade notification-model" id="notification-model" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <p class="text-center mrg-bt-10"><img src="<?= CommonHelper::getLogo() ?>" width="157" height="61"
                                              alt="logo">
        </p>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal"><span aria-hidden="true">&times;</span> <span
                        class="sr-only">Close</span>
                </button>
                <h2 class="text-center" id="notification_header"> Information</h2>
            </div>
            <div class="modal-body">
                <form>
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <h4 class="mrg-bt-30 text-dark" id="forgot-password-id"></h4>
                            <h4 class="mrg-bt-30" id="notification_msg">
                            </h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <button type="button" class="btn btn-default mrg-tp-10 col-md-12 col-xs-12" data-dismiss="modal">
                                Close
                            </button>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
    <style>
        .form-group.has-error .help-block{
            margin-left: 30%;
        }
        .ui-datepicker{z-index:1200 !important;}
        /*.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default{background:#ee1845 !important;color:#fff !important;border-radius:0% !important;-moz-border-radius:0% !important;-webkit-border-radius:0% !important;}*/
        .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default{background:#ee1845 ;color:#fff ;border-radius:0% ;-moz-border-radius:0% ;-webkit-border-radius:0% ;}
        .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active{border:1px solid #ee1845 !important;background:#ffffff url(images/ui-bg_glass_65_ffffff_1x400.png) 50% 50% repeat-x !important;font-weight:normal !important;color:#ee1845 !important;}
        .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{
            color: #444;
            cursor: default;
            background-color: #d2d6de;
            border: 1px solid #ddd;
            border-bottom-color: transparent;
        }
        .nav-tabs > li > a{
            color: #444444;
        }

        .nav-tabs > li{
            width: 50%;
            text-align: center;
        }
        .callout, .card, .info-box, .mb-3, .my-3, .small-box {
            margin-bottom: 1rem!important;
        }
        .img-fluid {
            max-width: 100%;
            width: 300px;
            height: 200px;
            object-fit: cover;
        }
        img {
            vertical-align: middle;
            border-style: none;
        }
    </style>
<?php
$this->registerCssFile(Yii::$app->request->baseUrl . '/plugins/selectize/css/stylesheet.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]);
$this->registerCssFile(Yii::$app->request->baseUrl . '/plugins/selectize/css/selectize.default.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/plugins/selectize/js/standalone/selectize.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/plugins/selectize/js/index.js', ['depends' => [\yii\web\JqueryAsset::className()]]);


?>
    <!--<script src="http://localhost/kande_pohe_cr4/js/selectFx.js"></script>-->

<link href='<?= Yii::$app->request->baseUrl ?>/plugins/gallery/css/lightgallery.css' rel='stylesheet' type='text/css'>
<?php $this->registerJsFile(Yii::$app->request->baseUrl . '/plugins/gallery/js/lightgallery.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php $this->registerJsFile(Yii::$app->request->baseUrl . '/plugins/gallery/js/lg-fullscreen.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php $this->registerJsFile(Yii::$app->request->baseUrl . '/plugins/gallery/js/lg-thumbnail.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php $this->registerJsFile(Yii::$app->request->baseUrl . '/plugins/gallery/js/lg-video.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php $this->registerJsFile(Yii::$app->request->baseUrl . '/plugins/gallery/js/lg-autoplay.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php $this->registerJsFile(Yii::$app->request->baseUrl . '/plugins/gallery/js/lg-zoom.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php $this->registerJsFile(Yii::$app->request->baseUrl . '/plugins/gallery/js/lg-hash.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php $this->registerJsFile(Yii::$app->request->baseUrl . '/plugins/gallery/js/lg-pager.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>

<?php /*$this->registerJsFile(Yii::$app->request->baseUrl . '/plugins/simplelightbox/simple-lightbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]); */ ?><!--
<link href='<? /*= Yii::$app->request->baseUrl */ ?>/plugins/simplelightbox/simplelightbox.min.css' rel='stylesheet'
      type='text/css'>-->
<link href='<?= Yii::$app->request->baseUrl ?>/plugins/cropping/imgareaselect.css' rel='stylesheet' type='text/css'>
<?php $this->registerJsFile(Yii::$app->request->baseUrl . '/plugins/cropping/jquery.imgareaselect.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php $this->registerJsFile(Yii::$app->request->baseUrl . '/plugins/cropping/jquery.form.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>

<?php
if ($model->iCountryId == 101) {
    if ($model->iDistrictID == 1) {
        $this->registerJs('
                    var selectize = $("select#user-idistrictid")[0].selectize;
                    selectize.clear();
                 ');
    }
    if ($model->iTalukaID == 1) {
        $this->registerJs('
                    var selectize = $("select#user-italukaid")[0].selectize;
                    selectize.clear();
                 ');
    }

}
if ($model->iCountryCAId == 101) {
    if ($model->iDistrictCAID == 1) {
        $this->registerJs('
                    var selectize = $("select#user-idistrictcaid")[0].selectize;
                    selectize.clear();
                 ');
    }
    if ($model->iTalukaCAID == 1) {
        $this->registerJs('
                    var selectize = $("select#user-italukacaid")[0].selectize;
                    selectize.clear();
                 ');
    }

}
$this->registerJs('
            selectboxClassWise("clspersonalinfo");
            selectboxClassWise("clsbasicinfo");

            selectboxClassWise("clseducation");

            selectboxClassWise("clslifestyle");
            selectboxClassWise("clsfamily");
            selectboxClassWise("clshoroscope");
            selectboxClassWise("clhobby");
           function selectboxClassWise(classname) {
               $("." + classname).selectize({});
           }

           $(".genderV").on("change",function(e){
              var genderVal = $(this).val();
              if(genderVal == "FEMALE") {
                $("#DOB").datepicker("option","maxDate","'.date('Y-m-d',strtotime('-18 year')).'");
                $("#DOB").datepicker("option","yearRange","-70:-18");
              }
              else {
                $("#DOB").datepicker("option","maxDate","'.date('Y-m-d',strtotime('-21 year')).'");
                $("#DOB").datepicker("option","yearRange","-70:-21");
              }
        });
');
?>
<?php #For Profile Photo Start
require_once __DIR__ . '/_photosection.php';
?>

<?php
$this->registerJs('

function lightBox() {
    if ($(".lightgallery").length) {
        //$(".lightgallery").lightGallery({selector: ".kp_gallery"});
    }
}
  $(function () {
$(document).on("mouseover", ".kp_gallery", function (e) {
    $(".lightgallery").lightGallery({selector: ".kp_gallery"});
});
$(document).on("click", ".kp_not_gallery", function (e) {
    var lg = $(".lightgallery");
    $lg.lightGallery();
    $lg.data("lightGallery").destroy(true);
});
 //notificationPopup("S", "Testinbg msgafdfas ", "INFO");
var formData = new FormData();
formData.append( "u_id", "'.$model->id.'");
photo_list();
function photo_list(){
                    $.ajax({
                        url: "'.\Yii::$app->urlManager->baseUrl.'/user/photo-list",
                        type: "POST",
                        data: formData,
                        mimeType: "multipart/form-data",
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data, textStatus, jqXHR) {
                            var DataObject = JSON.parse(data);
                            //loaderStop();
                            if (DataObject.STATUS == "S") {
                                $("#photo_list").html(DataObject.OUTPUT);
                                $("#profile_list_popup").html(DataObject.OUTPUT_ONE);
                                //notificationPopup(DataObject.STATUS, DataObject.MESSAGE, DataObject.TITLE);
                            } else {
                                //notificationPopup(DataObject.STATUS, DataObject.MESSAGE, DataObject.TITLE);
                            }
                            profile_photo();
                            lightBox();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        notificationPopup(\'ERROR\', \'Something went wrong. Please try again !\', \'Error\');
                        }
                    });
}

        $(".fileupload").change(function () {
        //Pace.restart();
        var tflag= 1;
            if (typeof (FileReader) != "undefined") {
                //var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
                var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg)$/;
                $($(this)[0].files).each(function () {
                    var file = $(this);
                    if (regex.test(file[0].name.toLowerCase())) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            //$(".mainpropic").attr("src", e.target.result);
                        }
                        reader.readAsDataURL(file[0]);
                    } else {
                        tflag= 0;
                        notificationPopup(\'ERROR\', file[0].name + " is not a valid image file.", \'Error\');
                        return false;
                    }
                });
                var file_len = $(this)[0].files.length;
                if (file_len != 0 && tflag == 1) {

                    var file = $(this);
                    var formObj = $("#propicform");
                    var formData = new FormData();
                     //formData.append( "fileInput", $("#file_browse")[0].files[0]);

                    if (file_len != 0) {
                        $.each($(this)[0].files, function (i, file) {
                            formData.append("fileInput_" + i, file);
                        });
                    }
                    var uid = userid;
                    loaderStart();
                    $.ajax({
                        url: "photoupload?id=" + uid + "&FILE=" + file,
                        type: "POST",
                        data: formData,
                        mimeType: "multipart/form-data",
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data, textStatus, jqXHR) {
                            var DataObject = JSON.parse(data);
                            loaderStop();
                            if (DataObject.STATUS == "S") {
                                $("#photo_list").html(DataObject.OUTPUT);
                                $("#profile_list_popup").html(DataObject.OUTPUT_ONE);
                                notificationPopup(DataObject.STATUS, DataObject.MESSAGE, DataObject.TITLE);
                            } else {
                                notificationPopup(DataObject.STATUS, DataObject.MESSAGE, DataObject.TITLE);
                            }
                            profile_photo();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        notificationPopup(\'ERROR\', \'Something went wrong. Please try again !\', \'Error\');
                        }
                    });
                }
            } else {
                alert("This browser does not support HTML5 FileReader.");
            }
        });
        var P_ID = "";
        var P_TYPE = "";
        function profile_photo(){
        $(".profile_delete").click(function(){
                P_ID = $(this).data("id");
                P_TYPE = "PHOTO_DELETE";
                $("#model_heading").html("Are you sure want to delete this photo ?");
        })
        $(".profile_set").click(function(){
                P_ID = $(this).data("id");
                P_TYPE = "PHOTO_PROFILE_SET";
                $("#model_heading").html("Are you sure want to set this photo as profile photo?");
        })
        }
        profile_photo();

        $(".yes").click(function(){
       // Pace.restart();
        loaderStart();
        var formDataPhoto = new FormData();
        formDataPhoto.append( "P_ID", P_ID);
        formDataPhoto.append( "P_TYPE", P_TYPE);
        formDataPhoto.append( "u_id", "'.$model->id.'");
                $.ajax({
                        url: "'.\Yii::$app->urlManager->baseUrl.'/user/photo-operation",

                        type: "POST",
                        data: formDataPhoto,
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data, textStatus, jqXHR) {
                            var DataObject = JSON.parse(data);
                            if (DataObject.STATUS == "S") {
                                loaderStop();
                                if(P_TYPE=="PHOTO_PROFILE_SET"){
                                    $("#photo_list").html(DataObject.OUTPUT);
                                    $("#profile_list_popup").html(DataObject.OUTPUT_ONE);
                                    $(".mainpropic").attr("src", DataObject.PROFILE_PHOTO);
                                    $(".profile_photo_one").attr("src", DataObject.PROFILE_PHOTO_ONE);
                                    notificationPopup(DataObject.STATUS, DataObject.MESSAGE, DataObject.TITLE);
                                }else{
                                    $("#photo_list").html(DataObject.OUTPUT);
                                    $("#profile_list_popup").html(DataObject.OUTPUT_ONE);
                                    if(DataObject.PROFILE_PHOTO != ""){
                                        $(".mainpropic").attr("src", DataObject.PROFILE_PHOTO);
                                    }
                                    if(DataObject.PROFILE_PHOTO_ONE != ""){
                                        $(".profile_photo_one").attr("src", DataObject.PROFILE_PHOTO_ONE);
                                    }
                                    notificationPopup(DataObject.STATUS, DataObject.MESSAGE, DataObject.TITLE);
                                }
                                //photo_list();
                            } else {
                                loaderStop();
                                   notificationPopup(DataObject.STATUS, DataObject.MESSAGE, DataObject.TITLE);
                            }

                        profile_photo();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        loaderStop();
                        notificationPopup(\'ERROR\', \'Something went wrong. Please try again !\', \'Error\');
                        }
                    });
        })

    });
function loaderStop(element) {
    if (typeof(element) === "undefined")element = "main-section";
    $("." + element).waitMe("hide");
}
function loaderStart(element) {
    if (typeof(element) === "undefined")element = "main-section";
    $(".modal").modal("hide");
    $("." + element).waitMe({effect: "bounce", text: "Please Wait...", bg: "rgba(255,255,255,0.7)", color: "#EA0B44"});
}


function notificationPopup(type, msg, title) {
    $(".modal").modal("hide");
    if (type == "SUCCESS" || type == "S") {
        var msg = "<span class=\"text-success\"><strong>&#10003;</strong></span> " + msg;
        $("#notification_msg").html(msg);
    } else if (type == "ERROR" || type == "E") {
    var msg = "<span class=\"text-error\"><strong>&#215;</strong></span> " + msg;
    $("#notification_msg").html(msg);
} else {
    $("#notification_msg").html(msg);
}
    $("#notification_header").html(title);
    $("#notification-model").modal("show");
}
   ');
?>
<style>
    /*.wp-block-image {
        margin-left: auto;
        margin-right: auto;
    }
    .logos figure {
        border: 1px solid #ccc;
        padding: 10px;
    }*/
</style>