<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SubscriptionsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subscriptions-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'subscriptions_id') ?>

    <?= $form->field($model, 'subscriptions_name') ?>

    <?= $form->field($model, 'subscriptions_price') ?>

    <?= $form->field($model, 'profile_duration') ?>

    <?= $form->field($model, 'no_of_contacts') ?>

    <?php // echo $form->field($model, 'no_of_pm') ?>

    <?php // echo $form->field($model, 'privacy_settings') ?>

    <?php // echo $form->field($model, 'validity_of_package') ?>

    <?php // echo $form->field($model, 'customer_care_support') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
