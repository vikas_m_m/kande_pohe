<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Subscriptions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subscriptions-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'subscriptions_name')->textInput() ?>

    <?= $form->field($model, 'subscriptions_price')->textInput() ?>

    <?= $form->field($model, 'profile_duration')->textInput() ?>

    <?= $form->field($model, 'no_of_contacts')->textInput() ?>

    <?= $form->field($model, 'no_of_pm')->textInput() ?>

    <?= $form->field($model, 'privacy_settings')->dropDownList([ 'Yes' => 'Yes', 'No' => 'No', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'validity_of_package')->textInput() ?>

    <?= $form->field($model, 'customer_care_support')->dropDownList([ 'Yes' => 'Yes', 'No' => 'No', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
