<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Subscriptions */

$this->title = 'Update Subscriptions: ' . $model->subscriptions_name;
$this->params['breadcrumbs'][] = ['label' => 'Subscriptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->subscriptions_id, 'url' => ['view', 'id' => $model->subscriptions_id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="box box-primary">
    <div class="box-header with-border">

    </div>
    <!-- /.box-header -->
    <!-- form start -->

    <div class="box-body">

        <div class="subscriptions-update">

            <!--<h1><?/*= Html::encode($this->title) */?></h1>-->


            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>

        </div>
    </div>
</div>
