<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Subscriptions */

$this->title = $model->subscriptions_name;
$this->params['breadcrumbs'][] = ['label' => 'Subscriptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-primary">
    <div class="box-body box-profile">

        <h3 class="profile-username text-center"><?php echo $this->title = ucwords($model->subscriptions_name); ?></h3>

        <p class="text-muted text-center">Subscriptions</p>
    </div>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
          # 'subscriptions_id',
            'subscriptions_name:ntext',
            'subscriptions_price',
            'profile_duration',
            'no_of_contacts',
            'no_of_pm',
            'privacy_settings',
            'validity_of_package',
            'customer_care_support',
        ],
    ]) ?>
    <div class="row">
        <div class="col-md-6">
            <?= Html::a('Update', ['update', 'id' => $model->subscriptions_id], ['class' => 'btn btn-block btn-primary']) ?>
        </div>
        <div class="col-md-6">
            <?= Html::a('Delete', ['delete', 'id' => $model->subscriptions_id], ['class' => 'btn btn-danger btn-block',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </div>
    </div>
</div>
