<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\SubscriptionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Subscriptions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscriptions-index">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                </div>

                <!-- /.box-header -->
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-6"></div>
                            <div class="col-sm-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example2" class="table table-bordered table-hover dataTable" role="grid"
                                       aria-describedby="example2_info">
                                    <thead>
                                    <tr role="row">

                                    </thead>
                                    <tbody>


                                    <p>
                                        <?= Html::a('Create Subscriptions', ['create'], ['class' => 'btn btn-success pull-right']) ?>
                                    </p>
                                    <?= GridView::widget([
                                        'dataProvider' => $dataProvider,
                                        'filterModel' => $searchModel,
                                        'columns' => [
                                            ['class' => 'yii\grid\SerialColumn'],

                                            # 'subscriptions_id',
                                            'subscriptions_name:ntext',
                                           # 'subscriptions_price',
                                            [
                                                'attribute'=>'subscriptions_price',
                                                'format'=>'raw',
                                                'value' => function($model, $key, $index)
                                                {
                                                    return '₹'.number_format($model->subscriptions_price,2);
                                                },
                                            ],
                                            #'profile_duration',
                                            [
                                                'attribute'=>'profile_duration',
                                                'format'=>'raw',
                                                'value' => function($model, $key, $index)
                                                {
                                                    return ($model->profile_duration)." Days";
                                                },
                                            ],
                                            #'no_of_contacts',
                                            #'no_of_pm',
                                           # 'privacy_settings',
                                           # 'validity_of_package',
                                            [
                                                'attribute'=>'validity_of_package',
                                                'format'=>'raw',
                                                'value' => function($model, $key, $index)
                                                {
                                                    return ($model->validity_of_package)." Days";
                                                },
                                            ],
                                            #'customer_care_support',

                                            ['class' => 'yii\grid\ActionColumn'],
                                        ],
                                    ]); ?>
                                    </tbody>
                                    <tfoot>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </div>
        <!-- /.col -->
    </div>

