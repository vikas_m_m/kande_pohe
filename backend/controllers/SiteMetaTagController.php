<?php

namespace backend\controllers;

use Yii;
use common\models\SiteMetaTag;
use common\models\SiteMetaTagSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\CommonHelper;
use common\models\Cities;
use common\models\CitiesSearch;

/**
 * SiteMetaTagController implements the CRUD actions for SiteMetaTag model.
 */
class SiteMetaTagController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'create', 'update', 'view', 'delete', 'caste', 'education', 'occupation', 'physical-status', 'marital-status', 'other-meta-tag'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }

    /**
     * Lists all SiteMetaTag models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SiteMetaTagSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,SiteMetaTag::CITY);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' =>'City List',
            'type' =>SiteMetaTag::CITY,
        ]);
    }

    /**
     * Displays a single SiteMetaTag model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the SiteMetaTag model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return SiteMetaTag the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SiteMetaTag::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Creates a new SiteMetaTag model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($type = '')
    {
        $model = new SiteMetaTag();
        if ($type == '') {
            #return $this->redirect(['create', 'type' => SiteMetaTag::CITY]);
            #return $this->redirect(['index']);
            $type = SiteMetaTag::CITY;
        }

        /*if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->meta_tag_id]);
        }*/
        #echo $type;exit;
        #$site_meta_tag = Yii::$app->request->post();
        #echo "<pre>"; print_r($site_meta_tag );exit;
        $common_model = new CommonHelper();
        #echo CommonHelper::create_slug('does __this -thing--work or not');exit;
        #echo $str_new;exit;
        $list = array();
        /*if($type  == SiteMetaTag::CITY){
            $list = CommonHelper::getEducationField();
        }*/
        $site_meta_tag = Yii::$app->request->post();
        #echo "<pre>"; print_r($site_meta_tag);exit;
        $model->scenario = SiteMetaTag::SCENARIO_ADD;
        if ($model->load(Yii::$app->request->post())) {
            #   $site_meta_tag = Yii::$app->request->post();
            #$str_new = CommonHelper::create_slug();
            $site_url = $site_meta_tag['SiteMetaTag']['site_url'];
            if ($site_url != '') {
                $model->site_url = strtolower(CommonHelper::create_slug($site_url));
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->meta_tag_id]);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
                'type' => $type,
                #  'list' => $list,
                'meta_tag_section' => ($type != SiteMetaTag::CITY) ? $type : 'index',
            ]);
        }
    }

    /**
     * Updates an existing SiteMetaTag model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $site_meta_tag = Yii::$app->request->post();
        $model->scenario = SiteMetaTag::SCENARIO_ADD;
        /*if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->meta_tag_id]);
        } */
        if ($model->load(Yii::$app->request->post())) {
            $site_url = $site_meta_tag['SiteMetaTag']['site_url'];
            if ($site_url != '') {
                $model->site_url = strtolower(CommonHelper::create_slug($site_url));
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->meta_tag_id]);
            }

        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SiteMetaTag model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /** New Start ***/
    public function actionCaste()
    {
        $searchModel = new SiteMetaTagSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,SiteMetaTag::CASTE);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' =>'Caste List',
            'type' =>SiteMetaTag::CASTE,
        ]);
    }
    public function actionEducation()
    {
        $searchModel = new SiteMetaTagSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,SiteMetaTag::EDUCATION);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' =>'Education List',
            'type' =>SiteMetaTag::EDUCATION,
        ]);
    }
    public function actionOccupation()
    {
        $searchModel = new SiteMetaTagSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,SiteMetaTag::OCCUPATION);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' =>'Occupation List',
            'type' =>SiteMetaTag::OCCUPATION,
        ]);
    }
    public function actionPhysicalStatus()
    {
        $searchModel = new SiteMetaTagSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,SiteMetaTag::PHYSICAL_STATUS);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' =>'Physical Status List',
            'type' =>SiteMetaTag::PHYSICAL_STATUS,
        ]);
    }
    public function actionMaritalStatus()
    {
        $searchModel = new SiteMetaTagSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,SiteMetaTag::MARITAL_STATUS);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' =>'Marital Status List',
            'type' =>SiteMetaTag::MARITAL_STATUS,
        ]);
    }

    public function actionOtherMetaTag()
    {
        $searchModel = new SiteMetaTagSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, SiteMetaTag::OTHER);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => 'Other Meta Management List',
            'type' => SiteMetaTag::OTHER,
        ]);
    }
    /** New End ***/
}
