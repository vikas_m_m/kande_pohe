<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use common\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\MailHelper;
use common\components\CommonHelper;
use common\components\MessageHelper;
use common\models\UserPhotos;
use yii\helpers\Html;
use yii\web\Response;
/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'create', 'update', 'view', 'delete', 'new-registered', 'userapprove', 'in-approval', 'user-in-own-words', 'user-profile-pic', 'profilepic', 'inownwords', 'profilepicu','set-profile-photo'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
        ]; #Only Access By Admin login.
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->getUserList(Yii::$app->request->queryParams, User::USER_ALL);
        #$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        /*$a  = MessageHelper::getMessageNotification('S', 'PHOTO_DELETE');
        CommonHelper::pr($a  );exit;*/
        $model = $this->findModel($id);
       # CommonHelper::pr(Yii::$app->request->post());exit;
        $model->scenario = User::SCENARIO_UPDATE_PROFILE_BY_ADMIN;
        if ($model->load(Yii::$app->request->post())) {

            $model->First_Name = Yii::$app->request->post('User')['First_Name'];
            $model->Last_Name = Yii::$app->request->post('User')['Last_Name'];
            $model->Profile_created_for = Yii::$app->request->post('User')['Profile_created_for'];
            $model->DOB = Yii::$app->request->post('User')['DOB'];
            // echo "hii";
            // die();
            $model->Age = CommonHelper::ageCalculator(Yii::$app->request->post('User')['DOB']);
            # $model->county_code = $model->county_code;//Yii::$app->request->post('User')['county_code'];
            #   $model->Mobile = $model->Mobile;//$NewMobileNo;
            $model->Gender = Yii::$app->request->post('User')['Gender'];
            $model->mother_tongue = Yii::$app->request->post('User')['mother_tongue'];
            #CommonHelper::pr( Yii::$app->request->post());

            #Start  Second Step
            $model->iReligion_ID = Yii::$app->request->post('User')['iReligion_ID'];
            $model->iCommunity_ID = Yii::$app->request->post('User')['iCommunity_ID'];
            $model->iSubCommunity_ID = Yii::$app->request->post('User')['iSubCommunity_ID'];
            $model->iGotraID = Yii::$app->request->post('User')['iGotraID'];
            $model->iMaritalStatusID = Yii::$app->request->post('User')['iMaritalStatusID'];
            $model->noc = Yii::$app->request->post('User')['noc'];
            $model->iCountryId = Yii::$app->request->post('User')['iCountryId'];
            $model->iStateId = Yii::$app->request->post('User')['iStateId'];
            $model->iCityId = Yii::$app->request->post('User')['iCityId'];

            if (Yii::$app->request->post('User')['iCountryId'] != 101) {
                $model->iDistrictID = 1;
                $model->iTalukaID = 1;
            } else {
                $model->iTalukaID = Yii::$app->request->post('User')['iTalukaID'];
                $model->iDistrictID = Yii::$app->request->post('User')['iDistrictID'];
            }
            $model->iTalukaID = 0; #TODO : This is for temporary base. Please remove this line when use taluka.

            $model->vAreaName = Yii::$app->request->post('User')['vAreaName'];

            $CityName = $model->cityName->vCityName;
            $StateName = $model->stateName->vStateName;
            $CountryName = $model->countryName->vCountryName;
            $Address = $model->vAreaName . " " . $CityName . " " . $StateName . " " . $CountryName;
            $LatLongArray = CommonHelper::getLatLong($Address);
            $model->latitude = $LatLongArray['latitude'];
            $model->longitude = $LatLongArray['longitude'];


            $model->iEducationLevelID = Yii::$app->request->post('User')['iEducationLevelID'];
            $model->iEducationFieldID = Yii::$app->request->post('User')['iEducationFieldID'];
            $model->iWorkingWithID = Yii::$app->request->post('User')['iWorkingWithID'];
            $model->iWorkingAsID = Yii::$app->request->post('User')['iWorkingAsID'];
            $model->iAnnualIncomeID = Yii::$app->request->post('User')['iAnnualIncomeID'];

            $model->iHeightID = Yii::$app->request->post('User')['iHeightID'];
            $model->vSkinTone = Yii::$app->request->post('User')['vSkinTone'];
            $model->vBodyType = Yii::$app->request->post('User')['vBodyType'];
            $model->vSmoke = Yii::$app->request->post('User')['vSmoke'];
            $model->vDrink = Yii::$app->request->post('User')['vDrink'];
            $model->vSpectaclesLens = Yii::$app->request->post('User')['vSpectaclesLens'];
            $model->vDiet = Yii::$app->request->post('User')['vDiet'];
            $model->weight = Yii::$app->request->post('User')['weight'];



            //$model->completed_step = $model->setCompletedStep('2');

            $model->iFatherStatusID = Yii::$app->request->post('User')['iFatherStatusID'];
            $model->iFatherWorkingAsID = Yii::$app->request->post('User')['iFatherWorkingAsID'];
            $model->iMotherStatusID = Yii::$app->request->post('User')['iMotherStatusID'];
            $model->iMotherWorkingAsID = Yii::$app->request->post('User')['iMotherWorkingAsID'];
            $model->nob = Yii::$app->request->post('User')['nob'];
            if (Yii::$app->request->post('User')['nob'] == 0) {
                $model->NobM = 0;
            } else {
                $model->NobM = Yii::$app->request->post('User')['NobM'];
            }


            $model->nos = Yii::$app->request->post('User')['nos'];
            if (Yii::$app->request->post('User')['nos'] == 0) {
                $model->NosM = 0;
            } else {
                $model->NosM = Yii::$app->request->post('User')['NosM'];
            }
            $model->iCountryCAId = Yii::$app->request->post('User')['iCountryCAId'];
            $model->iStateCAId = Yii::$app->request->post('User')['iStateCAId'];
            $model->iCityCAId = Yii::$app->request->post('User')['iCityCAId'];
            if (Yii::$app->request->post('User')['iCountryCAId'] != 101) {
                $model->iDistrictCAID = 1;
                $model->iTalukaCAID = 1;
            } else {
                $model->iDistrictCAID = Yii::$app->request->post('User')['iDistrictCAID'];
                $model->iTalukaCAID = Yii::$app->request->post('User')['iTalukaCAID'];
            }
            $model->iTalukaCAID = 0; #TODO : This is for temporary base. Please remove this line when use taluka.
            /*$model->iDistrictCAID = Yii::$app->request->post('User')['iDistrictCAID'];
            $model->iTalukaCAID = Yii::$app->request->post('User')['iTalukaCAID'];*/
            $model->vAreaNameCA = Yii::$app->request->post('User')['vAreaNameCA'];
            $model->vNativePlaceCA = Yii::$app->request->post('User')['vNativePlaceCA'];
            $model->vParentsResiding = Yii::$app->request->post('User')['vParentsResiding'];
            $model->vFamilyAffluenceLevel = Yii::$app->request->post('User')['vFamilyAffluenceLevel'];
            $model->vFamilyType = Yii::$app->request->post('User')['vFamilyType'];
            $family_property_array = Yii::$app->request->post('User')['vFamilyProperty'];
            if (is_array($family_property_array)) {
                $model->vFamilyProperty = implode(",", $family_property_array);
            } else {
                $model->vFamilyProperty = '';
            }
            $model->vDetailRelative = Yii::$app->request->post('User')['vDetailRelative'];


            $model->InterestID = "," . implode(",",  $_POST['User']['InterestID']);
            $model->FavioriteReadID = "," . implode(",",  $_POST['User']['FavioriteReadID']);
            $model->FaviouriteMusicID = implode(",",  $_POST['User']['FaviouriteMusicID']);
            $model->FavouriteCousinesID = implode(",",  $_POST['User']['FavouriteCousinesID']);
            $model->SportsFittnessID = implode(",",  $_POST['User']['SportsFittnessID']);
            $model->PreferredDressID = implode(",",  $_POST['User']['PreferredDressID']);
            $model->PreferredMovieID = implode(",",  $_POST['User']['PreferredMovieID']);


            $model->RaashiId = Yii::$app->request->post('User')['RaashiId'];
            $model->CharanId = Yii::$app->request->post('User')['CharanId'];
            $model->NadiId = Yii::$app->request->post('User')['NadiId'];
            $model->NakshtraId = Yii::$app->request->post('User')['NakshtraId'];
            $model->iGotraID = Yii::$app->request->post('User')['iGotraID'];
            $model->Mangalik = Yii::$app->request->post('User')['Mangalik'];

            $model->iCountryCAId = Yii::$app->request->post('User')['iCountryCAId'];
            $model->iStateCAId = Yii::$app->request->post('User')['iStateCAId'];
            $model->iCityCAId = Yii::$app->request->post('User')['iCityCAId'];
            $model->iDistrictCAID = Yii::$app->request->post('User')['iDistrictCAID'];
            $model->iTalukaCAID = Yii::$app->request->post('User')['iTalukaCAID'];
            $model->vAreaNameCA = Yii::$app->request->post('User')['vAreaNameCA'];


            //$model->Mangalik = Yii::$app->request->post('User')['GanId'];
            #CommonHelper::pr($model);
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $user_photos = UserPhotos::find()->where(['iUser_ID'=>$id])->all();
            //CommonHelper::pr($user_photos );
            return $this->render('update', [
                'model' => $model,
                'user_photos' => $user_photos ,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionApprove($id)
    {
        $model = $this->findModel($id);

        if ($model->validate()) {
            $model->status = 5;
            $model->completed_step = $model->setCompletedStep('10');
            $model->save();
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    public function actionDisapprove($id)
    {
        $model = $this->findModel($id);
        if ($model->validate()) {
            $model->status = 4;
            $model->save();
            return $this->redirect(Yii::$app->request->referrer);
        }
    }
    public function actionBlock($id)
    {
        $model = $this->findModel($id);
        if ($model->validate()) {
            $model->status = 6;
            $model->save();
            return $this->redirect(Yii::$app->request->referrer);
        }
    }



    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionNewRegistered()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->getUserList(Yii::$app->request->queryParams, User::USER_NEWLY_REGISTERED);

        return $this->render('usernewregister', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'UserType' => User::USER_NEWLY_REGISTERED,
        ]);
    }

    public function actionInApproval()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->getUserList(Yii::$app->request->queryParams, User::USER_IN_APPROVAL);

        return $this->render('userinapproval', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionUserapprove()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->getUserList(Yii::$app->request->queryParams, User::USER_APRROVED);

        return $this->render('userapprove', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionUserInOwnWords()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->searchInOwnWords(Yii::$app->request->queryParams);

        return $this->render('userinownwords', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionInownwords($id)
    {
       /* ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);*/
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            $USER_ARRAY = Yii::$app->request->post();
            $commentInOwnWordsAdmin = $USER_ARRAY['User']['commentInOwnWordsAdmin'];
            $ACTION_TYPE = $USER_ARRAY['submit'];
            $email_id = $model->email;

            if($ACTION_TYPE == 'APPROVE'){
                $model->eStatusInOwnWord = 'Approve';
                if ($model->save()) {
                    #$MAIL_DATA = array("EMAIL_TO" => $model->email, "NAME" => $model->First_Name . " " . $model->Last_Name, "COMMENT" => $commentInOwnWordsAdmin);
                    $MAIL_DATA = array("EMAIL_TO" => $email_id, "NAME" => $model->First_Name . " " . $model->Last_Name, "COMMENT" => $commentInOwnWordsAdmin);
                    MailHelper::SendMail('IN_OWN_WORDS_APPROVE', $MAIL_DATA);
                }
            }else{
                $model->eStatusInOwnWord = 'Disapprove';

                if ($model->save()) {
                    #$MAIL_DATA = array("EMAIL_TO" => $model->email, "NAME" => $model->First_Name . " " . $model->Last_Name, "COMMENT" => $commentInOwnWordsAdmin);
                    #$MAIL_DATA = array("EMAIL_TO" => 'parmarvikrantr@gmail.com', "NAME" => $model->First_Name . " " . $model->Last_Name, "COMMENT" => $commentInOwnWordsAdmin);
                    $MAIL_DATA = array("EMAIL_TO" => $email_id, "NAME" => $model->First_Name . " " . $model->Last_Name, "COMMENT" => $commentInOwnWordsAdmin);
                    $abc = MailHelper::SendMail('IN_OWN_WORDS_DISAPPROVE', $MAIL_DATA);
                    #MailHelper::SendMail('IN_OWN_WORDS_APPROVE', $MAIL_DATA);
                    #echo $abc ." Mail Send ===>".$email_id;exit;
                }
            }
        }
        return $this->render('inownwords', [
            'model' => $this->findModel($id),
        ]);
    }
    public function actionUserProfilePic()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->searchProfilePhoto(Yii::$app->request->queryParams);
        return $this->render('userprofilepic', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionProfilepic($id)
    {
        #  echo Yii::$app->params['frontendPATH'];
        /* $USER_PHOTO_MODEL = new UserPhotos();
         $USER_PHOTOS_APPROVE_LIST = $USER_PHOTO_MODEL->userPhotoListStatusWise($id,'Approve');
         foreach($USER_PHOTOS_APPROVE_LIST as $k=>$v){
             $profile_status = 'No';
             $profile_dir = 0;
             if ($v->Is_Profile_Photo == 'YES'){
                 $profile_status = 'Yes';
                 $profile_dir = 1;
                 $name= "200_" . $v->File_Name;
             }else{
                 $name = Yii::$app->params['thumbnailPrefix'] . "200_".$v->File_Name ;
                 $profile_status = 'No';
                 $profile_dir = 0;
             }
          #echo "<br>".CommonHelper::getPhotosBackendMail('USER', $id, $name, 200, '', $profile_status, $profile_dir);//
          #echo Html::img(CommonHelper::getPhotosBackendMail('USER', $id, $name, 200, '', $profile_status, $profile_dir), ['class' => 'img-responsive ', 'alt' => 'Photo' . $id]);

         }*/
        #echo "<pre>";print_r($USER_PHOTOS_APPROVE_LIST);
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {

            $USER_ARRAY = Yii::$app->request->post();
            $commentAdmin = $USER_ARRAY['User']['commentAdmin'];
            $photo_id = $USER_ARRAY['User']['iPhoto_ID'];
            $ACTION_TYPE = $USER_ARRAY['submit'];

            /*$PG = new UserPhotos();
            $UP = $PG->findByPhotoId($id, $photo_id);*/

            $profilePic_array = Yii::$app->request->post('profilePic');
            #echo "<pre>";print_r(Yii::$app->request->post('profilePic'));#exit;

            /*if(count($profilePic_array)>0){
                $dis_approve_status = 0;
                foreach($profilePic_array as $PK => $PV){
                    $PG = new UserPhotos();
                    $UP = $PG->findByPhotoId($id, $PK);
                    #echo "<pre>";print_r($UP);exit;
                    if($PV == 'Approve' ){
                        if ($UP->Is_Profile_Photo == 'YES') {
                            $model->eStatusPhotoModify = 'Approve';
                            $model->save();
                        }
                        $UP->eStatus = 'Approve';
                        if ($UP->save()) {}
                    }else{
                        $dis_approve_status = 1;
                        if ($UP->Is_Profile_Photo == 'YES') {
                            $model->eStatusPhotoModify = 'Disapprove';
                            $model->save();
                        }
                        $UP->eStatus = 'Disapprove';
                        if ($UP->save()) {}
                    }
                }
                if($dis_approve_status){
                    $MAIL_DATA = array("EMAIL_TO" => $model->email, "NAME" => $model->First_Name . " " . $model->Last_Name, "COMMENT" => $commentAdmin);
                    MailHelper::SendMail('PROFILE_PHOTO_DISAPPROVE', $MAIL_DATA);
                }
            }*/

            /*$PG = new UserPhotos();
            $UP = $PG->findByPhotoId($id, $photo_id);

            if ($ACTION_TYPE == 'Approve') {
                if ($UP->Is_Profile_Photo == 'YES') {
                    $model->eStatusPhotoModify = 'Approve';
                    $model->save();
                }
                $UP->eStatus = 'Approve';
                if ($UP->save()) {
                    $MAIL_DATA = array("EMAIL_TO" => $model->email, "NAME" => $model->First_Name . " " . $model->Last_Name, "COMMENT" => $commentAdmin);
                    #MailHelper::SendMail('PROFILE_PHOTO_APPROVE', $MAIL_DATA);
                }
            }
            else{
                if ($UP->Is_Profile_Photo == 'YES') {
                    $model->eStatusPhotoModify = 'Disapprove';
                    $model->save();
                }
                $UP->eStatus = 'Disapprove';
                if ($UP->save()) {
                    $MAIL_DATA = array("EMAIL_TO" => $model->email, "NAME" => $model->First_Name . " " . $model->Last_Name, "COMMENT" => $commentAdmin);
                    MailHelper::SendMail('PROFILE_PHOTO_DISAPPROVE', $MAIL_DATA);
                }
            }*/

        }
        $USER_PHOTO_MODEL = new UserPhotos();
        $USER_PHOTOS_APPROVE_LIST = $USER_PHOTO_MODEL->userPhotoListStatusWise($id,'Approve'); #Approve Photo
        $USER_PHOTOS_PENDING_LIST = $USER_PHOTO_MODEL->userPhotoListStatusWise($id,'Pending'); #Approve Photo
        $USER_PHOTOS_DISAPPROVE_LIST = $USER_PHOTO_MODEL->userPhotoListStatusWise($id,'Disapprove'); #Approve Photo
        /*echo "<pre>";print_r($USER_PHOTOS_APPROVE_LIST);
        echo "<pre>";print_r($USER_PHOTOS_PENDING_LIST);
        echo "<pre>";print_r($USER_PHOTOS_DISAPPROVE_LIST);*/
        #exit;
        #$USER_PHOTOS_PENDING_LIST = $USER_PHOTO_MODEL->findByUserId($id,''); #New Photos Which are in pending mode.
        #Yii::$app->getSession()->setFlash('error', 'Something went wrong. Please try again!');
        return $this->render('profilepic', [
            'model' => $this->findModel($id),
            'PHOTO_LIST' => $USER_PHOTOS_APPROVE_LIST,
            'USER_PHOTOS_PENDING_LIST' => $USER_PHOTOS_PENDING_LIST,
            'USER_PHOTOS_DISAPPROVE_LIST' => $USER_PHOTOS_DISAPPROVE_LIST,
        ]);
    }
    public function actionProfilepicu($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            $USER_ARRAY = Yii::$app->request->post();
            $commentAdmin = $USER_ARRAY['User']['commentAdmin'];
            $photo_id = $USER_ARRAY['User']['iPhoto_ID'];
            $ACTION_TYPE = $USER_ARRAY['submit'];

            /*$PG = new UserPhotos();
            $UP = $PG->findByPhotoId($id, $photo_id);*/

            $profilePic_array = Yii::$app->request->post('profilePic');
            #echo "<pre>";print_r(Yii::$app->request->post('profilePic'));#exit;
            $photo_html = '';
            if(count($profilePic_array)>0){
                $dis_approve_status = 0;
                foreach($profilePic_array as $PK => $PV){
                    $PG = new UserPhotos();
                    $UP = $PG->findByPhotoId($id, $PK);
                    /*echo "<pre>";print_r($photo_html);
                    echo "<pre>";print_r($UP->File_Name);
                    echo "<pre>";print_r($UP);exit;*/
                    if($PV == 'Approve' ){
                        if ($UP->Is_Profile_Photo == 'YES') {
                            $model->eStatusPhotoModify = 'Approve';
                            $model->save();
                        }
                        $UP->eStatus = 'Approve';
                        if ($UP->save()) {}
                    }else{
                        $dis_approve_status = 1;

                        $profile_status = 'No';
                        $profile_dir = 0;

                        if ($UP->Is_Profile_Photo == 'YES') {
                            $model->eStatusPhotoModify = 'Disapprove';
                            # $model->save();
                            $profile_status = 'Yes';
                            $profile_dir = 1;
                            $name= "200_" . $UP->File_Name;
                        }else{
                            $name = Yii::$app->params['thumbnailPrefix'] . "200_".$UP->File_Name ;
                            $profile_status = 'No';
                            $profile_dir = 0;
                        }
                        $UP->eStatus = 'Disapprove';
                        if ($UP->save()) {}

#                 echo "<br>".CommonHelper::getPhotosBackendMail('USER', $id, $name, 200, '', $profile_status, $profile_dir);

                        #echo Html::img(CommonHelper::getPhotosBackendMail('USER', $id, $name, 200, '', $profile_status, $profile_dir), ['class' => 'img-responsive ', 'alt' => 'Photo' . $id]);a
                        $photo_url = Html::img(CommonHelper::getPhotosBackendMail('USER', $id, $name, 200, '', $profile_status, $profile_dir), ['class' => 'img-responsive ', 'alt' => 'Photo' . $id]);
                        $photo_html .= "<span style='margin-left:10px !important;'>".$photo_url."</span>";
                    }
                }
                #echo "<pre>";print_r($photo_html);
#                exit;
                Yii::$app->getSession()->setFlash('success', 'Photo operation completed successfully.');
                if($dis_approve_status){
                    $MAIL_DATA = array("EMAIL_TO" => $model->email, "NAME" => $model->First_Name . " " . $model->Last_Name, "COMMENT" => $commentAdmin,'PHOTO'=>$photo_html);
                    MailHelper::SendMail('PROFILE_PHOTO_DISAPPROVE', $MAIL_DATA);
                }
            }
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionPhotosview($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            $USER_ARRAY = Yii::$app->request->post();
            $commentAdmin = $USER_ARRAY['User']['commentAdmin'];
            $photo_id = $USER_ARRAY['User']['iPhoto_ID'];
            $ACTION_TYPE = $USER_ARRAY['submit'];
            $PG = new UserPhotos();
            $UP = $PG->findByPhotoId($id, $photo_id);
            if ($ACTION_TYPE == 'Approve') {
                if ($UP->Is_Profile_Photo == 'YES') {
                    $model->eStatusPhotoModify = 'Approve';
                    $model->save();
                }
                $UP->eStatus = 'Approve';
                if ($UP->save()) {
                    $MAIL_DATA = array("EMAIL_TO" => $model->email, "NAME" => $model->First_Name . " " . $model->Last_Name, "COMMENT" => $commentAdmin);
                    MailHelper::SendMail('PROFILE_PHOTO_APPROVE', $MAIL_DATA);
                }
            } else {
                if ($UP->Is_Profile_Photo == 'YES') {
                    $model->eStatusPhotoModify = 'Disapprove';
                    $model->save();
                }
                $UP->eStatus = 'Disapprove';
                if ($UP->save()) {
                    $MAIL_DATA = array("EMAIL_TO" => $model->email, "NAME" => $model->First_Name . " " . $model->Last_Name, "COMMENT" => $commentAdmin);
                    MailHelper::SendMail('PROFILE_PHOTO_DISAPPROVE', $MAIL_DATA);
                }
            }

        }
        $USER_PHOTO_MODEL = new UserPhotos();
        $USER_PHOTOS_LIST = $USER_PHOTO_MODEL->findByUserId($id);
        return $this->render('photosview', [
            'model' => $this->findModel($id),
            'PHOTO_LIST' => $USER_PHOTOS_LIST
        ]);
    }
    public function actionSetProfilePhoto(){
        #echo "<pre>"; print_r(Yii::$app->request->get());exit;
        if (Yii::$app->request->get()) {
            $param = Yii::$app->request->get();
            $param = array_filter($param);
            if(count($param)==2){
                $user_id = base64_decode($param['uid']);
                $photo_id = base64_decode($param['pid']);
                #echo $user_id;
                #echo "<br>".$photo_id;
                $PG = new UserPhotos();
                $UP = $PG->findByPhotoId($user_id, $photo_id);
                if(count($UP)>0){
                    $PHOTO = $photo_name = $UP->File_Name;
                    $name = Yii::$app->params['thumbnailPrefix'] . "200_".$UP->File_Name ;
                    $photo_path_200 = CommonHelper::getPhotosBackendMail('USER', $user_id, $name, 200, '', 'No', 0,1);//
                    # echo "<br>".$PHOTO;
                    # echo "<pre>";print_r($photo_path_200);

                    #$FileName = $ActualImagePATH . $PHOTO;
                    $FileName = $photo_path_200['full_path'];
                    list($orig_width, $orig_height) = getimagesize($FileName);
                    $photo_path_dire = CommonHelper::getPhotosBackendMail('USER', $user_id, $name, 200, '', 'Yes', 0,1);//
                    #echo "<pre>";print_r($photo_path_dire);#exit;
                    $ProfilePhotoPath = $photo_path_dire['path'];
                    if (!is_dir($ProfilePhotoPath)) {
                        mkdir($ProfilePhotoPath, 0777);
                    }
                    $width = $orig_width;
                    $height = $orig_height;
                    $max_width = 500;
                    $max_height = 350;
                    # taller
                    if ($height > $max_height) {
                        $width = ($max_height / $height) * $width;
                        $height = $max_height;
                    }
                    # wider
                    if ($width > $max_width) {
                        $height = ($max_width / $width) * $height;
                        $width = $max_width;
                    }
                    list($txt, $ext) = explode(".", $PHOTO);
                    #$ActualImageName = Yii::$app->params['profilePrefix'] . "_" . $txt . '.' . $ext;
                    $ActualImageName = $TempImageName = Yii::$app->params['profilePrefix'] . rand() . "_" . $txt . '.' . $ext;
                    #echo "<pre>";print_r($ActualImageName);exit;
                    $image_p = imagecreatetruecolor($width, $height);
                    $image = imagecreatefromjpeg($FileName);
                    imagecopyresampled($image_p, $image, 0, 0, 0, 0,
                        $width, $height, $orig_width, $orig_height);
                    imagejpeg($image_p, $ProfilePhotoPath . $ActualImageName, 90);
                    $MainImagePath = $ProfilePhotoPath . $ActualImageName;
                    #echo "<br>".$MainImagePath;exit;
                    $ProfilePhotoSize = CommonHelper::getUserResizeRatio();
                    foreach ($ProfilePhotoSize as $KeySize => $ValueSize) {
                        $w1 = $h1 = Yii::$app->params['cropSize'];
                        $t_width = $ValueSize;     // Maximum thumbnail width
                        $t_height = $ValueSize;    // Maximum thumbnail height
                        $NewImagePath = $ProfilePhotoPath . $ValueSize . $ActualImageName;
                        #echo "<br>". $NewImagePath;
                        $ratio = ($t_width / $w1);
                        $nw = ceil($w1 * $ratio);
                        $nh = ceil($h1 * $ratio);
                        $nimg = imagecreatetruecolor($nw, $nh);
                        $im_src = imagecreatefromjpeg($MainImagePath);
                        imagecopyresampled($nimg, $im_src, 0, 0, 0, 0, $nw, $nh, $w1, $h1);
                        imagejpeg($nimg, $NewImagePath, 100);
                    }
                    $PG = new UserPhotos();
                    $PG->updateIsProfilePhoto($user_id);
                    $UserPhotosModel = $PG->findByPhotoId($user_id, $photo_id);

                    if (count($UserPhotosModel) != 0) {
                        $UserModel = User::findOne($user_id);
                        #echo "<pre>";print_r($UserModel);
                        CommonHelper::ProfilePhotoDeleteFromFolder($ProfilePhotoPath, $ProfilePhotoSize, $UserModel->propic); # Delete Photos from Directory.
                        #echo "<br>".$UserModel->propic;
                        $UserModel->propic = $ActualImageName;
                        #$UserModel->eStatusPhotoModify = 'Pending';
                        $UserModel->eStatusPhotoModify =  User::USER_PHOTO_APPROVE;
                        $UserModel->completed_step = $UserModel->setCompletedStep('7');

                        $ACTION_FLAG = $UserModel->save();
                        $UserPhotosModel->Is_Profile_Photo = 'YES';
                        //$UserPhotosModel->eStatus = 'Pending';
                        $UserPhotosModel->save();
                        list($STATUS, $MESSAGE, $TITLE) = MessageHelper::getMessageNotification('S', 'PROFILE_PHOTO_SET');
                        Yii::$app->getSession()->setFlash('success', '<b>Congratulation!</b> '.$MESSAGE);
                        #Yii::$app->session->setFlash('success', $MESSAGE);
                        /* $ProfilePhotoURL = CommonHelper::getUserUploadFolder(4, $Id);
                         if ($ACTION_FLAG) {
                             $ProfilePhoto = $ProfilePhotoURL . "200" . $NewProfilePhotoName . '?' . time();
                             $ProfilePhotoThumb = $ProfilePhotoURL . "30" . $NewProfilePhotoName . '?' . time();
                             list($STATUS, $MESSAGE, $TITLE) = MessageHelper::getMessageNotification('S', 'PROFILE_PHOTO_SET');
                         } else {
                             list($STATUS, $MESSAGE, $TITLE) = MessageHelper::getMessageNotification('E', 'PROFILE_PHOTO_SET');
                         }*/
                    }
                    unlink($MainImagePath);

                    # echo "<pre>";print_r($UP);

                }else{
                    Yii::$app->getSession()->setFlash('error', 'Photo Not Found!');

                }
            }else{
                Yii::$app->getSession()->setFlash('error', 'Missing User or Photo!');
            }

        }
        else{
            Yii::$app->getSession()->setFlash('error', 'Something went wrong. Please try again!');
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionPhotoList()
    {
        #$model = $this->findModel($id);
        #CommonHelper::pr(Yii::$app->request->post('u_id'));exit;
        $id = Yii::$app->request->post('u_id');
        $model = $this->findModel($id);
        $UserPhotos = new UserPhotos();
        $user_photos = $UserPhotos->findByUserId($id);
        $myModel = [
            'model' => $model,
            'user_photos' => $user_photos ,
        ];
        #CommonHelper::pr($myModel );exit;
        $HtmlOutput = $this->renderAjax('_photolist', $myModel);
        $return = array('STATUS' => 'S', 'MESSAGE' => '', 'TITLE' => '', 'OUTPUT' => $HtmlOutput);
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $return;
    }

    public function actionPhotoOperation()
    {
        $id = Yii::$app->request->post('u_id');
        $PG = new UserPhotos();
        $CM_HELPER = new CommonHelper();
        $P_ID = Yii::$app->request->post('P_ID');
        $P_TYPE = Yii::$app->request->post('P_TYPE');
        $OUTPUT_HTML = '';
        $OUTPUT_HTML_ONE = '';
        $PROFILE_PHOTO = '';
        $PROFILE_PHOTO_ONE = '';
        if ($P_ID != '' && $P_TYPE == 'PHOTO_DELETE' && $P_TYPE != '') {
            $USER_PHOTOS_LIST = $PG->findByPhotoId($id, $P_ID);
            #print_r($USER_PHOTOS_LIST );exit;
            if (count($USER_PHOTOS_LIST) != 0) {
                $IS_PROFILE = $USER_PHOTOS_LIST->Is_Profile_Photo;
                $PHOTO = $USER_PHOTOS_LIST->File_Name;
                $PATH = $CM_HELPER->getUserUploadFolder(1) . "/" . $id . "/";
                $USER_SIZE_ARRAY = $CM_HELPER->getUserResizeRatio();
                $CM_HELPER->photoDeleteFromFolder($PATH, $USER_SIZE_ARRAY, $PHOTO);
                $ACTION_FLAG = $USER_PHOTOS_LIST->delete();
                if ($IS_PROFILE == 'YES') {
                    #$model_user = User::findOne($id);
                    $UserModel = User::findOne($id);
                    $ProfilePhotoPath = CommonHelper::getUserUploadFolder(3, $id);
                    $ProfilePhotoSize = CommonHelper::getUserResizeRatio();
                    CommonHelper::ProfilePhotoDeleteFromFolder($ProfilePhotoPath, $ProfilePhotoSize, $UserModel->propic); # Delete Photos from Directory.
                    $UserModel = User::findOne($id);
                    $UserModel->propic = '';
                    $UserModel->eStatusPhotoModify = 'Pending';
                    #$UserModel->completed_step = $UserModel->setCompletedStep('7');
                    $UserModel->completed_step = CommonHelper::unsetStep($UserModel->completed_step, 7);
                    $UserModel->save();

                    $PROFILE_PHOTO = CommonHelper::getPhotos('USER', $id, $PHOTO, '200');
                    $PROFILE_PHOTO_ONE = CommonHelper::getPhotos('USER', $id, $PHOTO, '30');
                }
                if ($ACTION_FLAG) {
                    list($STATUS, $MESSAGE, $TITLE) = MessageHelper::getMessageNotification('S', 'PHOTO_DELETE');
                } else {
                    list($STATUS, $MESSAGE, $TITLE) = MessageHelper::getMessageNotification('E', 'PHOTO_DELETE');

                }
            }else{
                list($STATUS, $MESSAGE, $TITLE) = MessageHelper::getMessageNotification('S', 'PHOTO_DELETE');
            }
        } else {
            $PG->updateIsProfilePhoto($id);
            $USER_PHOTOS_LIST = $PG->findByPhotoId($id, $P_ID);
            if (count($USER_PHOTOS_LIST) != 0) {
                $USER_MODEL = User::findOne($id);
                #echo "<pre>";print_R($USER_MODEL);
                $PHOTO = $USER_PHOTOS_LIST->File_Name;
                $USER_MODEL->propic = $PHOTO;
                $USER_MODEL->eStatusPhotoModify = 'Pending';
                $USER_MODEL->completed_step = $USER_MODEL->setCompletedStep('7');
                $USER_MODEL->save();
                $ACTION_FLAG = $USER_MODEL->save();
                $USER_PHOTOS_LIST->Is_Profile_Photo = 'YES';
                $USER_PHOTOS_LIST->eStatus = 'Pending';
                $USER_PHOTOS_LIST->save();
                if ($ACTION_FLAG) {
                    $PROFILE_PHOTO .= CommonHelper::getPhotos('USER', $id, Yii::$app->params['thumbnailPrefix'] . "200_" . $PHOTO, '200');
                    $PROFILE_PHOTO_ONE = CommonHelper::getPhotos('USER', $id, Yii::$app->params['thumbnailPrefix'] . "30_" . $PHOTO, '30');
                    list($STATUS, $MESSAGE, $TITLE) = MessageHelper::getMessageNotification('S', 'PROFILE_PHOTO_SET');
                } else {
                    list($STATUS, $MESSAGE, $TITLE) = MessageHelper::getMessageNotification('E', 'PROFILE_PHOTO_SET');
                }
            }else{
                list($STATUS, $MESSAGE, $TITLE) = MessageHelper::getMessageNotification('S', 'PHOTO_DELETE');
            }
        }
        $user_photos = $PG->findByUserId($id);
        $model = $this->findModel($id);
        $myModel = [
            'model' => $model,
            'user_photos' => $user_photos ,
        ];
        $HtmlOutput = $this->renderAjax('_photolist', $myModel);
        $return = array('STATUS' => $STATUS, 'MESSAGE' => $MESSAGE, 'TITLE' => $TITLE, 'OUTPUT' => $HtmlOutput, 'PROFILE_PHOTO' => $PROFILE_PHOTO, 'PROFILE_PHOTO_ONE' => $PROFILE_PHOTO_ONE);
        return json_encode($return);
    }
    public function actionPhotoCropping()
    {
        $Id = Yii::$app->request->post('u_id');
        #$Id = Yii::$app->user->identity->id;
        $ProfilePhotoPath = CommonHelper::getUserUploadFolder(3, $Id);
        $ActualImagePATH = CommonHelper::getUserUploadFolder(1) . $Id . "/";
        $MaxWidth = Yii::$app->params['maxWidth'];
        $PHOTO = Yii::$app->request->post('image_name');
        $FileName = $ActualImagePATH . $PHOTO;
        list($orig_width, $orig_height) = getimagesize($FileName);
        if (!is_dir($ProfilePhotoPath)) {
            mkdir($ProfilePhotoPath, 0777);
        }
        $width = $orig_width;
        $height = $orig_height;
        $max_width = 500;
        $max_height = 350;
        # taller
        if ($height > $max_height) {
            $width = ($max_height / $height) * $width;
            $height = $max_height;
        }
        # wider
        if ($width > $max_width) {
            $height = ($max_width / $width) * $height;
            $width = $max_width;
        }
        list($txt, $ext) = explode(".", $PHOTO);
        #$ActualImageName = Yii::$app->params['profilePrefix'] . "_" . $txt . '.' . $ext;
        $ActualImageName = $TempImageName = Yii::$app->params['profilePrefix'] . rand() . "_" . $txt . '.' . $ext;
        $image_p = imagecreatetruecolor($width, $height);
        $image = imagecreatefromjpeg($FileName);
        imagecopyresampled($image_p, $image, 0, 0, 0, 0,
            $width, $height, $orig_width, $orig_height);
        imagejpeg($image_p, $ProfilePhotoPath . $ActualImageName, 90);
        $ProfilePhotoURL = CommonHelper::getUserUploadFolder(4, $Id);
        $ProfilePhotoURL = str_replace("backend","frontend",$ProfilePhotoURL);
        $return = array('PhotoCrop' => $ProfilePhotoURL . $ActualImageName . '?' . time(), 'ImageName' => $ActualImageName, 'ImagePath' => $ProfilePhotoPath . $ActualImageName);
        return json_encode($return);
    }
    public function actionSetProfilePhotoUser()
    {
        $Id = Yii::$app->request->post('u_id');
        #$Id = Yii::$app->user->identity->id;
        $ProfilePhotoPath = CommonHelper::getUserUploadFolder(3, $Id);
        $Photo = Yii::$app->request->post('image_name');
        $PhotoId = Yii::$app->request->post('image_id');
        $PID = Yii::$app->request->post('image_id');
        if ($Photo != '' && $PhotoId = !'') {
            list($txt, $ext) = explode(".", $Photo);
            $ActualImageName = $Photo;
            if (1) {
                $ProfilePhotoSize = CommonHelper::getUserResizeRatio();
                if (isset($_POST['t']) and $_POST['t'] == "ajax") {
                    extract(Yii::$app->request->post());
                    $MainImagePath = $ProfilePhotoPath . $ActualImageName;
                    foreach ($ProfilePhotoSize as $KeySize => $ValueSize) {
                        $t_width = $ValueSize;     // Maximum thumbnail width
                        $t_height = $ValueSize;    // Maximum thumbnail height
                        #rand()."_"
                        $NewImagePath = $ProfilePhotoPath . $ValueSize . $ActualImageName;
                        $ratio = ($t_width / $w1);
                        $nw = ceil($w1 * $ratio);
                        $nh = ceil($h1 * $ratio);
                        $nimg = imagecreatetruecolor($nw, $nh);
                        $im_src = imagecreatefromjpeg($MainImagePath);
                        imagecopyresampled($nimg, $im_src, 0, 0, $x1, $y1, $nw, $nh, $w1, $h1);
                        imagejpeg($nimg, $NewImagePath, 90);
                    }

                    /* Set Profile photo Name into database Store */
                    $PG = new UserPhotos();
                    $PG->updateIsProfilePhoto($Id);
                    $UserPhotosModel = $PG->findByPhotoId($Id, $PID);
                    if (count($UserPhotosModel) != 0) {
                        $model = $this->findModel($Id);
                        CommonHelper::ProfilePhotoDeleteFromFolder($ProfilePhotoPath, $ProfilePhotoSize, $model->propic); # Delete Photos from Directory.
                        $UserModel = User::findOne($Id);
                        $UserModel->propic = $ActualImageName;
                        $UserModel->eStatusPhotoModify = 'Pending';
                        $UserModel->completed_step = $UserModel->setCompletedStep('7');

                        $ACTION_FLAG = $UserModel->save();
                        $UserPhotosModel->Is_Profile_Photo = 'YES';
                        #$UserPhotosModel->eStatus = 'Pending';
                        $UserPhotosModel->save();
                        $ProfilePhotoURL = CommonHelper::getUserUploadFolder(4, $Id);
                        if ($ACTION_FLAG) {
                            $ProfilePhoto = $ProfilePhotoURL . "200" . $ActualImageName . '?' . time();
                            $ProfilePhotoThumb = $ProfilePhotoURL . "30" . $ActualImageName . '?' . time();
                            list($STATUS, $MESSAGE, $TITLE) = MessageHelper::getMessageNotification('S', 'PROFILE_PHOTO_SET');
                        } else {
                            list($STATUS, $MESSAGE, $TITLE) = MessageHelper::getMessageNotification('E', 'PROFILE_PHOTO_SET');
                        }
                    }
                    /* Set Profile photo Name into database  END */
                    #$ProfilePhotoURL = CommonHelper::getUserUploadFolder(4, $Id);
                    #$ProfilePhoto = $ProfilePhotoURL . "200" .Yii::$app->params['profilePrefix'].  $ActualImageName.'?'.time();
                    #$ProfilePhotoThumb = $ProfilePhotoURL . "30" .Yii::$app->params['profilePrefix']. $ActualImageName.'?'.time();
                    #list($STATUS, $MESSAGE, $TITLE) = MessageHelper::getMessageNotification('S', 'PROFILE_PHOTO_SET');
                    unlink($MainImagePath);
                }
            } else {
                $MESSAGE = Yii::$app->params['photoCopyError'];
                list($STATUS, $MESSAGE, $TITLE) = MessageHelper::getMessageNotification('E', 'PROFILE_PHOTO_SET');
            }
        } else {
            $STATUS = 'E';
            $MESSAGE = Yii::$app->params['photoMissingError'];
        }
        $TITLE = 'Profile Photo';
        $return = array('STATUS' => $STATUS, 'MESSAGE' => $MESSAGE, 'TITLE' => $TITLE, 'ProfilePhoto' => $ProfilePhoto, 'ProfilePhotoThumb' => $ProfilePhotoThumb);
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $return;
    }
    public function actionPhotoDelete()
    {
        unlink(Yii::$app->request->post('ImagePath'));
    }

    public function actionPhotoUpload($id)
    {
        //$id = Yii::$app->request->post('u_id');
        $id = base64_decode($id);
        //$id = Yii::$app->user->identity->id;
        if ($model = User::findOne($id)) {
            $FILE_COUNT = count($_FILES);
            $UserPhotos = new UserPhotos();
            $MaximumPhotoLimit = Yii::$app->params['total_files_allowed'];
            $TotalUploadPhotos = $UserPhotos->totalUploadPhotos($id);
            $TotalUploads = $TotalUploadPhotos + count($_FILES["__files"]['name']);
            $RemainingLimit = $MaximumPhotoLimit - $TotalUploadPhotos;
            if ($TotalUploads <= $MaximumPhotoLimit) {
                $model->scenario = User::SCENARIO_REGISTER6;
                if ($FILE_COUNT != 0) {
                    $CM_HELPER = new CommonHelper();
                    $PATH = $CM_HELPER->getUserUploadFolder(1) . $id . "/";
                    $URL = $CM_HELPER->getUserUploadFolder(2) . $id . "/";
                    $USER_SIZE_ARRAY = $CM_HELPER->getUserResizeRatio();
                    $PHOTO_ARRAY = CommonHelper::photoUploads($id, $_FILES["__files"], $PATH, $URL, $USER_SIZE_ARRAY, '');
                    if ($PHOTO_ARRAY['STATUS']) {
                        $ACTION_FLAG = false;
                        if (is_array($PHOTO_ARRAY['PhotoArray'])) {
                            foreach ($PHOTO_ARRAY['PhotoArray']['images'] as $PhotoKey => $PhotoValue) {
                                $PG = new UserPhotos();
                                $PG->iUser_ID = $id;
                                $PG->Is_Profile_Photo = 'NO';
                                $PG->eStatus = 'Approve';
                                $PG->dtCreated = CommonHelper::getTime();
                                $PG->dtModified = CommonHelper::getTime();
                                $PG->File_Name = $PhotoValue;
                                $ACTION_FLAG = $PG->save();
                            }
                        }
                        if ($ACTION_FLAG) {
                            list($STATUS, $MESSAGE, $TITLE) = MessageHelper::getMessageNotification('S', 'PHOTO_UPLOAD');
                            $MESSAGE = 'Photo(s) uploaded successfully.';
                        } else {
                            list($STATUS, $MESSAGE, $TITLE) = MessageHelper::getMessageNotification('E', 'PHOTO_UPLOAD');
                        }
                    } else {
                        list($STATUS, $MESSAGE, $TITLE) = MessageHelper::getMessageNotification('E', 'PHOTO_UPLOAD');
                        $MESSAGE = $PHOTO_ARRAY['MESSAGE'];
                    }
                }
                $user_photos= $UserPhotos->findByUserId($id);
                $model = $this->findModel($id);
                $myModel = [
                    'model' => $model,
                    'user_photos' => $user_photos ,
                ];
                $HtmlOutput = $this->renderAjax('_photolist', $myModel);
            } else {
                $STATUS = 'E';
                $MESSAGE = CommonHelper::replaceNotificationMessage(Yii::$app->params['uploadLimitError'], array('LIMIT' => $RemainingLimit));
                $TITLE = Yii::$app->params['titleWarrning'];
            }
            $return = array('STATUS' => $STATUS, 'MESSAGE' => $MESSAGE, 'TITLE' => $TITLE, 'HtmlOutput' => $HtmlOutput);
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $return;
        } else {
            return $this->redirect(Yii::getAlias('@web'));
        }
    }
}
