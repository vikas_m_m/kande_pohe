<?php

$root = '';
if ($_SERVER['HTTP_HOST'] == '103.21.58.248') {
    $root = (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['HTTP_HOST'] . "/kande_pohe/frontend/web/uploads/";
}else if ($_SERVER['HTTP_HOST'] == '127.0.0.1' || $_SERVER['HTTP_HOST'] == 'localhost' ) {
    $root = (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['HTTP_HOST'] . "/kande_pohe/frontend/web/uploads/";
}else{
    $root = (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['HTTP_HOST'] . "/frontend/web/uploads/";
}
#echo $root;exit;
return [
    'adminEmail' => 'donotreply@kande-pohe.com',
    #'adminEmail' => 'kandepohetest@gmail.com',
    'frontendURL' => $root ,
    'frontendPATH' =>  Yii::getAlias('@upload_path')."/frontend/web/uploads/" ,
];
