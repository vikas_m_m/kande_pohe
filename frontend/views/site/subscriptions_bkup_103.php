<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\components\CommonHelper;
use yii\helpers\ArrayHelper;
$color_array = array('PACKAGE_FREE' =>'', 'PACKAGE_SILVER' =>'green', 'PACKAGE_GOLD' =>'blue', 'PACKAGE_PLATINUM' =>'red');
$icons_array = array('PACKAGE_FREE' =>'fa fa-bullhorn','PACKAGE_SILVER'=>'fa fa-star-half-o','PACKAGE_GOLD'=>'fa fa-trophy','PACKAGE_PLATINUM' =>'fa fa-diamond');
?>
<main>
    <div class="container-fluid">
        <div class="row no-gutter bg-dark">
            <div class="no-gutter">
                <div class="col-lg-12 col-md-11 col-sm-11">
                    <div class="white-section mrg-tp-20 mrg-bt-10">
                        <h3>Subscription Package Details</h3>
                        <!--<div class="demo11">
                            <div class="container2">
                                <div class="row1">
                                    <?php /*if(count(@$subscriptions_data)>0){
                                        foreach($subscriptions_data as $k=>$v){ */?>
                                            <?php /*if($k==10){ */?>
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="pricingTable11 <?/*= $color_array[$v->short_name] */?>">
                                                        <div class="pricingTable-header">
                                                            <i class="<?/*= $icons_array[$v->short_name] */?>"></i>
                                                            <div class="price-value"> <i class="fa fa-inr"></i><?/*= number_format($v->subscriptions_price,2)*/?> <span class="month"></span> </div>
                                                        </div>
                                                        <h3 class="heading"><?/*= stripslashes($v->subscriptions_name)*/?></h3>
                                                        <div class="pricing-content">
                                                            <ul>
                                                                <li><b><?/*= ($v->profile_duration)*/?>Days</b> te</li>
                                                                <li><b><?/*= ($v->no_of_contacts)*/?></b> No of Contacts</li>
                                                                <li><b><?/*= ($v->no_of_pm)*/?></b> Personalized Messaging </li>
                                                                <li><b><?/*= ($v->privacy_settings)*/?></b> Privacy Settings </li>
                                                                <li><b><?/*= ($v->validity_of_package)*/?>Days</b> Validity of Package</li>
                                                                <li><b><?/*= ($v->customer_care_support)*/?></b> Customer Care Support  </li>
                                                            </ul>
                                                        </div>
                                                        <div class="pricingTable-signup">
                                                            <a href="<?/*= Yii::$app->getUrlManager()->getBaseUrl() . Yii::$app->params['userSignUp'] */?>"
                                                               title="Sign up Free" >Register Free</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php /*}*/?>
                                            <div class="col-md-3 col-sm-6">
                                                <div class="pricingTable11 <?/*= $color_array[$v->short_name] */?>">
                                                    <div class="pricingTable-header">
                                                        <i class="<?/*= $icons_array[$v->short_name] */?>"></i>
                                                        <div class="price-value"> <i class="fa fa-inr"></i><?/*= number_format($v->subscriptions_price,2)*/?> <span class="month"></span> </div>
                                                    </div>
                                                    <h3 class="heading"><?/*= stripslashes($v->subscriptions_name)*/?></h3>
                                                    <div class="pricing-content">
                                                        <ul>
                                                            <li><b><?/*= ($v->profile_duration)*/?>Days</b> </li>
                                                            <li><b><?/*= ($v->no_of_contacts)*/?></b> No of Contacts</li>
                                                            <li><b><?/*= ($v->no_of_pm)*/?></b> Personalized Messaging </li>
                                                            <li><b><?/*= ($v->privacy_settings)*/?></b> Privacy Settings </li>
                                                            <li><b><?/*= ($v->validity_of_package)*/?>Days</b> Validity of Package</li>
                                                            <li><b><?/*= ($v->customer_care_support)*/?></b> Customer Care Support  </li>
                                                        </ul>
                                                    </div>
                                                    <div class="pricingTable-signup">
                                                        <a href="<?/*= Yii::$app->getUrlManager()->getBaseUrl() . Yii::$app->params['userSignUp'] */?>"
                                                           title="Sign up Free" >Register Free</a>
                                                    </div>
                                                </div>
                                            </div>

                                        <?php /*} } */?>
                                </div>
                            </div>
                        </div>-->
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10 col-sm-12">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p><br>
                                <p>
                                    Pri quas audiam virtute ut, case utamur fuisset eam ut, iisque accommodare an eam. Reque blandit qui eu, cu vix nonumy volumus. Legendos intellegam id usu, vide oporteat vix eu, id illud principes has. Nam tempor utamur gubergren no.
                                </p><br>
                                <ul style="list-style-type:square">
                                    <li>Free Registration Feature</li>
                                    <li>Contact Feature</li>
                                    <li>Personal Messaging Feature</li>
                                    <li>Customer Support</li>
                                </ul>
                            </div>
                        </div>
                        <br>
                        <style>
                            .pricingTable11 .pricingTable-header i{
                                font-size: 25px;
                            }
                            .pricingTable11 .price-value{
                                font-size: 25px;
                            }
                            .pricingTable11 .pricingTable-header{
                                padding: 15px 0;
                            }
                            .pricingTable111{
                                text-align: center;
                                background: #FFC107;
                                margin: 0 71px;
                                box-shadow: 0 0 10px #ababab;
                                padding-bottom: 40px;
                                border-radius: 10px;
                                color: #0a0a0a;
                                transform: scale(1);
                                transition: all .5s ease 0s;
                                width: 80%;

                            }
                            .pricingTable-header1{
                                padding: 52px 0;
                                /* background: #f5f6f9; */
                                /* border-radius: 10px 10px 50% 50%; */
                                transition: all .5s ease 0s;
                            }
                            .heading1{
                                color: #ff4b4b;
                                font-size: 24px;
                                margin-bottom: 20px;
                                text-transform: uppercase;
                                font-weight: 700;
                                border-bottom: #ff4b4b 2px solid;
                            }
                            .price-description{
                                list-style: none;
                                padding: 0;
                                margin-bottom: 30px;
                            }
                            .price-description > li{
                                line-height: 30px;
                                color: #a7a8aa;
                            }

                        </style>
                        <div class="row">
                        <div class="demo11">
                            <div class="container1">
                                <div class="row1">
                                    <?php if(count(@$subscriptions_data)>0){?>
                                        <div class="col-md-3">
                                            <div class="pricingTable111 ">
                                                <div class="pricingTable-header1" >
                                                </div>
                                                <h3 class="heading1" >PLAN</h3>
                                                <div class="pricing-content">
                                                    <ul class="price-description">
                                                        <li><b>Duration</b> </li>
                                                        <li><b>Number of Contacts</b></li>
                                                        <li><b>Personalized Messaging </b></li>
                                                        <li><b>Privacy Settings</b></li>
                                                        <li><b>Validity of Package</b></li>
                                                        <li><b>Customer Care Support</b> </li>
                                                    </ul>
                                                </div>
                                                <div class="pricingTable-signup">
                                                    <BR>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        foreach($subscriptions_data as $k=>$v){ ?>
                                            <?php if($k==10){ ?>
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="pricingTable11 <?= $color_array[$v->short_name] ?>">
                                                        <div class="pricingTable-header">
                                                            <i class="<?= $icons_array[$v->short_name] ?>"></i>
                                                            <div class="price-value"> <i class="fa fa-inr"></i><?= number_format($v->subscriptions_price,2)?> <span class="month"></span> </div>
                                                        </div>
                                                        <h3 class="heading"><?= stripslashes($v->subscriptions_name)?></h3>
                                                        <div class="pricing-content">
                                                            <ul>
                                                                <li><b><?= ($v->profile_duration)?>Days</b> Duration of Profile on Website</li>
                                                                <li><b><?= ($v->no_of_contacts)?></b> No of Contacts</li>
                                                                <li><b><?= ($v->no_of_pm)?></b> Personalized Messaging </li>
                                                                <li><b><?= ($v->privacy_settings)?></b> Privacy Settings </li>
                                                                <li><b><?= ($v->validity_of_package)?>Days</b> Validity of Package</li>
                                                                <li><b><?= ($v->customer_care_support)?></b> Customer Care Support  </li>
                                                            </ul>
                                                        </div>
                                                        <div class="pricingTable-signup">
                                                            <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() . Yii::$app->params['userSignUp'] ?>"
                                                               title="Sign up Free" >Register Free</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }?>
                                            <div class="col-md-2 col-sm-6">
                                                <div class="pricingTable11 <?= $color_array[$v->short_name] ?>">
                                                    <div class="pricingTable-header">
                                                        <i class="<?= $icons_array[$v->short_name] ?>"></i>
                                                        <div class="price-value"> <i class="fa fa-inr"></i><?= number_format($v->subscriptions_price,2)?> <span class="month"></span> </div>
                                                    </div>
                                                    <h3 class="heading"><?= stripslashes($v->subscriptions_name)?></h3>
                                                    <div class="pricing-content">
                                                        <ul>
                                                            <li><b><?= ($v->profile_duration)?>Days</b></li>
                                                            <li><b><?= ($v->no_of_contacts)?></b></li>
                                                            <li><b><?= ($v->no_of_pm)?></b></li>
                                                            <li><b><?= ($v->privacy_settings)?></b></li>
                                                            <li><b><?= ($v->validity_of_package)?>Days</b></li>
                                                            <li><b><?= ($v->customer_care_support)?></b></li>
                                                        </ul>
                                                    </div>
                                                    <div class="pricingTable-signup">
                                                        <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() . Yii::$app->params['userSignUp'] ?>"
                                                           title="Sign up Free" >Register Free</a>
                                                    </div>
                                                </div>
                                            </div>

                                        <?php } } ?>
                                </div>
                            </div>
                        </div>
                            </div>
                        <div class="row">
                            <br>
                            <div class="col-md-1"></div>
                            <div class="col-md-10 col-sm-12">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p><br>
                                <p>
                                    Pri quas audiam virtute ut, case utamur fuisset eam ut, iisque accommodare an eam. Reque blandit qui eu, cu vix nonumy volumus. Legendos intellegam id usu, vide oporteat vix eu, id illud principes has. Nam tempor utamur gubergren no.
                                </p><br>
                                <ul style="list-style-type:square">
                                    <li>Free Registration Feature</li>
                                    <li>Contact Feature</li>
                                    <li>Personal Messaging Feature</li>
                                    <li>Customer Support</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
