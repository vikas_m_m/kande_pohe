<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\components\CommonHelper;
use yii\helpers\ArrayHelper;

?>
<main>
    <div class="container-fluid">
        <div class="row no-gutter bg-dark">
            <div class="col-md-2">
            </div>
            <div class="no-gutter">
                <div class="col-lg-8 col-md-9 col-sm-9 col-md-offset-0">
                    <div class="white-section mrg-tp-20 mrg-bt-10">
                        <h3>Sign Up</h3>
                        <!-- <span class="error">Oops! Please ensure all fields are valid</span> -->
                        <?php
                        $form = ActiveForm::begin([
                            'id' => 'form-signup',
                            //'action' => 'javascript:void(0)',
                            'action' => ['/site/register'],
                            'enableAjaxValidation' => true,
                            'enableClientValidation' => true,
                            'validateOnChange' => true,

                        ]);
                        ?>
                        <button type="button" class="btn btn-primary mrg-tp-10 col-xs-12" data-toggle="modal"
                                id="signup_model_btn"
                                data-target="#signup_model" style="display: none"></button>
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="form-cont">
                                    <?= $form->field($model, 'email', ["template" => '<span class="input input--akira">{input}<label class="input__label input__label--akira" for="input-22"> <span class="input__label-content input__label-content--akira">Email</span> </label></span>{error}'])->input('email', ['class' => 'input__field input__field--akira form-control']) ?>
                                </div>
                                <div class="form-cont">
                                    <?= $form->field($model, 'password_hash', ["template" => '<span class="input input--akira">{input}<label class="input__label input__label--akira" for="input-22"> <span class="input__label-content input__label-content--akira">Password</span> </label></span>{error}'])->input('password', ['class' => 'input__field input__field--akira form-control']) ?>
                                </div>
                                <div class="form-cont">
                                    <?= $form->field($model, 'repeat_password', ["template" => '<span class="input input--akira">{input}<label class="input__label input__label--akira" for="input-22"> <span class="input__label-content input__label-content--akira">Retype Password</span> </label></span>{error}'])->input('password', ['class' => 'input__field input__field--akira form-control']) ?>
                                </div>
                                <div class="form-cont">
                                    <?= $form->field($model, 'Profile_created_for')->dropDownList(
                                        ['' => 'Profile for', 'Self' => 'Self', 'Son' => 'Son', 'Daughter' => 'Daughter', 'Brother' => 'Brother', 'Sister' => 'Sister', 'Friend' => 'Friend'],
                                        ['class' => 'demo-default select-beast']
                                    )->label(false); ?>
                                </div>
                                <div>
                                    <div class="row">
                                        <div class="form-cont col-xs-6">
                                            <?= $form->field($model, 'First_Name', ["template" => '<span class="input input--akira">{input}<label class="input__label input__label--akira" for="input-22"> <span class="input__label-content input__label-content--akira">First Name</span> </label></span>{error}'])->input('text', ['class' => 'input__field input__field--akira form-control']) ?>
                                        </div>
                                        <div class="form-cont col-xs-6">
                                            <?= $form->field($model, 'Last_Name', ["template" => '<span class="input input--akira">{input}<label class="input__label input__label--akira" for="input-22"> <span class="input__label-content input__label-content--akira">Last Name</span> </label></span>{error}'])->input('text', ['class' => 'input__field input__field--akira form-control']) ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-cont">
                                    <div class="radio radio-new" id="IVA">

                                        <?= $form->field($model, 'Gender')->RadioList(
                                            ['MALE' => 'MALE', 'FEMALE' => 'FEMALE'],
                                            [
                                                'item' => function ($index, $label, $name, $checked, $value) {

                                                    $return = '<input type="radio" class="genderV" id="' . $value . '" name="' . $name . '" value="' . $value . '" >';
                                                    $return .= '<label for="' . $value . '">' . ucwords($label) . '</label>';
                                                    return $return;
                                                }
                                            ]
                                        )->label(false); ?>
                                    </div>
                                </div>
                                <div class="form-cont">
                                    <?= $form->field($model, 'DOB', ["template" => '<span class="input input--akira dobcl ">{input}<label class="input__label input__label--akira" for="input-22"> <span class="input__label-content input__label-content--akira">Date Of Birth</span> </label></span>{error}'])->input('text')
                                        ->widget(\yii\jui\DatePicker::classname(),
                                            [
                                                'dateFormat' => 'php:Y-m-d',
                                                'options' => [
                                                    'class' => 'input__field input__field--akira form-control',
                                                    'id' => 'user-dob',
                                                    'onchange' => ' $(".dobcl").addClass("input--filled");',
                                                    'onkeyup' => ' $(".hasDatepicker").val("");',

                                                ],
                                                'clientOptions' => [
                                                    'changeMonth' => true,
                                                    'yearRange' => '-70:-21',
                                                    'changeYear' => true,
                                                    'maxDate' => date('Y-m-d', strtotime('-21 year')),
                                                ],
                                            ]);
                                    ?>
                                </div>
                                <div>
                                    <div class="row">
                                        <div class="form-cont col-xs-6">
                                            <?= $form->field($model, 'county_code')->dropDownList(
                                                ['+91' => '+91'],
                                                ['class' => 'demo-default select-beast', 'prompt' => 'Country Code']
                                            )->label(false); ?>
                                        </div>
                                        <div class="form-cont col-xs-6">
                                            <?= $form->field($model, 'Mobile', ["template" => '<span class="input input--akira">{input}<label class="input__label input__label--akira" for="input-22"> <span class="input__label-content input__label-content--akira">Mobile No#</span> </label></span>{error}'])->input('text', ['class' => 'input__field input__field--akira form-control']) ?>
                                        </div>
                                    </div>
                                    <!--<div class="row">
                                        <h3>Package Information</h3>
                                        <div class="form-cont col-md-12 col-xs-12">
                                            <section class="section-package">
                                                <?php /*if(count(@$subscriptions_data)>0){
                                                    foreach($subscriptions_data as $k=>$v){
                                                        */?>
                                                        <div >
                                                            <input type="radio" name="package_type"
                                                                   id="package_type_<?/*=$v->subscriptions_id*/?>" value="<?/*=$v->subscriptions_id*/?>" <?/*= ($v->short_name == 'PACKAGE_SILVER') ? 'checked':''*/?> />
                                                            <label for="package_type_<?/*=$v->subscriptions_id*/?>" class="package_type">
                                                                <h2 class="text-center"><?/*= stripslashes($v->subscriptions_name)*/?></h2>
                                                                <p class="text-center package-price"><?/*= "₹".number_format($v->subscriptions_price,2)*/?></p>
                                                            </label>
                                                        </div>
                                                    <?php
                                    /*                                                    }
                                                                                    }
                                                                                    */?>
                                            </section>
                                        </div>
                                    </div>-->
                                    <!-- <div class="row">
                                      <div class="col-xs-12">
                                        <script src="https://www.google.com/recaptcha/api.js" type="text/javascript"></script>
                                        <div class="g-recaptcha" data-sitekey="6Lc2xSgTAAAAAGwyfpM9OHwcAa6GZa6-6ghDl4yY"></div>
                                        <div style="color:red;" id="cerror"></div>
                                      </div>
                                    </div> -->
                                    <div class="row">
                                        <div class="checkbox col-sm-12 checkbox-new" id="cjkbox">

                                            <?= $form->field($model, 'toc')->checkboxList(
                                                ['YES'],
                                                [
                                                    'item' => function ($index, $label, $name, $checked, $value) {

                                                        $return = '<input type="checkbox" id="toc" name="' . $name . '" value="YES" >';
                                                        $return .= '<label for="toc" class="control-label toccl">By clicking  ‘Sign Up Free’  you agree to our <a href="site/terms-of-use" title="Terms" target="_blank">Terms</a></label>';

                                                        return $return;
                                                    }
                                                ]
                                            )->label(false); ?>
                                        </div>
                                        <div class="col-sm-12">

                                            <?= Html::submitButton('Sign up free', ['class' => 'btn btn-primary mrg-tp-10', 'name' => 'signup-button', 'id' => 'btnSignup']) ?>
                                        </div>
                                    </div>
                                    <?php ActiveForm::end(); ?>

                                    <div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php # SIGN UP
$this->registerJs('
    $("body").on("submit","#form-signup",function(e){
      var form = $(this);
      if(form.find(".has-error").length) {
            return false;
      }
      if(!$("#btnSignup").is(":disabled")){
        $("#btnSignup").attr("disabled",true).html("Please Wait...");
        return true;
      }
      else {
        return false;
      }
      return true;
    });
    $("body").on("submit","#login-form",function(e){
      var form = $(this);
      if(form.find(".has-error").length) {
            return false;
      }
      if(!$("#loginbtn").is(":disabled")){
        var $this_l = $(".login-btn");
        $this_l.button("loading");
        setTimeout(function() {
            $this_l.button("reset");
        }, 8000);
        return true;
      }
      else {
        return false;
      }
      return true;
    });

    $("#suf").click(function(){
    $("#form-signup")[0].reset();
    });
    $(".login_button").click(function(){
    $("#login-form")[0].reset();
    });
    $("#form-search").on("submit",function(e){
      var icommunity_id = $("#user-icommunity_id").val();
      var iSubCommunity_ID = $("#user-iSubCommunity_ID").val();
    });

  ');
?>
