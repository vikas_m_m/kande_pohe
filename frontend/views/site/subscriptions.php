<link href='<?= Yii::$app->homeUrl ?>css/package.css' rel='stylesheet' type='text/css'>
<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\components\CommonHelper;
use yii\helpers\ArrayHelper;
$color_array = array('PACKAGE_FREE' =>'', 'PACKAGE_SILVER' =>'green', 'PACKAGE_GOLD' =>'blue', 'PACKAGE_PLATINUM' =>'red');
$icons_array = array('PACKAGE_FREE' =>'fa fa-bullhorn','PACKAGE_SILVER'=>'fa fa-star-half-o','PACKAGE_GOLD'=>'fa fa-trophy','PACKAGE_PLATINUM' =>'fa fa-diamond');
?>
<main>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="white-section my-4">
                    <h3>Subscription Package Details</h3>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p><br>
                            <p>
                                Pri quas audiam virtute ut, case utamur fuisset eam ut, iisque accommodare an eam. Reque blandit qui eu, cu vix nonumy volumus. Legendos intellegam id usu, vide oporteat vix eu, id illud principes has. Nam tempor utamur gubergren no.
                            </p><br>
                            <ul style="list-style-type:square">
                                <li>Free Registration Feature</li>
                                <li>Contact Feature</li>
                                <li>Personal Messaging Feature</li>
                                <li>Customer Support</li>
                            </ul>
                            <!--<a href="javascript:void(0)" class="btn btn-primary" data-toggle="modal" data-target="#contact-modal">Open Popup</a>
                            <a href="javascript:void(0)" class="btn btn-primary" data-toggle="modal" data-target="#number-modal">Open Popup1</a>
                            <a href="javascript:void(0)" class="btn btn-primary" data-toggle="modal" data-target="#send-modal">Open Popup2</a>-->
                        </div>

                    </div>

                    <!-- PACKAGES -->
                    <div class="packages mt-5 text-center">
                        <div class="row m-0 mx-auto">
                            <div class="col-md-4 p-0 hidden-sm hidden-xs pt-width">
                                <div class="pricingTable11 first-column first-package-column">
                                    <div class="pricingTable-header ">
                                        <i class="fa fa-bullhorn"></i>
                                        <div class="price-value"> <i class="fa fa-inr"></i>0.0 <span class="month"></span> </div>
                                    </div>
                                    <h3 class="heading" >PLAN</h3>
                                    <div class="pricing-content">
                                        <ul class="price-description">
                                            <li>Duration </li>
                                            <li>Number of Contacts</li>
                                            <li>Personalized Messaging </li>
                                            <li>Privacy Settings</li>
                                            <li>Validity of Package</li>
                                            <li>Customer Care Support </li>
                                        </ul>
                                    </div>
                                    <div class="pricingTable-signup">
                                        <a href="" title="Sign up Free" >Register Free</a>
                                    </div>
                                </div>
                            </div>
                            <?php if(count(@$subscriptions_data)>0){
                                foreach($subscriptions_data as $k=>$v){ ?>
                                    <div class="col-md-2 col-sm-3 pv-width">
                                        <div class="pricingTable11 <?= $color_array[$v->short_name] ?>">
                                            <div class="pricingTable-header">
                                                <i class="<?= $icons_array[$v->short_name] ?>"></i>
                                                <div class="price-value"> <i class="fa fa-inr"></i><?= number_format($v->subscriptions_price,2)?> <span class="month"></span> </div>
                                            </div>
                                            <h3 class="heading"><?= stripslashes($v->subscriptions_name)?></h3>
                                            <div class="pricing-content">
                                                <ul>
                                                    <li data-title="Duration"><?= ($v->profile_duration)?>Days</li>
                                                    <li data-title="Number of Contacts"><?= ($v->no_of_contacts)?></li>
                                                    <li data-title="Personalized Messaging">
                                                        <?php if($v->no_of_pm == 0) { ?>
                                                            <i class="fa fa-times  subscription-no"></i>
                                                        <?php }else{
                                                            echo $v->no_of_pm ;
                                                        } ?>
                                                    </li>
                                                    <li data-title="Privacy Settings">
                                                        <?php if($v->privacy_settings == 'No') { ?>
                                                            <i class="fa fa-times  subscription-no"></i>
                                                        <?php }else{ ?>
                                                            <i class="fa fa-check subscription-yes"></i>
                                                        <?php } ?>
                                                    </li>
                                                    <li data-title="Validity of Package"><?= ($v->validity_of_package)?>Days</li>
                                                    <li data-title="Customer Care Support">
                                                        <?php if($v->customer_care_support == 'No') { ?>
                                                            <i class="fa fa-times  subscription-no"></i>
                                                        <?php }else{ ?>
                                                            <i class="fa fa-check subscription-yes"></i>
                                                        <?php } ?>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="pricingTable-signup">
                                                <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() . Yii::$app->params['userSignUp'] ?>"
                                                   title="Sign up Free" >Register Free</a>
                                            </div>
                                        </div>
                                    </div>
                            <?php } }  ?>
                            <!--<div class="col-md-2 col-sm-3 pv-width">
                                <div class="pricingTable11 ">
                                    <div class="pricingTable-header">
                                        <i class="fa fa-bullhorn"></i>
                                        <div class="price-value"> <i class="fa fa-inr"></i>0.0 <span class="month"></span> </div>
                                    </div>
                                    <h3 class="heading">FREE</h3>
                                    <div class="pricing-content">
                                        <ul>
                                            <li data-title="Duration">365Days</li>
                                            <li data-title="Number of Contacts">0</li>
                                            <li data-title="Personalized Messaging"><i class="fa fa-times  subscription-no"></i></li>
                                            <li data-title="Privacy Settings"><i class="fa fa-times subscription-no"></i></li>
                                            <li data-title="Validity of Package">0Days</li>
                                            <li data-title="Customer Care Support"><i class="fa fa-times subscription-no"></i></li>
                                        </ul>
                                    </div>
                                    <div class="pricingTable-signup">
                                        <a href=""
                                           title="Sign up Free" >Register Free</a>
                                    </div>
                                </div>
                            </div>-->
                            <!--<div class="col-md-2 col-sm-3 pv-width">
                                <div class="pricingTable11 green">
                                    <div class="pricingTable-header">
                                        <i class="fa fa-star-half-o"></i>
                                        <div class="price-value"> <i class="fa fa-inr"></i>1,200.00 <span class="month"></span> </div>
                                    </div>
                                    <h3 class="heading">SILVER</h3>
                                    <div class="pricing-content">
                                        <ul>
                                            <li data-title="Duration">365Days</li>
                                            <li data-title="Number of Contacts">15</li>
                                            <li data-title="Personalized Messaging">15</li>
                                            <li data-title="Privacy Settings"><i class="fa fa-check subscription-yes"></i></li>
                                            <li data-title="Validity of Package">90Days</li>
                                            <li data-title="Customer Care Support"><i class="fa fa-check subscription-yes"></i></li>
                                        </ul>
                                    </div>
                                    <div class="pricingTable-signup">
                                        <a href=""
                                           title="Sign up Free" >Register Free</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-3 pv-width">
                                <div class="pricingTable11 red">
                                    <div class="pricingTable-header">
                                        <i class="fa fa-diamond"></i>
                                        <div class="price-value"> <i class="fa fa-inr"></i>2,400.00 <span class="month"></span> </div>
                                    </div>
                                    <h3 class="heading">PLATINUM</h3>
                                    <div class="pricing-content">
                                        <ul>
                                            <li data-title="Duration">365Days</li>
                                            <li data-title="Number of Contacts">60</li>
                                            <li data-title="Personalized Messaging">60</li>
                                            <li data-title="Privacy Settings"><i class="fa fa-check subscription-yes"></i></li>
                                            <li data-title="Validity of Package">365Days</li>
                                            <li data-title="Customer Care Support"><i class="fa fa-check subscription-yes"></i></li>
                                        </ul>
                                    </div>
                                    <div class="pricingTable-signup">
                                        <a href=""
                                           title="Sign up Free" >Register Free</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-3  pv-width">
                                <div class="pricingTable11 blue">
                                    <div class="pricingTable-header">
                                        <i class="fa fa-trophy"></i>
                                        <div class="price-value"> <i class="fa fa-inr"></i>1,800.00 <span class="month"></span> </div>
                                    </div>
                                    <h3 class="heading">GOLD</h3>
                                    <div class="pricing-content">
                                        <ul>
                                            <li data-title="Duration">365Days</li>
                                            <li data-title="Number of Contacts">60</li>
                                            <li data-title="Personalized Messaging">60</li>
                                            <li data-title="Privacy Settings"><i class="fa fa-check subscription-yes"></i></li>
                                            <li data-title="Validity of Package">180Days</li>
                                            <li data-title="Customer Care Support"><i class="fa fa-check subscription-yes"></i></li>
                                        </ul>
                                    </div>
                                    <div class="pricingTable-signup">
                                        <a href=""
                                           title="Sign up Free" >Register Free</a>
                                    </div>
                                </div>
                            </div>-->
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    </div>
</main>
