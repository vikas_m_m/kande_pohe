<?php
use yii\helpers\Html;
use common\components\CommonHelper;
?>
<?php
$msg = "";
$msg .= "<div class=\"mrg-bt-10 text-center\"><nav>";


$URL = $MainUrl = Yii::$app->request->url;
//$URL = $MainUrl = Yii::$app->request->url;

//die($URL);
$Page = $model['page'];
$Page += 1;
if (isset($_REQUEST['ref'])) {
    $URL = str_replace("&page=" . $Page, '', $URL);
    $PageURL = '&page=';
} else {
    $URL = str_replace("?page=" . $Page, '', $URL);
    $PageURL = '?page=';
}
$TotalRecords = $model['total_records'];
$Limit = $model['limit'];

$total = ceil($TotalRecords / $Limit);
$id = $Page;
if (1) {
    #if($Page ==0)
    $cur_page = $Page;
    $Page -= 1;
    $per_page = $Limit;
    $previous_btn = true;
    $next_btn = true;
    $first_btn = true;
    $last_btn = true;
    $start = $Page * $per_page;
    if ($TotalRecords > $Limit) {
        $no_of_paginations = ceil($TotalRecords / $per_page);
        if ($no_of_paginations < $cur_page) {
            return Yii::$app->response->redirect($_SESSION['previous_location']);
        } else {
            $_SESSION['previous_location'] = $MainUrl;
        }
        if ($cur_page >= 7) {
            $start_loop = $cur_page - 3;
            if ($no_of_paginations > $cur_page + 3)
                $end_loop = $cur_page + 3;
            else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
                $start_loop = $no_of_paginations - 6;
                $end_loop = $no_of_paginations;
            } else {
                $end_loop = $no_of_paginations;
            }
        } else {
            $start_loop = 1;
            if ($no_of_paginations > 7)
                $end_loop = 7;
            else
                $end_loop = $no_of_paginations;
        }
        $msg .= '<ul class="pagination pagination-lg">';
        if ($previous_btn && $cur_page > 1) {
            $pre = $cur_page - 1;
            $msg .= '<li class="page-item first" p="' . $pre . '">
                        <a class="page-link" href="javascript:void(0)" aria-label="Previous" onclick="load_more('.$pre.')">
                            <span aria-hidden="true"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i></span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>';
        }
        for ($i = $start_loop; $i <= $end_loop; $i++) {
            if ($cur_page == $i)
                $msg .= '<li class="page-item active"><a class="page-link" href="javascript:void(0)" onclick="load_more('.$i .')">' . $i . '</a></li>';
            else
                $msg .= '<li class="page-item "><a class="page-link" href="javascript:void(0)" onclick="load_more('.$i .')">' . $i . '</a></li>';
        }
        if ($next_btn && $cur_page < $no_of_paginations) {
            $nex = $cur_page + 1;
            $msg .= '<li class="page-item last">
                                                <a class="page-link" href="javascript:void(0)"  onclick="load_more('.$nex .')" aria-label="Next">
                                                    <span aria-hidden="true"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i></span>
                                                    <span class="sr-only">Next</span>
                                                </a>
                                            </li>';
        }
        $msg = $msg . "</ul>" . @$goto . @$total_string . "";
    }
}
$msg .= "</nav></div>";
if($TotalRecords>0) {
    $msg .= "<div class=\"row text-center\"> <b>" . $TotalRecords . " Total records found</b></div>";
}
echo $msg;
?>
