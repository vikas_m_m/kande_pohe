<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use common\components\CommonHelper;
use common\components\MessageHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;
$id = 0;
if (!Yii::$app->user->isGuest) {
    $Id = Yii::$app->user->identity->id;
}
$M1 = array();
?>
<style>
</style>

<script src="https://maps.googleapis.com/maps/api/js?key=<?=GOOGLE_MAP_API_KEY?>"></script>
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<?php
/*<!--<script src="https://googlemaps.github.io/js-marker-clusterer/examples/data.json"></script>-->
<!--<script type="text/javascript" src="https://googlemaps.github.io/js-marker-clusterer/src/markerclusterer.js"></script>-->
<!--<script>
    function initialize() {
        var center = new google.maps.LatLng(37.4419, -122.1419);
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 3,
            center: center,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        var markers = [];
        for (var i = 0; i < 100; i++) {
            var dataPhoto = data.photos[i];
            var latLng = new google.maps.LatLng(dataPhoto.latitude,
                dataPhoto.longitude);
            var marker = new google.maps.Marker({
                position: latLng
            });
            markers.push(marker);
        }
        var markerCluster = new MarkerClusterer(map, markers, {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
        console.log(markerCluster);
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>-->

<!--    <script src="https://maps.googleapis.com/maps/api/js?key=--><?//=GOOGLE_MAP_API_KEY?><!--"></script>-->*/
 ?>
<script>
    var markerData = {
        "events": [
        ]
    };
    var age_start_bs = 21;
    <?php if(Yii::$app->user->identity->Gender == 'MALE') { ?>
            age_start_bs = 18;
    <?php } ?>
</script>
<main class="search-blocks">
    <div class="container-block" style="position: relative;">
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Modal Header</h4>
                    </div>
                    <div class="modal-body">
                        <p>Some text in the modal.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <div class="search-bar">
            <div class="row">
                <div class="col-sm-6">
                    <div class="input-group tags-default">
                        <input type="text" data-role="tagsinput" size="36" placeholder="State or City or Area Or District..." class="form-control" name="search_by_keyword"  id="search_by_keyword" >
                        <span class="input-group-addon p-0" id="basic-addon2"><button onclick="requestSearchFilter('','',100)"><i class="fa fa-search"></i><span>&nbsp; Search</span></button></span>
                    </div>
                </div>
                <div class="col-sm-6">
                    <!--<a href="" class="text-left black mt-2"><u>Save Search</u></a>-->
                    <!--<div class="map-view">
                        <a href="javascript:void(0)" class="active" onclick="change_view('list')" id="list_btn">List</a>
                        <a href="javascript:void(0)" onclick="change_view('map')" id="map_btn">Map</a>
                    </div>-->
                </div>
            </div>
        </div>
        <div class="row filter-row m-0">
            <div class="col-md-3 col-sm-3 p-0">
                <div class="filter-tabs-block">
                    <div>
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active filters-mob"><a href="#tab2" aria-controls="Advanced Search" role="tab" data-toggle="tab"><i class="fa fa-filter"></i>&nbsp;Filter</a></li>
                            <li class="dropdown search-profile pull-right m-0"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>&nbsp;Search By Profile ID</a>
                                <ul class="dropdown-menu">
                                    <div class="pop-over">
                                        <form class="form-inline" >
                                            <a href="javasscript:vod(0)" type="button" class="btn btn-default pull-right" onclick="search_by_profile_id()"><i class="glyphicon glyphicon-search text-danger"></i></a>
                                            <input type="text" class="form-control pull-left search_by_profile" placeholder="Enter Profile ID" name="search_by_profile">
                                        </form>
                                    </div>
                                </ul>
                            </li>
                        </ul>



                        <div class="tab-content f-section"  style="display:none">
                            <div class="filterPanel reset-filter-panel mr-4 pt-3 text-right">
                                <a href="javascript:void(0)" class="pink" onclick="resetAll()"> <i class="fa fa-repeat mr-2"></i>Reset</a>
                                <a href="javascript:void(0)" class="pink close-tab ml-4 hidden-sm hidden-md hidden-lg"> <i class="fa fa-times mr-2"></i>Close</a>
                            </div>

                            <div role="tabpanel" class="tab-pane active" id="tab2">

                                <div class="bs-accordian">
                                    <?php

                                    $form = ActiveForm::begin([
                                        'id' => 'formsearch',
                                        'action' => ['search/basic-search'],
                                        'options' => ['data-pjax' => 0],
                                        'layout' => 'horizontal',
                                        'validateOnChange' => false,
                                        'validateOnSubmit' => true,
                                        'fieldConfig' => [
                                            'template' => "{label}{beginWrapper}\n{input}\n{hint}\n{endWrapper}",
                                            'horizontalCssClasses' => [
                                                'label' => 'col-sm-3 col-xs-3',
                                                'offset' => '',
                                                'wrapper' => 'col-sm-8 col-xs-8',
                                                'error' => '',
                                                'hint' => '',
                                            ]
                                        ]
                                    ]);
                                    ?>
                                    <div class="panel-group" id="accordion">
                                        <div class="panel panel-default normal-filter fullscreen-element">
                                            <div id="collapseOne2" class="panel-collapse collapse in" style="display: block;">
                                                <div class="panel-body">
                                                    <?php /*
                                                    <!--
                                                    <div class="filter01">
                                                        <label class="filter-label multiple-label">Religion <i class="fa fa-caret-up"></i></label>
                                                        <div class="form-cont bs hovertool">
                                                            <?= $form->field($TempModel, 'iReligion_ID')->dropDownList(
                                                                ArrayHelper::map(CommonHelper::getReligion(), 'iReligion_ID', 'vName'),
                                                                ['class' => 'demo-defaultq select-beastq multiple',
                                                                    'multiple' => 'true',
                                                                    'id' => 'iReligion_ID',
                                                                    'prompt' => 'Religion',
                                                                    'onchange' => 'requestSearchFilter()']
                                                            )->label(true)->error(false); ?>
                                                        </div>
                                                    </div>
                                                    -->

                                            <?php */ ?>

                                                    <div class="filter01">
                                                        <label class="filter-label">Community</label>
                                                        <div class="multiselect-wrapper">
                                                            <select class="multiselect select4 community_ids" name="iCommunity_ID" id="iCommunity_ID" multiple onchange="requestSearchFilter()">
                                                                <?php
                                                                $community = CommonHelper::getCommunity();
                                                                //print_r($community );
                                                                if(count($community )>0){
                                                                    foreach($community as $k=>$v){ ?>
                                                                        <option value="<?=$v->iCommunity_ID?>"><?= CommonHelper::setInputVal($v->vName, 'text') ?> </option>
                                                                    <?php }
                                                                }
                                                                ?>
                                                            </select>
                                                            <input value="activate selectator" id="activate_selectator4" type="hidden">
                                                        </div>
                                                    </div>

                                                    <div class="filter01">
                                                        <label class="filter-label">Age (in years)</label>
                                                        <div class="slidecontainer">
                                                            <div id="slider-range-age"></div>
                                                            <input type="text" id="age" name="User[AgeFrom]" readonly onfocus="requestSearchFilter()" class="slider_textbox">
                                                            <input type="text" id="age1" name="User[AgeTo]"  readonly onchange="requestSearchFilter()" class="slider_textbox">
                                                        </div>
                                                    </div>

                                                    <div class="filter01">
                                                        <label class="filter-label">Height (in inch)</label>
                                                        <div class="slidecontainer">

                                                            <div id="slider-range-height"></div>
                                                            <input type="text" id="height" name="height_from" readonly onfocus="requestSearchFilter()">
                                                            <input type="text" id="height1"  name="height_to" readonly onfocus="requestSearchFilter()">
                                                        </div>
                                                    </div>

                                                    <div class="filter01">
                                                        <label class="filter-label">Weight (in kgs)</label>
                                                        <div class="slidecontainer">

                                                            <div id="slider-range-weight"></div>
                                                            <input type="text" id="weight" name="weight" readonly onfocus="requestSearchFilter()">
                                                            <input type="text" id="weight1" name="weight1" readonly onfocus="requestSearchFilter()">
                                                        </div>
                                                    </div>

                                                    <div class="filter01">
                                                        <label class="filter-label">Tags</label>
                                                        <div class="multiselect-wrapper">
                                                            <select class="multiselect select4 tags_ids" name="tags_ids" id="tags_ids" multiple onchange="requestSearchFilter()">
                                                                <?php
                                                                if(count($TagList)>0){
                                                                    foreach($TagList as $k=>$v){ ?>
                                                                        <option value="<?=$v->ID?>"><?= CommonHelper::setInputVal($v->Name, 'text') ?> </option>
                                                                    <?php }
                                                                }
                                                                ?>
                                                            </select>
                                                            <input value="activate selectator" id="activate_selectator4" type="hidden">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default advanced-filter">
                                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseOne1">
                                                <h4 class="panel-title">Advanced Filter  <i class="fa fa-angle-double-up indicator pull-right"></i></h4>
                                            </div>
                                            <div id="collapseOne1" class="panel-collapse collapse">
                                                <div class="panel-body fullscreen-element">

                                                    <div class="filter01">
                                                        <label class="filter-label">Marital Status</label>
                                                        <div class="check-wrapper column-2">
                                                            <?php
                                                            $maritalStatus = CommonHelper::getMaritalStatus();
                                                            //print_r($maritalStatus);
                                                            if(count($maritalStatus )>0){
                                                                foreach($maritalStatus as $k=>$v){ ?>
                                                                    <div class="checkbox ">
                                                                        <input type="checkbox" name="Marital_Status" id="marital_status_<?=$v->iMaritalStatusID?>" onclick="requestSearchFilter()" value="<?=$v->iMaritalStatusID?>">
                                                                        <label for="marital_status_<?=$v->iMaritalStatusID?>"><?= CommonHelper::setInputVal($v->vName, 'text') ?></label>
                                                                    </div>
                                                                <?php }
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <div class="filter01">
                                                        <label class="filter-label">Manglik</label>
                                                        <div class="radio-wrapper column-2">
                                                            <?php
                                                            $manglikList = ['Yes' => 'Yes', 'No' => 'No','DoNotMatter'=>'Do Not Matter'];
                                                            if(count($manglikList )>0){
                                                                foreach($manglikList as $k=>$v){ ?>
                                                                    <div class="radio ">
                                                                        <input type="radio" name="manglik_status" id="manglik_status_<?=$k?>" value="<?=$k?>" onclick="requestSearchFilter()" >
                                                                        <label for="manglik_status_<?=$k?>"><?= CommonHelper::setInputVal($v, 'text') ?></label>
                                                                    </div>
                                                                <?php }
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <div class="filter01">
                                                        <label class="filter-label">Gotra</label>
                                                        <div class="multiselect-wrapper">
                                                            <select class="multiselect select4 gotra_ids" name="gotra_id" id="gotra_id" multiple onchange="requestSearchFilter()">
                                                                <?php
                                                                $gotra_list = CommonHelper::getGotra();
                                                                if(count($gotra_list)>0){
                                                                    foreach($gotra_list as $k=>$v){ ?>
                                                                        <option value="<?=$v->iGotraID?>"><?= CommonHelper::setInputVal($v->vName, 'text') ?> </option>
                                                                    <?php }
                                                                }
                                                                ?>
                                                            </select>
                                                            <input value="activate selectator" id="activate_selectator4" type="hidden">
                                                        </div>
                                                    </div>
                                                    <div class="filter01">
                                                        <label class="filter-label">Education Field</label>
                                                        <div class="multiselect-wrapper">
                                                            <select class="multiselect select4 education_field" name="education_field" id="education_field" multiple onchange="requestSearchFilter()">
                                                                <?php
                                                                $educationLevel = CommonHelper::getEducationField();
                                                                if(count($educationLevel)>0){
                                                                    foreach($educationLevel as $k=>$v){ ?>
                                                                        <option value="<?=$v->iEducationFieldID?>"><?= CommonHelper::setInputVal($v->vEducationFieldName, 'text') ?> </option>
                                                                    <?php }
                                                                }
                                                                ?>
                                                            </select>
                                                            <input value="activate selectator" id="activate_selectator4" type="hidden">
                                                        </div>
                                                    </div>
                                                    <div class="filter01">
                                                        <label class="filter-label">Occupation</label>
                                                        <div class="multiselect-wrapper">
                                                            <select class="multiselect select4 occupations" name="occupations_ids" id="occupations_ids" multiple onchange="requestSearchFilter()">
                                                                <?php
                                                                $workingAsList = CommonHelper::getWorkingAS();
                                                                if(count($workingAsList)>0){
                                                                    foreach($workingAsList as $k=>$v){ ?>
                                                                        <option value="<?=$v->iWorkingAsID?>"><?= CommonHelper::setInputVal($v->vWorkingAsName, 'text') ?> </option>
                                                                    <?php }
                                                                }
                                                                ?>
                                                            </select>
                                                            <input value="activate selectator" id="activate_selectator4" type="hidden">
                                                        </div>
                                                    </div>
                                                    <div class="filter01">
                                                        <label class="filter-label">Physical Status</label>
                                                        <div class="radio-wrapper column-2">
                                                            <?php
                                                            $physical_status = ['None' => 'None', 'Physical Disability' => 'Physical Disability'];
                                                            if(count($physical_status)>0){
                                                                foreach($physical_status as $k=>$v){ ?>
                                                                    <div class="checkbox ">
                                                                        <input type="checkbox" name="physical_status" id="physical_status_<?=$k?>" onclick="requestSearchFilter()" value="<?=$k?>">
                                                                        <label for="physical_status_<?=$k?>"><?= CommonHelper::setInputVal($v, 'text') ?></label>
                                                                    </div>
                                                                <?php }
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <div class="filter01">
                                                        <label class="filter-label">Skin Tone</label>
                                                        <div class="check-wrapper">
                                                            <?php
                                                            $skinTone = CommonHelper::getSkinTone();
                                                            //print_r($maritalStatus);
                                                            if(count($skinTone )>0){
                                                                foreach($skinTone as $k=>$v){ ?>
                                                                    <div class="checkbox ">
                                                                        <input type="checkbox" name="skin_tone" id="skin_tone_<?=$v->ID?>" onclick="requestSearchFilter()" value="<?=$v->ID?>">
                                                                        <label for="skin_tone_<?=$v->ID?>"><?= CommonHelper::setInputVal($v->Name, 'text') ?></label>
                                                                    </div>
                                                                <?php }
                                                            }
                                                            ?>

                                                        </div>
                                                    </div>
                                                    <div class="filter01">
                                                        <label class="filter-label">Body Type</label>
                                                        <div class="check-wrapper">

                                                            <?php
                                                            $bodyType = CommonHelper::getBodyType();
                                                            if(count($bodyType )>0){
                                                                foreach($bodyType as $k=>$v){ ?>
                                                                    <div class="checkbox ">
                                                                        <input type="checkbox" name="body_type" id="body_type_<?=$v->ID?>" onclick="requestSearchFilter()" value="<?=$v->ID?>">
                                                                        <label for="body_type_<?=$v->ID?>"><?= CommonHelper::setInputVal($v->Name, 'text') ?></label>
                                                                    </div>
                                                                <?php }
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <div class="filter01">
                                                        <label class="filter-label">Smoking Habits</label>
                                                        <div class="radio-wrapper column-2">
                                                            <?php
                                                            $smokeList = Yii::$app->params['smokeArray'];
                                                            if(count($smokeList)>0){
                                                                foreach($smokeList as $k=>$v){ ?>
                                                                    <div class="checkbox ">
                                                                        <input type="checkbox" name="smoke_type" id="smoke_type_<?=$v?>" onclick="requestSearchFilter()" value="<?=$k?>">
                                                                        <label for="smoke_type_<?=$v?>"><?= CommonHelper::setInputVal($k, 'text') ?></label>
                                                                    </div>
                                                                <?php }
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <div class="filter01">
                                                        <label class="filter-label">Drinking Habits</label>
                                                        <div class="radio-wrapper column-2">
                                                            <?php
                                                            $drinkList = Yii::$app->params['drinkArray'];
                                                            if(count($drinkList )>0){
                                                                foreach($drinkList as $k=>$v){ ?>

                                                                    <div class="checkbox ">
                                                                        <input type="checkbox" name="drink_type" id="drink_type_<?=$v?>" onclick="requestSearchFilter()" value="<?=$k?>">
                                                                        <label for="drink_type_<?=$v?>"><?= CommonHelper::setInputVal($k, 'text') ?></label>
                                                                    </div>
                                                                <?php }
                                                            }
                                                            ?>

                                                        </div>
                                                    </div>
                                                    <div class="filter01">
                                                        <label class="filter-label">Diet</label>
                                                        <div class="check-wrapper">
                                                            <?php
                                                            $dietList = CommonHelper::getDiet();
                                                            if(count($dietList )>0){
                                                                foreach($dietList as $k=>$v){ ?>
                                                                    <div class="checkbox ">
                                                                        <input type="checkbox" name="diet" id="diet_<?=$v->iDietID?>" onclick="requestSearchFilter()" value="<?=$v->iDietID?>">
                                                                        <label for="diet_<?=$v->iDietID?>"><?= CommonHelper::setInputVal($v->vName, 'text') ?></label>
                                                                    </div>
                                                                <?php }
                                                            }
                                                            ?>

                                                        </div>
                                                    </div>
                                                    <div class="filter01 hidden">
                                                        <label class="filter-label">Salary (in lacs)</label>
                                                        <div class="slidecontainer">

                                                            <div id="slider-range"></div>
                                                            <input type="text" id="amount" readonly>
                                                            <input type="text" id="amount1" readonly>

                                                        </div>
                                                    </div>
                                                    <input type="hidden" id="pagination_page" readonly value="0">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php ActiveForm::end(); ?>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-9 col-sm-9 p-0" style="min-height:600px;">
                <!-- Map -->
                <div class="list-unstyled ad-prof map_list" style="display: none;">
                    <div id="map" class="map__wrapper" data-js="map"  data-pin="40.7290822|-74.0015273" data-center="22.5726|88.36391" data-city="kolkata"></div>
                    <!--<div id="map-containr"><div id="map"></div></div>-->
                </div>

                <!-- List -->
                <div class="list-strucure list_strucure ">
                    <?php /*\yii\widgets\Pjax::begin(); */?>
                    <div class="col-md-12">
                        <div class="sidebar1">
                            <div class="add_data_ajax">
                            </div>
                            <div class="paginationData"></div>
                        </div>

                    </div>
                    <?php /*\yii\widgets\Pjax::end(); */?>

                </div>

            </div>
        </div>

    </div>
</main>


<?php if (!Yii::$app->user->isGuest) { ?>
    <div class="modal fade" id="sendInterest" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <p class="text-center mrg-bt-10">
                <img src="<?= CommonHelper::getLogo() ?>" width="157" height="61" alt="logo">
            </p>

            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close"
                            data-dismiss="modal"><span aria-hidden="true">&times;</span> <span
                            class="sr-only">Close</span>
                    </button>
                    <h4 class="text-center"> Send Interest </h4>
                </div>
                <!-- Modal Body -->
                <div class="modal-body">
                    <form>
                        <div class="text-center">
                            <!--<p class="mrg-bt-10 font-15"><span class="text-success"><strong>&#10003;</strong></span> Your interest has been sent successfully to</p>-->
                            <div class="fb-profile-text mrg-bt-30 text-dark">
                                <h1 id="to_name"></h1>(<span class="sub-text mrg-tp-10" id="to_rg_number"></span>)
                            </div>

                            <h6 class="mrg-bt-30 font-15 text-dark">
                                <strong><?= Yii::$app->params['sendInterest'] ?></strong>
                            </h6>
                            <!--<h6 class="mrg-bt-30 font-15 text-dark"><strong>Request the member to add the following details</strong></h6>-->

                            <div class="checkbox mrg-tp-0 profile-control">
                                <!--<input id="Photo" type="checkbox" name="Photo" value="check1">
                                <label for="Photo" class="control-label">Photo</label>
                                <input id="Horoscope" type="checkbox" name="Horoscope" value="check1">
                                <label for="Horoscope" class="control-label">Horoscope</label>-->
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <button type="button" class="btn active pull-right send_request" data-id=""
                                                data-parentid=""> Send
                                            Interest
                                        </button>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 ">
                                        <button type="button" class="btn active pull-left" data-dismiss="modal">Back
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
                <!-- Modal Footer -->
            </div>
        </div>
    </div>

    <!-- Contact Modal -->
    <div class="modal fade phone_number_modal" id="phone_number_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header pnm-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span> <span
                                class="sr-only">Close</span>
                    </button>
                    <h2 class="text-center">Contact Details of <span class="cd-modal-name"></span><?= $model->FullName; ?></h2>
                </div>
                <div class="modal-body text-center">
                    <div class="row">
                        <div class="col-sm-10 mx-auto" style="float: none;">
                            <p>Would you like to view <span class="cd-modal-name"><?= $model->FullName; ?></span>’s mobile number or send an SMS?</p>
                            <div class="row mt-5">
                                <div class="col-md-6">
                                    <a href="javascript:void(0);" class="btn btn-call contact_display">View Mobile / Email</a>
                                    <small>Available Mobile No’s:<span class="cd-modal-contact">00</span></small>
                                </div>
                                <div class="col-md-6">
                                    <a href="javascript:void(0);" class="btn btn-call message_display">Send SMS/Whats App </a>
                                    <small>Available Messages:<span class="cd-modal-message">00</span></small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>
    <div class="modal fade send_sms_modal" id="send-sms-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span> <span
                                class="sr-only">Close</span>
                    </button>
                    <h2 class="text-center">Send SMS to <?= $model->FullName; ?></h2>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-11  mx-auto" style="float: none;">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="radio ">
                                        <input type="radio" name="send-sms" id="Standard SMS" value="Standard" checked>
                                        <label for="Standard SMS"><strong>Standard SMS</strong></label>
                                    </div>

                                    <div class="sms">
                                        <textarea disabled="" class="form-control standard_sms">Enter Message</textarea>
                                        <div class="disabled desable-text mb-4">You cannot change standard SMS </div>

                                        <div class="pink">You can still send SMS to <span class="cd-modal-message">70</span> members</div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="radio ">
                                        <input type="radio" name="send-sms" id="Personalized SMS" value="Personalized">
                                        <label for="Personalized SMS"><strong>Personalized SMS</strong></label>
                                    </div>

                                    <div class="sms">
                                        <textarea class="form-control personalized_sms" placeholder="Enter Your Message Here..." maxlength="155"></textarea>
                                        <div class="desable-text mb-4">155 Character maximum </div>

                                        <div class="checkbox ">
                                            <input type="checkbox" name="share_number" class="share_number" id="share" value="Yes">
                                            <label for="share">Share my mobile number along with SMS</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="text-center"><button type="button" class="btn btn-primary send_sms_btn">Send SMS </button></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>

    </div>
    <!-- Contact Number and Email Modal -->
    <div class="modal fade number_modal" id="number-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header pnm-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span> <span
                                class="sr-only">Close</span>
                    </button>
                    <h2 class="text-center">Contact Details of <?= $model->FullName; ?></h2>
                </div>
                <div class="modal-body text-center">
                    <div class="row">
                        <div class="col-sm-10 mx-auto" style="float: none;">
                            <div class="my-3 call text-left">
                                <label>Mobile No:</label><span class="display_phone_number"> </span>
                            </div>
                            <div class="my-3 call text-left">
                                <label>Email ID:</label><span class="display_email_number"> </span>
                            </div>

                            <button type="button" class="btn btn-primary mt-5 contact-display-okbtn" data-dismiss="modal">OK </button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>

    </div>
    <?php
    require_once dirname(__DIR__) . '/user/_useroperation.php';
}else{ ?>
    <div class="modal fade" id="sendMail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <p class="text-center mrg-bt-10">
                <img src="<?= CommonHelper::getLogo() ?>" width="157" height="61" alt="logo">
            </p>
            <?php Pjax::begin(['id' => 'my_index', 'enablePushState' => false]); ?>
            <div class="send_message">
                <div class="modal-content ">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span> <span
                                class="sr-only">Close</span></button>
                        <?php
                        if (!Yii::$app->user->isGuest) { ?>
                            <h2 class="text-center">Please Wait</h2>
                        <?php } else { ?>
                            <h2 class="text-center">Information</h2>
                        <?php } ?>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 mrg-tp-10 profile-control">
                                <?php
                                if (!Yii::$app->user->isGuest) { ?>
                                    <i class="fa fa-spinner fa-spin pink"></i> Loading Information...
                                <?php } else { ?>
                                    <div class="notice kp_warning marg-l"><p><?= Yii::$app->params['loginFirst'] ?></p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <a href="<?= Yii::$app->homeUrl ?>?ref=login" class="btn active pull-right"
                                           title="Login" id="login_button">Login</a>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 ">
                                        <!--<a href="<?/*= Yii::$app->homeUrl */?>?ref=signup" title="Sign up Free"-->
                                        <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() . Yii::$app->params['userSignUp'] ?>" title="Sign up Free"
                                           class="btn active pull-left">Sign up Free</a>
                                    </div>

                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php Pjax::end(); ?>
        </div>
    </div>
<?php }
?>
<!--#new Search-->
<!--<script src='https://cdn.rawgit.com/googlemaps/v3-utility-library/master/infobox/src/infobox.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.2/TweenMax.min.js'></script>-->
<?php
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/slider.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/cover/jquery-ui.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

#new Search
$this->registerJsFile(Yii::$app->request->baseUrl . '/search/js/custom.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/search/js/selectFx.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/search/js/slider-range.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/search/js/fm.selectator.jquery.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerJsFile('https://cdn.rawgit.com/googlemaps/v3-utility-library/master/infobox/src/infobox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.2/TweenMax.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerCssFile(Yii::$app->request->baseUrl . '/search/css/fm.selectator.jquery.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]);
$this->registerCssFile(Yii::$app->request->baseUrl . '/search/css/bootstrap-tagsinput/bootstrap-tagsinput.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/search/css/bootstrap-tagsinput/bootstrap-tagsinput.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

/*$this->registerJsFile(Yii::$app->request->baseUrl . '/js/cover/jquery-ui.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);*/
?>
<!--<link href="<?/*=Yii::$app->request->baseUrl . '/search/css/search_page.css'*/?>" rel="stylesheet" >-->
<!--<script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.2/TweenMax.min.js'></script>-->
<?php
$this->registerJs('
       jQuery(document).ready(function($) {
        $("#myCarousel").carousel({
                interval: 5000
        });
        $("#carousel-text").html($("#slide-content-0").html());
        //Handles the carousel thumbnails
       $("[id^=carousel-selector-]").click( function(){
         var id = this.id.substr(this.id.lastIndexOf("-") + 1);
         var id = parseInt(id);
         $("#myCarousel").carousel(id);
       });
        // When the carousel slides, auto update the text
        $("#myCarousel").on("slid.bs.carousel", function (e) {
          var id = $(".item.active").data("slide-number");
          $("#carousel-text").html($("#slide-content-"+id).html());
        });
});
    $(document).on("click",".send_email",function(e){
        var formData = new FormData();
        formData.append("ToUserId", $(this).data("id"));
        formData.append("UserId", "' . Yii::$app->user->identity->id . '");
        loaderStart();
         $.ajax({
                        url: "' . Yii::$app->homeUrl . 'user/send-email-profile",
                        type: "POST",
                        data: formData,
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data, textStatus, jqXHR) {
                            loaderStop();
                            var DataObject = JSON.parse(data);
                            notificationPopup(DataObject.STATUS, DataObject.MESSAGE, DataObject.TITLE);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                                notificationPopup(\'ERROR\', \'Something went wrong. Please try again !\', \'Error\');
                                loaderStop();
                        }
         });

    });
    $(document).on("click",".send_request",function(e){
      Pace.restart();
      //loaderStart();
      var formData = new FormData();
      formData.append("ToUserId", $(".send_request").data("id"));
      formData.append("Name", $("#to_name").text());
      formData.append("RGNumber", $("#to_rg_number").text());
      formData.append("Action", "SEND_INTEREST");
      var type= $(this).data("type");
      if(type == "map"){
      //alert(" SI MAP1 ");
          $("#sendInterest .send_request").attr("data-type","");
          sendRequestDashboard("' . Url::to(['user/send-int-dashboard']) . '",".requests","MAP_SL",$(this).data("id"),formData);
      }else{
        sendRequestDashboard("' . Url::to(['user/send-int-dashboard']) . '",".requests","SL",$(this).data("parentid"),formData);
      }
    });
    $(document).on("click",".shortlistUser",function(e){
        loaderStart();
         var formData = new FormData();
        formData.append("Action", "SHORTLIST_USER");
        //formData.append("Page",  "PROFILE");
        formData.append("ToUserId", $(this).data("id"));
        formData.append("Name", $(this).data("name"));
        sendRequestDashboard("' . Url::to(['user/user-request']) . '",".requests","SLU",$(this).data("id"),formData);
    });

    $(document).on("click",".a_b_d",function(e){
      Pace.restart();
      //loaderStart();
      var formData = new FormData();
      formData.append("ToUserId", $("#a_b_d_id").val());
      formData.append("Name", $(".to_name").text());
      formData.append("RGNumber", $(".to_rg_number").text());
      formData.append("Action",  $("#a_b_d_type").val());
      //alert("in");return false;
      //sendRequestDashboard("' . Url::to(['user/user-request']) . '",".requests","R_A_D_B",$(this).data("parentid"),formData);
       var type= $(this).data("typemap");

       /*console.log("TUI "+$("#a_b_d_id").val());
       console.log("Name "+$(".to_rg_number").text());
       console.log("RGNumber "+$(".to_rg_number").text());
       console.log("Action "+$("#a_b_d_type").val());
       console.log(formData);return false;*/
      if(type == "map"){
          $("#accept_decline .a_b_d").attr("data-typemap","");
          sendRequestDashboard("' . Url::to(['user/user-request']) . '",".requests","MAP_R_A_D_B",$("#a_b_d_id").val(),formData);
      }else{
        sendRequestDashboard("' . Url::to(['user/user-request']) . '",".requests","R_A_D_B",$(this).data("parentid"),formData);;
      }
    });
    var formDataContact = new FormData();
    formDataContact.set("UserId", ' . Yii::$app->user->identity->id. ');
    $(document).on("click",".phone_number_display",function(e){
    
    formDataContact.set("ToUserId", $(this).data("id"));
    
          loaderStart();
          $(".display_phone_number").html("");
          $(".display_email_number").html("");
         $.ajax({
                        url: "' . Url::to(['user/available-balance']) . '",
                        type: "POST",
                        data: formDataContact,
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data, textStatus, jqXHR) {
                            loaderStop();
                            var DataObject = JSON.parse(data);
                            if(DataObject.STATUS == "S"){
                                   $(".cd-modal-message").html(DataObject.details.available_message);
                                   $(".cd-modal-contact").html(DataObject.details.available_contact);
                                   $("#phone_number_modal").modal();
                            }else if(DataObject.STATUS == "E"){
                                notificationPopup(DataObject.STATUS, DataObject.MESSAGE, DataObject.TITLE);
                            }else{
                                $("#upgrade_modal").modal();
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        loaderStop();
                                notificationPopup(\'ERROR\', \'Something went wrong. Please try again !\', \'Error\');
                        }
         });
    });
    
    $(document).on("click",".contact_display",function(e){
    loaderStart();
         $.ajax({
                        url: "'.Url::to(['user/display-private-details']).'",
                        type: "POST",
                        data: formDataContact,
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data, textStatus, jqXHR) {
                            var DataObject = JSON.parse(data);
                            loaderStop();
                            if(DataObject.STATUS == "S"){
                                   $(".display_email_number").html(DataObject.details.email);
                                   $(".display_phone_number").html(DataObject.details.number);
                                    $(".number_modal").modal();
                                   //#TODO counter plus and maintain the history of view contact member wise and plan wise.
                                    $.ajax({
                                            //url: "' . Url::to(['user/no-of-contact']) . '",
                                            url: "' . Url::to(['user/operation-number']) . '",
                                            type: "POST",
                                            data: formDataContact,
                                            contentType: false,
                                            cache: false,
                                            processData: false,
                                            success: function (data, textStatus, jqXHR) {
                                                var DataObject = JSON.parse(data);
                                                 if(DataObject.STATUS == "E"){
                                                    //notificationPopup(DataObject.STATUS, DataObject.MESSAGE, DataObject.TITLE);
                                                    showNotification(DataObject.STATUS,DataObject.MESSAGE,"P");
                                                }
                                            },
                                            error: function (jqXHR, textStatus, errorThrown) {
                                                    //notificationPopup(\'ERROR\', \'Something went wrong. Please try again !\', \'Error\');
                                                    //loaderStop();
                                            }
                                    });
                                //sendRequest("' . Url::to(['user/user-request']) . '",".requests",formData);
                            }else if(DataObject.STATUS == "E"){
                                notificationPopup(DataObject.STATUS, DataObject.MESSAGE, DataObject.TITLE);
                            }else{
                                $("#upgrade_modal").modal();
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        loaderStop();
                                notificationPopup(\'ERROR\', \'Something went wrong. Please try again !\', \'Error\');
                        }
         });
    });
    
    $(document).on("click",".message_display",function(e){
    loaderStart();
         $.ajax({
                        //url: "send-sms-detail",
                        url: "'.Url::to(['user/send-sms-detail']).'",
                        //url: "send-message",
                        type: "POST",
                        data: formDataContact,
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data, textStatus, jqXHR) {
                            var DataObject = JSON.parse(data);
                            loaderStop();
                            if(DataObject.STATUS == "S"){
                                   $("input:radio[name=\'send-sms\'][id=\'Standard SMS\']").prop("checked", true);
                                   $(".cd-modal-message").html(DataObject.details.available_message);
                                   $(".standard_sms").html(DataObject.details.standard_message);
                                   //$(".personalized_sms").html(DataObject.details.personalized_message);
                                   $(".personalized_sms").val("");
                                    $(".send_sms_modal").modal();
                            }else if(DataObject.STATUS == "E"){
                                notificationPopup(DataObject.STATUS, DataObject.MESSAGE, DataObject.TITLE);
                            }else{
                                $("#upgrade_modal").modal();
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                                notificationPopup(\'ERROR\', \'Something went wrong. Please try again !\', \'Error\');
                        }
         });
    });
    $(document).on("click",".send_sms_btn",function(e){
    var sms_type = $("input[name=\'send-sms\']:checked").val();
    var sms_phone_share = "No";
    if(sms_type == "Personalized"){
        if($("input[name=\'share_number\']").prop("checked") == true){
            //do something
            sms_phone_share = "Yes";
        }
        //var sms_phone_share = $("input[name=\'share_number\']:checked").val();
        sms_content = $(".personalized_sms").val();
        sms_content = sms_content.trim();
        if(sms_content.length == 0){
            showNotification("E","Please Enter Your Personalized SMS","P");
            return false;
        }
    }
    else{
        var sms_content = $(".standard_sms").val();
        sms_phone_share = "No";
    }

    loaderStart();
    formDataContact.set("sms_content",  sms_content);
    formDataContact.set("sms_type",  sms_type);
    formDataContact.set("sms_phone_share",  sms_phone_share);
         $.ajax({
                        //url: "send-message",
                        url: "'.Url::to(['user/send-message']).'",
                        type: "POST",
                        data: formDataContact,
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data, textStatus, jqXHR) {
                            var DataObject = JSON.parse(data);
                            loaderStop();
                            if(DataObject.STATUS == "S"){
                                   //$(".cd-modal-message").html(DataObject.details.available_message);
                                   //$(".standard_sms").html(DataObject.details.standard_message);
                                    //$(".send_sms_modal").modal();
                                    notificationPopup(DataObject.STATUS, DataObject.MESSAGE, DataObject.TITLE);
                            }else if(DataObject.STATUS == "E"){
                                notificationPopup(DataObject.STATUS, DataObject.MESSAGE, DataObject.TITLE);
                            }else{
                                $("#upgrade_modal").modal();
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                                notificationPopup(\'ERROR\', \'Something went wrong. Please try again !\', \'Error\');
                        }
         });
    });
    
');
?>
<script>
    function resetAll() {
        location.reload();
    }
    var currentRequest = null;
    $('#slider-range-age').change(function(e) {
        var setIndex = (this.id == "upperlimit") ? 1 : 0;
        //alert($(this).val());
    });
    $(function() {
        $('.f-section').show();
        setTimeout(function(){ requestSearchFilter(); }, 1000);
    });
    function requestSearchFilter(x='',y='',z='') {
        loaderStart("container-block");
        var CommunityIds = [];//$('#iCommunity_ID').val();
        var AgeFrom = $('#age').val();
        var AgeTo = $('#age1').val();
        var HeightFrom = $('#height').val();
        var HeightTo = $('#height1').val();
        var WeightFrom = $('#weight').val(); //TODO
        var WeightTo = $('#weight1').val(); //TODO
        var TagsIds = [];
        var MaritalStatusID = [];
        var manglikStatus = '';
        var GotraIds = [];
        var educationFieldIds = [];
        var occupationsIds = [];
        var PhysicalStatusID = [];
        var SkinToneID = [];
        var BodyTypeID = [];
        var SmokeTypeID = [];
        var DrinkTypeID = [];
        var DietID = [];
        var profile_id = '';
        var by_keywords = '';

        var Page = (x > 0) ? x : 0;
        if(y!=''){
            profile_id = y;
        }else{
            if(z!=''){
                by_keywords = $('#search_by_keyword').val();
            }
            $('.community_ids  option:selected').each(function() {
                CommunityIds.push($(this).val());
            });
            $('.tags_ids  option:selected').each(function() {
                TagsIds.push($(this).val());
            });
            $.each($("input[name='Marital_Status']:checked"), function(){
                MaritalStatusID.push($(this).val());
            });
            if($('input[name="manglik_status"]:checked').val()){
                manglikStatus = $('input[name="manglik_status"]:checked').val();
            }
            $('.gotra_ids option:selected').each(function() {
                GotraIds.push($(this).val());
            });
            $('.education_field option:selected').each(function() {
                educationFieldIds.push($(this).val());
            });
            $('.occupations option:selected').each(function() {
                occupationsIds.push($(this).val());
            });
            $.each($("input[name='physical_status']:checked"), function(){
                PhysicalStatusID.push($(this).val());
            });
            $.each($("input[name='skin_tone']:checked"), function(){
                SkinToneID.push($(this).val());
            });
            $.each($("input[name='body_type']:checked"), function(){
                BodyTypeID.push($(this).val());
            });
            $.each($("input[name='smoke_type']:checked"), function(){
                SmokeTypeID.push($(this).val());
            });
            $.each($("input[name='drink_type']:checked"), function(){
                DrinkTypeID.push($(this).val());
            });
            $.each($("input[name='diet']:checked"), function(){
                DietID.push($(this).val());
            });
        }
        var csrfToken = $('meta[name="csrf-token"]').attr("content");
        //$(".add_data_ajax").css("display", "none");
        var dataToSet = {
            _csrf: csrfToken,
            CommunityIds: CommunityIds,
            AgeTo : AgeTo,
            AgeFrom : AgeFrom,
            HeightFrom : HeightFrom,
            HeightTo : HeightTo,
            WeightFrom : WeightFrom,
            WeightTo : WeightTo,
            MaritalStatusID : MaritalStatusID,
            ManglikStatus : manglikStatus,
            GotraIds : GotraIds,
            EducationFieldIds : educationFieldIds,
            OccupationsIds : occupationsIds,
            PhysicalStatusID : PhysicalStatusID,
            SkinToneID : SkinToneID,
            BodyTypeID : BodyTypeID,
            SmokeTypeID : SmokeTypeID,
            DrinkTypeID : DrinkTypeID,
            DietID : DietID,
            Page: Page,
            Profile_id: profile_id,
            TagsIds: TagsIds,
            search_by_keyword: by_keywords,
        };
//console.log(dataToSet);return false;
        currentRequest =  $.ajax({
            type: "POST",
            url: "<?php echo Yii::$app->getUrlManager()->createUrl('search/ajax-basic-search'); ?>",
            data: dataToSet,
            beforeSend : function()    {
                if(currentRequest != null) {
                    currentRequest.abort();
                }
                loaderStart("container-block");
            },
            success: function (dataRecord) {
                loaderStop();
                var data = JSON.parse(dataRecord);
                $(".total_records").html(data.TotalRecords);
                $(".ajax_class").css("display", "none");
                $(".add_data_ajax").css("display", "block");
                $(".add_data_ajax").html(data.RecordData);
                $(".state_data_ajax").html(data.stateDiv);
                $(".paginationData").html(data.PaginationData);
                $(window).scrollTop(0);
                loaderStop("container-block");
                markerData = { "events": data.map_records_array };
                //console.log(data.map_records_array);
                webJS.gmap.googleMapsInitCallback();
                /*markerData = {
                    "events": [

                        {
                            "city_id": "ahmedabad",
                            "city_name": "Ahmedabad",
                            "lat": "23.0225",
                            "lng": "72.5714",
                            "place_img":"images/image11.jpg",
                            "place_name": "Vikrant Parmar",
                            "age": "20",
                            "user_height": "5'7",
                            "community": "Gujarati",
                            "occupation": "Web Developer",
                        },
                        {
                            "city_id": "kolkata",
                            "city_name": "Kolkata",
                            "lat": "22.5726",
                            "lng": "88.3639",
                            "place_img":"images/image1.jpg",
                            "place_name": "Swapnil 11Kanherkar",
                            "age": "30",
                            "user_height": "5'7",
                            "community": "Maratha",
                            "occupation": "Web Designer",
                        },
                        {
                            "city_id": "kolkata",
                            "city_name": "Kolkata",
                            "lat": "22.5726",
                            "lng": "88.3639",
                            "place_img":"images/image1.jpg",
                            "place_name": "Swapnil 11Kanherkar",
                            "age": "30",
                            "user_height": "5'7",
                            "community": "Maratha",
                            "occupation": "Web Designer",
                        },

                    ],
                };*/
                //console.log(markerData);
            },
            error: function (exception) {
                //console.log(exception);
                loaderStop("container-block");
                notificationPopup('ERROR', exception.responseText, 'Error');
            }
        })
    }
    function load_more(x){
        //$('#pagination_page').val(x);
        requestSearchFilter(x);
    }
    function search_by_profile_id(){
        var search_by_profile = $('.search_by_profile').val();
        if(search_by_profile !=''){
            requestSearchFilter(0,search_by_profile);
        }else{
            showNotification("E", "Please enter profile id");
        }
        return false;
        //requestSearchFilter(x);
    }
    function change_view(x){
        requestSearchFilter();
        if(x =='map'){
            $('.list_strucure').hide();
            $('.map_list').show();
            $('#list_btn').removeClass('active');
            $('#map_btn').addClass('active');
        }else{
            $('.list_strucure').show();
            $('.map_list').hide();
            $('#map_btn').removeClass('active');
            $('#list_btn').addClass('active');
        }
    }
</script>



<!-- Custom Scrollbar  -->
<?php
#$this->registerCssFile(Yii::$app->request->baseUrl . '/css/jquery.mCustomScrollbar.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]);
#$this->registerJsFile(Yii::$app->request->baseUrl . '/js/jquery.mCustomScrollbar.concat.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<link href="<?php echo Yii::$app->request->baseUrl . '/css/jquery.mCustomScrollbar.css'?>" rel="stylesheet">
<script src="<?php echo Yii::$app->request->baseUrl . '/js/jquery.mCustomScrollbar.concat.min.js'?>"></script>
<!--<link href="http://216.10.253.222/kande_pohe/css/jquery.mCustomScrollbar.css" rel="stylesheet">
<script src="http://216.10.253.222/kande_pohe/js/jquery.mCustomScrollbar.concat.min.js"></script>-->
<script type="text/javascript">
    if(screen.width > 768) {
        $('.fullscreen-element').each(function () {
          "use strict";
          $(this).mCustomScrollbar({
            autoHideScrollbar: false,
            scrollbarPosition: 'outside'
          });
        });
    }
    function send_interest_map(id,name,rgnumber){
        $("#sendInterest .send_request").attr("data-type","map");
        $("#to_name").html(name);
        $("#to_rg_number").html(rgnumber);
        $("#send_request_id").val(id);
        $("#sendInterest .send_request").attr("data-id",id);
        $("#sendInterest").modal();
    }
    function accept_decline_map(id,name,rgnumber,type){
        $("#accept_decline .a_b_d").attr("data-typemap","map");
        /*$("#sendInterest .send_request").attr("data-type","map");
        $("#to_name").html(name);
        $("#to_rg_number").html(rgnumber);
        $("#send_request_id").val(id);
        $("#sendInterest .send_request").attr("data-id",id);
        $("#sendInterest").modal();*/
        $(".to_name").html(name);
        $(".to_rg_number").html(rgnumber);
        $("#a_b_d_id").val(id);
        $("#a_b_d_type").val(type);
        $(".main_title_popup").html(type);
        if ($(this).data("type") == 'Accept Interest') {
            $(".main_msg_popup").html(AcceptInterest);
        } else if ($(this).data("type") == 'Decline Interest') {
            $(".main_msg_popup").html(DeclineInterest);
        } else {
            $(".main_msg_popup").html(CancelInterest);
        }
        $("#accept_decline").modal();
        if ($(this).closest('p').attr('class') === undefined) {
            //$(".a_b_d").attr("data-parentid", $(this).closest('li').attr('class'));
            //$("#a_b_d_parentid").val($(this).closest('li').attr('class'));
        } else {
            //$(".a_b_d").attr("data-parentid", $(this).closest('p').attr('class'));
            //$("#a_b_d_parentid").val($(this).closest('p').attr('class'));
        }
    }
//    search_by_keyword

</script>
<style>
   .filter01 .multiselect-wrapper, .filter01 .form-cont div.cs-skin-border
    {
        height: auto;
    }
   /*
       .mCSB_container  {
           overflow: unset;
       }
       .bootstrap-tagsinput input{
           width: 100%;
       }*/

</style>

<script type="text/javascript">
    $('.filters-mob>a').click(function() {
        $('.f-section').addClass('show');
    });
    $('.close-tab').click(function() {
        $('.f-section').removeClass('show');
    });
</script>