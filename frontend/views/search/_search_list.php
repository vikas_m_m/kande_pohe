<?php
use yii\helpers\Html;
use common\components\CommonHelper;
?>
<?php
if($model['total_records'] == 0){ ?>
<div class="white-section listing1 border-sharp mrg-tp-10">
    <div class="row mrg-tp-10">
        <div class="col-md-12">
            <div class="notice kp_info">
                <p><?= @Yii::$app->params['noRecordsFoundInSearchList'] ?></p>
            </div>
            <div class="clearfix"></div>
<span class="pull-right"><a href='<?=Yii::$app->homeUrl ?>' class="text-right">Back To
        Home Page<i class="fa fa-angle-right"></i></a></span>
        </div>
    </div>
</div>
<?php }else{
    $Model = $model['records'];
    $Photos = $model['photos'];
    $Id = $model['Id'];
    foreach ($Model as $SK => $SV) {
    ?>
    <div class="white-section <?= ($SK == 0) ? 'listing1' : ''; ?> border-sharp mrg-tp-10">
        <div class="row">
            <div class="col-sm-3">
                <div class="prof-pic">
                    <div class="drop-effect"></div>
                    <div class="slider">
                        <div id="carousel-example-generic_<?= $SK ?>"
                             class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <?php
                                if (is_array($Photos[$SV->id])) {
                                    foreach ($Photos[$SV->id] as $K => $V) {
                                        $SELECTED = '';
                                        $Photo = Yii::$app->params['thumbnailPrefix'] . '200_' . $V->File_Name;
                                        $Yes = 'No';
                                        if ($V['Is_Profile_Photo'] == 'YES') {
                                            $SELECTED = "active";
                                            $Photo = '200' . $SV->propic;
                                            $Yes = 'Yes';
                                        } ?>
                                        <div
                                            class="item <?= ($K == 0) ? 'active' : ''; ?> kp_pic_dis_dwn">
                                            <?= Html::img(CommonHelper::getPhotos('USER', $SV->id, $Photo, 200, '', $Yes, CommonHelper::getVisiblePhoto($SV->id, $V['eStatus'])), ['width' => '205', 'height' => '205', 'alt' => 'Profile', 'class' => 'img-responsive']); ?>
                                        </div>
                                    <?php
                                    }
                                } else { ?>
                                    <div class="item active kp_pic_dis_dwn">
                                        <?= Html::img(CommonHelper::getPhotos('USER', $SV->id, $Photos[$SV->id], 120), ['width' => '205', 'height' => '205', 'alt' => 'Profile', 'class' => 'img-responsive item']); ?>
                                    </div>
                                <?php }
                                ?>
                            </div>
                            <!-- Controls -->
                            <?php if (is_array($Photos[$SV->id])) {
                                if (count($Photos[$SV->id]) > 1) { ?>
                                    <a class="left carousel-control"
                                       href="#carousel-example-generic_<?= $SK ?>"
                                       data-slide="prev"> <span
                                            class="glyphicon glyphicon-chevron-left"></span>
                                    </a> <a class="right carousel-control"
                                            href="#carousel-example-generic_<?= $SK ?>"
                                            data-slide="next">
                                                                    <span
                                                                        class="glyphicon glyphicon-chevron-right"></span>
                                    </a>
                                <?php }
                            } ?>
                        </div>
                    </div>
                </div>
                <?php if (is_array($Photos[$SV->id])) { ?>
                    <!-- <p class="text-right"><a href="#" data-toggle="modal"
                                              data-target="#photo">View
                             Album <i class="fa fa-angle-right"></i></a></p>-->
                <?php } ?>
            </div>
            <div class="col-sm-9 search_listing_div">
                <div class="name-panel">
                    <h2 class="nameplate">
                        <?php
                        if (!Yii::$app->user->isGuest) { ?>
                            <a href="<?= Yii::$app->homeUrl ?>user/profile?uk=<?= $SV->Registration_Number ?>&source=profile_viewed_by"
                               class="name"
                               title="<?= $SV->Registration_Number ?>"><?= $SV->FullName; ?></a>
                        <?php } else { ?>
                            <?= $SV->FullName; ?>
                        <?php } ?>
                        <span class="font-light">(<?= $SV->Registration_Number ?>)</span>
                        <?php
                        #$USER_FACEBOOK = \common\models\User::weightedCheck(11);
                        $USER_PHONE = \common\models\User::weightedCheck(8);
                        $USER_EMAIL = \common\models\User::weightedCheck(9);
                        $USER_APPROVED = \common\models\User::weightedCheck(10);
                        if ($USER_PHONE && $USER_EMAIL && $USER_APPROVED) {
                            ?>
                            <span class="premium"></span>
                        <?php } ?>
                    </h2>
                    <?php $USER_PHONE = \common\models\User::weightedCheck(8); ?>
                    <p>Profile created for <?= $SV->Profile_created_for; ?> |
                        Last
                        online <?= (CommonHelper::get_last_login_time_diff($SV->LastLoginTime) > 30) ? 'more than a month ago' : CommonHelper::DateTime($SV->LastLoginTime, 7); ?>
                        |
                                                                <span class="pager-icon">
                                                    <a href="javascript:void(0)">
                                                        <i class="fa fa-mobile"></i>
                                                      <span
                                                          class="badge <?php if ($SV->ePhoneVerifiedStatus != 'Yes') { ?>badge1<?php } ?>">
                                                          <i class="fa fa-check"></i>
                                                      </span>
                                                    </a>
                                                            </span>
                    </p>
                </div>
                <dl class="dl-horizontal mrg-tp-10">
                    <dt>Personal Details</dt>
                    <dd><?= CommonHelper::getAge($SV->DOB); ?>
                        yrs, <?= CommonHelper::setInputVal($SV->height->vName, 'text'); ?>
                        <?= ($SV->RaashiId > 0) ? ", " . CommonHelper::setInputVal($SV->raashiName->Name, 'text') : ''; ?>
                    </dd>
                    <dt>Marital Status</dt>
                    <dd><?= CommonHelper::setInputVal($SV->maritalStatusName->vName, 'text') ?></dd>
                    <dt>Religion Community</dt>
                    <dd><?= CommonHelper::setInputVal($SV->religionName->vName, 'text') . ',' . CommonHelper::setInputVal($SV->communityName->vName, 'text') ?></dd>
                    <dt>Education</dt>
                    <dd><?= CommonHelper::setInputVal($SV->educationLevelName->vEducationLevelName, 'text') ?></dd>
                    <dt>Profession</dt>
                    <dd><?= CommonHelper::setInputVal($SV->workingAsName->vWorkingAsName, 'text') ?></dd>
                    <dt>Current Location</dt>
                    <dd><?= CommonHelper::setInputVal($SV->cityName->vCityName, 'text') . ', ' . CommonHelper::setInputVal($SV->countryName->vCountryName, 'text') ?></dd>
                </dl>
            </div>
        </div>
        <?php
        if (!Yii::$app->user->isGuest) {
            if ($SV->Gender != Yii::$app->user->identity->Gender) {
                // CommonHelper::pr($SV);
                ?>
                <div class="clearfix gray-bg mob-bg">
                    
                        <div class="profile-control-vertical">
                            <ul class="list-unstyled pull-right">
                                <?php
                                $Value = \common\models\UserRequestOp::checkSendInterest(Yii::$app->user->identity->id, $SV->id);
                                $Shortlisted = 0;
                                if (Yii::$app->user->identity->id == $Value->from_user_id && $Value->short_list_status_from_to == 'Yes') {
                                    $Shortlisted = 1;
                                }
                                if (Yii::$app->user->identity->id == $Value->to_user_id && $Value->short_list_status_to_from == 'Yes') {
                                    $Shortlisted = 1;
                                }
                                ?>
                                <li class="sl__<?= $SV->id ?>">
                                    <?php if ($Shortlisted) { ?>
                                        <a href="javascript:void(0)">Shortlisted
                                            <i
                                                class="fa fa-list-ul"></i></a>
                                    <?php } else { ?>
                                        <a href="javascript:void(0)"
                                           class="short_list_<?= $SV->id ?> shortlistUser"
                                           data-id="<?= $SV->id ?>"
                                           data-name="<?= $SV->fullName ?>">Shortlist
                                            <i class="fa fa-list-ul"></i></a>
                                    <?php } ?>

                                </li>
                                <li class="s__<?= $SV->id ?>">
                                    <?php
                                    #CommonHelper::pr(\common\models\UserRequestOp::checkSendInterest(Yii::$app->user->identity->id, $ValueRM->id));
                                    if (count($Value)) {
                                        if ($Id == $Value->from_user_id && $Value->profile_viewed_from_to == 'Yes') {
                                            $ViewerId = $Value->to_user_id;
                                        } else {
                                            $ViewerId = $Value->from_user_id;
                                        }
                                    } else {
                                        $ViewerId = $Value->id;
                                    }
                                    $UserInfoModel = \common\models\User::getUserInfroamtion($ViewerId);

                                    if (count($Value) == 0 || ($Id == $Value->from_user_id && $Value->send_request_status_from_to == 'No' && $Value->send_request_status_to_from == 'No') || ($Id == $Value->to_user_id && $Value->send_request_status_to_from == 'No' && $Value->send_request_status_from_to == 'No')) { ?>
                                        <a href="javascript:void(0)"
                                           class=" sendinterestpopup"
                                           role="button"
                                           data-target="#sendInterest"
                                           data-toggle="modal"
                                           data-id="<?= $SV->id ?>"
                                           data-name="<?= $SV->fullName ?>"
                                           data-rgnumber="<?= $SV->Registration_Number ?>">Send
                                            Interest
                                            <i class="fa fa-heart-o"></i>
                                        </a>
                                    <?php } else if (($Id == $Value->from_user_id && $Value->send_request_status_from_to == 'Yes' && $Value->send_request_status_to_from != 'Yes') || ($Id == $Value->to_user_id && $Value->send_request_status_to_from == 'Yes' && $Value->send_request_status_from_to != 'Yes')) { ?>
                                        <a href="javascript:void(0)"
                                           class=" ci accept_decline " role="button"
                                           data-target="#accept_decline"
                                           data-toggle="modal"
                                           data-type="Cancel Interest"
                                           data-id="<?= $SV->id ?>"
                                           data-name="<?= $SV->fullName ?>"
                                           data-rgnumber="<?= $SV->Registration_Number ?>">Cancel Interest
                                            <i class="fa fa-close"></i> </a>

                                    <?php } else if (($Id == $Value->to_user_id && $Value->send_request_status_from_to == 'Yes' && $Value->send_request_status_to_from != 'Yes') || ($Id == $Value->from_user_id && $Value->send_request_status_to_from == 'Yes' && $Value->send_request_status_from_to != 'Yes')) {
                                        ?>
                                        <a href="javascript:void(0)"
                                           class=" accept_decline adbtn"
                                           role="button"
                                           data-target="#accept_decline"
                                           data-toggle="modal"
                                           data-id="<?= $UserInfoModel->id ?>"
                                           data-name="<?= $UserInfoModel->fullName ?>"
                                           data-rgnumber="<?= $UserInfoModel->Registration_Number ?>"
                                           data-type="Accept Interest">
                                            Accept
                                            <i class="fa fa-check"></i>
                                        </a>
                                        <a href="javascript:void(0)"
                                           class="accept_decline adbtn"
                                           role="button"
                                           data-target="#accept_decline"
                                           data-toggle="modal"
                                           data-id="<?= $UserInfoModel->id ?>"
                                           data-name="<?= $UserInfoModel->fullName ?>"
                                           data-rgnumber="<?= $UserInfoModel->Registration_Number ?>"
                                           data-type="Decline Interest"
                                            >
                                            Decline
                                            <i class="fa fa-close"></i> </a>
                                    <?php } else if ($Value->send_request_status_from_to == 'Accepted' || $Value->send_request_status_to_from == 'Accepted') {
                                        ?>
                                        <a href="javascript:void(0)" class=""
                                           role="button"
                                           data-target="#" data-toggle="modal"
                                           data-id="<?= $UserInfoModel->id ?>"
                                           data-name="<?= $UserInfoModel->fullName ?>"
                                           data-rgnumber="<?= $UserInfoModel->Registration_Number ?>"
                                           data-type="Connected">Connected
                                            <i class="fa fa-heart"></i> </a>
                                    <?php } else if ($Value->send_request_status_from_to == 'Rejected' || $Value->send_request_status_to_from == 'Rejected') {
                                        ?>
                                        <a href="javascript:void(0)" class=" "
                                           role="button"
                                           data-target="#" data-toggle="modal"
                                           data-id="<?= $UserInfoModel->id ?>"
                                           data-name="<?= $UserInfoModel->fullName ?>"
                                           data-rgnumber="<?= $UserInfoModel->Registration_Number ?>"
                                           data-type="Connected">Rejected <i
                                                class="fa fa-close"></i>
                                        </a>

                                    <?php } else { ?>
                                        <a href="javascript:void(0)"
                                           class="btn btn-link isent"
                                           role="button">Interest
                                            Sent <i class="fa fa-heart"></i></a>
                                    <?php } ?>
                                    <!--
                                                                    <a href="javascript:void(0)" class="isent"
                                                                       role="button">Interest Sent <i
                                                                            class="fa fa-heart"></i></a>
                                                                <?php /*} else { */ ?>
                                                                    <a href="javascript:void(0)"
                                                                       class="sendinterestpopup"
                                                                       role="button"
                                                                       data-target="#sendInterest"
                                                                       data-toggle="modal"
                                                                       data-id="<? /*= $SV->id */ ?>"
                                                                       data-name="<? /*= $SV->FullName */ ?>"
                                                                       data-rgnumber="<? /*= $SV->Registration_Number */ ?>">Send
                                                                        Interest
                                                                        <i class="fa fa-heart-o"></i>
                                                                    </a>
                                                                --><?php /*} */ ?>
                                </li>

                                <li>
                                    <!--<a href="#" data-toggle="modal"
                                       class="send_email"
                                       <?php /*if (Yii::$app->user->isGuest) { */?>data-target="#sendMail"<?php /*} else { */?> data-id="<?/*= $SV->id */?>" <?php /*} */?>>Send
                                        Email <i class="fa fa-envelope-o"></i>
                                    </a>-->
                                    <a  href="javascript:void(0)" class="btn phone_number_display bg-secondary" data-id="<?= $SV->id ?>">&nbsp;View Contact Details</a>
                                </li>

                            </ul>
                        </div>
                    
                </div>

            <?php } ?>

        <?php } else { ?>
            <div class="row gray-bg">
                <div class="col-sm-12">
                    <div class="profile-control-vertical">
                        <ul class="list-unstyled pull-right">
                            <li>
                                <a href="javascript:void(0)" class="send_email"
                                   role="button"
                                   data-target="#sendMail" data-toggle="modal"
                                    >Shortlist <i class="fa fa-list-ul"></i></a>
                            </li>
                            <li class="s__<?= $SV->id ?>">
                                <a href="javascript:void(0)" class="send_email"
                                   role="button"
                                   data-target="#sendMail" data-toggle="modal"
                                    >Send Interest <i class="fa fa-heart-o"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" data-toggle="modal"
                                   class="send_email"
                                   data-target="#sendMail">Send Email <i
                                        class="fa fa-envelope-o"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
<?php } ?>

<?php } ?>
