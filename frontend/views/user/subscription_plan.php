<link href='<?= Yii::$app->homeUrl ?>css/package.css' rel='stylesheet' type='text/css'>
<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\components\CommonHelper;
use yii\helpers\ArrayHelper;

$color_array = array('PACKAGE_FREE' =>'', 'PACKAGE_SILVER' =>'green', 'PACKAGE_GOLD' =>'blue', 'PACKAGE_PLATINUM' =>'red');
$icons_array = array('PACKAGE_FREE' =>'fa fa-bullhorn','PACKAGE_SILVER'=>'fa fa-star-half-o','PACKAGE_GOLD'=>'fa fa-trophy','PACKAGE_PLATINUM' =>'fa fa-diamond');
?>
<main>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="white-section my-4">
                    <h3>Select your subscription plan</h3>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <?php if (Yii::$app->session->hasFlash('error')): ?>
                                <div class="notice kp_error"><h4>Error!</h4>
                                    <p><?= Yii::$app->session->getFlash('error') ?></p>
                                </div>
                            <?php endif; ?>
                            <?php /*if($subscription_detail != NULL){ */?><!--
                                <div class="alert alert-kp fade in">
                                    <h4>Note:</h4>
                                    <p>Change this and that and try again. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum.</p>
                                    <p>
                                        <?/*= html::a('<button type="button" class="btn btn-warning">Contact Us</button>', ['site/contact-us']) */?>
                                    </p>
                                </div>
                            <?php /*}else{ */?>
                                <div class="alert alert-kp fade in">
                                    <h4>Note:</h4>
                                    <p>Change this and that and try again. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum.</p>
                                    <p>If your existing plan have remaining number of contacts and Personalized Messaging.</p>
                                    <p>
                                        <?/*= html::a('<button type="button" class="btn btn-warning">Contact Us</button>', ['site/contact-us']) */?>
                                    </p>
                                </div>
                            --><?php /*}*/?>
                        </div>
                    </div>
                    <!-- PACKAGES -->
                    <div class="packages mt-5 text-center">
                        <div class="row m-0 mx-auto">
                            <div class="col-md-4 p-0 hidden-sm hidden-xs pt-width">
                                <div class="pricingTable11 first-column first-package-column">
                                    <div class="pricingTable-header ">
                                        <i class="fa fa-bullhorn"></i>
                                        <div class="price-value"> <i class="fa fa-inr"></i>0.0 <span class="month"></span> </div>
                                    </div>
                                    <h3 class="heading" >PLAN</h3>
                                    <div class="pricing-content">
                                        <ul class="price-description">
                                            <li>Duration </li>
                                            <li>Number of Contacts</li>
                                            <li>Personalized Messaging </li>
                                            <li>Privacy Settings</li>
                                            <li>Validity of Package</li>
                                            <li>Customer Care Support </li>
                                        </ul>
                                    </div>
                                    <div class="pricingTable-signup">
                                        <a href="" title="Sign up Free" >Register Free</a>
                                    </div>
                                </div>
                            </div>
                            <?php if(count(@$subscriptions_data)>0){
                                foreach($subscriptions_data as $k=>$v){ ?>
                                    <div class="col-md-2 col-sm-3 pv-width">
                                    <div class="pricingTable11 <?= $color_array[$v->short_name] ?>">
                                    <div class="pricingTable-header">
                                        <i class="<?= $icons_array[$v->short_name] ?>"></i>
                                        <div class="price-value"> <i class="fa fa-inr"></i><?= number_format($v->subscriptions_price,2)?> <span class="month"></span> </div>
                                    </div>
                                    <h3 class="heading"><?= stripslashes($v->subscriptions_name)?></h3>
                                    <div class="pricing-content">
                                        <ul>
                                            <li data-title="Duration"><?= ($v->profile_duration)?>Days</li>
                                            <li data-title="Number of Contacts"><?= ($v->no_of_contacts)?></li>
                                            <li data-title="Personalized Messaging">
                                                <?php if($v->no_of_pm == 0) { ?>
                                                    <i class="fa fa-times  subscription-no"></i>
                                                <?php }else{
                                                    echo $v->no_of_pm ;
                                                } ?>
                                            </li>
                                            <li data-title="Privacy Settings">
                                                <?php if($v->privacy_settings == 'No') { ?>
                                                    <i class="fa fa-times  subscription-no"></i>
                                                <?php }else{ ?>
                                                    <i class="fa fa-check subscription-yes"></i>
                                                <?php } ?>
                                            </li>
                                            <li data-title="Validity of Package"><?= ($v->validity_of_package)?>Days</li>
                                            <li data-title="Customer Care Support">
                                                <?php if($v->customer_care_support == 'No') { ?>
                                                    <i class="fa fa-times  subscription-no"></i>
                                                <?php }else{ ?>
                                                    <i class="fa fa-check subscription-yes"></i>
                                                <?php } ?>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="pricingTable-signup">
                                    <?php if($v->short_name == 'PACKAGE_FREE'){ ?>
                                        <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ."/user/subscription-free-plan?type=".$v->short_name ?>" title="Select" >Select</a>
                                    <?php }else{ ?>
                                        <?php
                                        $btn_text  = 'Select';
                                        if(@$subscription_detail != NULL){

                                            $btn_text  = 'Select';
                                        }  ?>
                                    <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() . Yii::$app->params['subscriptionPlanProcess']."?type=".$v->short_name ?>"
                                       title="Select" > <?= $btn_text ?></a>
                                <?php }?>
                                </div>
                                </div>
                                </div>
                            <?php } }  ?>
                            <!--<div class="col-md-2 col-sm-3 pv-width">
                                <div class="pricingTable11 ">
                                    <div class="pricingTable-header">
                                        <i class="fa fa-bullhorn"></i>
                                        <div class="price-value"> <i class="fa fa-inr"></i>0.0 <span class="month"></span> </div>
                                    </div>
                                    <h3 class="heading">FREE</h3>
                                    <div class="pricing-content">
                                        <ul>
                                            <li data-title="Duration">365Days</li>
                                            <li data-title="Number of Contacts">0</li>
                                            <li data-title="Personalized Messaging"><i class="fa fa-times  subscription-no"></i></li>
                                            <li data-title="Privacy Settings"><i class="fa fa-times subscription-no"></i></li>
                                            <li data-title="Validity of Package">0Days</li>
                                            <li data-title="Customer Care Support"><i class="fa fa-times subscription-no"></i></li>
                                        </ul>
                                    </div>
                                    <div class="pricingTable-signup">
                                        <a href=""
                                           title="Sign up Free" >Register Free</a>
                                    </div>
                                </div>
                            </div>-->
                            <!--<div class="col-md-2 col-sm-3 pv-width">
                                <div class="pricingTable11 green">
                                    <div class="pricingTable-header">
                                        <i class="fa fa-star-half-o"></i>
                                        <div class="price-value"> <i class="fa fa-inr"></i>1,200.00 <span class="month"></span> </div>
                                    </div>
                                    <h3 class="heading">SILVER</h3>
                                    <div class="pricing-content">
                                        <ul>
                                            <li data-title="Duration">365Days</li>
                                            <li data-title="Number of Contacts">15</li>
                                            <li data-title="Personalized Messaging">15</li>
                                            <li data-title="Privacy Settings"><i class="fa fa-check subscription-yes"></i></li>
                                            <li data-title="Validity of Package">90Days</li>
                                            <li data-title="Customer Care Support"><i class="fa fa-check subscription-yes"></i></li>
                                        </ul>
                                    </div>
                                    <div class="pricingTable-signup">
                                        <a href=""
                                           title="Sign up Free" >Register Free</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-3 pv-width">
                                <div class="pricingTable11 red">
                                    <div class="pricingTable-header">
                                        <i class="fa fa-diamond"></i>
                                        <div class="price-value"> <i class="fa fa-inr"></i>2,400.00 <span class="month"></span> </div>
                                    </div>
                                    <h3 class="heading">PLATINUM</h3>
                                    <div class="pricing-content">
                                        <ul>
                                            <li data-title="Duration">365Days</li>
                                            <li data-title="Number of Contacts">60</li>
                                            <li data-title="Personalized Messaging">60</li>
                                            <li data-title="Privacy Settings"><i class="fa fa-check subscription-yes"></i></li>
                                            <li data-title="Validity of Package">365Days</li>
                                            <li data-title="Customer Care Support"><i class="fa fa-check subscription-yes"></i></li>
                                        </ul>
                                    </div>
                                    <div class="pricingTable-signup">
                                        <a href=""
                                           title="Sign up Free" >Register Free</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-3  pv-width">
                                <div class="pricingTable11 blue">
                                    <div class="pricingTable-header">
                                        <i class="fa fa-trophy"></i>
                                        <div class="price-value"> <i class="fa fa-inr"></i>1,800.00 <span class="month"></span> </div>
                                    </div>
                                    <h3 class="heading">GOLD</h3>
                                    <div class="pricing-content">
                                        <ul>
                                            <li data-title="Duration">365Days</li>
                                            <li data-title="Number of Contacts">60</li>
                                            <li data-title="Personalized Messaging">60</li>
                                            <li data-title="Privacy Settings"><i class="fa fa-check subscription-yes"></i></li>
                                            <li data-title="Validity of Package">180Days</li>
                                            <li data-title="Customer Care Support"><i class="fa fa-check subscription-yes"></i></li>
                                        </ul>
                                    </div>
                                    <div class="pricingTable-signup">
                                        <a href=""
                                           title="Sign up Free" >Register Free</a>
                                    </div>
                                </div>
                            </div>-->
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    </div>
</main>

