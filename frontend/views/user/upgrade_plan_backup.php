<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\components\CommonHelper;
use yii\helpers\ArrayHelper;

$color_array = array('PACKAGE_FREE' =>'', 'PACKAGE_SILVER' =>'green', 'PACKAGE_GOLD' =>'blue', 'PACKAGE_PLATINUM' =>'red');
$icons_array = array('PACKAGE_FREE' =>'fa fa-bullhorn','PACKAGE_SILVER'=>'fa fa-star-half-o','PACKAGE_GOLD'=>'fa fa-trophy','PACKAGE_PLATINUM' =>'fa fa-diamond');
?>
<main>
    <div class="container-fluid">
        <div class="row no-gutter bg-dark">
            <div class="no-gutter">
                <div class="col-md-12 col-sm-12">
                    <div class="white-section mrg-tp-20 mrg-bt-10">
                        <h3>Upgrade/Change Your Plan</h3>
                        <div class="demo11">
                            <div class="container">
                                <!--<div class="notice kp_success"><p>Information Not Available.</p></div>-->
                                <?php if (Yii::$app->session->hasFlash('error')): ?>
                                    <div class="notice kp_error"><h4>Error!</h4>
                                        <p><?= Yii::$app->session->getFlash('error') ?></p>
                                    </div>
                                <?php endif; ?>
                                <?php if($subscription_detail != NULL){ ?>
                                    <div class="alert alert-kp fade in">
                                        <h4>Note:</h4>
                                        <p>Change this and that and try again. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum.</p>
                                        <p>
                                            <?= html::a('<button type="button" class="btn btn-warning">Contact Us</button>', ['site/contact-us']) ?>
                                        </p>
                                    </div>
                                <?php }else{ ?>
                                    <div class="alert alert-kp fade in">
                                        <h4>Note:</h4>
                                        <p>Change this and that and try again. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum.</p>
                                        <p>If your existing plan have remaining number of contacts and Personalized Messaging.</p>
                                        <p>
                                            <?= html::a('<button type="button" class="btn btn-warning">Contact Us</button>', ['site/contact-us']) ?>
                                        </p>
                                    </div>
                                <?php }?>
                                <div class="row1">
                                    <?php if(@$subscriptions_data != NULL){
                                        foreach($subscriptions_data as $k=>$v){
                                            if($v->short_name != 'PACKAGE_FREE'){
                                                ?>
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="pricingTable11 <?= $color_array[$v->short_name] ?>">
                                                        <div class="pricingTable-header">
                                                            <i class="<?= $icons_array[$v->short_name] ?>"></i>
                                                            <div class="price-value"> <i class="fa fa-inr"></i><?= number_format($v->subscriptions_price,2)?> <span class="month"></span> </div>
                                                        </div>
                                                        <h3 class="heading"><?= stripslashes($v->subscriptions_name)?></h3>
                                                        <div class="pricing-content">
                                                            <ul>
                                                                <li><b><?= ($v->profile_duration)?>Days</b> Duration of Profile on Website</li>
                                                                <li><b><?= ($v->no_of_contacts)?></b> No of Contacts</li>
                                                                <li><b><?= ($v->no_of_pm)?></b> Personalized Messaging </li>
                                                                <li><b><?= ($v->privacy_settings)?></b> Privacy Settings </li>
                                                                <li><b><?= ($v->validity_of_package)?>Days</b> Validity of Package</li>
                                                                <li><b><?= ($v->customer_care_support)?></b> Customer Care Support  </li>
                                                            </ul>
                                                        </div>
                                                        <div class="pricingTable-signup">
                                                            <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() . Yii::$app->params['upgradeProcess']."?type=".$v->short_name ?>"
                                                               title="Upgrade No" >Upgrade Now</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }
                                        }
                                    } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

