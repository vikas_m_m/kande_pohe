<?php
use common\components\CommonHelper;
$color_array = array('PACKAGE_FREE'=>'timeline-yellow','PACKAGE_SILVER'=>'timeline-green', 'PACKAGE_GOLD'=>'timeline-blue','PACKAGE_PLATINUM'=>'timeline-red');
$color_text_array = array('PACKAGE_FREE'=>'timeline6-yellow','PACKAGE_SILVER'=>'timeline6-green', 'PACKAGE_GOLD'=>'timeline6-blue','PACKAGE_PLATINUM'=>'timeline6-red');
$icon_color_array = array('PACKAGE_FREE'=>'timeline-icon-yellow', 'PACKAGE_SILVER'=>'timeline-icon-green', 'PACKAGE_GOLD'=>'timeline-icon-blue', 'PACKAGE_PLATINUM'=>'timeline-icon-red');
$icons_array = array('PACKAGE_FREE'=>'fa fa-bullhorn','PACKAGE_SILVER'=>'fa fa-star-half-o', 'PACKAGE_GOLD'=>'fa fa-trophy','PACKAGE_PLATINUM'=>'fa fa-diamond');
use yii\helpers\Html;

?>

<div class="main-section">
    <main data-ng-app="privacyApp" data-ng-controller="privacyController">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 ">
                        <div class="row">
                            <div class="white-section">
                                <h3> <p class="mrg-bt-10"><i class="fa fa-history text-warning"></i> Subscription History </p>
                                </h3>
                                <div class="two-column">
                                    <div class="row">
                                        <div class="col-md-9">
                                            <?php if (Yii::$app->session->hasFlash('success')): ?>
                                                <div class="notice kp_success">
                                                    <p><?= Yii::$app->session->getFlash('success') ?></p>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                        <div class="col-md-3">
                                            <?= html::a('Upgrade Your Plan', ['/upgrade-plan'], ["title" => 'Upgrade Your Plan',"class"=>"btn btn-primary mrg-tp-10 col-md-3"]) ?>
                                            <br>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="main-timeline2">
                                                <?php if(count(@$subscription_history) > 0) {
                                                    foreach($subscription_history as $k=>$v){
                                                        ?>
                                                        <div class="timeline">
                                                            <span class="icon <?=$icons_array[$v->usd_short_name]?> <?=$icon_color_array[$v->usd_short_name]?>"></span>
                                                            <span href="javascript:void();" class="timeline-content <?=$color_array[$v->usd_short_name]?>">
                                                                <h3 class="title">
                                                                    <?= stripslashes($v->usd_subscription_name)?>
                                                                    [<?= CommonHelper::DateTime($v->usd_subscription_end_date, 26); ?>]
                                                                </h3>
                                                                <p class="description">
                                                                <ul>
                                                                    <li><b><?=$v->usd_profile_duration?> Days</b> Duration of Profile on Website</li>
                                                                    <li><b><?=$v->usd_no_of_contacts?></b> No of Contacts</li>
                                                                    <li><b><?=$v->usd_no_of_pm?></b> Personalized Messaging </li>
                                                                    <li><b><?=$v->usd_privacy_settings?></b> Privacy Settings </li>
                                                                    <li><b><?=$v->usd_validity_of_package?></b> Validity of Package</li>
                                                                    <li><b><?=$v->usd_customer_care_support?></b> Customer Care Support  </li>
                                                                </ul>
                                                                <hr>
                                                                <ul>
                                                                    <li><b><?= CommonHelper::DateTime($v->usd_subscription_start_date, 26); ?></b> Start Date</li>
                                                                    <li><b><?= CommonHelper::DateTime($v->usd_subscription_end_date, 26); ?></b> Expiration Date </li>
                                                                </ul>
                                                                <?php if($k==0 && strtotime($v->usd_subscription_end_date) < strtotime(date('Y-m-d H:i:s')) && $v->usd_short_name != 'PACKAGE_FREE' ){ ?>
                                                                <span class="label label-default  pull-right">
                                                                     <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() . Yii::$app->params['upgradeProcess']."?type=".$v->usd_short_name ?>"
                                                                        title="Renew Subscription" class="text-white">Renew</a>
                                                                </span>
                                                                <?php } ?>

                                                                <?php
                                                                if($k == 1 && $v->usd_short_name != 'PACKAGE_FREE' ){ ?>
<!--                                                                    <span class="pull-right  text-justify"><b>Note:</b> Plan remaining benefit (No of Contacts and Personalized Messaging) merge with current subscription plan.</span>-->
                                                                <?php }
                                                                ?>
                                                                <?php if($k > 0 ){
                                                                    if(strtotime($v->usd_subscription_end_date) < strtotime(date('Y-m-d H:i:s')) ){ ?>
                                                                        <span class="label label-danger text-center  pull-right">Expired</span>
                                                                    <?php }else{ ?>
                                                                        <span class="label label-danger text-center pull-right">Close</span>
                                                                    <?php } }
                                                                ?>
                                                                </p>
                                                                <h4 class="price">
                                                                    <b><i class="fa fa-inr"></i><?= number_format($v->usd_subscriptions_price,2)?></b> /-
                                                                </h4>
                                                            </span>
                                                        </div>
                                                    <?php }  }
                                                else{ ?>
                                                    <div class="timeline">
                                                        <span class="icon <?=$icons_array['PACKAGE_FREE']?> <?=$icon_color_array['PACKAGE_FREE']?>"></span>
                                                        <a href="javascript:void();" class="timeline-content <?=$color_array['PACKAGE_FREE']?>">
                                                            <h3 class="title">Free [<?= CommonHelper::DateTime(date('Y-m-d',$UserModel->created_at), 26); ?>]
                                                            </h3>
                                                            <p class="description">
                                                            <ul>
                                                                <li><b>365 Days</b> Duration of Profile on Website</li>
                                                                <li><b>0</b> No of Contacts</li>
                                                                <li><b>0</b> Personalized Messaging </li>
                                                                <li><b>No</b> Privacy Settings </li>
                                                                <li><b>N/A</b> Validity of Package</li>
                                                                <li><b>No</b> Customer Care Support  </li>
                                                            </ul>
                                                            </p>
                                                            <h4 class="price">
                                                                <b><i class="fa fa-inr"></i>0</b> /-
                                                            </h4>
                                                        </a>
                                                    </div>
                                                <?php } ?>


                                                <!--<div class="timeline">
                                                    <span class="icon <?/*=$icons_array['PACKAGE_SILVER']*/?> <?/*=$icon_color_array['PACKAGE_SILVER']*/?>"></span>
                                                    <a href="javascript:void();" class="timeline-content <?/*=$color_array['PACKAGE_SILVER']*/?>">
                                                        <h3 class="title">Silver [01st Nov, 2018]
                                                        </h3>
                                                        <p class="description">
                                                        <ul>
                                                            <li><b>365 Days</b> Duration of Profile on Website</li>
                                                            <li><b>0</b> No of Contacts</li>
                                                            <li><b>0</b> Personalized Messaging </li>
                                                            <li><b>No</b> Privacy Settings </li>
                                                            <li><b>N/A</b> Validity of Package</li>
                                                            <li><b>No</b> Customer Care Support  </li>
                                                        </ul>
                                                        <hr>
                                                        <ul>
                                                            <li><b>01st Nov, 2018</b> Start Date</li>
                                                            <li><b>20th Nov, 2018</b> Expiration Date </li>
                                                        </ul>
                                                        </p>
                                                        <h4 class="price">
                                                            <b><i class="fa fa-inr"></i>1,200.00</b> /-
                                                        </h4>
                                                    </a>
                                                </div>-->

                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <br>
                                            <br>
                                            <br>
                                            <hr>
                                            <br>
                                            <br>
                                            <br>
                                        </div>
                                        <div class="col-md-12">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</div>

<style>
    /******************* Timeline Demo - 2 *****************/
    .main-timeline2{padding-top:50px;overflow:hidden;position:relative}
    .main-timeline2:before{content:"";width:7px;height:100%;background:#084772;margin:0 auto;position:absolute;top:80px;left:0;right:0}
    .main-timeline2 .timeline{width:50%;float:left;padding:20px 60px;border-top:7px solid #084772;border-right:7px solid #084772;border-radius:0 30px 0 0;position:relative;right:-3.5px}
    .main-timeline2 .icon{display:block;width:50px;height:50px;line-height:50px;border-radius:50%;background:#e84c47;border:1px solid #fff;text-align:center;font-size:25px;color:#fff;box-shadow:0 0 0 2px #e84c47;position:absolute;top:-30px;left:0}
    .main-timeline2 .timeline-content{display:block;padding:30px 10px 10px;border-radius:20px;background:#e84c47;color:#fff;position:relative}
    .main-timeline2 .timeline-content:hover{text-decoration:none;color:#fff}
    .main-timeline2 .timeline-content:after,.main-timeline2 .timeline-content:before{content:"";display:block;width:10px;height:50px;border-radius:10px;background:#e84c47;border:1px solid #fff;position:absolute;top:-35px;left:50px}
    .main-timeline2 .timeline-content:after{left:auto;right:50px}
    .main-timeline2 .title{font-size:24px;margin:0}
    .main-timeline2 .price{font-size:24px;margin:0; text-align: center;color:#FFF;}
    .main-timeline2 .description{font-size:15px;letter-spacing:1px;margin:0 0 5px}
    .main-timeline2 .timeline:nth-child(2n){border-right:none;border-left:7px solid #084772;border-radius:30px 0 0;right:auto;left:-3.5px}
    .main-timeline2 .timeline:nth-child(2n) .icon{left:auto;right:0;box-shadow:0 0 0 2px #4bd9bf}
    .main-timeline2 .timeline:nth-child(2){margin-top:130px}
    .main-timeline2 .timeline:nth-child(odd){margin:-130px 0 30px}
    .main-timeline2 .timeline:nth-child(even){margin-bottom:80px}
    .main-timeline2 .timeline:first-child,.main-timeline2 .timeline:last-child:nth-child(even){margin:0 0 30px}

    /*.main-timeline2 .timeline:nth-child(2n) .icon,.main-timeline2 .timeline:nth-child(2n) .timeline-content,.main-timeline2 .timeline:nth-child(2n) .timeline-content:after,.main-timeline2 .timeline:nth-child(2n) .timeline-content:before{background:#4bd9bf}
    .main-timeline2 .timeline:nth-child(3n) .icon,.main-timeline2 .timeline:nth-child(3n) .timeline-content,.main-timeline2 .timeline:nth-child(3n) .timeline-content:after,.main-timeline2 .timeline:nth-child(3n) .timeline-content:before{background:#ff9e09}*/
    /*.main-timeline2 .timeline:nth-child(3n) .icon{box-shadow:0 0 0 2px #ff9e09}
    .main-timeline2 .timeline:nth-child(4n) .icon,.main-timeline2 .timeline:nth-child(4n) .timeline-content,.main-timeline2 .timeline:nth-child(4n) .timeline-content:after,.main-timeline2 .timeline:nth-child(4n) .timeline-content:before{background:#3ebae7}
    .main-timeline2 .timeline:nth-child(4n) .icon{box-shadow:0 0 0 2px #3ebae7}*/
    @media only screen and (max-width:767px){.main-timeline2:before{left:0;right:auto}
        .main-timeline2 .timeline,.main-timeline2 .timeline:nth-child(even),.main-timeline2 .timeline:nth-child(odd){width:100%;float:none;padding:20px 30px;margin:0 0 30px;border-right:none;border-left:7px solid #084772;border-radius:30px 0 0;right:auto;left:0}
        .main-timeline2 .icon{left:auto;right:0}
    }
    @media only screen and (max-width:480px){.main-timeline2 .title{font-size:18px}
    }

    .timeline-red, .timeline-red:before, .timeline-red:after{
        background:#ff4b4b !important;
    }
    .timeline-icon-red{
        background:#ff4b4b !important;
        box-shadow: 0 0 0 2px #ff4b4b !important;
    }
    .timeline-yellow, .timeline-yellow:before, .timeline-yellow:after{
        background:#ff9624 !important;
    }
    .timeline-icon-yellow{
        background:#ff9624 !important;
        box-shadow: 0 0 0 2px #ff9624 !important;
    }
    .timeline-blue, .timeline-blue:before, .timeline-blue:after{
        background:#4b64ff !important;
    }
    .timeline-icon-blue{
        background:#4b64ff !important;
        box-shadow: 0 0 0 2px #4b64ff !important;
    }
    .timeline-green, .timeline-green:before, .timeline-green:after{
        background:#40c952 !important;
    }
    .timeline-icon-green{
        background:#40c952 !important;
        box-shadow: 0 0 0 2px #40c952 !important;
    }

    /******************* Timeline Demo - 6 *****************/
    /*.demo{background:#f2f2f2}
    .main-timeline6{overflow:hidden;position:relative}
    .main-timeline6 .timeline{width:50%;float:right;position:relative;z-index:1}
    .main-timeline6 .timeline:after,.main-timeline6 .timeline:before{position:absolute;top:50%;content:"";display:block;clear:both}
    .main-timeline6 .timeline:before{width:40%;height:6px;background:#9f005d;left:0;z-index:-1;transform:translateY(-50%)}
    .main-timeline6 .timeline:after{width:6px;height:70%;background:#9f005d;left:-3px}
    .main-timeline6 .timeline-content{width:65%;float:right;padding:0 0 30px 30px;margin-right:15px;background:#fff;border-radius:10px;box-shadow:3px 3px 5px 6px #ccc}
    .main-timeline6 .timeline-content:after,.main-timeline6 .timeline-content:before{content:"";width:26px;height:26px;border-radius:50%;background:#9f005d;position:absolute;top:50%;left:-13px;z-index:1;transform:translateY(-50%)}
    .main-timeline6 .timeline-content:after{left:30%;transform:translate(-50%,-50%)}
    .main-timeline6 .year{display:block;font-size:28px;font-weight:700;color:#9f005d;text-align:center;padding-left:50px}
    .main-timeline6 .content-inner{padding:35px 15px 35px 110px;margin-right:-15px;background:#9f005d;border-radius:150px 0 0 150px;position:relative}
    .main-timeline6 .content-inner:after,.main-timeline6 .content-inner:before{content:"";border-left:15px solid #640026;border-top:10px solid transparent;position:absolute;top:-10px;right:0}
    .main-timeline6 .content-inner:after{border-top:none;border-bottom:10px solid transparent;top:auto;bottom:-10px}
    .main-timeline6 .icon{width:110px;height:100%;text-align:center;position:absolute;top:0;left:0}
    .main-timeline6 .icon i{font-size:60px;font-weight:700;color:#fff;position:absolute;top:50%;left:50%;transform:translate(-50%,-50%)}
    .main-timeline6 .title{font-size:22px;font-weight:700;color:#fff;margin:0 0 5px}
    .main-timeline6 .price{font-size:22px;font-weight:700;text-align: center;color:#FFF;}
    .main-timeline6 .description{font-size:14px;color:#fff;margin:0}
    .main-timeline6 .timeline:nth-child(2n) .icon,.main-timeline6 .timeline:nth-child(2n):after,.main-timeline6 .timeline:nth-child(2n):before{left:auto;right:0}
    .main-timeline6 .timeline:nth-child(2n):after{right:-3px}
    .main-timeline6 .timeline:nth-child(2n) .timeline-content{float:left;padding:0 30px 30px 0;margin:0 0 0 15px}
    .main-timeline6 .timeline:nth-child(2n) .timeline-content:after,.main-timeline6 .timeline:nth-child(2n) .timeline-content:before{left:auto;right:-13px}
    .main-timeline6 .timeline:nth-child(2n) .timeline-content:after{right:30%;margin-right:-25px}
    .main-timeline6 .timeline:nth-child(2n) .year{padding:0 50px 0 0;color:#05b1ff}
    .main-timeline6 .timeline:nth-child(2n) .content-inner{padding:35px 110px 35px 15px;margin:0 0 0 -15px;border-radius:0 150px 150px 0}
    .main-timeline6 .timeline:nth-child(2n) .content-inner:after,.main-timeline6 .timeline:nth-child(2n) .content-inner:before{border:none;border-right:15px solid #027dcd;border-top:10px solid transparent;right:auto;left:0}
    .main-timeline6 .timeline:nth-child(2n) .content-inner:after{border-top:none;border-bottom:10px solid transparent}
    .main-timeline6 .timeline:nth-child(2){margin-top:200px}
    .main-timeline6 .timeline:nth-child(odd){margin:-190px 0 0}
    .main-timeline6 .timeline:nth-child(even){margin-bottom:70px}
    .main-timeline6 .timeline:first-child,.main-timeline6 .timeline:last-child:nth-child(even){margin:0}
    .main-timeline6 .timeline:nth-child(2n) .content-inner,.main-timeline6 .timeline:nth-child(2n) .timeline-content:after,.main-timeline6 .timeline:nth-child(2n) .timeline-content:before,.main-timeline6 .timeline:nth-child(2n):after,.main-timeline6 .timeline:nth-child(2n):before{background:#05b1ff}
    .main-timeline6 .timeline:nth-child(3n) .content-inner,.main-timeline6 .timeline:nth-child(3n) .timeline-content:after,.main-timeline6 .timeline:nth-child(3n) .timeline-content:before,.main-timeline6 .timeline:nth-child(3n):after,.main-timeline6 .timeline:nth-child(3n):before{background:#00a3a9}
    .main-timeline6 .timeline:nth-child(3n) .content-inner:after,.main-timeline6 .timeline:nth-child(3n) .content-inner:before{border-left-color:#006662}
    .main-timeline6 .timeline:nth-child(3n) .year{color:#00a3a9}
    .main-timeline6 .timeline:nth-child(4n) .content-inner,.main-timeline6 .timeline:nth-child(4n) .timeline-content:after,.main-timeline6 .timeline:nth-child(4n) .timeline-content:before,.main-timeline6 .timeline:nth-child(4n):after,.main-timeline6 .timeline:nth-child(4n):before{background:#f92534}
    .main-timeline6 .timeline:nth-child(4n) .content-inner:after,.main-timeline6 .timeline:nth-child(4n) .content-inner:before{border-right-color:#92070e}
    .main-timeline6 .timeline:nth-child(4n) .year{color:#f92534}
    @media only screen and (max-width:990px) and (min-width:768px){.main-timeline6 .timeline:after{height:80%}
    }
    @media only screen and (max-width:767px){.main-timeline6 .timeline:last-child,.main-timeline6 .timeline:nth-child(even),.main-timeline6 .timeline:nth-child(odd){margin:0}
        .main-timeline6 .timeline{width:95%;margin:15px 15px 15px 0!important}
        .main-timeline6 .timeline .timeline-content:after,.main-timeline6 .timeline .timeline-content:before,.main-timeline6 .timeline:after,.main-timeline6 .timeline:before{display:none}
        .main-timeline6 .timeline-content,.main-timeline6 .timeline:nth-child(2n) .timeline-content{width:100%;float:none;padding:0 0 30px 30px;margin:0}
        .main-timeline6 .content-inner,.main-timeline6 .timeline:nth-child(2n) .content-inner{padding:35px 15px 35px 110px;margin:0 -15px 0 0;border-radius:150px 0 0 150px}
        .main-timeline6 .timeline:nth-child(2n) .content-inner:after,.main-timeline6 .timeline:nth-child(2n) .content-inner:before{border:none;border-left:15px solid #027dcd;border-top:10px solid transparent;right:0;left:auto}
        .main-timeline6 .timeline:nth-child(2n) .content-inner:after{border-top:none;border-bottom:10px solid transparent}
        .main-timeline6 .timeline:nth-child(2n) .icon{top:0;left:0}
        .main-timeline6 .timeline:nth-child(4n) .content-inner:after,.main-timeline6 .timeline:nth-child(4n) .content-inner:before{border-left-color:#92070e}
    }
    .timeline2{
        color:#fff;
    }
    .timeline6-red{
        color: #ff4b4b !important;
    }
    .timeline6-red:after,.timeline6-red:before, .timeline6-red > .timeline-content:after, .timeline6-red >.timeline-content:before{
        background: #ff4b4b !important;
    }

    .timeline6-blue{
        color: #4b64ff !important;
    }
    .timeline6-blue:after,.timeline6-blue:before, .timeline6-blue > .timeline-content:after, .timeline6-blue >.timeline-content:before{
        background: #4b64ff !important;
    }

    .timeline6-yellow{
        color: #ff9624 !important;
    }
    .timeline6-yellow:after,.timeline6-yellow:before, .timeline6-yellow > .timeline-content:after, .timeline6-yellow >.timeline-content:before{
        background: #ff9624 !important;
    }

    .timeline6-green{
        color: #40c952 !important;
    }
    .timeline6-green:after,.timeline6-green:before, .timeline6-green > .timeline-content:after, .timeline6-green >.timeline-content:before{
        background: #40c952 !important;
    }*/

</style>