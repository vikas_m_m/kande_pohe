<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\components\CommonHelper;
use yii\helpers\ArrayHelper;

use yii\helpers\Url;
use yii\widgets\Pjax;

$id = 0;
$style = ' class_b';
if (!Yii::$app->user->isGuest) {
    $Id = Yii::$app->user->identity->id;
    $style = '';
}
$M1 = array();
?>
<main>
    <div class="container-fluid">
        <div class="row no-gutter bg-dark">
            <div class="col-md-1">

            </div>
            <div class="no-gutter">
                <div class="col-lg-10 col-md-10 col-sm-10 ">
                    <div class="white-section mrg-tp-20 mrg-bt-10">
                        <h3><?php
                            if (count(@$slug_record) > 0) {
                                echo $slug_record->title . " Matrimony";
                            } else {
                                echo ucfirst(@$slug) . " Matrimony";
                            }
                            ?></h3>
                        <?php if(count(@$slug_record)==0){ ?>
                        <div class="row ">
                            <div class="text-center tab-content">
                                <div class="alert alert-warning">
                                    <strong>Relevant profile not found. Try another option!</strong>
                                </div>
                            </div>
                        </div>
                        <?php }else{ ?>
                        <div class="row">
                            <div class="col-md-12">
                                <p><?php echo $slug_record->description; ?></p>
                                <br>
                            </div>
                        </div>
                            <div class="row">
                            <ul class="nav nav-pills nav-pills-custom">
                                <li class="active text-center"><a data-toggle="pill" href="#home">Bride Profiles</a></li>
                                <li class="text-center"><a data-toggle="pill" href="#menu1">Groom Profiles</a></li>
                            </ul>
                            <div class="tab-content tab-content-custom_1">
                                <div id="home" class="tab-pane fade in active ">
                                    <?php if(count(@$record_list_Female)==0){ ?>
                                        <div class="row ">
                                            <div class="text-center tab-content">
                                                <div class="alert alert-warning">
                                                    <strong>Relevant profile not found. Try another option!</strong>
                                                </div>
                                            </div>
                                        </div>
                                    <?php }else{ ?>
                                        <!--<table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th>Name</th>
                                            <th>Gender</th>
                                            <th>Age</th>
                                            <th>City</th>
                                            <th>Occupation</th>
                                            <th>Education</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php /*foreach ($record_list_Female as $fk => $fv) {*/ ?>
                                            <tr>
                                                <th scope="row">
                                                    <? /*= Html::img(CommonHelper::getPhotos('USER', $fv->id, "63" . $fv->propic, 63, '', 'Yes', CommonHelper::getVisiblePhoto($fv->id, $fv->eStatusPhotoModify)), ['alt' => $fv->FullName, 'style' => '    width: 63px !important;']); */ ?>
                                                </th>
                                                <th scope="row">
                                                    <? /*= $fv->FullName. "(".$fv->Registration_Number.")"; */ ?></td>
                                                <td><? /*= ucfirst(strtolower($fv->Gender))*/ ?></td>
                                                <td><? /*= CommonHelper::getAge($fv->DOB); */ ?> yrs</td>
                                                <td><? /*= CommonHelper::setInputVal($fv->cityName->vCityName, 'text'); */ ?></td>
                                                <td><? /*= CommonHelper::setInputVal($fv->workingAsName->vWorkingAsName);*/ ?></td>
                                                <td><? /*= CommonHelper::setInputVal($fv->educationLevelName->vEducationLevelName, 'text') */ ?></td>
                                            </tr>
                                        <?php /*} */ ?>
                                        </tbody>
                                    </table>-->


                                        <?php
                                        foreach ($record_list_Female as $SK => $SV) {
                                            ?>
                                            <div
                                                class="white-section <?= ($SK == 0) ? 'listing1' : ''; ?> border-sharp mrg-tp-10">
                                                <div class="row mrg-bt-20">
                                                    <div class="col-sm-12">

                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-3 col-xs-4">
                                                        <div class="prof-pic">
                                                            <div class="drop-effect"></div>
                                                            <div class="slider">
                                                                <div id="carousel-example-generic_<?= $SK ?>"
                                                                     class="carousel slide"
                                                                     data-ride="carousel">
                                                                    <!-- Wrapper for slides -->
                                                                    <div class="carousel-inner">
                                                                        <?php
                                                                        if (is_array($photos_female[$SV->id])) {
                                                                            foreach ($photos_female[$SV->id] as $K => $V) {
                                                                                $SELECTED = '';
                                                                                $Photo = Yii::$app->params['thumbnailPrefix'] . '200_' . $V->File_Name;
                                                                                $Yes = 'No';
                                                                                if ($V['Is_Profile_Photo'] == 'YES') {
                                                                                    $SELECTED = "active";
                                                                                    $Photo = '200' . $SV->propic;
                                                                                    $Yes = 'Yes';
                                                                                } ?>
                                                                                <div
                                                                                    class="item <?= ($K == 0) ? 'active' : ''; ?> kp_pic_dis_dwn">
                                                                                    <?= Html::img(CommonHelper::getPhotos('USER', $SV->id, $Photo, 200, '', $Yes, CommonHelper::getVisiblePhoto($SV->id, $V['eStatus'])), ['width' => '205', 'height' => '205', 'alt' => 'Profile', 'class' => 'img-responsive '.$style]); ?>
                                                                                </div>
                                                                            <?php
                                                                            }
                                                                        } else { ?>
                                                                            <div class="item active kp_pic_dis_dwn">
                                                                                <?= Html::img(CommonHelper::getPhotos('USER', $SV->id, $Photos[$SV->id], 120), ['width' => '205', 'height' => '205', 'alt' => 'Profile', 'class' => 'img-responsive item '.$style]); ?>
                                                                            </div>
                                                                        <?php }
                                                                        ?>
                                                                    </div>
                                                                    <!-- Controls -->
                                                                    <?php if (is_array($photos_female[$SV->id])) {
                                                                        if (count($photos_female[$SV->id]) > 1) { ?>
                                                                            <a class="left carousel-control"
                                                                               href="#carousel-example-generic_<?= $SK ?>"
                                                                               data-slide="prev"> <span
                                                                                    class="glyphicon glyphicon-chevron-left"></span>
                                                                            </a> <a class="right carousel-control"
                                                                                    href="#carousel-example-generic_<?= $SK ?>"
                                                                                    data-slide="next">
                                                                    <span
                                                                        class="glyphicon glyphicon-chevron-right"></span>
                                                                            </a>
                                                                        <?php }
                                                                    } ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php if (is_array($photos_female[$SV->id])) { ?>

                                                        <?php } ?>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <div class="name-panel">
                                                            <h2 class="nameplate">
                                                                <?php
/*                                                                if (!Yii::$app->user->isGuest) { */?>
                                                                    <a href="<?= Yii::$app->homeUrl ?>user/profile?uk=<?= $SV->Registration_Number ?>&source=profile_viewed_by"
                                                                       class="name"
                                                                       title="<?= $SV->Registration_Number ?>">
                                                                        <?= $SV->FullName; ?>
                                                                    </a>
                                                                <?php /*} else { */?><!--
                                                                    <?/*= $SV->FullName; */?>
                                                                --><?php /*} */?>
                                                                <span
                                                                    class="font-light">(<?= $SV->Registration_Number ?>
                                                                    )</span>
                                                                <?php
                                                                #$USER_FACEBOOK = \common\models\User::weightedCheck(11);
                                                                $USER_PHONE = \common\models\User::weightedCheck(8);
                                                                $USER_EMAIL = \common\models\User::weightedCheck(9);
                                                                $USER_APPROVED = \common\models\User::weightedCheck(10);
                                                                if ($USER_PHONE && $USER_EMAIL && $USER_APPROVED) {
                                                                    ?>
                                                                    <span class="premium"></span>
                                                                <?php } ?>
                                                            </h2>
                                                            <?php $USER_PHONE = \common\models\User::weightedCheck(8); ?>
                                                            <p>Profile created for <?= $SV->Profile_created_for; ?> |
                                                                Last
                                                                online <?= CommonHelper::DateTime($SV->LastLoginTime, 28); ?>
                                                                |
                                                        <span class="pager-icon">
                                                    <a href="javascript:void(0)">
                                                        <i class="fa fa-mobile"></i>
                                                      <span
                                                          class="badge <?php if ($SV->ePhoneVerifiedStatus != 'Yes') { ?>badge1<?php } ?>">
                                                          <i class="fa fa-check"></i>
                                                      </span>
                                                    </a>
                                                            </span>
                                                            </p>
                                                        </div>
                                                        <dl class="dl-horizontal mrg-tp-20">
                                                            <dt>Personal Details</dt>
                                                            <dd><?= CommonHelper::getAge($SV->DOB); ?>
                                                                yrs, <?= CommonHelper::setInputVal($SV->height->vName, 'text'); ?>
                                                                <?= ($SV->RaashiId > 0) ? ", " . CommonHelper::setInputVal($SV->raashiName->Name, 'text') : ''; ?>
                                                            </dd>
                                                            <dt>Marital Status</dt>
                                                            <dd><?= CommonHelper::setInputVal($SV->maritalStatusName->vName, 'text') ?></dd>
                                                            <dt>Religion Community</dt>
                                                            <dd><?= CommonHelper::setInputVal($SV->religionName->vName, 'text') . ',' . CommonHelper::setInputVal($SV->communityName->vName, 'text') ?></dd>
                                                            <dt>Education</dt>
                                                            <dd><?= CommonHelper::setInputVal($SV->educationLevelName->vEducationLevelName, 'text') ?></dd>
                                                            <dt>Profession</dt>
                                                            <dd><?= CommonHelper::setInputVal($SV->workingAsName->vWorkingAsName, 'text') ?></dd>
                                                            <dt>Current Location</dt>
                                                            <dd><?= CommonHelper::setInputVal($SV->cityName->vCityName, 'text') . ', ' . CommonHelper::setInputVal($SV->countryName->vCountryName, 'text') ?></dd>
                                                        </dl>
                                                    </div>
                                                </div>
                                                <?php
                                                if (!Yii::$app->user->isGuest) {
                                                    if ($SV->Gender != Yii::$app->user->identity->Gender) {
                                                        // CommonHelper::pr($SV);
                                                        ?>
                                                        <!--<div class="row gray-bg">
                                                            <div class="col-sm-12">
                                                                <div class="profile-control-vertical">
                                                                    <ul class="list-unstyled pull-right">
                                                                        <?php
                                                        /*                                                                        $Value = \common\models\UserRequestOp::checkSendInterest(Yii::$app->user->identity->id, $SV->id);
                                                                                                                                $Shortlisted = 0;
                                                                                                                                if (Yii::$app->user->identity->id == $Value->from_user_id && $Value->short_list_status_from_to == 'Yes') {
                                                                                                                                    $Shortlisted = 1;
                                                                                                                                }
                                                                                                                                if (Yii::$app->user->identity->id == $Value->to_user_id && $Value->short_list_status_to_from == 'Yes') {
                                                                                                                                    $Shortlisted = 1;
                                                                                                                                }
                                                                                                                                */ ?>
                                                                        <li class="sl__<? /*= $SV->id */ ?>">
                                                                            <?php /*if ($Shortlisted) { */ ?>
                                                                                <a href="javascript:void(0)">Shortlisted <i
                                                                                        class="fa fa-list-ul"></i></a>
                                                                            <?php /*} else { */ ?>
                                                                                <a href="javascript:void(0)"
                                                                                   class="short_list_<? /*= $SV->id */ ?> shortlistUser"
                                                                                   data-id="<? /*= $SV->id */ ?>"
                                                                                   data-name="<? /*= $SV->fullName */ ?>">Shortlist
                                                                                    <i class="fa fa-list-ul"></i></a>
                                                                            <?php /*} */ ?>

                                                                        </li>
                                                                        <li class="s__<? /*= $SV->id */ ?>">
                                                                            <?php
                                                        /*                                                                            if (count($Value)) {
                                                                                                                                        if ($Id == $Value->from_user_id && $Value->profile_viewed_from_to == 'Yes') {
                                                                                                                                            $ViewerId = $Value->to_user_id;
                                                                                                                                        } else {
                                                                                                                                            $ViewerId = $Value->from_user_id;
                                                                                                                                        }
                                                                                                                                    } else {
                                                                                                                                        $ViewerId = $Value->id;
                                                                                                                                    }
                                                                                                                                    $UserInfoModel = \common\models\User::getUserInfroamtion($ViewerId);

                                                                                                                                    if (count($Value) == 0 || ($Id == $Value->from_user_id && $Value->send_request_status_from_to == 'No' && $Value->send_request_status_to_from == 'No') || ($Id == $Value->to_user_id && $Value->send_request_status_to_from == 'No' && $Value->send_request_status_from_to == 'No')) { */ ?>
                                                                                <a href="javascript:void(0)"
                                                                                   class=" sendinterestpopup"
                                                                                   role="button"
                                                                                   data-target="#sendInterest"
                                                                                   data-toggle="modal"
                                                                                   data-id="<? /*= $SV->id */ ?>"
                                                                                   data-name="<? /*= $SV->fullName */ ?>"
                                                                                   data-rgnumber="<? /*= $SV->Registration_Number */ ?>">Send
                                                                                    Interest
                                                                                    <i class="fa fa-heart-o"></i>
                                                                                </a>
                                                                            <?php /*} else if (($Id == $Value->from_user_id && $Value->send_request_status_from_to == 'Yes' && $Value->send_request_status_to_from != 'Yes') || ($Id == $Value->to_user_id && $Value->send_request_status_to_from == 'Yes' && $Value->send_request_status_from_to != 'Yes')) { */ ?>
                                                                                <a href="javascript:void(0)"
                                                                                   class=" ci " role="button"
                                                                                   data-target="#"
                                                                                   data-toggle="modal"
                                                                                   data-id="<? /*= $UserInfoModel->id */ ?>"
                                                                                   data-name="<? /*= $UserInfoModel->fullName */ ?>"
                                                                                   data-rgnumber="<? /*= $UserInfoModel->Registration_Number */ ?>">Cancel
                                                                                    Interest
                                                                                    <i class="fa fa-close"></i> </a>

                                                                            <?php /*} else if (($Id == $Value->to_user_id && $Value->send_request_status_from_to == 'Yes' && $Value->send_request_status_to_from != 'Yes') || ($Id == $Value->from_user_id && $Value->send_request_status_to_from == 'Yes' && $Value->send_request_status_from_to != 'Yes')) {
                                                                                */ ?>
                                                                                <a href="javascript:void(0)"
                                                                                   class=" accept_decline adbtn"
                                                                                   role="button"
                                                                                   data-target="#accept_decline"
                                                                                   data-toggle="modal"
                                                                                   data-id="<? /*= $UserInfoModel->id */ ?>"
                                                                                   data-name="<? /*= $UserInfoModel->fullName */ ?>"
                                                                                   data-rgnumber="<? /*= $UserInfoModel->Registration_Number */ ?>"
                                                                                   data-type="Accept Interest">
                                                                                    Accept
                                                                                    <i class="fa fa-check"></i>
                                                                                </a>
                                                                                <a href="javascript:void(0)"
                                                                                   class="accept_decline adbtn"
                                                                                   role="button"
                                                                                   data-target="#accept_decline"
                                                                                   data-toggle="modal"
                                                                                   data-id="<? /*= $UserInfoModel->id */ ?>"
                                                                                   data-name="<? /*= $UserInfoModel->fullName */ ?>"
                                                                                   data-rgnumber="<? /*= $UserInfoModel->Registration_Number */ ?>"
                                                                                    >
                                                                                    Decline
                                                                                    <i class="fa fa-close"></i> </a>
                                                                            <?php /*} else if ($Value->send_request_status_from_to == 'Accepted' || $Value->send_request_status_to_from == 'Accepted') {
                                                                                */ ?>
                                                                                <a href="javascript:void(0)" class=""
                                                                                   role="button"
                                                                                   data-target="#" data-toggle="modal"
                                                                                   data-id="<? /*= $UserInfoModel->id */ ?>"
                                                                                   data-name="<? /*= $UserInfoModel->fullName */ ?>"
                                                                                   data-rgnumber="<? /*= $UserInfoModel->Registration_Number */ ?>"
                                                                                   data-type="Connected">Connected
                                                                                    <i class="fa fa-heart"></i> </a>
                                                                            <?php /*} else if ($Value->send_request_status_from_to == 'Rejected' || $Value->send_request_status_to_from == 'Rejected') {
                                                                                */ ?>
                                                                                <a href="javascript:void(0)" class=" "
                                                                                   role="button"
                                                                                   data-target="#" data-toggle="modal"
                                                                                   data-id="<? /*= $UserInfoModel->id */ ?>"
                                                                                   data-name="<? /*= $UserInfoModel->fullName */ ?>"
                                                                                   data-rgnumber="<? /*= $UserInfoModel->Registration_Number */ ?>"
                                                                                   data-type="Connected">Rejected <i
                                                                                        class="fa fa-close"></i> </a>

                                                                            <?php /*} else { */ ?>
                                                                                <a href="javascript:void(0)"
                                                                                   class="btn btn-link isent" role="button">Interest
                                                                                    Sent <i class="fa fa-heart"></i></a>
                                                                            <?php /*} */ ?>
                                                                        </li>

                                                                        <li>
                                                                            <a href="#" data-toggle="modal"
                                                                               class="send_email"
                                                                               <?php /*if (Yii::$app->user->isGuest) { */ ?>data-target="#sendMail"<?php /*} else { */ ?> data-id="<? /*= $SV->id */ ?>" <?php /*} */ ?>>Send
                                                                                Email <i class="fa fa-envelope-o"></i>
                                                                            </a>
                                                                        </li>

                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>-->

                                                    <?php } ?>

                                                <?php } else { ?>
                                                    <!--<div class="row gray-bg">
                                                        <div class="col-sm-12">
                                                            <div class="profile-control-vertical">
                                                                <ul class="list-unstyled pull-right">
                                                                    <li>
                                                                        <a href="javascript:void(0)" class="send_email"
                                                                           role="button"
                                                                           data-target="#sendMail" data-toggle="modal"
                                                                            >Shortlist <i class="fa fa-list-ul"></i></a>
                                                                    </li>
                                                                    <li class="s__<? /*= $SV->id */ ?>">
                                                                        <a href="javascript:void(0)" class="send_email"
                                                                           role="button"
                                                                           data-target="#sendMail" data-toggle="modal"
                                                                            >Send Interest <i class="fa fa-heart-o"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#" data-toggle="modal" class="send_email"
                                                                           data-target="#sendMail">Send Email <i
                                                                                class="fa fa-envelope-o"></i></a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>-->
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                    <?php }?>
                                </div>
                                <div id="menu1" class="tab-pane fade">
                                    <?php if(count(@$record_list_Male)==0){ ?>
                                        <div class="row ">
                                            <div class="text-center tab-content">
                                                <div class="alert alert-warning">
                                                    <strong>Relevant profile not found. Try another option!</strong>
                                                </div>
                                            </div>
                                        </div>
                                    <?php }else{ ?>
                                        <!--<table class="table table-hover">
                                            <thead>
                                            <tr>
                                                <th></th>
                                                <th>Name</th>
                                                <th>Gender</th>
                                                <th>Age</th>
                                                <th>City</th>
                                                <th>Occupation</th>
                                                <th>Education</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php /*foreach ($record_list_Male as $fk => $fv) {*/ ?>
                                                <tr>
                                                    <th scope="row">
                                                        <? /*= Html::img(CommonHelper::getPhotos('USER', $fv->id, "63" . $fv->propic, 63, '', 'Yes', CommonHelper::getVisiblePhoto($fv->id, $fv->eStatusPhotoModify)), ['alt' => $fv->FullName, 'style' => '    width: 63px !important;']); */ ?>
                                                    </th>
                                                    <th scope="row">
                                                    <? /*= $fv->FullName. "(".$fv->Registration_Number.")"; */ ?></td>
                                                    <td><? /*= ucfirst(strtolower($fv->Gender))*/ ?></td>
                                                    <td><? /*= CommonHelper::getAge($fv->DOB); */ ?> yrs</td>
                                                    <td><? /*= CommonHelper::setInputVal($fv->cityName->vCityName, 'text'); */ ?></td>
                                                    <td><? /*= CommonHelper::setInputVal($fv->workingAsName->vWorkingAsName);*/ ?></td>
                                                    <td><? /*= CommonHelper::setInputVal($fv->educationLevelName->vEducationLevelName, 'text') */ ?></td>
                                                </tr>
                                            <?php /*} */ ?>


                                            </tbody>

                                        </table>-->

                                        <?php
                                        foreach ($record_list_Male as $SK => $SV) {
                                            ?>
                                            <div
                                                class="white-section <?= ($SK == 0) ? 'listing1' : ''; ?> border-sharp mrg-tp-10">
                                                <div class="row mrg-bt-20">
                                                    <div class="col-sm-12">

                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-3 col-xs-4">
                                                        <div class="prof-pic">
                                                            <div class="drop-effect"></div>
                                                            <div class="slider">
                                                                <div id="carousel-example-generic_<?= $SK ?>"
                                                                     class="carousel slide"
                                                                     data-ride="carousel">
                                                                    <!-- Wrapper for slides -->
                                                                    <div class="carousel-inner">
                                                                        <?php
                                                                        if (is_array($photos_male[$SV->id])) {
                                                                            foreach ($photos_male[$SV->id] as $K => $V) {
                                                                                $SELECTED = '';
                                                                                $Photo = Yii::$app->params['thumbnailPrefix'] . '200_' . $V->File_Name;
                                                                                $Yes = 'No';
                                                                                if ($V['Is_Profile_Photo'] == 'YES') {
                                                                                    $SELECTED = "active";
                                                                                    $Photo = '200' . $SV->propic;
                                                                                    $Yes = 'Yes';
                                                                                } ?>
                                                                                <div
                                                                                    class="item <?= ($K == 0) ? 'active' : ''; ?> kp_pic_dis_dwn">
                                                                                    <?= Html::img(CommonHelper::getPhotos('USER', $SV->id, $Photo, 200, '', $Yes, CommonHelper::getVisiblePhoto($SV->id, $V['eStatus'])), ['width' => '205', 'height' => '205', 'alt' => 'Profile', 'class' => 'img-responsive'.$style]); ?>
                                                                                </div>
                                                                            <?php
                                                                            }
                                                                        } else { ?>
                                                                            <div class="item active kp_pic_dis_dwn">
                                                                                <?= Html::img(CommonHelper::getPhotos('USER', $SV->id, $Photos[$SV->id], 120), ['width' => '205', 'height' => '205', 'alt' => 'Profile', 'class' => 'img-responsive item'.$style]); ?>
                                                                            </div>
                                                                        <?php }
                                                                        ?>
                                                                    </div>
                                                                    <!-- Controls -->
                                                                    <?php if (is_array($photos_male[$SV->id])) {
                                                                        if (count($photos_male[$SV->id]) > 1) { ?>
                                                                            <a class="left carousel-control"
                                                                               href="#carousel-example-generic_<?= $SK ?>"
                                                                               data-slide="prev"> <span
                                                                                    class="glyphicon glyphicon-chevron-left"></span>
                                                                            </a> <a class="right carousel-control"
                                                                                    href="#carousel-example-generic_<?= $SK ?>"
                                                                                    data-slide="next">
                                                                    <span
                                                                        class="glyphicon glyphicon-chevron-right"></span>
                                                                            </a>
                                                                        <?php }
                                                                    } ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php if (is_array($photos_male[$SV->id])) { ?>

                                                        <?php } ?>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <div class="name-panel">
                                                            <h2 class="nameplate">
                                                                <?php
/*                                                                if (!Yii::$app->user->isGuest) { */?>
                                                                    <a href="<?= Yii::$app->homeUrl ?>user/profile?uk=<?= $SV->Registration_Number ?>&source=profile_viewed_by"
                                                                       class="name"
                                                                       title="<?= $SV->Registration_Number ?>">
                                                                        <?= $SV->FullName; ?>
                                                                    </a>
                                                                <?php /*} else { */?><!--
                                                                    <?/*= $SV->FullName; */?>
                                                                --><?php /*} */?>
                                                                <span
                                                                    class="font-light">(<?= $SV->Registration_Number ?>
                                                                    )</span>
                                                                <?php
                                                                #$USER_FACEBOOK = \common\models\User::weightedCheck(11);
                                                                $USER_PHONE = \common\models\User::weightedCheck(8);
                                                                $USER_EMAIL = \common\models\User::weightedCheck(9);
                                                                $USER_APPROVED = \common\models\User::weightedCheck(10);
                                                                if ($USER_PHONE && $USER_EMAIL && $USER_APPROVED) {
                                                                    ?>
                                                                    <span class="premium"></span>
                                                                <?php } ?>
                                                            </h2>
                                                            <?php $USER_PHONE = \common\models\User::weightedCheck(8); ?>
                                                            <p>Profile created for <?= $SV->Profile_created_for; ?> |
                                                                Last
                                                                online <?= CommonHelper::DateTime($SV->LastLoginTime, 28); ?>
                                                                |
                                                        <span class="pager-icon">
                                                    <a href="javascript:void(0)">
                                                        <i class="fa fa-mobile"></i>
                                                      <span
                                                          class="badge <?php if ($SV->ePhoneVerifiedStatus != 'Yes') { ?>badge1<?php } ?>">
                                                          <i class="fa fa-check"></i>
                                                      </span>
                                                    </a>
                                                            </span>
                                                            </p>
                                                        </div>
                                                        <dl class="dl-horizontal mrg-tp-20">
                                                            <dt>Personal Details</dt>
                                                            <dd><?= CommonHelper::getAge($SV->DOB); ?>
                                                                yrs, <?= CommonHelper::setInputVal($SV->height->vName, 'text'); ?>
                                                                <?= ($SV->RaashiId > 0) ? ", " . CommonHelper::setInputVal($SV->raashiName->Name, 'text') : ''; ?>
                                                            </dd>
                                                            <dt>Marital Status</dt>
                                                            <dd><?= CommonHelper::setInputVal($SV->maritalStatusName->vName, 'text') ?></dd>
                                                            <dt>Religion Community</dt>
                                                            <dd><?= CommonHelper::setInputVal($SV->religionName->vName, 'text') . ',' . CommonHelper::setInputVal($SV->communityName->vName, 'text') ?></dd>
                                                            <dt>Education</dt>
                                                            <dd><?= CommonHelper::setInputVal($SV->educationLevelName->vEducationLevelName, 'text') ?></dd>
                                                            <dt>Profession</dt>
                                                            <dd><?= CommonHelper::setInputVal($SV->workingAsName->vWorkingAsName, 'text') ?></dd>
                                                            <dt>Current Location</dt>
                                                            <dd><?= CommonHelper::setInputVal($SV->cityName->vCityName, 'text') . ', ' . CommonHelper::setInputVal($SV->countryName->vCountryName, 'text') ?></dd>
                                                        </dl>
                                                    </div>
                                                </div>
                                                <?php
                                                if (!Yii::$app->user->isGuest) {
                                                    if ($SV->Gender != Yii::$app->user->identity->Gender) {
                                                        // CommonHelper::pr($SV);
                                                        ?>
                                                        <!--<div class="row gray-bg">
                                                            <div class="col-sm-12">
                                                                <div class="profile-control-vertical">
                                                                    <ul class="list-unstyled pull-right">
                                                                        <?php
                                                        /*                                                                        $Value = \common\models\UserRequestOp::checkSendInterest(Yii::$app->user->identity->id, $SV->id);
                                                                                                                                $Shortlisted = 0;
                                                                                                                                if (Yii::$app->user->identity->id == $Value->from_user_id && $Value->short_list_status_from_to == 'Yes') {
                                                                                                                                    $Shortlisted = 1;
                                                                                                                                }
                                                                                                                                if (Yii::$app->user->identity->id == $Value->to_user_id && $Value->short_list_status_to_from == 'Yes') {
                                                                                                                                    $Shortlisted = 1;
                                                                                                                                }
                                                                                                                                */ ?>
                                                                        <li class="sl__<? /*= $SV->id */ ?>">
                                                                            <?php /*if ($Shortlisted) { */ ?>
                                                                                <a href="javascript:void(0)">Shortlisted <i
                                                                                        class="fa fa-list-ul"></i></a>
                                                                            <?php /*} else { */ ?>
                                                                                <a href="javascript:void(0)"
                                                                                   class="short_list_<? /*= $SV->id */ ?> shortlistUser"
                                                                                   data-id="<? /*= $SV->id */ ?>"
                                                                                   data-name="<? /*= $SV->fullName */ ?>">Shortlist
                                                                                    <i class="fa fa-list-ul"></i></a>
                                                                            <?php /*} */ ?>

                                                                        </li>
                                                                        <li class="s__<? /*= $SV->id */ ?>">
                                                                            <?php
                                                        /*                                                                            if (count($Value)) {
                                                                                                                                        if ($Id == $Value->from_user_id && $Value->profile_viewed_from_to == 'Yes') {
                                                                                                                                            $ViewerId = $Value->to_user_id;
                                                                                                                                        } else {
                                                                                                                                            $ViewerId = $Value->from_user_id;
                                                                                                                                        }
                                                                                                                                    } else {
                                                                                                                                        $ViewerId = $Value->id;
                                                                                                                                    }
                                                                                                                                    $UserInfoModel = \common\models\User::getUserInfroamtion($ViewerId);

                                                                                                                                    if (count($Value) == 0 || ($Id == $Value->from_user_id && $Value->send_request_status_from_to == 'No' && $Value->send_request_status_to_from == 'No') || ($Id == $Value->to_user_id && $Value->send_request_status_to_from == 'No' && $Value->send_request_status_from_to == 'No')) { */ ?>
                                                                                <a href="javascript:void(0)"
                                                                                   class=" sendinterestpopup"
                                                                                   role="button"
                                                                                   data-target="#sendInterest"
                                                                                   data-toggle="modal"
                                                                                   data-id="<? /*= $SV->id */ ?>"
                                                                                   data-name="<? /*= $SV->fullName */ ?>"
                                                                                   data-rgnumber="<? /*= $SV->Registration_Number */ ?>">Send
                                                                                    Interest
                                                                                    <i class="fa fa-heart-o"></i>
                                                                                </a>
                                                                            <?php /*} else if (($Id == $Value->from_user_id && $Value->send_request_status_from_to == 'Yes' && $Value->send_request_status_to_from != 'Yes') || ($Id == $Value->to_user_id && $Value->send_request_status_to_from == 'Yes' && $Value->send_request_status_from_to != 'Yes')) { */ ?>
                                                                                <a href="javascript:void(0)"
                                                                                   class=" ci " role="button"
                                                                                   data-target="#"
                                                                                   data-toggle="modal"
                                                                                   data-id="<? /*= $UserInfoModel->id */ ?>"
                                                                                   data-name="<? /*= $UserInfoModel->fullName */ ?>"
                                                                                   data-rgnumber="<? /*= $UserInfoModel->Registration_Number */ ?>">Cancel
                                                                                    Interest
                                                                                    <i class="fa fa-close"></i> </a>

                                                                            <?php /*} else if (($Id == $Value->to_user_id && $Value->send_request_status_from_to == 'Yes' && $Value->send_request_status_to_from != 'Yes') || ($Id == $Value->from_user_id && $Value->send_request_status_to_from == 'Yes' && $Value->send_request_status_from_to != 'Yes')) {
                                                                                */ ?>
                                                                                <a href="javascript:void(0)"
                                                                                   class=" accept_decline adbtn"
                                                                                   role="button"
                                                                                   data-target="#accept_decline"
                                                                                   data-toggle="modal"
                                                                                   data-id="<? /*= $UserInfoModel->id */ ?>"
                                                                                   data-name="<? /*= $UserInfoModel->fullName */ ?>"
                                                                                   data-rgnumber="<? /*= $UserInfoModel->Registration_Number */ ?>"
                                                                                   data-type="Accept Interest">
                                                                                    Accept
                                                                                    <i class="fa fa-check"></i>
                                                                                </a>
                                                                                <a href="javascript:void(0)"
                                                                                   class="accept_decline adbtn"
                                                                                   role="button"
                                                                                   data-target="#accept_decline"
                                                                                   data-toggle="modal"
                                                                                   data-id="<? /*= $UserInfoModel->id */ ?>"
                                                                                   data-name="<? /*= $UserInfoModel->fullName */ ?>"
                                                                                   data-rgnumber="<? /*= $UserInfoModel->Registration_Number */ ?>"
                                                                                    >
                                                                                    Decline
                                                                                    <i class="fa fa-close"></i> </a>
                                                                            <?php /*} else if ($Value->send_request_status_from_to == 'Accepted' || $Value->send_request_status_to_from == 'Accepted') {
                                                                                */ ?>
                                                                                <a href="javascript:void(0)" class=""
                                                                                   role="button"
                                                                                   data-target="#" data-toggle="modal"
                                                                                   data-id="<? /*= $UserInfoModel->id */ ?>"
                                                                                   data-name="<? /*= $UserInfoModel->fullName */ ?>"
                                                                                   data-rgnumber="<? /*= $UserInfoModel->Registration_Number */ ?>"
                                                                                   data-type="Connected">Connected
                                                                                    <i class="fa fa-heart"></i> </a>
                                                                            <?php /*} else if ($Value->send_request_status_from_to == 'Rejected' || $Value->send_request_status_to_from == 'Rejected') {
                                                                                */ ?>
                                                                                <a href="javascript:void(0)" class=" "
                                                                                   role="button"
                                                                                   data-target="#" data-toggle="modal"
                                                                                   data-id="<? /*= $UserInfoModel->id */ ?>"
                                                                                   data-name="<? /*= $UserInfoModel->fullName */ ?>"
                                                                                   data-rgnumber="<? /*= $UserInfoModel->Registration_Number */ ?>"
                                                                                   data-type="Connected">Rejected <i
                                                                                        class="fa fa-close"></i> </a>

                                                                            <?php /*} else { */ ?>
                                                                                <a href="javascript:void(0)"
                                                                                   class="btn btn-link isent" role="button">Interest
                                                                                    Sent <i class="fa fa-heart"></i></a>
                                                                            <?php /*} */ ?>
                                                                        </li>

                                                                        <li>
                                                                            <a href="#" data-toggle="modal"
                                                                               class="send_email"
                                                                               <?php /*if (Yii::$app->user->isGuest) { */ ?>data-target="#sendMail"<?php /*} else { */ ?> data-id="<? /*= $SV->id */ ?>" <?php /*} */ ?>>Send
                                                                                Email <i class="fa fa-envelope-o"></i>
                                                                            </a>
                                                                        </li>

                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>-->

                                                    <?php } ?>

                                                <?php } else { ?>
                                                    <!--<div class="row gray-bg">
                                                        <div class="col-sm-12">
                                                            <div class="profile-control-vertical">
                                                                <ul class="list-unstyled pull-right">
                                                                    <li>
                                                                        <a href="javascript:void(0)" class="send_email"
                                                                           role="button"
                                                                           data-target="#sendMail" data-toggle="modal"
                                                                            >Shortlist <i class="fa fa-list-ul"></i></a>
                                                                    </li>
                                                                    <li class="s__<? /*= $SV->id */ ?>">
                                                                        <a href="javascript:void(0)" class="send_email"
                                                                           role="button"
                                                                           data-target="#sendMail" data-toggle="modal"
                                                                            >Send Interest <i class="fa fa-heart-o"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#" data-toggle="modal" class="send_email"
                                                                           data-target="#sendMail">Send Email <i
                                                                                class="fa fa-envelope-o"></i></a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>-->
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                    <?php }?>
                                </div>

                            </div>
                        </div>
                        <?php }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>


<?php
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/slider.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/cover/jquery-ui.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php

$this->registerJs('
       jQuery(document).ready(function($) {
        $("#myCarousel").carousel({
                interval: 5000
        });

        $("#carousel-text").html($("#slide-content-0").html());

        //Handles the carousel thumbnails
       $("[id^=carousel-selector-]").click( function(){
         var id = this.id.substr(this.id.lastIndexOf("-") + 1);
         var id = parseInt(id);
         $("#myCarousel").carousel(id);
       });


        // When the carousel slides, auto update the text
        $("#myCarousel").on("slid.bs.carousel", function (e) {
          var id = $(".item.active").data("slide-number");
          $("#carousel-text").html($("#slide-content-"+id).html());
        });
});
    $(document).on("click",".send_email",function(e){
        var formData = new FormData();
        formData.append("ToUserId", $(this).data("id"));
        formData.append("UserId", "' . @Yii::$app->user->identity->id . '");
        loaderStart();
         $.ajax({
                        url: "' . @Yii::$app->homeUrl . 'user/send-email-profile",
                        type: "POST",
                        data: formData,
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data, textStatus, jqXHR) {
                            loaderStop();
                            var DataObject = JSON.parse(data);
                            notificationPopup(DataObject.STATUS, DataObject.MESSAGE, DataObject.TITLE);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                                notificationPopup(\'ERROR\', \'Something went wrong. Please try again !\', \'Error\');
                                loaderStop();
                        }
         });

    });
    $(document).on("click",".send_request",function(e){
      Pace.restart();
      loaderStart();
      var formData = new FormData();
      formData.append("ToUserId", $(this).data("id"));
      formData.append("Action", "SEND_INTEREST");
      sendRequestDashboard("' . Url::to(['user/send-int-dashboard']) . '",".requests","SL",$(this).data("parentid"),formData);
    });
    $(document).on("click",".shortlistUser",function(e){
        loaderStart();
         var formData = new FormData();
        formData.append("Action", "SHORTLIST_USER");
        //formData.append("Page",  "PROFILE");
        formData.append("ToUserId", $(this).data("id"));
        formData.append("Name", $(this).data("name"));
        sendRequestDashboard("' . Url::to(['user/user-request']) . '",".requests","SLU",$(this).data("id"),formData);
    });

    $(document).on("click",".a_b_d",function(e){
      Pace.restart();
      loaderStart();
      var formData = new FormData();
      formData.append("ToUserId", $(this).data("id"));
      formData.append("Name", $(".to_name").text());
      formData.append("RGNumber", $(".to_rg_number").text());
      formData.append("Action",  $(this).data("type"));
      sendRequestDashboard("' . Url::to(['user/user-request']) . '",".requests","R_A_D_B",$(this).data("parentid"),formData);
      //sendRequest("' . Url::to(['user/user-request']) . '",".requests",formData);
    });

');
?>
<style>
    .bs label {
        font-weight: bold;
        display: block;
        position: inherit;
        color: #696767;
        height: 20px;
    }

    #amount {
        -webkit-border-radius: 2px;
        border: #c4c4c4 1px solid;
        padding: 1px;
        -moz-box-shadow: 0 0 5px #dad9d9 inset;
        -webkit-box-shadow: 0 0 5px #dad9d9 inset;
        box-shadow: 0 0 5px #dad9d9 inset;
        height: 42px;
    }

    .box {
        margin: 0px !important;
    }

    .cs-select span {
        padding: 0.4em 01em !important;
    }

    div.cs-select {
        height: 42px !important;
    }
</style>