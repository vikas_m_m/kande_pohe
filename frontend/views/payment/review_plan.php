<link href='<?= Yii::$app->homeUrl ?>css/package.css' rel='stylesheet' type='text/css'>
<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\components\CommonHelper;
use yii\helpers\ArrayHelper;
$color_array = array('PACKAGE_FREE' =>'', 'PACKAGE_SILVER' =>'green', 'PACKAGE_GOLD' =>'blue', 'PACKAGE_PLATINUM' =>'red');
$icons_array = array('PACKAGE_FREE' =>'fa fa-bullhorn','PACKAGE_SILVER'=>'fa fa-star-half-o','PACKAGE_GOLD'=>'fa fa-trophy','PACKAGE_PLATINUM' =>'fa fa-diamond');

?>
<style>
    /*.pricingTable11.active{transform:scale(1.05);z-index:1;}*/
    /*.pricingTable11.active .month,.pricingTable11.active .price-value,.pricingTable11.active .pricingTable-header i{color:#fff}*/
    /*.pricingTable11.red.active .pricingTable-signup a,.pricingTable11.red.active .pricingTable-header{background:#ff4b4b}*/
    /*.pricingTable11.blue.active .pricingTable-signup a,.pricingTable11.blue.active .pricingTable-header{background:#4b64ff}*/
    /*.pricingTable11.green.active .pricingTable-signup a,.pricingTable11.green.active .pricingTable-header{background:#40c952}*/
</style>
<main>
    <div class="container">
        <div class="row no-gutter bg-dark">
            <div class="no-gutter">
                <div class="col-md-12 col-sm-12">
                    <div class="white-section mrg-tp-20 mrg-bt-10">
                        <h3>Review Your Plan</h3>
                        <div class="demo">
                            <div class="container">
                                <!--<div class="notice kp_success"><p>Information Not Available.</p></div>-->
                                <?php if (Yii::$app->session->hasFlash('error')): ?>
                                    <div class="notice kp_error"><h4>Error!</h4>
                                        <p><?= Yii::$app->session->getFlash('error') ?></p>
                                    </div>
                                <?php endif; ?>
                                <?php if(@$subscription_model != NULL){ ?>
                                <?php }else{ ?>
                                    <div class="alert alert-kp fade in">
                                        <h4>Note:</h4>
                                        <p>Details does not available  at this moment.</p>
                                        <p>
                                            <?= html::a('<button type="button" class="btn btn-warning">Contact Us</button>', ['site/contact-us']) ?>
                                        </p>
                                    </div>
                                <?php }?>
                                <div class="row">
                                    <?php if(@$subscription_model != NULL){ ?>
                                        <?php
                                        $form = ActiveForm::begin([
                                            'id' => 'form-review-plan',
                                            'validateOnChange' => false,
                                            'action' => ['checkout-process'],
                                        ]);
                                        ?>
                                        <div class="col-md-12 ">
                                            <div class="col-sm-3 col-md-offset-2">
                                                <div class="pricingTable11 <?= $color_array[$subscription_model->short_name] ?> active">
                                                    <div class="pricingTable-header">
                                                        <i class="<?= $icons_array[$subscription_model->short_name] ?>"></i>
                                                        <div class="price-value"> <i class="fa fa-inr"></i><?= number_format($subscription_model->subscriptions_price,2)?> <span class="month"></span> </div>
                                                    </div>
                                                    <h3 class="heading"><?= stripslashes($subscription_model->subscriptions_name)?></h3>
                                                </div>
                                            </div>
                                            <div class="col-sm-7">
                                                <div class="name-panel">
                                                    <h2 class="nameplate"><?= $UserModel->FullName; ?>
                                                        <span class="font-light">
                                                (<?= CommonHelper::setInputVal($UserModel->Registration_Number, 'text') ?>)
                                                            </span>
                                                    </h2>
                                                    <hr>
                                                </div>
                                                <div class="pricing-content1">
                                                    <ul>
                                                        <li><b><?= ($subscription_model->profile_duration)?>Days</b> Profile Duration</li>
                                                        <li><b><?= ($subscription_model->no_of_contacts)?></b> No of Contacts</li>
                                                        <li><b><?= ($subscription_model->no_of_pm)?></b> Personalized Messaging </li>
                                                        <li><b><?= ($subscription_model->privacy_settings)?></b> Privacy Settings </li>
                                                        <li><b><?= ($subscription_model->validity_of_package)?>Days</b> Validity of Package</li>
                                                        <li><b><?= ($subscription_model->customer_care_support)?></b> Customer Care Support  </li>
                                                    </ul>
                                                </div>
                                                <hr>
                                                <div class="hidden">
                                                    <input type="text" name="tid" id="tid" readonly />
                                                    <input type="text" name="package_name" id="package_name" value="<?=$subscription_model->short_name?>" readonly />
                                                </div>
                                            </div>
                                            <div class="row ">

                                                <div class="col-md-3 col-md-offset-2">
                                                    <div class="form-cont">
                                                        <div class="form-cont">
                                                            <?= Html::submitButton('CONTINUE', ['class' => 'btn btn-primary mrg-tp-10 ', 'name' => 'register2']) ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-cont">
                                                        <div class="form-cont">
                                                            <?= html::a('Cancel', ['/upgrade-plan'], ["title" => 'Cancel',"class"=>"btn btn-primary mrg-tp-10"]) ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php ActiveForm::end(); ?>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php
$this->registerJs('
        window.onload = function() {
        var d = new Date().getTime();
        document.getElementById("tid").value = d;
    };
 ');
?>
