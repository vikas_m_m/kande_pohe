<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\components\CommonHelper;
use yii\helpers\ArrayHelper;
$encFile =Yii::getAlias('@app'). '/views/payment/lib/Crypto.php';
$file_status = include_once($encFile);
if(!$file_status){
    Yii::$app->session->setFlash('error', 'Something went wrong. Please try again.');
    return $this->redirect(Yii::$app->getUrlManager()->getBaseUrl() .Yii::$app->params['upgradePlan'] );
}
$host_type = Yii::$app->params['host_type'];
$working_key=Yii::$app->params['cca_working_key'.$host_type];//'42960F1D8334C78F964BC0EC52E30EB1';//Shared by CCAVENUES
$access_code=Yii::$app->params['cca_access_code'.$host_type];//'AVND02GA51CJ46DNJC';//Shared by CCAVENUES
$merchant_data='';

foreach (@$checkout_data as $key => $value){
    $merchant_data.=$key.'='.$value.'&';
}
#CommonHelper::pr(@$checkout_data);exit;
$encrypted_data=encrypt($merchant_data,$working_key); // Method for encrypting the data.

if($host_type != "_live" ){
    $production_url='https://test.ccavenue.com/transaction/transaction.do?command=initiateTransaction&encRequest='.$encrypted_data.'&access_code='.$access_code;
}else{
    #$production_url='https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction&encRequest='.$encrypted_data.'&access_code='.$access_code;
    $production_url='https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction';
}
#die($production_url);
?>
<main>
    <div class="container-fluid">
        <div class="row no-gutter bg-dark">
            <div class="no-gutter">
                <div class="col-md-10 col-md-offset-1 col-sm-12 mrg-bt-30">
                    <div class="white-section mrg-tp-20 mrg-bt-30" style="min-height: 515px;">
                        <h3>Checkout</h3>
                        <div class="demo">
                            <div class="container1">
                                <!--<div class="notice kp_success"><p>Information Not Available.</p></div>-->
                                <?php if (Yii::$app->session->hasFlash('error')): ?>
                                    <div class="notice kp_error"><h4>Error!</h4>
                                        <p><?= Yii::$app->session->getFlash('error') ?></p>
                                    </div>
                                <?php endif; ?>
                                <?php if(@$checkout_data == NULL){ ?>
                                    <div class="alert alert-kp fade in">
                                        <h4>Note:</h4>
                                        <p>Details does not available  at this moment.</p>
                                        <p>
                                            <?= html::a('<button type="button" class="btn btn-warning">Contact Us</button>', ['site/contact-us']) ?>
                                        </p>
                                    </div>
                                <?php }?>
                                <div class="mrg-tp-20 mrg-bt-10">
                                    <?php if($checkout_data != NULL){ ?>
                                    <div class="notice kp_info"><h4>Information!</h4>
                                        <p>Please wait while we are redirecting you to CcAvenue. Please do not reload page... <i class="fa fa-refresh fa-spin" style=""></i> </p>

                                    </div>
                                        <!--<iframe src="<?php /*echo $production_url*/?>" id="paymentFrame" width="80%" height="450" frameborder="0" scrolling="No" ></iframe>-->
                                        <form method="post" name="redirect" action="<?=$production_url?>">
<?php

echo "<input type=hidden name=encRequest value=$encrypted_data>";
echo "<input type=hidden name=access_code value=$access_code>";
                                        ?>
                                        </form>
                                        </center>
                                        <script language='javascript'>document.redirect.submit();</script>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php
$this->registerJs('
$(document).ready(function(){
    		 window.addEventListener("message", function(e) {
		    	 $("#paymentFrame").css("height",e.data[\'newHeight\']+\'px\');
		 	 }, false);
		});
 ');
?>
