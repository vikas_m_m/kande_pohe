<?php
use yii\helpers\Html;
use common\components\CommonHelper;
?>
<header role="header">
  <!-- over-header -->
  <div class="over-header">
    <div class="header-inner">
      <div class="container">
        <div class="row">
          <div class="col-xs-4">
            <div class="logo">
              <?php if ((in_array(Yii::$app->request->pathInfo, Yii::$app->params['publicPages']))) { ?>
                <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/" title="logo">
                  <?= Html::img('@web/images/logo-inner.png', ['width' => '235', 'height' => 83, 'alt' => 'logo']); ?>
                </a>
              <?php } else { ?>
                <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= Yii::$app->params['userDashboard'] ?>"
                   title="logo">
                  <?= Html::img('@web/images/logo-inner.png', ['width' => '235', 'height' => 83, 'alt' => 'logo']); ?>
                </a>
              <?php } ?>

            </div>
          </div>
          <div class="col-xs-8">
            <div class="primary-menu">
              <div class="navbar navbar-default" role="navigation">
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                  <div class="navbar-collapse collapse sidebar-navbar-collapse">
                    <ul class="nav navbar-nav">
                      <?php if (!Yii::$app->user->isGuest) { ?>
                        <li><?= html::a('<i class="ti-power-off m-r-5"></i>Logout</a>', [Yii::$app->params['userLogout']], ['data-method' => 'post', 'class' => 'logout']) ?></li>
                        <?php if (Yii::$app->user->identity->eEmailVerifiedStatus == 'Yes' || Yii::$app->user->identity->ePhoneVerifiedStatus == 'Yes') { ?>
                        <?php } ?>
                      <?php } else { ?>
                        <?php
                        if (@Yii::$app->request->pathInfo !='sign-up') { ?>

                          <li>
                            <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() . Yii::$app->params['PopUPLogin'] ?>"
                               title="Login" id="login_button"><strong>Login</strong></a>
                          </li>
                          <li>
                            <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() . Yii::$app->params['userSignUp'] ?>"
                               title="Sign up Free" id="suf"><strong>Register Free</strong></a>
                          </li>
                        <?php }else{ ?>
                          <li>
                            <a href="<?= Yii::$app->homeUrl ?>"
                               title="Home" id="suf"><strong>Home</strong></a>
                          </li>
                          <li>
                            <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() . Yii::$app->params['PopUPLogin'] ?>"
                               title="Login" id="login_button"><strong>Login</strong></a>
                          </li>
                        <?php }?>
                        <!--<li>
                    <a href="<?/*= Yii::$app->getUrlManager()->getBaseUrl() . Yii::$app->params['PopUPSignUp'] */?>"
                       title="Sign up Free" id="suf">Sign up Free</a>
                  </li>-->
                      <?php } ?>
                    </ul>
                  </div>
                  <!--/.nav-collapse -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>