<?php
  use yii\helpers\Html;
  #echo Yii::getAlias('@web');
  #echo "==>".Yii::$app->homeUrl;
  #Yii::$app->getUrlManager()->getBaseUrl() . Yii::$app->params['PopUPLogin']
?>

<header role="header">
  <!-- over-header -->
  <div class="over-header">
    <div class="header-inner">
      <div class="container">
        <div class="row">
          <div class="col-xs-4">
            <!--<div class="logo"><a href="<?/*= Yii::$app->homeUrl */?>" title="logo">
                <?/*= Html::img('@web/images/logo-inner.png', ['width' => '235', 'height' => 83, 'alt' => 'logo', 'title' => 'Kande Pohe']); */?> </a>
            </div>-->
            <div class="logo">
              <?php if ((in_array(Yii::$app->request->pathInfo, Yii::$app->params['publicPages']))) { ?>
                <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/" title="logo">
                  <?= Html::img('@web/images/logo-inner.png', ['width' => '235', 'height' => 83, 'alt' => 'logo']); ?>
                </a>
              <?php } else { ?>
                <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= Yii::$app->params['userDashboard'] ?>"
                   title="logo">
                  <?= Html::img('@web/images/logo-inner.png', ['width' => '235', 'height' => 83, 'alt' => 'logo']); ?>
                </a>
              <?php } ?>

            </div>
          </div>
          <div class="col-xs-8">
            <div class="help pull-right">
              <ul class="list-inline">
                <?php if (!Yii::$app->user->isGuest) { ?>
                  <li><?= html::a('<i class="ti-power-off m-r-5"></i>Logout</a>', [Yii::$app->params['userLogout']], ['data-method' => 'post', 'class' => 'logout']) ?></li>
                  <?php if (Yii::$app->user->identity->eEmailVerifiedStatus == 'Yes' || Yii::$app->user->identity->ePhoneVerifiedStatus == 'Yes') { ?>
                  <?php } ?>
                <?php } else { ?>
                  <?php
                  if (@Yii::$app->request->pathInfo !='sign-up') { ?>

                  <li>
                    <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() . Yii::$app->params['PopUPLogin'] ?>"
                       title="Login" id="login_button"><strong>Login</strong></a>
                  </li>
                  <li>
                    <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() . Yii::$app->params['userSignUp'] ?>"
                       title="Sign up Free" id="suf"><strong>Register Free</strong></a>
                  </li>
                <?php }else{ ?>
                  <li>
                    <a href="<?= Yii::$app->homeUrl ?>"
                       title="Home" id="suf"><strong>Home</strong></a>
                  </li>
                    <li>
                      <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() . Yii::$app->params['PopUPLogin'] ?>"
                         title="Login" id="login_button"><strong>Login</strong></a>
                    </li>
                    <?php }?>
                  <!--<li>
                    <a href="<?/*= Yii::$app->getUrlManager()->getBaseUrl() . Yii::$app->params['PopUPSignUp'] */?>"
                       title="Sign up Free" id="suf">Sign up Free</a>
                  </li>-->
                <?php } ?>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>