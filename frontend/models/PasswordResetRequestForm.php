<?php
namespace frontend\models;

use common\components\CommonHelper;
use common\components\MailHelper;
use common\components\SmsHelper;
use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;
    public $First_Name;
    public $Last_Name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            //['email', 'required', 'message' => 'Please enter your email address.'],
            ['email', 'required', 'message' => 'Please enter your email address or phone number.'],
            //['email', 'email'], #TODO REVERT
            /*['email', 'exist',
                'targetClass' => '\common\models\User',
                #'filter' => ['status' => User::STATUS_ACTIVE],
                'filter' => ['status' => [User::STATUS_ACTIVE, User::STATUS_APPROVE]],
                'message' => Yii::$app->params['resetPasswordWrongEmailIdMessage'],//'There is no user with such email.'
            ],*/
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            'status' => [User::STATUS_ACTIVE, User::STATUS_APPROVE],
            'email' => $this->email,
        ]);

        if (!$user) {
            return false;
        }
        
        $user->scenario = User::SCENARIO_SFP;
        if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                return false;
            }
        }
        $resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
        $MAIL_DATA = array("EMAIL" => $this->email, "EMAIL_TO" => $this->email, "NAME" => $user->FullName, "LINK" => $resetLink);
        return MailHelper::SendMail('FORGOT_PASSWORD', $MAIL_DATA);
        /*return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
            ->setTo($this->email)
            ->setSubject('Password reset for ' . Yii::$app->name)
            ->send();*/
    }
    public function sendForgotPasswordOTP($otp)
    {
        /* @var $user User */
        $user = User::findOne([
            'status' => [User::STATUS_ACTIVE, User::STATUS_APPROVE],
            'Mobile' => $this->email,
        ]);
        if (!$user) {
            return false;
        }
        #$user->scenario = User::SCENARIO_FP_OTP;

        if ($user) {
            $user->generatePasswordResetToken();
            #$user->password_reset_token = $token;
            $user->password_reset_otp = $otp;
            if (!$user->save()) {
                return false;
            }
        }
        $SMSArray = array('OTP'=>$otp,'NAME'=>$user->FullName);
        $SMS_FLAG = SmsHelper::SendSMS($user->Mobile,'FORGOT_PASSWORD_OTP',$SMSArray );
        return $user->password_reset_token;
    }

}
