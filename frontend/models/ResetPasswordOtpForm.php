<?php
namespace frontend\models;

use common\components\CommonHelper;
use yii\base\Model;
use yii\base\InvalidParamException;
use common\models\User;

/**
 * Password reset form
 */
class ResetPasswordOtpForm extends Model
{
    public $password;
    public $repassword;
    public $email;
    public $password_otp;

    /**
     * @var \common\models\User
     */
    private $_user;


    /**
     * Creates a form model given a token.
     *
     * @param string $token
     * @param array $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($token, $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException('Password reset token cannot be blank.');
        }
        $this->_user = User::findByPasswordResetOTPToken($token);
        if (!$this->_user) {
            throw new InvalidParamException('Your password reset request expired.');
        }
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password'], 'required', 'message' => 'Please create your New password.'],
            [['repassword'], 'required', 'message' => 'Please re-type your New password.'],
            [['password_otp'], 'required', 'message' => 'Please enter your OTP.'],
             [['password','repassword'], 'string', 'min' => 6],
             [['password_otp'], 'string', 'min' => 4],
            [['password', 'repassword'], 'string', 'length' => [6, 255]],
            [['repassword'], 'compare', 'compareAttribute' => 'password', 'message' => "New password and Retype Password is not matching. Please try again."],
        ];
    }

    /**
     * Resets password.
     *
     * @return boolean if password was reset.
     */
    public function resetPassword()
    {
        $user = $this->_user;
        $user->password_hash = $user->setPassword($this->password);
        $user->removePasswordResetToken();
        $user->removePasswordResetOTPToken();

        return $user->save(false);
    }
    public function getCurrentUser()
    {
        $user = $this->_user;
        return $user;
    }
}
