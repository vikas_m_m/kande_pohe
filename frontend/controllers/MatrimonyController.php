<?php

namespace frontend\controllers;

use common\components\MessageHelper;
use common\components\SmsHelper;
use common\models\Cities;
use common\models\otherlibraries\Compressimage;
use common\models\SiteCms;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
#use frontend\components\CommonHelper;
use common\components\CommonHelper;
use common\components\MailHelper;
use common\models\User;

//use common\models\PartenersReligion;
//use common\models\PartnersMaritalStatus;
use common\models\UserPartnerPreference;

use common\models\PartnersAnnualIncome;
use common\models\PartnersBodyType;
use common\models\PartnersCharan;
use common\models\PartnersCities;
use common\models\PartnersCommunity;
use common\models\PartnersCountries;
use common\models\PartnersDiet;
use common\models\PartnersDrink;
use common\models\PartnersFamilyAffluenceLevel;
use common\models\PartnersFamilyType;
use common\models\PartnersFavouriteCousines;
use common\models\PartnersFavouriteMusic;
use common\models\PartnersFavouriteReads;
use common\models\PartnersFitnessActivities;
use common\models\PartnersInterest;
use common\models\PartnersMothertongue;
use common\models\PartnersNadi;
use common\models\PartnersNakshtra;
use common\models\PartnersPreferredDressType;
use common\models\PartnersPreferredMovies;
use common\models\PartnersRaashi;
use common\models\PartnersReligion;
use common\models\PartnersSkinTone;
use common\models\PartnersSmoke;
use common\models\PartnersSpectacles;
use common\models\PartnersStates;
use common\models\PartnersSubcommunity;
use common\models\PartnerWorkingAs;
use common\models\PartnerWorkingWith;
use common\models\PartenersReligion;
//use common\models\UserPartnerPreference;
use common\models\PartnersMaritalStatus;
use common\models\PartnersGotra;
use common\models\PartnersFathersStatus;
use common\models\PartnersMothersStatus;
use common\models\PartnersEducationalLevel;
use common\models\PartnersEducationField;

use common\models\SiteMetaTag;
use common\models\UserPhotos;

use yii\widgets\ActiveForm;
use yii\web\Response;


/**
 * Matrimony controller
 */
class MatrimonyController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex($slug  = '')
    {
        return $this->redirect(Yii::getAlias('@web'));
    }


    public function actionCity($categories = null)
    {

        #$record = SiteMetaTag::findOne(['type'=>$slug]);
       // $slug = Yii::$app->request->get('id');

        $slug = $categories;
       # echo " hii => ", Yii::$app->request->get('slug');exit;
        #$record = SiteMetaTag::findOne(['type'=>$slug]);
        // $slug = Yii::$app->request->get('id');
        $record = SiteMetaTag::find()->where(['site_url' => $slug,'type'=>SiteMetaTag::CITY])->one();
        #echo "count => ".count($record);
        #CommonHelper::pr($record->type);
        #CommonHelper::pr($record);exit;
        $record_list_Male = array();
        $record_list_Female = array();
        $photos_male = array();
        $photos_female = array();
        if(count($record)>0){
            if($record->ref_table_id != NULL ) {
                $record_list_Male = $this->getSearchList($record, 'MALE');
                $record_list_Female = $this->getSearchList($record, 'FEMALE');
                if (count($record_list_Male)) {
                    foreach ($record_list_Male as $SK => $SV) {
                        $photos_male[$SV->id] = $this->getPhotoList($SV->id);
                    }
                }
                if (count($record_list_Female)) {
                    foreach ($record_list_Female as $SK => $SV) {
                        $photos_female[$SV->id] = $this->getPhotoList($SV->id);
                    }
                }
            }
        }else{
        }
        return $this->render('matrimony', [
            # 'model' => $model,
            'photos_male' => $photos_male,
            'photos_female' => $photos_female,
            'slug_record' => $record,
            'slug' => $slug,
            'record_list_Male' => $record_list_Male,
            'record_list_Female' => $record_list_Female,
        ]);
    }
    public function actionEducation($categories = null)
    {

    	

        #$record = SiteMetaTag::findOne(['type'=>$slug]);
       // $slug = Yii::$app->request->get('id');

        $slug = $categories;

        $record = SiteMetaTag::find()->where(['site_url' => $slug,'type'=>SiteMetaTag::EDUCATION])->one();
       
        #echo "count => ".count($record);
        #CommonHelper::pr($record->type);
        #CommonHelper::pr($record);exit;
        $record_list_Male = array();
        $record_list_Female = array();
        $photos_male = array();
        $photos_female = array();
        if(count($record)>0){
            if($record->ref_table_id != NULL ) {
                $record_list_Male = $this->getSearchList($record, 'MALE');
                $record_list_Female = $this->getSearchList($record, 'FEMALE');
                if (count($record_list_Male)) {
                    foreach ($record_list_Male as $SK => $SV) {
                        $photos_male[$SV->id] = $this->getPhotoList($SV->id);
                    }
                }
                if (count($record_list_Female)) {
                    foreach ($record_list_Female as $SK => $SV) {
                        $photos_female[$SV->id] = $this->getPhotoList($SV->id);
                    }
                }
            }
        }else{
        }
        return $this->render('matrimony', [
            # 'model' => $model,
            'photos_male' => $photos_male,
            'photos_female' => $photos_female,
            'slug_record' => $record,
            'slug' => $slug,
            'record_list_Male' => $record_list_Male,
            'record_list_Female' => $record_list_Female,
        ]);
    }
    public function actionOccupation($categories = null)
    {

       # echo " hii => ", Yii::$app->request->get('slug');exit;
        #$record = SiteMetaTag::findOne(['type'=>$slug]);
        // $slug = Yii::$app->request->get('id');

        $slug = $categories;

        $record = SiteMetaTag::find()->where(['site_url' => $slug,'type'=>SiteMetaTag::OCCUPATION])->one();
        #echo "count => ".count($record);
        #CommonHelper::pr($record->type);
        #CommonHelper::pr($record);exit;
        $record_list_Male = array();
        $record_list_Female = array();
        $photos_male = array();
        $photos_female = array();
        if(count($record)>0){
            if($record->ref_table_id != NULL ) {
                $record_list_Male = $this->getSearchList($record, 'MALE');
                $record_list_Female = $this->getSearchList($record, 'FEMALE');
                if (count($record_list_Male)) {
                    foreach ($record_list_Male as $SK => $SV) {
                        $photos_male[$SV->id] = $this->getPhotoList($SV->id);
                    }
                }
                if (count($record_list_Female)) {
                    foreach ($record_list_Female as $SK => $SV) {
                        $photos_female[$SV->id] = $this->getPhotoList($SV->id);
                    }
                }
            }
        }else{
        }
        return $this->render('matrimony', [
            # 'model' => $model,
            'photos_male' => $photos_male,
            'photos_female' => $photos_female,
            'slug_record' => $record,
            'slug' => $slug,
            'record_list_Male' => $record_list_Male,
            'record_list_Female' => $record_list_Female,
        ]);
    }
    public function actionPhysicalStatus($categories = null)
    {


       # echo " hii => ", Yii::$app->request->get('slug');exit;
        #$record = SiteMetaTag::findOne(['type'=>$slug]);
        // $slug = Yii::$app->request->get('id');
        $slug = $categories;
        $record = SiteMetaTag::find()->where(['site_url' => $slug,'type'=>SiteMetaTag::PHYSICAL_STATUS])->one();
        #echo "count => ".count($record);
        #CommonHelper::pr($record->type);
        #CommonHelper::pr($record);exit;
        $record_list_Male = array();
        $record_list_Female = array();
        $photos_male = array();
        $photos_female = array();
        if(count($record)>0){
            if($record->ref_table_id != NULL ) {
                $record_list_Male = $this->getSearchList($record, 'MALE');
                $record_list_Female = $this->getSearchList($record, 'FEMALE');
                if (count($record_list_Male)) {
                    foreach ($record_list_Male as $SK => $SV) {
                        $photos_male[$SV->id] = $this->getPhotoList($SV->id);
                    }
                }
                if (count($record_list_Female)) {
                    foreach ($record_list_Female as $SK => $SV) {
                        $photos_female[$SV->id] = $this->getPhotoList($SV->id);
                    }
                }
            }
        }else{
        }
        return $this->render('matrimony', [
            # 'model' => $model,
            'photos_male' => $photos_male,
            'photos_female' => $photos_female,
            'slug_record' => $record,
            'slug' => $slug,
            'record_list_Male' => $record_list_Male,
            'record_list_Female' => $record_list_Female,
        ]);
    }
    public function actionMaritalStatus($categories = null)
    {

       # echo " hii => ", Yii::$app->request->get('slug');exit;
        #$record = SiteMetaTag::findOne(['type'=>$slug]);
        $slug = $categories;//Yii::$app->request->get('id');
        $record = SiteMetaTag::find()->where(['site_url' => $slug,'type'=>SiteMetaTag::MARITAL_STATUS])->one();
        #echo "count => ".count($record);
        #CommonHelper::pr($record->type);
        #CommonHelper::pr($record);exit;
        $record_list_Male = array();
        $record_list_Female = array();
        $photos_male = array();
        $photos_female = array();
        if(count($record)>0){
            if($record->ref_table_id != NULL ) {
                $record_list_Male = $this->getSearchList($record, 'MALE');
                $record_list_Female = $this->getSearchList($record, 'FEMALE');
                if (count($record_list_Male)) {
                    foreach ($record_list_Male as $SK => $SV) {
                        $photos_male[$SV->id] = $this->getPhotoList($SV->id);
                    }
                }
                if (count($record_list_Female)) {
                    foreach ($record_list_Female as $SK => $SV) {
                        $photos_female[$SV->id] = $this->getPhotoList($SV->id);
                    }
                }
            }
        }else{
        }
        return $this->render('matrimony', [
            # 'model' => $model,
            'photos_male' => $photos_male,
            'photos_female' => $photos_female,
            'slug_record' => $record,
            'slug' => $slug,
            'record_list_Male' => $record_list_Male,
            'record_list_Female' => $record_list_Female,
        ]);
    }
    public function actionCaste($categories = null)
    {

       # echo " hii => ", Yii::$app->request->get('slug');exit;
        #$record = SiteMetaTag::findOne(['type'=>$slug]);
        // $slug = Yii::$app->request->get('id');
        $slug = $categories;
        $record = SiteMetaTag::find()->where(['site_url' => $slug,'type'=>SiteMetaTag::CASTE])->one();
        #echo "count => ".count($record);
       # CommonHelper::pr($record->type);exit;
        #CommonHelper::pr($record);exit;
        $record_list_Male = array();
        $record_list_Female = array();
        $photos_male = array();
        $photos_female = array();
        if(count($record)>0){
            if($record->ref_table_id != NULL ) {
                $record_list_Male = $this->getSearchList($record, 'MALE');
                $record_list_Female = $this->getSearchList($record, 'FEMALE');
                if (count($record_list_Male)) {
                    foreach ($record_list_Male as $SK => $SV) {
                        $photos_male[$SV->id] = $this->getPhotoList($SV->id);
                    }
                }
                if (count($record_list_Female)) {
                    foreach ($record_list_Female as $SK => $SV) {
                        $photos_female[$SV->id] = $this->getPhotoList($SV->id);
                    }
                }
            }
        }else{
        }
        return $this->render('matrimony', [
            # 'model' => $model,
            'photos_male' => $photos_male,
            'photos_female' => $photos_female,
            'slug_record' => $record,
            'slug' => $slug,
            'record_list_Male' => $record_list_Male,
            'record_list_Female' => $record_list_Female,
        ]);
    }

    public function actionMatrimony($slug='')
    {
        #$record = SiteMetaTag::findOne(['type'=>$slug]);
        $record = SiteMetaTag::find()->where(['site_url' => $slug])->one();
        #echo "count => ".count($record);
        #CommonHelper::pr($record->type);
        #CommonHelper::pr($record);exit;
        $record_list_Male = array();
        $record_list_Female = array();
        $photos_male = array();
        $photos_female = array();
        if(count($record)>0){
            #$Model = User::searchBasic($WHERE, $Offset, $Limit);
            #$PreferencesLocation = User::getSearchByPopularCriteria($Gender, $record->type, $record->ref_table_id,10);
            if($record->ref_table_id != NULL ) {
                $record_list_Male = $this->getSearchList($record, 'MALE');
                $record_list_Female = $this->getSearchList($record, 'FEMALE');
                if (count($record_list_Male)) {
                    foreach ($record_list_Male as $SK => $SV) {
                        $photos_male[$SV->id] = $this->getPhotoList($SV->id);
                    }
                }
                if (count($record_list_Female)) {
                    foreach ($record_list_Female as $SK => $SV) {
                        $photos_female[$SV->id] = $this->getPhotoList($SV->id);
                    }
                }
            }


            # CommonHelper::pr($record_list_Male);exit;
            # CommonHelper::pr($record_list_Female);exit;
        }else{
            # echo " in else";exit;
        }

        #exit;
        /*if (!Yii::$app->user->isGuest) {
            return $this->redirect(Yii::getAlias('@web'));
        }*/
        # $model = new User();
        #$model->scenario = User::SCENARIO_REGISTER;
        #CommonHelper::pr($record);
        return $this->render('matrimony', [
            # 'model' => $model,
            'photos_male' => $photos_male,
            'photos_female' => $photos_female,
            'slug_record' => $record,
            'slug' => $slug,
            'record_list_Male' => $record_list_Male,
            'record_list_Female' => $record_list_Female,
        ]);
    }
    public function getSearchList($record, $gender){

        $WhereId = '';
        $WHERE  = '';
        if (!Yii::$app->user->isGuest) {
            $Id = Yii::$app->user->identity->id;
            $WhereId = " AND user.id != " . $Id;
        }
        $WHERE .= " AND user.Gender = '".$gender."'";
        if($record->type == SiteMetaTag::CITY){
            $temp_Id = $record->ref_table_id;
            $WHERE .= ($temp_Id != '') ? ' AND user.iCityId IN (' . $temp_Id . ')' : '';
        }else  if($record->type == SiteMetaTag::EDUCATION){
            $temp_Id = $record->ref_table_id;
            $WHERE .= ($temp_Id != '') ? ' AND user.iEducationFieldID IN (' . $temp_Id . ')' : '';
        }else  if($record->type == SiteMetaTag::OCCUPATION){ #TODO
            $temp_Id = $record->ref_table_id;
            $WHERE .= ($temp_Id != '') ? ' AND user.iWorkingWithID IN (' . $temp_Id . ')' : '';
        }else  if($record->type == SiteMetaTag::PHYSICAL_STATUS){ #TODO
            $temp_Id = $record->ref_table_id;
            $WHERE .= ($temp_Id != '') ? ' AND user.vDisability = "' . $temp_Id . '" ' : '';
        }else if($record->type == SiteMetaTag::MARITAL_STATUS){
            $temp_Id = $record->ref_table_id;
            $WHERE .= ($temp_Id != '') ? ' AND user.iMaritalStatusID IN (' . $temp_Id . ')' : '';
        } else if ($record->type == SiteMetaTag::CASTE) {
            $temp_Id = $record->ref_table_id;
            $WHERE .= ($temp_Id != '') ? ' AND user.iCommunity_ID IN (' . $temp_Id . ')' : '';
        }

        $WHERE .= $WhereId;
        $record_list = User::searchBasic($WHERE, 0, 10);
        return $record_list;
    }

    public function getPhotoList($Id)
    {
        $UserPhotoModel = new UserPhotos();
        #$PhotoList = $UserPhotoModel->findByUserId($Id);
        $PhotoList = $UserPhotoModel->userPhotoList($Id);
        if (count($PhotoList)) {
            $Photos = $PhotoList;
        } else {
            $Photos = CommonHelper::getUserDefaultPhoto();
        }
        return $Photos;
    }
}