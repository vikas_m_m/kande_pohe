<?php

namespace frontend\controllers;

use common\components\CommonHelper;

use common\models\UserPhotos;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


use common\models\User;
use common\models\UserPartnerPreference;
use common\components\MailHelper;
use common\models\EmailQueue;
use common\models\PartnersMaritalStatus;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set('max_execution_time', 300);
error_reporting(E_ALL);
/**
 * Site controller
 */
class CronController extends Controller
{
    /**
     * @inheritdoc
     */
    /*public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }*/
    public $STATUS;
    public $MESSAGE;
    public $TITLE;



    public function beforeAction($action) {
        //$this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],

        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->redirect(Yii::getAlias('@web'));

    }
    #https://localhost/kande_pohe_cr4/cron/new-registered-members-to-female
    public function actionNewRegisteredMembersToFemale()
    {
        #$Gender = (Yii::$app->user->identity->Gender == 'MALE') ? 'FEMALE' : 'MALE';
        $RecentlyJoinedMembers = User::cronFindRecentJoinedUserList('MALE',5);
        #CommonHelper::pr($RecentlyJoinedMembers); exit;
        if(count(@$RecentlyJoinedMembers)>0){
            $all_female_active_profile = User::cronFindRecentJoinedUserList('FEMALE');
            #CommonHelper::pr($all_femail_active_profile); exit;
            $c = new CommonHelper();
            if(count($RecentlyJoinedMembers) > 0){
                if(count($RecentlyJoinedMembers) > 0){
                    foreach($all_female_active_profile as $k1=>$v1){
                        $UserModel = User::findOne($v1->id);
                        #  CommonHelper::pr($v1 ); exit;
                        $mail_html = '';
                        $email_to = $UserModel->email;
                        $FullName = $UserModel->FullName;
                        foreach($RecentlyJoinedMembers as $k=>$v){
                            $id = $v->id;
                            $Model = User::findOne($id);
                            $age = $c->getAge($Model->DOB);
                            $mFullName = $Model->FullName;
                            $height = CommonHelper::setInputVal(@$Model->height->vName, 'text');
                            $religionName = CommonHelper::setInputVal(@$Model->religionName->vName, 'text');
                            $motherTongue = CommonHelper::setInputVal(@$Model->motherTongue->Name, 'text');
                            $communityName = CommonHelper::setInputVal(@$Model->communityName->vName, 'text');
                            $cityName = CommonHelper::setInputVal(@$Model->cityName->vCityName, 'text');
                            $countryName = CommonHelper::setInputVal(@$Model->countryName->vCountryName, 'text');
                            $educationLevelName = CommonHelper::setInputVal(@$Model->educationLevelName->vEducationLevelName, 'text');
                            $workingAsName = CommonHelper::setInputVal(@$Model->workingAsName->vWorkingAsName, 'text');
                            $vNativePlaceCA = CommonHelper::setInputVal(@$Model->vNativePlaceCA, 'text');
                            $a  = CommonHelper::setInputVal(@$Model->tYourSelf, 'text');
                            $dot ='';
                            if(strlen($a ) > 100){
                                $dot ='...';
                            }
                            $tYourSelf =  $c->truncate($a, 100).$dot;
                            #CommonHelper::pr($vNativePlaceCA); exit;
                            $PHOTO = CommonHelper::getPhotos('USER', $id, "200" . $Model->propic, 200, '', 'Yes');
                            $LINK = CommonHelper::getSiteUrl('FRONTEND', 1) . 'user/profile?uk=' . $Model->Registration_Number."&source=profile_viewed_by&e_id=".base64_encode($email_to);
                            $PHOTO = '<img src="' . $PHOTO . '" width="200"  alt="Profile Photo">';
                            $mail_html .= '
<hr>
                    <table border="0" cellpadding="1" cellspacing="1" style="width:100%">
                    <tbody>
                        <tr>
                            <td><span style="color:#ff0000"><strong>&nbsp;<a href="'.$LINK.'" style="color:#ea0b44;text-decoration: none; ">'.$mFullName.'</a></strong></span></td>
                            <td style="text-align:right"><span style="color:#ff0000"><span style="color:#333333">'.date('d-m-Y').'</span></span></td>
                        </tr>
                    </tbody>
                </table>
                <table style="width:100%">
                    <tbody>
                        <tr>
                            <td>Age</td>
                            <td>: '.$age.'</td>
                            <td rowspan="8" style="text-align:right"><a href="'.$LINK.'">'.$PHOTO.'</a></td>
                        </tr>
                         <tr>
                            <td>Education</td>
                            <td>: '.$educationLevelName.'</td>
                        </tr>
                        <tr>
                            <td>Height</td>
                            <td>: '.$height.'</td>
                        </tr>
                        <tr>
                            <td>Cast</td>
                            <td>: '.$religionName.'</td>
                        </tr>
                        <tr>
                            <td>Profession</td>
                            <td>: '.$workingAsName.'</td>
                        </tr>
                        <tr>
                            <td>Location</td>
                            <td>: '.$cityName . ', ' . $countryName.'</td>
                        </tr>
                        <tr>
                            <td>Native Place</td>
                            <td>: '.ucwords($vNativePlaceCA).'</td>
                        </tr>
                    </tbody>
                </table>
<p>&nbsp;</p>

<p><a href="'.$LINK.'" style="background: #ea0b44; outline: none; color: #fff; border-color: #204d74; margin-top: 10px; padding: 19px 29px; border-radius: 2px; box-shadow: none; border: none; font-size: 16px; font-family: \'Open Sans\', sans-serif; font-weight: bold; text-decoration: none; cursor: pointer;">View His Full Profile </a></p>
		';
                            $mail_html .= ' <br> ';
                        }
                        $view_all_link = CommonHelper::getSiteUrl('FRONTEND', 1) . 'search/basic-search?ref='.Yii::$app->params['ref']['recently_joined']."&e_id=".base64_encode($email_to) ;

                        $mail_html .= '<br> <br> <hr><br> <p><a href="'.$view_all_link.'" style="background: #FFC107; outline: none; color: #fff; border-color: #204d74; margin-top: 10px;  margin-left: 30%;padding: 19px 29px; border-radius: 2px; box-shadow: none; border: none; font-size: 16px; font-family: \'Open Sans\', sans-serif; font-weight: bold; text-decoration: none; cursor: pointer; text-align:right;">View All Latest Profiles </a></p> ';

                        $MAIL_DATA = array("EMAIL_TO" => $email_to, "NAME" => $FullName,"MEMBER_PROFILE_LIST" => $mail_html);
                        $MAIL_STATUS = MailHelper::SendMail('NOTIFICATION_PROFILE_OF_GROOM', $MAIL_DATA,'',1);
                        # CommonHelper::pr($MAIL_STATUS );exit;
                        $model_eq = new EmailQueue;
                        $model_eq->eq_type = 'WEEKLY-NOTIFICATION';
                        $model_eq->eq_to_email_id = $MAIL_STATUS['to_mail'];
                        #$model_eq->eq_from_email_id = $MAIL_STATUS['from_mail'];
                        $model_eq->eq_subject = $MAIL_STATUS['mail_subject'];
                        $model_eq->eq_body = $MAIL_STATUS['mail_message'];
                        $model_eq->eq_to_name = $MAIL_STATUS['from_name'];
                        $model_eq->eq_to_user_id = $v1->id;
                        $model_eq->eq_send_status = 'PENDING';
                        $model_eq->eq_dt_added = date('Y-m-d H:i:s');
                        if($model_eq->save()){
                            #die("in ");
                            echo "<br><span style='color:green'> Mail Inserted successfully  </span> ";
                        }else{
                            #die("else");
                            echo "<br><span style='color:red'> Mail Inserted Failed  </span> ";
                        }
                    }
                }
            }
        }
    }
    #https://localhost/kande_pohe_cr4/cron/new-registered-members-to-male
    public function actionNewRegisteredMembersToMale()
    {
        #TODO VIEW ALL AND LOGIN
        #$Gender = (Yii::$app->user->identity->Gender == 'MALE') ? 'FEMALE' : 'MALE';
        $RecentlyJoinedMembers = User::cronFindRecentJoinedUserList('FEMALE',5);
        #CommonHelper::pr($RecentlyJoinedMembers); exit;
        if(count(@$RecentlyJoinedMembers)>0){
            $all_male_active_profile = User::cronFindRecentJoinedUserList('MALE');
            # CommonHelper::pr($all_male_active_profile); exit;
            $c = new CommonHelper();
            if(count($RecentlyJoinedMembers) > 0){
                foreach($all_male_active_profile as $k1=>$v1){
                    $UserModel = User::findOne($v1->id);
                    #  CommonHelper::pr($v1 ); exit;
                    $mail_html = '';
                    $email_to = $UserModel->email;
                    $FullName = $UserModel->FullName;
                    foreach($RecentlyJoinedMembers as $k=>$v){
                        $id = $v->id;
                        $Model = User::findOne($id);
                        $age = $c->getAge($Model->DOB);
                        $mFullName = $Model->FullName;
                        $height = CommonHelper::setInputVal(@$Model->height->vName, 'text');
                        $religionName = CommonHelper::setInputVal(@$Model->religionName->vName, 'text');
                        $motherTongue = CommonHelper::setInputVal(@$Model->motherTongue->Name, 'text');
                        $communityName = CommonHelper::setInputVal(@$Model->communityName->vName, 'text');
                        $cityName = CommonHelper::setInputVal(@$Model->cityName->vCityName, 'text');
                        $countryName = CommonHelper::setInputVal(@$Model->countryName->vCountryName, 'text');
                        $educationLevelName = CommonHelper::setInputVal(@$Model->educationLevelName->vEducationLevelName, 'text');
                        $workingAsName = CommonHelper::setInputVal(@$Model->workingAsName->vWorkingAsName, 'text');
                        $vNativePlaceCA = CommonHelper::setInputVal(@$Model->vNativePlaceCA, 'text');
                        $a  = CommonHelper::setInputVal(@$Model->tYourSelf, 'text');
                        $dot ='';
                        if(strlen($a ) > 100){
                            $dot ='...';
                        }
                        $tYourSelf =  $c->truncate($a, 100).$dot;
                        #CommonHelper::pr($vNativePlaceCA); exit;
                        $PHOTO = CommonHelper::getPhotos('USER', $id, "200" . $Model->propic, 200, '', 'Yes');
                        $LINK = CommonHelper::getSiteUrl('FRONTEND', 1) . 'user/profile?uk=' . $Model->Registration_Number."&source=profile_viewed_by&e_id=".base64_encode($email_to);
                        $PHOTO = '<img src="' . $PHOTO . '" width="200"  alt="Profile Photo">';
                        $mail_html .= '
<hr>
                    <table border="0" cellpadding="1" cellspacing="1" style="width:100%">
                    <tbody>
                        <tr>
                            <td><span style="color:#ff0000"><strong>&nbsp;<a href="'.$LINK.'" style="color:#ea0b44;text-decoration: none; ">'.$mFullName.'</a></strong></span></td>
                            <td style="text-align:right"><span style="color:#ff0000"><span style="color:#333333">'.date('d-m-Y').'</span></span></td>
                        </tr>
                    </tbody>
                </table>

                <table style="width:100%">
                    <tbody>
                        <tr>
                            <td>Age</td>
                            <td>: '.$age.'</td>
                            <td rowspan="8" style="text-align:right"><a href="'.$LINK.'">'.$PHOTO.'</a></td>
                        </tr>
                         <tr>
                            <td>Education</td>
                            <td>: '.$educationLevelName.'</td>
                        </tr>
                        <tr>
                            <td>Height</td>
                            <td>: '.$height.'</td>
                        </tr>
                        <tr>
                            <td>Cast</td>
                            <td>: '.$religionName.'</td>
                        </tr>
                        <tr>
                            <td>Profession</td>
                            <td>: '.$workingAsName.'</td>
                        </tr>
                        <tr>
                            <td>Location</td>
                            <td>: '.$cityName . ', ' . $countryName.'</td>
                        </tr>
                        <tr>
                            <td>Native Place</td>
                            <td>: '.ucwords($vNativePlaceCA).'</td>
                        </tr>
                    </tbody>
                </table>
<p>&nbsp;</p>

<p><a href="'.$LINK.'" style="background: #ea0b44; outline: none; color: #fff; border-color: #204d74; margin-top: 10px; padding: 19px 29px; border-radius: 2px; box-shadow: none; border: none; font-size: 16px; font-family: \'Open Sans\', sans-serif; font-weight: bold; text-decoration: none; cursor: pointer;">View Her Full Profile </a></p>
		';
                        $mail_html .= ' <br> ';
                    }
                    $view_all_link = CommonHelper::getSiteUrl('FRONTEND', 1) . 'search/basic-search?ref='.Yii::$app->params['ref']['recently_joined']."&e_id=".base64_encode($email_to) ;

                    $mail_html .= '<br> <br> <hr><br> <p><a href="'.$view_all_link.'" style="background: #FFC107; outline: none; color: #fff; border-color: #204d74; margin-top: 10px;  margin-left: 30%;padding: 19px 29px; border-radius: 2px; box-shadow: none; border: none; font-size: 16px; font-family: \'Open Sans\', sans-serif; font-weight: bold; text-decoration: none; cursor: pointer; text-align:right;">View All Latest Profiles </a></p> ';

                    $MAIL_DATA = array("EMAIL_TO" => $email_to, "NAME" => $FullName,"MEMBER_PROFILE_LIST" => $mail_html);
                    $MAIL_STATUS = MailHelper::SendMail('NOTIFICATION_PROFILE_OF_GROOM', $MAIL_DATA,'',1);
                    #CommonHelper::pr($MAIL_STATUS );exit;
                    $model_eq = new EmailQueue;
                    $model_eq->eq_type = 'WEEKLY-NOTIFICATION';
                    $model_eq->eq_to_email_id = $MAIL_STATUS['to_mail'];
                    #$model_eq->eq_from_email_id = $MAIL_STATUS['from_mail'];
                    $model_eq->eq_subject = $MAIL_STATUS['mail_subject'];
                    $model_eq->eq_body = $MAIL_STATUS['mail_message'];
                    $model_eq->eq_to_name = $MAIL_STATUS['from_name'];
                    $model_eq->eq_to_user_id = $v1->id;
                    $model_eq->eq_send_status = 'PENDING';
                    $model_eq->eq_dt_added = date('Y-m-d H:i:s');
                    if($model_eq->save()){
                        #die("in ");
                        echo "<br><span style='color:green'> Mail Inserted successfully  </span> ";
                    }else{
                        #die("else");
                        echo "<br><span style='color:red'> Mail Inserted Failed  </span> ";
                    }
                }
            }
        }
    }
    #https://localhost/kande_pohe_cr4/cron/recommended-matches-members-to-male
    public function actionRecommendedMatchesMembersToMale()
    {
        #TODO VIEW ALL AND LOGIN
        $all_male_active_profile = User::cronFindRecentJoinedUserList('MALE');
        $gender = 'FEMALE';
        if(count(@$all_male_active_profile)>0){
            foreach($all_male_active_profile as $k1=>$v1){
                $UserModel = User::findOne($v1->id);
                $mail_html = '';
                $email_to = $UserModel->email;
                $FullName = $UserModel->FullName;

                $WHERE ='';
                $main_user_id = $v1->id;
                $UPP = UserPartnerPreference::findByUserId($main_user_id) ;
                if ($UPP != NULL) {
                    if(@$UPP->ID !=''){
                        $age_from = $UPP->age_from;
                        $age_to = $UPP->age_to;
                        $height_from = $UPP->height_from;
                        $height_to = $UPP->height_to;
                        $PartnersMaritalStatus = PartnersMaritalStatus::findAllByUserId($main_user_id);
                        #$PartenersReligionIDs = CommonHelper::convertArrayToString($PartenersReligion, 'iReligion_ID');
                        $PartnersMaritalPreferences = CommonHelper::convertArrayToString($PartnersMaritalStatus, 'iMarital_Status_ID');
                        $ids = join('","',$PartnersMaritalPreferences);
                        $WHERE .= " AND user.Gender = '".$gender."' " ;
                        if(count($PartnersMaritalPreferences)>0){
                            $WHERE .= ' AND user.iMaritalStatusID In("' .$ids . '" )';
                        }
                        $WHERE .= ($age_from != '') ? ' AND ( (user.Age >= "' . $age_from . '") OR (TIMESTAMPDIFF(YEAR, user.DOB, CURDATE()) >= "' . $age_from . '"))' : '';
                        $WHERE .= ($age_to != '') ? ' AND ((user.Age <= "' . $age_to . '") OR (TIMESTAMPDIFF(YEAR, user.DOB, CURDATE()) <= "' . $age_to . '")) ' : '';
                        $WHERE .= ($height_from != '') ? ' AND master_heights.iHeightID >= "' . $height_from . '" ' : '';
                        $WHERE .= ($height_to != '') ? ' AND master_heights.iHeightID <= "' . $height_to . '" ' : '';
                        $matches_list = User::searchBasic($WHERE, 0, 5);
                        #CommonHelper::pr($matches_list);
                        if(count($matches_list)>0){
                            $c = new CommonHelper();
                            foreach($matches_list as $k=>$v){
                                $id = $v->id;
                                $Model = User::findOne($id);
                                $age = $c->getAge($Model->DOB);
                                $mFullName = $Model->FullName;
                                $height = CommonHelper::setInputVal(@$Model->height->vName, 'text');
                                $religionName = CommonHelper::setInputVal(@$Model->religionName->vName, 'text');
                                $motherTongue = CommonHelper::setInputVal(@$Model->motherTongue->Name, 'text');
                                $communityName = CommonHelper::setInputVal(@$Model->communityName->vName, 'text');
                                $cityName = CommonHelper::setInputVal(@$Model->cityName->vCityName, 'text');
                                $countryName = CommonHelper::setInputVal(@$Model->countryName->vCountryName, 'text');
                                $educationLevelName = CommonHelper::setInputVal(@$Model->educationLevelName->vEducationLevelName, 'text');
                                $workingAsName = CommonHelper::setInputVal(@$Model->workingAsName->vWorkingAsName, 'text');
                                $vNativePlaceCA = CommonHelper::setInputVal(@$Model->vNativePlaceCA, 'text');
                                $a  = CommonHelper::setInputVal(@$Model->tYourSelf, 'text');
                                $dot ='';
                                if(strlen($a ) > 100){
                                    $dot ='...';
                                }
                                $tYourSelf =  $c->truncate($a, 100).$dot;
                                $PHOTO = CommonHelper::getPhotos('USER', $id, "200" . $Model->propic, 200, '', 'Yes');
                                $LINK = CommonHelper::getSiteUrl('FRONTEND', 1) . 'user/profile?uk=' . $Model->Registration_Number."&source=profile_viewed_by&e_id=".base64_encode($email_to);
                                $PHOTO = '<img src="' . $PHOTO . '" width="200"  alt="Profile Photo">';
                                $mail_html .= '
<hr>
                    <table border="0" cellpadding="1" cellspacing="1" style="width:100%">
                    <tbody>
                        <tr>
                            <td><span style="color:#ff0000"><strong>&nbsp;<a href="'.$LINK.'" style="color:#ea0b44;text-decoration: none; ">'.$mFullName.'</a></strong></span></td>
                            <td style="text-align:right"><span style="color:#ff0000"><span style="color:#333333">'.date('d-m-Y').'</span></span></td>
                        </tr>
                    </tbody>
                </table>

                <table style="width:100%">
                    <tbody>
                        <tr>
                            <td>Age</td>
                            <td>: '.$age.'</td>
                            <td rowspan="8" style="text-align:right"><a href="'.$LINK.'">'.$PHOTO.'</a></td>
                        </tr>
                         <tr>
                            <td>Education</td>
                            <td>: '.$educationLevelName.'</td>
                        </tr>
                        <tr>
                            <td>Height</td>
                            <td>: '.$height.'</td>
                        </tr>
                        <tr>
                            <td>Cast</td>
                            <td>: '.$religionName.'</td>
                        </tr>
                        <tr>
                            <td>Profession</td>
                            <td>: '.$workingAsName.'</td>
                        </tr>
                        <tr>
                            <td>Location</td>
                            <td>: '.$cityName . ', ' . $countryName.'</td>
                        </tr>
                        <tr>
                            <td>Native Place</td>
                            <td>: '.ucwords($vNativePlaceCA).'</td>
                        </tr>
                    </tbody>
                </table>
<p>&nbsp;</p>

<p><a href="'.$LINK.'" style="background: #ea0b44; outline: none; color: #fff; border-color: #204d74; margin-top: 10px; padding: 19px 29px; border-radius: 2px; box-shadow: none; border: none; font-size: 16px; font-family: \'Open Sans\', sans-serif; font-weight: bold; text-decoration: none; cursor: pointer;">View Her Full Profile </a></p>
		';
                                $mail_html .= ' <br> ';
                            }
                            $view_all_link = CommonHelper::getSiteUrl('FRONTEND', 1) . 'search/basic-search?ref='.Yii::$app->params['ref']['recently_joined']."&e_id=".base64_encode($email_to) ;

                            $mail_html .= '<br> <br> <hr><br> <p><a href="'.$view_all_link.'" style="background: #FFC107; outline: none; color: #fff; border-color: #204d74; margin-top: 10px;  margin-left: 30%;padding: 19px 29px; border-radius: 2px; box-shadow: none; border: none; font-size: 16px; font-family: \'Open Sans\', sans-serif; font-weight: bold; text-decoration: none; cursor: pointer; text-align:right;">View All Profiles </a></p> ';

                            $MAIL_DATA = array("EMAIL_TO" => $email_to, "NAME" => $FullName,"MEMBER_PROFILE_LIST" => $mail_html);
                            $MAIL_STATUS = MailHelper::SendMail('NOTIFICATION_WEEKLY_RECOMMENDED_MATCHES_PROFILE_OF_GROOM', $MAIL_DATA,'',1);
                            #CommonHelper::pr($MAIL_STATUS );exit;
                            $model_eq = new EmailQueue;
                            $model_eq->eq_type = 'WEEKLY-RECOMMENDED-MATCHES-NOTIFICATION';
                            $model_eq->eq_to_email_id = $MAIL_STATUS['to_mail'];
                            #$model_eq->eq_from_email_id = $MAIL_STATUS['from_mail'];
                            $model_eq->eq_subject = $MAIL_STATUS['mail_subject'];
                            $model_eq->eq_body = $MAIL_STATUS['mail_message'];
                            $model_eq->eq_to_name = $MAIL_STATUS['from_name'];
                            $model_eq->eq_to_user_id = $v1->id;
                            $model_eq->eq_send_status = 'PENDING';
                            $model_eq->eq_dt_added = date('Y-m-d H:i:s');
                            if($model_eq->save()){
                                #die("in ");
                                echo "<br><span style='color:green'> Mail Inserted successfully  </span> ";
                            }else{
                                #die("else");
                                echo "<br><span style='color:red'> Mail Inserted Failed  </span> ";
                            }
                        }
                    }
                }else{
                }
            }
        }
        else{
            echo "<br><span style='color:red; text-align: center; '>Records not available yet</span> ";
        }
    }

#https://localhost/kande_pohe_cr4/cron/recommended-matches-members-to-female
    public function actionRecommendedMatchesMembersToFemale()
    {
        #TODO VIEW ALL AND LOGIN
        #$Gender = (Yii::$app->user->identity->Gender == 'MALE') ? 'FEMALE' : 'MALE';
        $all_male_active_profile = User::cronFindRecentJoinedUserList('FEMALE');
        #CommonHelper::pr($RecentlyJoinedMembers); exit;
        $gender = 'MALE';
        if(count(@$all_male_active_profile)>0){
            foreach($all_male_active_profile as $k1=>$v1){
                $UserModel = User::findOne($v1->id);
                #  CommonHelper::pr($v1 ); exit;
                $mail_html = '';
                $email_to = $UserModel->email;
                $FullName = $UserModel->FullName;

                $WHERE ='';
                $main_user_id = $v1->id;
                $UPP = UserPartnerPreference::findByUserId($main_user_id) ;
                #CommonHelper::pr($UPP ); exit;
                if ($UPP != NULL) {
                    #CommonHelper::pr($UPP); exit;
                    if(@$UPP->ID !=''){
                        $age_from = $UPP->age_from;
                        $age_to = $UPP->age_to;
                        $height_from = $UPP->height_from;
                        $height_to = $UPP->height_to;
                        $PartnersMaritalStatus = PartnersMaritalStatus::findAllByUserId($main_user_id);
                        #$PartenersReligionIDs = CommonHelper::convertArrayToString($PartenersReligion, 'iReligion_ID');
                        $PartnersMaritalPreferences = CommonHelper::convertArrayToString($PartnersMaritalStatus, 'iMarital_Status_ID');
                        $ids = join('","',$PartnersMaritalPreferences);
                        $WHERE .= " AND user.Gender = '".$gender."' " ;
                        if(count($PartnersMaritalPreferences)>0){
                            $WHERE .= ' AND user.iMaritalStatusID In("' .$ids . '" )';
                        }
                        $WHERE .= ($age_from != '') ? ' AND ( (user.Age >= "' . $age_from . '") OR (TIMESTAMPDIFF(YEAR, user.DOB, CURDATE()) >= "' . $age_from . '"))' : '';
                        $WHERE .= ($age_to != '') ? ' AND ((user.Age <= "' . $age_to . '") OR (TIMESTAMPDIFF(YEAR, user.DOB, CURDATE()) <= "' . $age_to . '")) ' : '';
                        $WHERE .= ($height_from != '') ? ' AND master_heights.iHeightID >= "' . $height_from . '" ' : '';
                        $WHERE .= ($height_to != '') ? ' AND master_heights.iHeightID <= "' . $height_to . '" ' : '';
                        $matches_list = User::searchBasic($WHERE, 0, 5);
                        #CommonHelper::pr($matches_list);
                        if(count($matches_list)>0){
                            $c = new CommonHelper();
                            foreach($matches_list as $k=>$v){
                                $id = $v->id;
                                $Model = User::findOne($id);
                                $age = $c->getAge($Model->DOB);
                                $mFullName = $Model->FullName;
                                $height = CommonHelper::setInputVal(@$Model->height->vName, 'text');
                                $religionName = CommonHelper::setInputVal(@$Model->religionName->vName, 'text');
                                $motherTongue = CommonHelper::setInputVal(@$Model->motherTongue->Name, 'text');
                                $communityName = CommonHelper::setInputVal(@$Model->communityName->vName, 'text');
                                $cityName = CommonHelper::setInputVal(@$Model->cityName->vCityName, 'text');
                                $countryName = CommonHelper::setInputVal(@$Model->countryName->vCountryName, 'text');
                                $educationLevelName = CommonHelper::setInputVal(@$Model->educationLevelName->vEducationLevelName, 'text');
                                $workingAsName = CommonHelper::setInputVal(@$Model->workingAsName->vWorkingAsName, 'text');
                                $vNativePlaceCA = CommonHelper::setInputVal(@$Model->vNativePlaceCA, 'text');
                                $a  = CommonHelper::setInputVal(@$Model->tYourSelf, 'text');
                                $dot ='';
                                if(strlen($a ) > 100){
                                    $dot ='...';
                                }
                                $tYourSelf =  $c->truncate($a, 100).$dot;
                                #CommonHelper::pr($vNativePlaceCA); exit;
                                $PHOTO = CommonHelper::getPhotos('USER', $id, "200" . $Model->propic, 200, '', 'Yes');
                                $LINK = CommonHelper::getSiteUrl('FRONTEND', 1) . 'user/profile?uk=' . $Model->Registration_Number."&source=profile_viewed_by&e_id=".base64_encode($email_to);
                                $PHOTO = '<img src="' . $PHOTO . '" width="200"  alt="Profile Photo">';
                                $mail_html .= '
<hr>
                    <table border="0" cellpadding="1" cellspacing="1" style="width:100%">
                    <tbody>
                        <tr>
                            <td><span style="color:#ff0000"><strong>&nbsp;<a href="'.$LINK.'" style="color:#ea0b44;text-decoration: none; ">'.$mFullName.'</a></strong></span></td>
                            <td style="text-align:right"><span style="color:#ff0000"><span style="color:#333333">'.date('d-m-Y').'</span></span></td>
                        </tr>
                    </tbody>
                </table>
                <table style="width:100%">
                    <tbody>
                        <tr>
                            <td>Age</td>
                            <td>: '.$age.'</td>
                            <td rowspan="8" style="text-align:right"><a href="'.$LINK.'">'.$PHOTO.'</a></td>
                        </tr>
                         <tr>
                            <td>Education</td>
                            <td>: '.$educationLevelName.'</td>
                        </tr>
                        <tr>
                            <td>Height</td>
                            <td>: '.$height.'</td>
                        </tr>
                        <tr>
                            <td>Cast</td>
                            <td>: '.$religionName.'</td>
                        </tr>
                        <tr>
                            <td>Profession</td>
                            <td>: '.$workingAsName.'</td>
                        </tr>
                        <tr>
                            <td>Location</td>
                            <td>: '.$cityName . ', ' . $countryName.'</td>
                        </tr>
                        <tr>
                            <td>Native Place</td>
                            <td>: '.ucwords($vNativePlaceCA).'</td>
                        </tr>
                    </tbody>
                </table>
<p>&nbsp;</p>

<p><a href="'.$LINK.'" style="background: #ea0b44; outline: none; color: #fff; border-color: #204d74; margin-top: 10px; padding: 19px 29px; border-radius: 2px; box-shadow: none; border: none; font-size: 16px; font-family: \'Open Sans\', sans-serif; font-weight: bold; text-decoration: none; cursor: pointer;">View His Full Profile </a></p>
		';
                                $mail_html .= ' <br> ';
                            }
                            $view_all_link = CommonHelper::getSiteUrl('FRONTEND', 1) . 'search/basic-search?ref='.Yii::$app->params['ref']['recently_joined']."&e_id=".base64_encode($email_to) ;

                            $mail_html .= '<br> <br> <hr><br> <p><a href="'.$view_all_link.'" style="background: #FFC107; outline: none; color: #fff; border-color: #204d74; margin-top: 10px;  margin-left: 30%;padding: 19px 29px; border-radius: 2px; box-shadow: none; border: none; font-size: 16px; font-family: \'Open Sans\', sans-serif; font-weight: bold; text-decoration: none; cursor: pointer; text-align:right;">View All Profiles </a></p> ';

                            $MAIL_DATA = array("EMAIL_TO" => $email_to, "NAME" => $FullName,"MEMBER_PROFILE_LIST" => $mail_html);
                            $MAIL_STATUS = MailHelper::SendMail('NOTIFICATION_WEEKLY_RECOMMENDED_MATCHES_PROFILE_OF_GROOM', $MAIL_DATA,'',1);
                            #CommonHelper::pr($MAIL_STATUS );exit;
                            $model_eq = new EmailQueue;
                            $model_eq->eq_type = 'WEEKLY-RECOMMENDED-MATCHES-NOTIFICATION';
                            $model_eq->eq_to_email_id = $MAIL_STATUS['to_mail'];
                            #$model_eq->eq_from_email_id = $MAIL_STATUS['from_mail'];
                            $model_eq->eq_subject = $MAIL_STATUS['mail_subject'];
                            $model_eq->eq_body = $MAIL_STATUS['mail_message'];
                            $model_eq->eq_to_name = $MAIL_STATUS['from_name'];
                            $model_eq->eq_to_user_id = $v1->id;
                            $model_eq->eq_send_status = 'PENDING';
                            $model_eq->eq_dt_added = date('Y-m-d H:i:s');
                            if($model_eq->save()){
                                #die("in ");
                                echo "<br><span style='color:green'> Mail Inserted successfully  </span> ";
                            }else{
                                #die("else");
                                echo "<br><span style='color:red'> Mail Inserted Failed  </span> ";
                            }
                        }
                    }
                }else{
                }
            }
        }else{
            echo "<br><span style='color:red; text-align: center; '>Records not available yet</span> ";
        }
    }

    # https://localhost/kande_pohe_cr4/cron/send-mail?type=WEEKLY-RECOMMENDED-MATCHES-NOTIFICATION
    # https://localhost/kande_pohe_cr4/cron/send-mail?type=WEEKLY-NOTIFICATION
    public function actionSendMail($type='WEEKLY-NOTIFICATION')
    {
        $order = array("DESC"=>'DESC',"ASC"=>'ASC');
        $random_keys = array_rand($order,1);
        $RecentlyJoinedMembersMail = EmailQueue::get_weekly_notification_new_joined($type,20,$random_keys);
       # CommonHelper::pr($RecentlyJoinedMembersMail);
        if(count($RecentlyJoinedMembersMail)>0){
            $ctr = 0;
            foreach($RecentlyJoinedMembersMail as $k1=>$v1){
                $status = MailHelper::SendMailCron($v1);
                $eq_id = $v1->eq_id;
                $model_eq = EmailQueue::findOne($eq_id);
                $model_eq->eq_send_status = 'YES';
                $ctr++;
                if($model_eq->save()){
                    echo "<br><span style='color:green'>".$ctr."  => Mail Sent successfully  </span> ";
                    $status =   $model_eq->delete();
                }else{
                    echo "<br><span style='color:red'>".$ctr."  => Mail sending Failed  </span> ";
                    $status =   $model_eq->delete();

                }
            }
        }else{
            echo "<br><span style='color:red; text-align: center; '>Mail not available yet</span> ";
        }
    }
    /*$MAIL_DATA = array("EMAIL" => 'parmarvikrantr@gmail.com', "EMAIL_TO" => 'parmarvikrantr@gmail.com', "NAME" => "Vix P", "PIN" => "121212", "MINUTES" => "12121212");
        $MAIL_STATUS = MailHelper::SendMail('EMAIL_VERIFICATION_PIN', $MAIL_DATA);
        die("as".$MAIL_STATUS);*/
}