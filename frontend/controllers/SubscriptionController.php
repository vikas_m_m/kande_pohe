<?php
namespace frontend\controllers;

use common\components\CommonHelper;
use common\components\MessageHelper;
use common\components\SmsHelper;
use common\models\Mailbox;
use common\models\PartnersAnnualIncome;
use common\models\PartnersBodyType;
use common\models\PartnersCharan;
use common\models\PartnersCities;
use common\models\PartnersCommunity;
use common\models\PartnersCountries;
use common\models\PartnersDiet;
use common\models\PartnersDrink;
use common\models\PartnersFamilyAffluenceLevel;
use common\models\PartnersFamilyType;
use common\models\PartnersFavouriteCousines;
use common\models\PartnersFavouriteMusic;
use common\models\PartnersFavouriteReads;
use common\models\PartnersFitnessActivities;
use common\models\PartnersInterest;
use common\models\PartnersMothertongue;
use common\models\PartnersNadi;
use common\models\PartnersNakshtra;
use common\models\PartnersPreferredDressType;
use common\models\PartnersPreferredMovies;
use common\models\PartnersRaashi;
use common\models\PartnersReligion;
use common\models\PartnersSkinTone;
use common\models\PartnersSmoke;
use common\models\PartnersSpectacles;
use common\models\PartnersStates;
use common\models\PartnersSubcommunity;
use common\models\PartnerWorkingAs;
use common\models\PartnerWorkingWith;
use common\models\SubscriptionLog;
use common\models\Tags;
use common\models\UserPhotos;
use common\models\UserRequest;
use common\models\UserRequestOp;
use common\models\UserSubscriptionDetails;
use common\models\Subscriptions;
use common\models\UserTag;
use common\models\Wightege;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
#use frontend\components\CommonHelper;

use common\models\User;
use common\models\PartenersReligion;
use common\models\UserPartnerPreference;
use common\models\PartnersMaritalStatus;
use common\models\PartnersGotra;
use common\models\PartnersFathersStatus;
use common\models\PartnersMothersStatus;
use common\models\PartnersEducationalLevel;
use common\models\PartnersEducationField;
use yii\widgets\ActiveForm;
use yii\web\Response;
use common\components\MailHelper;

/**
 * Site controller
 */
class SubscriptionController extends Controller
{
    /**
     * @inheritdoc
     */
    /*public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }*/
    public $STATUS;
    public $MESSAGE;
    public $TITLE;

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],

        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        /*$model = new User();
        $model->scenario = User::SCENARIO_REGISTER;*/
        return $this->redirect(['user/dashboard']);
        /*return $this->render('index',
            ['model' => $model]
        );*/
    }

    public function actionUpgradeProcess()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $Id = Yii::$app->user->identity->id;
        $UserModel = User::findOne($Id);
        #$subscription_detail = UserSubscriptionDetails::find()->where(['usd_user_id'=>$Id])->orderBy(['usd_id'=>SORT_DESC])->one();
        $subscription_detail = UserSubscriptionDetails::getUserSubscriptionDetail($Id);
        $type = Yii::$app->request->get('type');
        if($type == ''){
            Yii::$app->session->setFlash('error', 'Kindly Select the valid subscription package.');
            return $this->redirect(Yii::$app->getUrlManager()->getBaseUrl() .Yii::$app->params['upgradePlan'] );
        }else{
            $subscription_model = Subscriptions::getPlanTypeDetails($type);
            #CommonHelper::pr($subscription_model);exit;
            if($subscription_model == NULL){
                Yii::$app->session->setFlash('error', 'Your selected plan is no available yet.');
                return $this->redirect(Yii::$app->getUrlManager()->getBaseUrl() .Yii::$app->params['upgradePlan'] );
            }else{
                if($subscription_model != NULL){

                    /*$current_date_time = date('Y-m-d H:i:s');
                    $days = $subscription_model->validity_of_package - 1;
                    $end_date= date("Y-m-d 23:59:59", strtotime("+".$days." days", strtotime($current_date_time)));
                    $user_subscription_model = new UserSubscriptionDetails();
                    $user_subscription_model->usd_user_id = $Id;
                    $user_subscription_model->usd_subscription_start_date = $current_date_time;
                    $user_subscription_model->usd_subscription_end_date = $end_date;
                    $user_subscription_model->usd_status = 'Activated';
                    $user_subscription_model->usd_subscription_name = $subscription_model->subscriptions_name;
                    $user_subscription_model->usd_short_name = $subscription_model->short_name;
                    $user_subscription_model->usd_subscriptions_price = $subscription_model->subscriptions_price;
                    $user_subscription_model->usd_profile_duration = $subscription_model->profile_duration;
                    $user_subscription_model->usd_privacy_settings = $subscription_model->privacy_settings;
                    $user_subscription_model->usd_validity_of_package = $subscription_model->validity_of_package;
                    $user_subscription_model->usd_customer_care_support = $subscription_model->customer_care_support;
                    $user_subscription_model->usd_dt_added = $current_date_time;
                    $user_subscription_model->usd_no_of_contacts = $subscription_model->no_of_contacts;
                    $user_subscription_model->usd_no_of_pm = $subscription_model->no_of_pm;
                    if($subscription_detail != null){
                        if($subscription_detail->usd_status == 'Activated'){ #TODO If Existing plan active and have remaining featuers then forward them to new plan.
                            $user_subscription_model->usd_no_of_contacts = $subscription_model->no_of_contacts + $subscription_detail->usd_no_of_contacts;
                            $user_subscription_model->usd_no_of_pm = $subscription_model->no_of_pm  + $subscription_detail->usd_no_of_pm;
                        }
                    }*/
                    #CommonHelper::pr($user_subscription_model);exit;
                    /* if(@$user_subscription_model->save()){
                         Yii::$app->session->setFlash('success', 'Congratulation! Your subscription package successfully changed.');
                         return $this->redirect(Yii::$app->getUrlManager()->getBaseUrl() .'/subscription-history' );
                     }else{
                         Yii::$app->session->setFlash('error', 'Something went wrong. Kindly contact to support team.');
                         return $this->redirect(Yii::$app->getUrlManager()->getBaseUrl() .Yii::$app->params['upgradePlan'] );
                     }*/
                }
            }
        }
        #var_dump($subscription_detail);exit;
        return $this->render('/payment/review_plan', [
            'UserModel' => $UserModel,
            'subscription_detail' => $subscription_detail,
            'subscription_model' => $subscription_model,
        ]);
    }
    public function actionCheckoutProcess()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $Id = Yii::$app->user->identity->id;
        $UserModel = User::findOne($Id);
        /*$encFile =Yii::getAlias('@app'). '\views\payment\lib\Crypto.php';
        $file_status = include_once($encFile);
        if(!$file_status){
            var_dump($file_status);
            Yii::$app->session->setFlash('error', 'Something went wrong. Please try again.');
            return $this->redirect(Yii::$app->getUrlManager()->getBaseUrl() .Yii::$app->params['upgradePlan'] );
        }else{*/
        $session = Yii::$app->session;
        $checkout_data = '';
        if(1){
            if(count(Yii::$app->request->post())>0){
                if(Yii::$app->request->post('tid') =='' || Yii::$app->request->post('package_name') =='' ){
                }else{
                    /*
                    21004:billing_name: Required parameter missing.,
                    21005:billing_address: Required parameter missing.,
                    21009:billing_country: Required parameter missing.,
                    21010:billing_tel: Required parameter missing.,
                    21007:billing_state: Required parameter missing.,
                    21008:billing_zip: Required parameter missing.,
                    21006:billing_city: Required parameter missing.
                    */
                    /*if(@$UserModel->vAreaName == '' || @$UserModel->countryName->vCountryName == '' || @$UserModel->stateName->vStateName == '' || @$UserModel->cityName->vCityName == '' ){
                        Yii::$app->session->setFlash('error', "kindly complete your address details under My Profile first.");
                        $url = Yii::$app->urlManager->createAbsoluteUrl(Yii::$app->params['upgradePlan']);//userMyProfile
        #echo "<script> window.location = '".$url."'; </script>";exit;
                    }*/
                    $package_name = Yii::$app->request->post('package_name');
                    $page = @Yii::$app->request->post('page');
                    #CommonHelper::pr($page);
                    $subscription_model = Subscriptions::getPlanTypeDetails($package_name);

                    $request_array['tid'] =   Yii::$app->request->post('tid');
                    //$request_array['integration_type'] =   'iframe_normal';
                    $request_array['order_id'] =  $UserModel->id."KP".time();
                    $request_array['billing_name'] =  @$UserModel->FullName;
                    $request_array['billing_email'] =  @$UserModel->email;
                    $request_array['billing_address'] = '';//@$UserModel->vAreaName;// (@$UserModel->vAreaName!='')?@$UserModel->vAreaName : @$UserModel->countryName->vCountryName;
                    $request_array['billing_country'] = @$UserModel->countryName->vCountryName;
                    $request_array['billing_state'] =@$UserModel->stateName->vStateName;
                    $request_array['billing_city'] =@$UserModel->cityName->vCityName;
                    $request_array['billing_zip'] = '';//'380015' ;//@$UserModel->FullName;
                    $request_array['billing_tel'] =@$UserModel->Mobile;
                    //$request_array['billing_tel'] =  @$UserModel->Mobile;

                    $request_array['amount'] =   number_format($subscription_model->subscriptions_price,2,".","");
                    $request_array['currency'] =  'INR';
                    $request_array['language'] =  'EN';
                    $request_array['customer_identifier'] =  $UserModel->id;

                    $request_array['merchant_id'] =  Yii::$app->params['cca_merchant_id'];
                    #$request_array['mode'] =  'TEST';
                    $request_array['merchant_param1'] =  $UserModel->Registration_Number; #User ID
                    $request_array['merchant_param2'] =  Yii::$app->request->post('package_name'); # Plan name
                    /*$request_array['cancel_url'] =  'https://localhost/cc_avenue_php/IFRAME_KIT/ccavResponseHandler.php';//Yii::$app->getUrlManager()->getBaseUrl()."/plan-cancel";
                    $request_array['redirect_url'] = 'https://localhost/cc_avenue_php/IFRAME_KIT/ccavResponseHandler.php';// Yii::$app->getUrlManager()->getBaseUrl();//."/plan-confirmation";*/
                    #$request_array['cancel_url'] =  Yii::$app->urlManager->createAbsoluteUrl('plan-cancel');
                    $pname = Yii::$app->params["upgradeProcess"].'?type='.$package_name;
                    if($page=='selectPlan'){
                        $pname = Yii::$app->params["selectPlan"].'?type='.$package_name;
                    }
                    $request_array['from_page'] =  $page;

                    $request_array['cancel_url'] =  Yii::$app->urlManager->createAbsoluteUrl($pname);
                    $request_array['redirect_url'] = Yii::$app->urlManager->createAbsoluteUrl('plan-confirmation');
                    #CommonHelper::pr($request_array);
                    $session->set('checkout_data', $request_array);
                }
            }
            $checkout_data = $session->get('checkout_data');
        }
        #CommonHelper::pr($checkout_data);exit;
        #var_dump($subscription_detail);exit;
        return $this->render('/payment/checkout', [
            'UserModel' => $UserModel,
            'checkout_data' => $checkout_data,
        ]);
    }
    public function actionPlanConfirmation(){
        $session = Yii::$app->session;
        $checkout_data = @$session->get('checkout_data')['from_page'];
        #CommonHelper::pr($checkout_data);exit;
        //$encFile =Yii::getAlias('@app'). '\views\payment\lib\Crypto.php';
        $encFile =Yii::getAlias('@app'). '/views/payment/lib/Crypto.php';
        $file_status = include_once($encFile);
        if(!$file_status){
            Yii::$app->session->setFlash('error', 'Something went wrong. Please try again.');
            #return $this->redirect(Yii::$app->getUrlManager()->getBaseUrl() .Yii::$app->params['upgradePlan'] );
            $url = Yii::$app->urlManager->createAbsoluteUrl(Yii::$app->params['upgradePlan']);
            if($checkout_data =='selectPlan'){
                $url = Yii::$app->urlManager->createAbsoluteUrl(Yii::$app->params['selectPlan']);
            }
            echo "<script> window.location = '".$url."'; </script>";exit;
        }
        error_reporting(0);
        $host_type = Yii::$app->params['host_type'];
        $workingKey=  Yii::$app->params['cca_working_key'.$host_type];//Yii::$app->params['cca_working_key'];//'42960F1D8334C78F964BC0EC52E30EB1';		//Working Key should be provided here.
        $encResponse= $_POST["encResp"];			//This is the response sent by the CCAvenue Server
        $rcvdString=decrypt($encResponse,$workingKey);		//Crypto Decryption used as per the specified working key.
        $order_status= $status_msg="";
        $decryptValues=explode('&', $rcvdString);
        $dataSize=sizeof($decryptValues);
#CommonHelper::pr($decryptValues);exit;
        $information =array();
        for($i = 0; $i < $dataSize; $i++)
        {
            $information=explode('=',$decryptValues[$i]);
            if($i==3)	$order_status=$information[1];
            if($i==8)	$status_msg=$information[1];
        }

        $res_array = array();
        if($order_status==="Success")
        {
            for($i = 0; $i < $dataSize; $i++)
            {
                $information=explode('=',$decryptValues[$i]);
                $res_array[$information[0]] = $information[1];
            }
            $update_status  = $this->actionUpgradeUserPlan($res_array);
            if($update_status == 1){
                Yii::$app->session->setFlash('success', 'Congratulation! Your plan has been upgrade successfully.');
                $url = Yii::$app->urlManager->createAbsoluteUrl('subscription-history');
                if($checkout_data =='selectPlan'){
                    $url = Yii::$app->urlManager->createAbsoluteUrl(Yii::$app->params['registrationBasicDetails']);
                }
                echo "<script> window.location = '".$url."'; </script>";exit;
                #return $this->redirect(Yii::$app->getUrlManager()->getBaseUrl() .'/subscription-history' );
                #echo "<script> window.location = '".$this->redirect(Yii::$app->getUrlManager()->getBaseUrl() .'/subscription-history' )."'; </script>";exit;
            }else if($update_status == 2){
                Yii::$app->session->setFlash('error', 'Something went wrong. Kindly contact to support team.');
            }else if($update_status == 3){
                Yii::$app->session->setFlash('error', 'Missing details. Kindly contact to support team.');
            }
        }
        else if($order_status==="Aborted")
        {
            Yii::$app->session->setFlash('error', 'Last operation have been cancelled.');
        }
        else if($order_status==="Failure")
        {
            Yii::$app->session->setFlash('error', 'Last operation have been failed.');
        }
        else
        {
            if($status_msg !=''){
                Yii::$app->session->setFlash('error', $status_msg."<br>For more information contact to our support team.");
            }else{
                Yii::$app->session->setFlash('error', 'Security Error. Illegal access detected.');
            }

        }
        $url = Yii::$app->urlManager->createAbsoluteUrl(Yii::$app->params['upgradePlan']);
        if($checkout_data =='selectPlan'){
            $url = Yii::$app->urlManager->createAbsoluteUrl(Yii::$app->params['selectPlan']);
        }
        echo "<script> window.location = '".$url."'; </script>";exit;
        #return $this->redirect(Yii::$app->getUrlManager()->getBaseUrl() .Yii::$app->params['upgradePlan'] );
    }
    public function actionUpgradeUserPlan($res_array){
        # CommonHelper::pr($res_array);
        $user_id = $res_array['merchant_param1'];
        $package_name = $res_array['merchant_param2'];
        $subscription_model = Subscriptions::getPlanTypeDetails($package_name);
        $Id = Yii::$app->user->identity->id;
        $UserModel = User::findOne($Id);
        #$subscription_detail = UserSubscriptionDetails::find()->where(['usd_user_id'=>$Id])->orderBy(['usd_id'=>SORT_DESC])->one();
        $subscription_detail = UserSubscriptionDetails::getUserSubscriptionDetail($Id);
        #CommonHelper::pr($subscription_detail );
        if($subscription_model != NULL){
            $current_date_time = date('Y-m-d H:i:s');
            $days = $subscription_model->validity_of_package - 1;
            $end_date= date("Y-m-d 23:59:59", strtotime("+".$days." days", strtotime($current_date_time)));
            $user_subscription_model = new UserSubscriptionDetails();
            $user_subscription_model->usd_user_id = $Id;
            $user_subscription_model->usd_subscription_start_date = $current_date_time;
            $user_subscription_model->usd_subscription_end_date = $end_date;
            $user_subscription_model->usd_status = 'Activated';
            $user_subscription_model->usd_subscription_name = $subscription_model->subscriptions_name;
            $user_subscription_model->usd_short_name = $subscription_model->short_name;
            #$user_subscription_model->usd_subscriptions_price = $subscription_model->subscriptions_price;
            $user_subscription_model->usd_profile_duration = $subscription_model->profile_duration;
            $user_subscription_model->usd_privacy_settings = $subscription_model->privacy_settings;
            $user_subscription_model->usd_validity_of_package = $subscription_model->validity_of_package;
            $user_subscription_model->usd_customer_care_support = $subscription_model->customer_care_support;
            $user_subscription_model->usd_dt_added = $current_date_time;
            $user_subscription_model->usd_no_of_contacts = $subscription_model->no_of_contacts;
            $user_subscription_model->usd_no_of_pm = $subscription_model->no_of_pm;

            $user_subscription_model->usd_subscriptions_price = $res_array['amount'];//$subscription_model->subscriptions_price;
            $user_subscription_model->usd_payment_status = $res_array['order_status'];//$subscription_model->subscriptions_price;
            $user_subscription_model->usd_order_id = $res_array['order_id'];
            $user_subscription_model->usd_transaction_id = $res_array['tracking_id'];
            $user_subscription_model->usd_bank_ref_no = $res_array['bank_ref_no'];
            $user_subscription_model->usd_order_status = $res_array['order_status'];
            $user_subscription_model->usd_payment_comment = $res_array['failure_message'];
            $user_subscription_model->usd_payment_mode = $res_array['payment_mode'];
            $user_subscription_model->usd_card_name = $res_array['card_name'];
            $user_subscription_model->usd_currency = $res_array['currency'];
            $user_subscription_model->usd_response_code = $res_array['response_code'];

            if($subscription_detail != null){
                if($subscription_detail->usd_status == 'Activated'){ #TODO If Existing plan active and have remaining featuers then forward them to new plan.
                    $user_subscription_model->usd_no_of_contacts = $subscription_model->no_of_contacts + $subscription_detail->usd_no_of_contacts;
                    $user_subscription_model->usd_no_of_pm = $subscription_model->no_of_pm  + $subscription_detail->usd_no_of_pm;
                }
            }
            #CommonHelper::pr($user_subscription_model);exit;
            if(@$user_subscription_model->save()){
                $UserModel->completed_step = $UserModel->setCompletedStep('12');
                $UserModel->save();
                #Yii::$app->session->setFlash('success', 'Congratulation! Your subscription package successfully changed.');
                #return $this->redirect(Yii::$app->getUrlManager()->getBaseUrl() .'/subscription-history' );
                return 1;
            }else{
                return 2; #not updated
                #Yii::$app->session->setFlash('error', 'Something went wrong. Kindly contact to support team.');
                #return $this->redirect(Yii::$app->getUrlManager()->getBaseUrl() .Yii::$app->params['upgradePlan'] );
            }
        }else{
            return 3; #missing something
        }

    }


    public function actionSubscriptionPlanProcess()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $Id = Yii::$app->user->identity->id;
        $UserModel = User::findOne($Id);
        #$subscription_detail = UserSubscriptionDetails::find()->where(['usd_user_id'=>$Id])->orderBy(['usd_id'=>SORT_DESC])->one();
        $subscription_detail = UserSubscriptionDetails::getUserSubscriptionDetail($Id);
        #CommonHelper::pr($subscription_detail);
        if($subscription_detail){
            return $this->redirect(Yii::$app->getUrlManager()->getBaseUrl() .Yii::$app->params['upgradePlan'] );
        }
        $type = Yii::$app->request->get('type');
        if($type == ''){
            Yii::$app->session->setFlash('error', 'Kindly Select the valid subscription package.');
            return $this->redirect(Yii::$app->getUrlManager()->getBaseUrl() .Yii::$app->params['selectPlan'] );
        }else{
            $subscription_model = Subscriptions::getPlanTypeDetails($type,0);
            #CommonHelper::pr($subscription_model);exit;
            if($subscription_model == NULL){
                Yii::$app->session->setFlash('error', 'Your selected plan is no available yet.');
                return $this->redirect(Yii::$app->getUrlManager()->getBaseUrl() .Yii::$app->params['selectPlan'] );
            }else{
                if($subscription_model != NULL){
                }
            }
        }
        #var_dump($subscription_detail);exit;
        return $this->render('/payment/select_plan', [
            'UserModel' => $UserModel,
            'subscription_detail' => $subscription_detail,
            'subscription_model' => $subscription_model,
        ]);
    }
}
