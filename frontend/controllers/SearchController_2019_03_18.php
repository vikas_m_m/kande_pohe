<?php

namespace frontend\controllers;

use common\models\Mailbox;
use common\models\UserPhotos;
use common\models\UserRequest;
use common\models\UserRequestOp;
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use common\models\User;
use yii\widgets\ActiveForm;
use yii\web\Response;
use common\components\MailHelper;
use common\components\CommonHelper;
use common\components\MessageHelper;
use common\components\SmsHelper;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Site controller
 */
class SearchController extends Controller
{
    /**
     * @inheritdoc
     */
    /*public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }*/
    public $STATUS;
    public $MESSAGE;
    public $TITLE;

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],

        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->redirect(['search/basic-search']);
    }

    public function actionBasicSearch($ref = '',$e_id='')
    {
        $request = Yii::$app->request;
        $session = Yii::$app->session;
        $params = $request->bodyParams;




        #CommonHelper::pr($params);exit;
        //http://localhost/KandePohe/search/basic-search?search-type=basic&profile-for=FEMALE&Community=1&sub-community=1&agerange=19&height=2
        $WHERE = '';
        if (Yii::$app->user->isGuest) {
            #TODO SET URL IN SESSION
            if($e_id !=''){
                #https://localhost/kande_pohe_cr4/search/basic-search?ref=recently_joined&e_id=cGFybWFydmlrcmFudHJAZ21haWwuY29t
                $email = base64_decode($e_id);
                $qstring = Yii::$app->homeUrl .'search/basic-search?ref='.$ref;//$_SERVER['REDIRECT_QUERY_STRING'];
               # die("==>".$qstring);
                $session->set('last_url', $qstring);
                $session->set('url_email', ($email));
            }
            return $this->redirect(Yii::$app->homeUrl . "?ref=login");
            exit;
            #$TempModel = new User();
        } else {
            $id = Yii::$app->user->identity->id;
            $TempModel = User::findOne($id);
            $WhereId = " AND user.id != " . $id;
        }
        if ($TempModel->load(Yii::$app->request->post())) {
            $Gender = $params['User']['Profile_for'];
            $Community = $params['User']['iCommunity_ID'];
            $CountryId = $params['User']['iCountryId'];
            $Gotra = $params['User']['iGotraID'];
            $Mangalik = $params['User']['Mangalik'];
            $SubCommunity = $params['User']['iSubCommunity_ID'];
            $HeightFrom = $params['User']['HeightFrom'];
            $HeightTo = $params['User']['HeightTo'];
            $ReligionID = $params['User']['iReligion_ID'];
            $MaritalStatusID = $params['User']['Marital_Status'];
            #$AgeFrom = $params['User']['AgeFrom'];
            #$AgeTo = $params['User']['AgeFrom'];
            /*if ($params['User']['AgeTo'] != '') {
                list($AgeFrom, $AgeTo) = explode("-", $params['User']['Agerange']);
            } else {*/
            $AgeFrom = $params['User']['AgeFrom'];
            $AgeTo = $params['User']['AgeTo'];
            //}
            $session->set('Profile_for', $Gender);
            $session->set('iCommunity_ID', $Community);
            $session->set('iCountryId', $CountryId);
            $session->set('Mangalik', $Mangalik);
            $session->set('iGotraID', $Gotra);
            $session->set('iSubCommunity_ID', $SubCommunity);
            $session->set('HeightFrom', $HeightFrom);
            $session->set('HeightTo', $HeightTo);
            $session->set('iReligion_ID', $ReligionID);
            $session->set('Marital_Status', $MaritalStatusID);
            $session->set('AgeFrom', $AgeFrom);
            $session->set('AgeTo', $AgeTo);
        } else {
            if ($ref != '') {
                $ReffArray = Yii::$app->params['ref'];
                if (array_key_exists($ref, $ReffArray)) {
                    if ($TempModel->Gender == 'MALE') {
                        $Gender = "FEMALE";
                    } else if ($TempModel->Gender == 'FEMALE') {
                        $Gender = "MALE";
                    }
                    $session->set('Profile_for', $Gender);
                    if ($Gender == '') {
                        if ($TempModel->Gender == 'MALE') {
                            $WHERE .= " AND user.Gender = 'FEMALE'";
                        } else if ($TempModel->Gender == 'FEMALE') {
                            $WHERE .= " AND user.Gender = 'MALE'";
                        }
                    }
                    $WHERE .= ($Gender != '') ? ' AND user.Gender = "' . $Gender . '" ' : '';
                    $Limit = Yii::$app->params['searchingLimit'];
                    $Offset = (Yii::$app->request->get('Offset') == 0) ? 0 : Yii::$app->request->get('Offset');
                    $Page = (Yii::$app->request->get('page') == 0 || Yii::$app->request->get('page') == '') ? 0 : Yii::$app->request->get('page');
                    if ($Page) {
                        $Page = $Page - 1;
                        $Offset = $Limit * $Page;
                    } else {
                        $Page = 0;
                        $Offset = 0;
                    }
                    if ($ref != 'recently_joined') {
                        $SearchStatus = 0;
                        $TotalRecords = count(UserRequestOp::getShortList($id, 0));
                        $ShortList = UserRequestOp::getShortList($id, $Offset, $Limit);
                        #CommonHelper::pr($Model);exit;
                        #$UserPhotoModel = new UserPhotos();
                        $Photos = array();
                        if (count($TotalRecords)) {
                            foreach ($ShortList as $Key => $Value) {
                                if ($Value->from_user_id == $id) {
                                    $Model[$Key] = $Value->toUserInfo;
                                    $UserTempId = $Value->to_user_id;
                                } else {
                                    $UserTempId = $Value->from_user_id;
                                    $Model[$Key] = $Value->fromUserInfo;
                                }
                                $Photos[$UserTempId] = $this->getPhotoList($UserTempId);
                            }
                        }
                    } else {
                        $SearchStatus = 1;
                        $TotalRecords = count(User::searchBasic($WHERE, 0));
                        $Model = User::searchBasic($WHERE, $Offset, $Limit);
                        #$Photos = array();
                        if (count($Model)) {
                            foreach ($Model as $SK => $SV) {
                                $Photos[$SV->id] = $this->getPhotoList($SV->id);
                            }
                        }
                    }
                } else {
                    return $this->render('searchlist',
                        [
                            'ErrorStatus' => 1,
                            'ErrorMessage' => Yii::$app->params['searchListInCorrectErrorMessage']
                        ]
                    );
                }
            } else {
                $Gender = $session->get('Profile_for');
                $Community = $session->get('iCommunity_ID');
                $CountryId = $session->get('iCountryId');
                $Gotra = $session->get('iGotraID');
                $Mangalik = $session->get('Mangalik');
                $SubCommunity = $session->get('iSubCommunity_ID');
                $HeightFrom = $session->get('HeightFrom');
                $HeightTo = $session->get('HeightTo');
                $ReligionID = $session->get('iReligion_ID');
                $MaritalStatusID = $session->get('Marital_Status');
                $AgeFrom = $session->get('AgeFrom');
                $AgeTo = $session->get('AgeTo');
            }
        }
        if ($ref == '') {

            $SearchStatus = 1;
            #$WHERE = '';
            if ($Gender == '') {
                if ($TempModel->Gender == 'MALE') {
                    $WHERE .= " AND user.Gender = 'FEMALE'";
                } else if ($TempModel->Gender == 'FEMALE') {
                    $WHERE .= " AND user.Gender = 'MALE'";
                }
            }
            $WHERE .= ($Gender != '') ? ' AND user.Gender = "' . $Gender . '" ' : '';

            $WHERE .= ($Community != '') ? ' AND user.iCommunity_ID = "' . $Community . '" ' : '';
            $WHERE .= ($CountryId != '') ? ' AND user.iCountryId = "' . $CountryId. '" ' : '';
            $WHERE .= ($Mangalik != '') ? ' AND user.Mangalik = "' . $Mangalik . '" ' : '';
            $WHERE .= ($Gotra != '') ? ' AND user.iGotraID = "' . $Gotra . '" ' : '';
            $WHERE .= ($SubCommunity != '') ? ' AND user.iSubCommunity_ID = "' . $SubCommunity . '" ' : '';
            #$WHERE .= ($HeightFrom != '') ? ' AND user.iHeightID = "' . $HeightFrom . '" ' : '';
            $WHERE .= ($ReligionID != '') ? ' AND user.iReligion_ID = "' . $ReligionID . '" ' : '';
            #$WHERE .= ($MaritalStatusID != '') ? ' AND user.Marital_Status = "' . $MaritalStatusID . '" ' : '';
            $WHERE .= ($MaritalStatusID != '') ? ' AND user.iMaritalStatusID = "' . $MaritalStatusID . '" ' : '';
            $WHERE .= ($AgeFrom != '') ? ' AND ( (user.Age >= "' . $AgeFrom . '") OR (TIMESTAMPDIFF(YEAR, user.DOB, CURDATE()) >= "' . $AgeFrom . '"))' : '';
            $WHERE .= ($AgeTo != '') ? ' AND ((user.Age <= "' . $AgeTo . '") OR (TIMESTAMPDIFF(YEAR, user.DOB, CURDATE()) <= "' . $AgeTo . '")) ' : '';

            $WHERE .= ($HeightFrom != '') ? ' AND master_heights.Centimeters >= "' . $HeightFrom . '" ' : '';
            $WHERE .= ($HeightTo != '') ? ' AND master_heights.Centimeters <= "' . $HeightTo . '" ' : '';

            $WHERE .= $WhereId;
            #echo $WHERE;exit;
            $Limit = Yii::$app->params['searchingLimit'];
            $Offset = (Yii::$app->request->get('Offset') == 0) ? 0 : Yii::$app->request->get('Offset');
            $Page = (Yii::$app->request->get('page') == 0 || Yii::$app->request->get('page') == '') ? 0 : Yii::$app->request->get('page');
            if ($Page) {
                $Page = $Page - 1;
                $Offset = $Limit * $Page;
            } else {
                $Page = 0;
                $Offset = 0;
            }
            $TotalRecords = count(User::searchBasic($WHERE, 0));
            $Model = User::searchBasic($WHERE, $Offset, $Limit);
            $Photos = array();
            if (count($Model)) {
                foreach ($Model as $SK => $SV) {
                    $Photos[$SV->id] = $this->getPhotoList($SV->id);
                }
            }
            #$id = Yii::$app->user->identity->id;
            #$TempModel = ($id != null) ? User::findOne($id) : array();
            $TempModel->iCommunity_ID = $Community;
            $TempModel->iCountryId = $CountryId;
            $TempModel->iSubCommunity_ID = $SubCommunity;
            $TempModel->iReligion_ID = $ReligionID;
            $TempModel->Marital_Status = $MaritalStatusID;
            $TempModel->HeightFrom = $HeightFrom;
            $TempModel->HeightTo = $HeightTo;
            $TempModel->Profile_for = $Gender;
            $TempModel->AgeFrom = $AgeFrom;
            $TempModel->AgeTo = $AgeTo;
            $TempModel->Mangalik = $Mangalik;
            $TempModel->iGotraID = $Gotra;
        }
        return $this->render('searchlist',
            [
                'ErrorStatus' => 0,
                'SearchStatus' => $SearchStatus,
                'Model' => $Model,
                'TotalRecords' => $TotalRecords,
                'Photos' => $Photos,
                'Offset' => $Offset,
                'Limit' => $Limit,
                'Page' => $Page,
                'TempModel' => $TempModel

            ]
        );
    }

    public function actionAjaxBasicSearch()
    {
        $request = Yii::$app->request;
        $session = Yii::$app->session;
        $params = $request->bodyParams;

        $WHERE = '';
        if (Yii::$app->user->isGuest) {
            return $this->redirect(Yii::$app->homeUrl . "?ref=login");
            exit;
            #$TempModel = new User();
        } else {
            $id = Yii::$app->user->identity->id;
            $TempModel = User::findOne($id);
            $WhereId = " AND user.id != " . $id;
        }


        $Gender = "";

        $Community = $params['iCommunity_ID'];
        $CountryId = $params['iCountryId'];
        $StateId = $params['iStateId'];
        $CityId = $params['iCityId'];
        $Mangalik = $params['Mangalik'];
        $Gotra = $params['iGotraID'];
        $SubCommunity = $params['iSubCommunity_ID'];
        $HeightFrom = $params['HeightFrom'];
        $HeightTo = $params['HeightTo'];
        $ReligionID = $params['iReligion_ID'];
        $MaritalStatusID = $params['MaritalStatusID'];
        $AgeFrom = $params['AgeFrom'];
        $AgeTo = $params['AgeTo'];


        $SearchStatus = 1;
        #$WHERE = '';
        if ($Gender == '') {
            if ($TempModel->Gender == 'MALE') {
                $WHERE .= " AND user.Gender = 'FEMALE'";
            } else if ($TempModel->Gender == 'FEMALE') {
                $WHERE .= " AND user.Gender = 'MALE'";
            }
        }
        $WHERE .= ($Gender != '') ? ' AND user.Gender = "' . $Gender . '" ' : '';

        $WHERE .= ($Community != '') ? ' AND user.iCommunity_ID = "' . $Community . '" ' : '';
        $WHERE .= ($CountryId != '') ? ' AND user.iCountryId = "' . $CountryId. '" ' : '';
        $WHERE .= ($StateId != '') ? ' AND user.iStateId = "' . $StateId. '" ' : '';
        $WHERE .= ($CityId != '') ? ' AND user.iCityId = "' . $CityId. '" ' : '';
        $WHERE .= ($Mangalik != '') ? ' AND user.Mangalik = "' . $Mangalik . '" ' : '';
        $WHERE .= ($Gotra != '') ? ' AND user.iGotraID = "' . $Gotra . '" ' : '';
        $WHERE .= ($SubCommunity != '') ? ' AND user.iSubCommunity_ID = "' . $SubCommunity . '" ' : '';
        #$WHERE .= ($HeightFrom != '') ? ' AND user.iHeightID = "' . $HeightFrom . '" ' : '';
        $WHERE .= ($ReligionID != '') ? ' AND user.iReligion_ID = "' . $ReligionID . '" ' : '';
        #$WHERE .= ($MaritalStatusID != '') ? ' AND user.Marital_Status = "' . $MaritalStatusID . '" ' : '';
        $WHERE .= ($MaritalStatusID != '') ? ' AND user.iMaritalStatusID = "' . $MaritalStatusID . '" ' : '';
        $WHERE .= ($AgeFrom != '') ? ' AND ( (user.Age >= "' . $AgeFrom . '") OR (TIMESTAMPDIFF(YEAR, user.DOB, CURDATE()) >= "' . $AgeFrom . '"))' : '';
        $WHERE .= ($AgeTo != '') ? ' AND ((user.Age <= "' . $AgeTo . '") OR (TIMESTAMPDIFF(YEAR, user.DOB, CURDATE()) <= "' . $AgeTo . '")) ' : '';

        $WHERE .= ($HeightFrom != '') ? ' AND master_heights.Centimeters >= "' . $HeightFrom . '" ' : '';
        $WHERE .= ($HeightTo != '') ? ' AND master_heights.Centimeters <= "' . $HeightTo . '" ' : '';

        $WHERE .= $WhereId;
        #echo $WHERE;exit;
        $Limit = Yii::$app->params['searchingLimit'];
        $Offset = (Yii::$app->request->get('Offset') == 0) ? 0 : Yii::$app->request->get('Offset');
        $Page = (Yii::$app->request->get('page') == 0 || Yii::$app->request->get('page') == '') ? 0 : Yii::$app->request->get('page');
        if ($Page) {
            $Page = $Page - 1;
            $Offset = $Limit * $Page;
        } else {
            $Page = 0;
            $Offset = 0;
        }
        $TotalRecords = count(User::searchBasic($WHERE, 0));
        $Model = User::searchBasic($WHERE, $Offset, $Limit);
        $Photos = array();
        if (count($Model)) {
            foreach ($Model as $SK => $SV) {
                $Photos[$SV->id] = $this->getPhotoList($SV->id);
            }
        }
        #$id = Yii::$app->user->identity->id;
        #$TempModel = ($id != null) ? User::findOne($id) : array();
        $TempModel->iCommunity_ID = $Community;
        $TempModel->iCountryId = $CountryId;
        $TempModel->iStateId = $StateId;
        $TempModel->iCityId = $CityId;
        $TempModel->Mangalik = $Mangalik;
        $TempModel->iGotraID = $Gotra;
        $TempModel->iSubCommunity_ID = $SubCommunity;
        $TempModel->iReligion_ID = $ReligionID;
        $TempModel->Marital_Status = $MaritalStatusID;
        $TempModel->HeightFrom = $HeightFrom;
        $TempModel->HeightTo = $HeightTo;
        $TempModel->Profile_for = $Gender;
        $TempModel->AgeFrom = $AgeFrom;
        $TempModel->AgeTo = $AgeTo;

        $recordData = "";

        $sendToNext = "";

        if ($TotalRecords == 0) {
            $sendToNext .= "<div class=\"white-section listing border-sharp mrg-tp-10\">
                                            <div class=\"row mrg-tp-10\">
                                                <div class=\"col-md-12\">
                                                    <div class=\"notice kp_info\">
                                                        <p>" . Yii::$app->params['noRecordsFoundInSearchList'] . "</p>
                                                    </div>
                                                    <div class=\"clearfix\"></div>
                                                    <span class=\"pull-right\"><a href='" . Yii::$app->homeUrl . "' class=\"text-right\">Back To
                                        Home Page<i class=\"fa fa-angle-right\"></i></a></span>
                                                </div>
                                            </div>
                                        </div>";
            $recordData = $sendToNext;
        } else {
            foreach ($Model as $SK => $SV) {
                if ($SK == 0) {
                    $text = "listing";
                } else {
                    $text = "";
                }

                $sendToNext .= "<div class='white-section " . $text . " border-sharp mrg-tp-10'>";
                $sendToNext .= "<div class='row'><div class='col-sm-3 col-xs-4'><div class='prof-pic'><div class=\"drop-effect\"></div>";

                $sendToNext .= "<div class=\"slider\">
                                    <div id=\"carousel-example-generic_$SK\" class=\"carousel slide\" data-ride=\"carousel\">
                                        <div class=\"carousel-inner\">";

                if (is_array($Photos[$SV->id])) {
                    foreach ($Photos[$SV->id] as $K => $V) {
                        $SELECTED = '';
                        $Photo = Yii::$app->params['thumbnailPrefix'] . '200_' . $V->File_Name;
                        $Yes = 'No';
                        if ($V['Is_Profile_Photo'] == 'YES') {
                            $SELECTED = "active";
                            $Photo = '200' . $SV->propic;
                            $Yes = 'Yes';
                        }
                        if ($K == 0) {
                            $st = 'active';
                        } else {
                            $st = '';
                        }
                        $sendToNext .= "<div class=\"item $st kp_pic_dis_dwn\"> " . Html::img(CommonHelper::getPhotos('USER', $SV->id, $Photo, 200, '', $Yes, CommonHelper::getVisiblePhoto($SV->id, $V['eStatus'])), ['width' => '205', 'height' => '205', 'alt' => 'Profile', 'class' => 'img-responsive']) . " </div>";
                    }
                } else {
                    $sendToNext .= "<div class=\"item active kp_pic_dis_dwn\">" . Html::img(CommonHelper::getPhotos('USER', $SV->id, $Photos[$SV->id], 120), ['width' => '205', 'height' => '205', 'alt' => 'Profile', 'class' => 'img-responsive item']) . " </div>";
                }

                $sendToNext .= "</div>";

                $sendToNext .= "</div></div>";
                $sendToNext .= "</div></div>";

                $sendToNext .= "<div class=\"col-sm-9\"><div class=\"name-panel\"><h2 class=\"nameplate\">";

                if (!Yii::$app->user->isGuest) {
                    $sendToNext .= "<a href='" . Yii::$app->homeUrl . "user/profile?uk=" . $SV->Registration_Number . "&source=profile_viewed_by' class=\"name\" title=" . $SV->Registration_Number . ">
                                            $SV->FullName </a>";
                } else {
                    $sendToNext .= $SV->FullName;
                }

                $sendToNext .= "<span class=\"font-light\">($SV->Registration_Number)</span>";

                $USER_PHONE = \common\models\User::weightedCheck(8);
                $USER_EMAIL = \common\models\User::weightedCheck(9);
                $USER_APPROVED = \common\models\User::weightedCheck(10);
                if ($USER_PHONE && $USER_EMAIL && $USER_APPROVED) {
                    $sendToNext .= "<span class=\"premium\"></span>";
                }
                $sendToNext .= "</h2>";

                $USER_PHONE = \common\models\User::weightedCheck(8);

                $sendToNext .= "<p>Profile created for $SV->Profile_created_for |
                                    Last online " . CommonHelper::DateTime($SV->LastLoginTime, 28) . "|";

                $sendToNext .= "<span class=\"pager-icon\"><a href=\"javascript:void(0)\"><i class=\"fa fa-mobile\"></i>";

                if ($SV->ePhoneVerifiedStatus != 'Yes') {
                    $badges = "badge1";
                }

                $sendToNext .= "<span class=\"badge '$badges\"><i class=\"fa fa-check\"></i> </span>";

                $sendToNext .= "</a></span></p>";

                $sendToNext .= "</div>";


                $sendToNext .= "<dl class=\"dl-horizontal mrg-tp-20\">
                                <dt>Personal Details</dt>";

                $rsNm = CommonHelper::setInputVal($SV->raashiName->Name, 'text');

                if ($SV->RaashiId > 0) {
                    $rs = ",'.$rsNm.'";
                } else {
                    $rs = "";
                }

                $sendToNext .= "<dd> " . CommonHelper::getAge($SV->DOB) . " yrs, " . CommonHelper::setInputVal($SV->height->vName, 'text') . " $rs </dd>";

                $sendToNext .= "<dt>Marital Status</dt>
                                <dd>" . CommonHelper::setInputVal($SV->maritalStatusName->vName, 'text') . "</dd>";

                $sendToNext .= "<dt>Religion Community</dt>
                                <dd>" . CommonHelper::setInputVal($SV->religionName->vName, 'text') . ',' . CommonHelper::setInputVal($SV->communityName->vName, 'text') . "</dd>";

                $sendToNext .= "<dt>Education</dt>
                                <dd>" . CommonHelper::setInputVal($SV->educationLevelName->vEducationLevelName, 'text') . "</dd>";

                $sendToNext .= "<dt>Profession</dt>
                                <dd>" . CommonHelper::setInputVal($SV->workingAsName->vWorkingAsName, 'text') . "</dd>";

                $sendToNext .= "<dt>Current Location</dt>
                                <dd>" . CommonHelper::setInputVal($SV->cityName->vCityName, 'text') . ', ' . CommonHelper::setInputVal($SV->countryName->vCountryName, 'text') . "</dd>";

                $sendToNext .= "</dl>";


                $sendToNext .= "</div></div>";


                if (!Yii::$app->user->isGuest) {
                    if ($SV->Gender != Yii::$app->user->identity->Gender) {

                        $sendToNext .= "<div class=\"row gray-bg\">
                            <div class=\"col-sm-12\">
                                <div class=\"profile-control-vertical\">";

                        $sendToNext .= "<ul class=\"list-unstyled pull-right\">";


                        $Value = \common\models\UserRequestOp::checkSendInterest(Yii::$app->user->identity->id, $SV->id);
                        $Shortlisted = 0;
                        if (Yii::$app->user->identity->id == $Value->from_user_id && $Value->short_list_status_from_to == 'Yes') {
                            $Shortlisted = 1;
                        }
                        if (Yii::$app->user->identity->id == $Value->to_user_id && $Value->short_list_status_to_from == 'Yes') {
                            $Shortlisted = 1;
                        }
                        $sendToNext .= "<li class=\"sl__$SV->id\">";

                        if ($Shortlisted) {
                            $sendToNext .= "<a href=\"javascript:void(0)\">Shortlisted
                                    <i class=\"fa fa-list-ul\"></i></a>";
                        } else {
                            $sendToNext .= "<a href=\"javascript:void(0)\"
                                   class=\"short_list_$SV->id shortlistUser\"
                                   data-id=\"$SV->id\"
                                   data-name=\"$SV->fullName\">Shortlist
                                    <i class=\"fa fa-list-ul\"></i></a>";
                        }
                        $sendToNext .= "</li>";

                        $sendToNext .= "<li class=\"s__$SV->id\">";

                        if (count($Value)) {
                            $Id = 0;
                            if ($Id == $Value->from_user_id && $Value->profile_viewed_from_to == 'Yes') {
                                $ViewerId = $Value->to_user_id;
                            } else {
                                $ViewerId = $Value->from_user_id;
                            }
                        } else {
                            $ViewerId = $Value->id;
                        }

                        $UserInfoModel = \common\models\User::getUserInfroamtion($ViewerId);

                        if (count($Value) == 0 || ($Id == $Value->from_user_id && $Value->send_request_status_from_to == 'No' && $Value->send_request_status_to_from == 'No') || ($Id == $Value->to_user_id && $Value->send_request_status_to_from == 'No' && $Value->send_request_status_from_to == 'No')) {
                            $sendToNext .= "<a href=\"javascript:void(0)\" class=\" sendinterestpopup\" role=\"button\" data-target=\"#sendInterest\" data-toggle=\"modal\" data-id=\"$SV->id\" data-name=\"$SV->fullName\" data-rgnumber=\"$SV->Registration_Number\">Send Interest<i class=\"fa fa-heart-o\"></i></a>"; }
                            else if (($Id == $Value->from_user_id && $Value->send_request_status_from_to == 'Yes' && $Value->send_request_status_to_from != 'Yes') || ($Id == $Value->to_user_id && $Value->send_request_status_to_from == 'Yes' && $Value->send_request_status_from_to != 'Yes')) { ?>
                            <?php $sendToNext .= "<a href=\"javascript:void(0)\"
                                   class=\" ci \" role=\"button\"
                                   data-target=\"#\"
                                   data-toggle=\"modal\"
                                   data-id=\"$UserInfoModel->id \"
                                   data-name=\"$UserInfoModel->fullName \"
                                   data-rgnumber=\"$UserInfoModel->Registration_Number \">Cancel
                                    Interest
                                    <i class=\"fa fa-close\"></i> </a>";

                        } else if (($Id == $Value->to_user_id && $Value->send_request_status_from_to == 'Yes' && $Value->send_request_status_to_from != 'Yes') || ($Id == $Value->from_user_id && $Value->send_request_status_to_from == 'Yes' && $Value->send_request_status_from_to != 'Yes')) {
                            ?>
                            <?php $sendToNext .= "<a href=\"javascript:void(0)\"
                               class=\" accept_decline adbtn\"
                               role=\"button\"
                               data-target=\"#accept_decline\"
                               data-toggle=\"modal\"
                               data-id=\"$UserInfoModel->id \"
                               data-name=\"$UserInfoModel->fullName \"
                               data-rgnumber=\"$UserInfoModel->Registration_Number \"
                               data-type=\"Accept Interest\">
                                Accept
                                <i class=\"fa fa-check\"></i>
                            </a>
                            <a href=\"javascript:void(0)\"
                               class=\"accept_decline adbtn\"
                               role=\"button\"
                               data-target=\"#accept_decline\"
                               data-toggle=\"modal\"
                               data-id=\"$UserInfoModel->id \"
                               data-name=\"$UserInfoModel->fullName \"
                               data-rgnumber=\"$UserInfoModel->Registration_Number \"
                            >
                                Decline
                                <i class=\"fa fa-close\"></i> </a>"; ?>

                        <?php } else if ($Value->send_request_status_from_to == 'Accepted' || $Value->send_request_status_to_from == 'Accepted') {
                            ?>
                            <?php $sendToNext .= " <a href=\"javascript:void(0)\" class=\"\"
                               role=\"button\"
                               data-target=\"#\" data-toggle=\"modal\"
                               data-id=\"$UserInfoModel->id \"
                               data-name=\"$UserInfoModel->fullName \"
                               data-rgnumber=\"$UserInfoModel->Registration_Number \"
                               data-type=\"Connected\">Connected
                                <i class=\"fa fa-heart\"></i> </a>"; ?>

                        <?php } else if ($Value->send_request_status_from_to == 'Rejected' || $Value->send_request_status_to_from == 'Rejected') {
                            ?>
                            <?php $sendToNext .= "<a href=\"javascript:void(0)\" class=\" \"
                               role=\"button\"
                               data-target=\"#\" data-toggle=\"modal\"
                               data-id=\"$UserInfoModel->id \"
                               data-name=\"$UserInfoModel->fullName \"
                               data-rgnumber=\"$UserInfoModel->Registration_Number \"
                               data-type=\"Connected\">Rejected <i
                                        class=\"fa fa-close\"></i>
                            </a>"; ?>
                        <?php } else { ?>
                            <?php $sendToNext .= "<a href=\"javascript:void(0)\"
                               class=\"btn btn-link isent\"
                               role=\"button\">Interest
                                Sent <i class=\"fa fa-heart\"></i></a>"; ?>
                        <?php }
                        $sendToNext .= "</li>"; ?>

                        <?php
                        if (Yii::$app->user->isGuest) {
                            $emailConrt = "data-target='\#sendMail\'";
                        } else {
                            $emailConrt = "data-id='$SV->id'";
                        }


                        $sendToNext .= "<li>
                            <a href=\"#\" data-toggle=\"modal\"
                               class=\"send_email\" $emailConrt
                               ?>>Send Email <i class=\"fa fa-envelope-o\"></i>
                            </a>
                        </li>";

                        $sendToNext .= "</ul>";
                        $sendToNext .= "</div>
                            </div>
                        </div>";

                    } ?>

                <?php } else {

                    $sendToNext .=  "<div class=\"row gray-bg\">
                        <div class=\"col-sm-12\">
                            <div class=\"profile-control-vertical\">
                                <ul class=\"list-unstyled pull-right\">
                                    <li>
                                        <a href=\"javascript:void(0)\" class=\"send_email\"
                                           role=\"button\"
                                           data-target=\"#sendMail\" data-toggle=\"modal\"
                                        >Shortlist <i class=\"fa fa-list-ul\"></i></a>
                                    </li>
                                    <li class=\"s__$SV->id\">
                                        <a href=\"javascript:void(0)\" class=\"send_email\"
                                           role=\"button\"
                                           data-target=\"#sendMail\" data-toggle=\"modal\"
                                        >Send Interest <i class=\"fa fa-heart-o\"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"#\" data-toggle=\"modal\"
                                           class=\"send_email\"
                                           data-target=\"#sendMail\">Send Email <i
                                                    class=\"fa fa-envelope-o\"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>" ;
                    ?>

                <?php }

                $sendToNext .= "</div>";


            }
            $msg = "";
            $msg .= "<div class=\"mrg-bt-10 text-center\"><nav>";


            $URL = $MainUrl = "/kande_pohe/search/basic-search";
            //$URL = $MainUrl = Yii::$app->request->url;

            //die($URL);

            $Page += 1;
            if (isset($_REQUEST['ref'])) {
                $URL = str_replace("&page=" . $Page, '', $URL);
                $PageURL = '&page=';
            } else {
                $URL = str_replace("?page=" . $Page, '', $URL);
                $PageURL = '?page=';
            }
            $total = ceil($TotalRecords / $Limit);
            $id = $Page;
            if (1) {
                #if($Page ==0)
                $cur_page = $Page;
                $Page -= 1;
                $per_page = $Limit;
                $previous_btn = true;
                $next_btn = true;
                $first_btn = true;
                $last_btn = true;
                $start = $Page * $per_page;
                if ($TotalRecords > $Limit) {
                    $no_of_paginations = ceil($TotalRecords / $per_page);
                    if ($no_of_paginations < $cur_page) {
                        return Yii::$app->response->redirect($_SESSION['previous_location']);
                    } else {
                        $_SESSION['previous_location'] = $MainUrl;
                    }
                    if ($cur_page >= 7) {
                        $start_loop = $cur_page - 3;
                        if ($no_of_paginations > $cur_page + 3)
                            $end_loop = $cur_page + 3;
                        else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
                            $start_loop = $no_of_paginations - 6;
                            $end_loop = $no_of_paginations;
                        } else {
                            $end_loop = $no_of_paginations;
                        }
                    } else {
                        $start_loop = 1;
                        if ($no_of_paginations > 7)
                            $end_loop = 7;
                        else
                            $end_loop = $no_of_paginations;
                    }
                    $msg .= '<ul class="pagination pagination-lg">';
                    if ($previous_btn && $cur_page > 1) {
                        $pre = $cur_page - 1;
                        $msg .= '<li class="page-item first" p="' . $pre . '">
                        <a class="page-link" href="' . $URL . $PageURL . $pre . '" aria-label="Previous">
                            <span aria-hidden="true">Previous</span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>';
                    }
                    for ($i = $start_loop; $i <= $end_loop; $i++) {
                        if ($cur_page == $i)
                            $msg .= '<li class="page-item active"><a class="page-link" href="' . $URL . $PageURL . $i . '">' . $i . '</a></li>';
                        else
                            $msg .= '<li class="page-item "><a class="page-link" href="' . $URL . $PageURL . $i . '">' . $i . '</a></li>';
                    }
                    if ($next_btn && $cur_page < $no_of_paginations) {
                        $nex = $cur_page + 1;
                        $msg .= '<li class="page-item last">
                                                <a class="page-link" href="' . $URL . $PageURL . $nex . '" aria-label="Next">
                                                    <span aria-hidden="true">Next</span>
                                                    <span class="sr-only">Next</span>
                                                </a>
                                            </li>';
                    }
                    $msg = $msg . "</ul>" . $goto . $total_string . "";
                }
            }
            $msg .= "</nav></div>";
            $recordData = $sendToNext;
        }


       $stateDiv = "";$stateDiv .= "<div class=\"col-md-6\" >
                                                        <div class=\"box\">
                                                            <div class=\"mid-col\">
                                                                <div class=\"form-cont bs hovertool\"
                                                                     data-toggle=\"tooltip\"
                                                                     data-placement=\"top\"
                                                                     data-original-title= ".Yii::$app->params['messageReligionBS']." />";


                                                                    if (isset($CountryId)) {
                                                                        $CountryId = $CountryId;
                                                                    }else{
                                                                        $CountryId = "";
                                                                    }


        $stateDiv .=  Html::dropDownList('State', null,
                                                                            ArrayHelper::map(CommonHelper::getState($CountryId), 'iStateId', 'vStateName'),
                                                                                ['class' => 'demo-default select-beast',
                                                                                    'id' => 'iStateId',
                                                                                    'prompt' => 'State',
                                                                                    'onchange' => 'requestSearchFilter(this.value)']
                                                                            );

        $stateDiv .="</div></div></div></div>";


        $cityDiv = ""; $cityDiv .= "<div class=\"col-md-6\">
                                                        <div class=\"box\">
                                                            <div class=\"mid-col\">
                                                                <div class=\"form-cont bs hovertool\"
                                                                     data-toggle=\"tooltip\"
                                                                     data-placement=\"top\"
                                                                     data-original-title= ".Yii::$app->params['messageReligionBS']." />";


                                                                    if (isset($StateId)) {
                                                                        $StateId = $StateId;
                                                                    }else{
                                                                        $StateId = "";
                                                                    }


        $cityDiv .=  Html::dropDownList('City', null,
                                                                            ArrayHelper::map(CommonHelper::getCity($StateId), 'iCityId', 'vCityName'),
                                                                                ['class' => 'demo-default select-beast',
                                                                                    'id' => 'iCityId',
                                                                                    'multiple'=>'multiple',
                                                                                    'prompt' => 'City',
                                                                                    'onchange' => 'requestSearchFilter(this.value)']
                                                                            );

        $cityDiv .="</div></div></div></div>"
        ?>
<?php

        $array = array(
            'ErrorStatus' => 0,
            'SearchStatus' => $SearchStatus,
            'Model' => $Model,
            'TotalRecords' => $TotalRecords,
            'Photos' => $Photos,
            'Offset' => $Offset,
            'Limit' => $Limit,
            'Page' => $Page,
            'TempModel' => $TempModel,
            'RecordData' => $recordData,
            'PaginationData' => $msg,
            'stateDiv' => $stateDiv,
            'cityDiv' => $cityDiv,
        );
        return json_encode($array);
    }

    public
    function getPhotoList($Id)
    {
        $UserPhotoModel = new UserPhotos();
        #$PhotoList = $UserPhotoModel->findByUserId($Id);
        $PhotoList = $UserPhotoModel->userPhotoList($Id);
        if (count($PhotoList)) {
            $Photos = $PhotoList;
        } else {
            $Photos = CommonHelper::getUserDefaultPhoto();
        }
        return $Photos;
    }

    public
    function actionAdvancedSearch()
    {
        $request = Yii::$app->request;
        $session = Yii::$app->session;
        $params = $request->bodyParams;
        #CommonHelper::pr($params);exit;
        //http://localhost/KandePohe/search/basic-search?search-type=basic&profile-for=FEMALE&Community=1&sub-community=1&agerange=19&height=2
        if (Yii::$app->user->isGuest) {
            $TempModel = new User();
        } else {
            $id = Yii::$app->user->identity->id;
            $TempModel = User::findOne($id);
        }

        if ($TempModel->load(Yii::$app->request->post())) {
            $Gender = $params['User']['Profile_for'];
            $Community = $params['User']['iCommunity_ID'];
            $Mangalik = $params['User']['Mangalik'];
            $SubCommunity = $params['User']['iSubCommunity_ID'];
            $Height = $params['User']['iHeightID'];
            $ReligionID = $params['User']['iReligion_ID'];
            $MaritalStatusID = $params['User']['Marital_Status'];
            if ($params['User']['Agerange'] != '') {
                list($AgeFrom, $AgeTo) = explode("-", $params['User']['Agerange']);
            } else {

                $AgeFrom = $params['User']['AgeFrom'];
                $AgeTo = $params['User']['AgeTo'];
            }

            $session->set('Profile_for', $Gender);
            $session->set('iCommunity_ID', $Community);
            $session->set('Mangalik', $Mangalik);
            $session->set('iSubCommunity_ID', $SubCommunity);
            $session->set('iHeightID', $Height);
            $session->set('iReligion_ID', $ReligionID);
            $session->set('Marital_Status', $MaritalStatusID);
            $session->set('AgeFrom', $AgeFrom);
            $session->set('AgeTo', $AgeTo);
        } else {
            $Gender = $session->get('Profile_for');
            $Community = $session->get('iCommunity_ID');
            $Mangalik = $session->get('Mangalik');
            $SubCommunity = $session->get('iSubCommunity_ID');
            $Height = $session->get('iHeightID');
            $ReligionID = $session->get('iReligion_ID');
            $MaritalStatusID = $session->get('Marital_Status');
            $AgeFrom = $session->get('AgeFrom');
            $AgeTo = $session->get('AgeTo');
        }
        $WHERE = '';
        $WHERE .= ($Gender != '') ? ' AND user.Gender = "' . $Gender . '" ' : '';
        $WHERE .= ($Community != '') ? ' AND user.iCommunity_ID = "' . $Community . '" ' : '';
        $WHERE .= ($Mangalik != '') ? ' AND user.Mangalik = "' . $Mangalik . '" ' : '';
        $WHERE .= ($SubCommunity != '') ? ' AND user.iSubCommunity_ID = "' . $SubCommunity . '" ' : '';
        $WHERE .= ($Height != '') ? ' AND user.iHeightID = "' . $Height . '" ' : '';
        $WHERE .= ($ReligionID != '') ? ' AND user.iReligion_ID = "' . $ReligionID . '" ' : '';
        $WHERE .= ($MaritalStatusID != '') ? ' AND user.Marital_Status = "' . $MaritalStatusID . '" ' : '';
        $WHERE .= ($AgeFrom != '') ? ' AND user.Age >= "' . $AgeFrom . '" ' : '';
        $WHERE .= ($AgeTo != '') ? ' AND user.Age <= "' . $AgeTo . '" ' : '';
        $Limit = Yii::$app->params['searchingLimit'];
        $Offset = (Yii::$app->request->get('Offset') == 0) ? 0 : Yii::$app->request->get('Offset');
        $Page = (Yii::$app->request->get('page') == 0 || Yii::$app->request->get('page') == '') ? 0 : Yii::$app->request->get('page');
        if ($Page) {
            $Page = $Page - 1;
            $Offset = $Limit * $Page;
        } else {
            $Page = 0;
            $Offset = 0;
        }

        $TotalRecords = count(User::searchBasic($WHERE, 0));
        $Model = User::searchBasic($WHERE, $Offset, $Limit);

        $UserPhotoModel = new UserPhotos();
        $Photos = array();
        if (count($Model)) {
            foreach ($Model as $SK => $SV) {
                $PhotoList = $UserPhotoModel->findByUserId($SV->id);
                if (count($PhotoList)) {
                    $Photos[$SV->id] = $PhotoList;
                } else {
                    $Photos[$SV->id] = CommonHelper::getUserDefaultPhoto();
                }
            }
        }
        #$id = Yii::$app->user->identity->id;
        #$TempModel = ($id != null) ? User::findOne($id) : array();
        $TempModel->iCommunity_ID = $Community;
        $TempModel->Mangalik = $Mangalik;
        $TempModel->iSubCommunity_ID = $SubCommunity;
        $TempModel->iReligion_ID = $ReligionID;
        $TempModel->Marital_Status = $MaritalStatusID;
        $TempModel->iHeightID = $Height;
        $TempModel->Profile_for = $Gender;
        $TempModel->AgeFrom = $AgeFrom;
        $TempModel->AgeTo = $AgeTo;
        return $this->render('advancesearch',
            [
                'Model' => $Model,
                'TotalRecords' => $TotalRecords,
                'Photos' => $Photos,
                'Offset' => $Offset,
                'Limit' => $Limit,
                'Page' => $Page,
                'TempModel' => $TempModel

            ]
        );
    }

    function actionRenderCall($model, $view, $show = false, $popup = false, $flag = false, $temp = array())
    {
        return $this->renderAjax($view, [
            'model' => $model,
            'show' => $show,
            'popup' => $popup,
            'flag' => $flag,
            'temp' => $temp,
        ]);
    }

    public
    function actionShortList()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $Id = Yii::$app->user->identity->id;
        $Limit = Yii::$app->params['searchingLimit'];
        $Offset = (Yii::$app->request->get('Offset') == 0) ? 0 : Yii::$app->request->get('Offset');
        $Page = (Yii::$app->request->get('page') == 0 || Yii::$app->request->get('page') == '') ? 0 : Yii::$app->request->get('page');
        if ($Page) {
            $Page = $Page - 1;
            $Offset = $Limit * $Page;
        } else {
            $Page = 0;
            $Offset = 0;
        }
        $ShortList = UserRequestOp::getMyShortListed($Id, $Offset, $Limit);
        #$ShortList = UserRequestOp::getShortList($Id, $Offset, $Limit);
        foreach ($ShortList as $Key => $Value) {
            #CommonHelper::pr($Value);exit;
            if ($Value->from_user_id == $Id) {
                $ModelInfo[$Key] = $Value->toUserInfo;
            } else {
                $ModelInfo[$Key] = $Value->fromUserInfo;
            }
        }
        #CommonHelper::pr($ModelInfo);
        #CommonHelper::pr($ShortList);exit;
        return $this->render('shortlist', [
            'Model' => $ShortList,
            'Id' => $Id,
            'TotalRecords' => count($ShortList),
            'Offset' => $Offset,
            'Limit' => $Limit,
            'Page' => $Page,
        ]);
    }
}