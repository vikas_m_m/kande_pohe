<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

use \yii\web\Request;
$baseUrl = str_replace('/frontend/web', '', (new Request)->getBaseUrl());

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl' => $baseUrl,
            'parsers' => [
                'application/json' => 'yii\web\JsonParser', // required for POST input via `php://input`
            ]
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'baseUrl' => $baseUrl,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            /*'rules' => array(
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),*/

            /*'rules' => [
                #'' => 'site/index',
                '<action>' => 'site/<action>',
                '<action>' => 'user/<action>',
                '<action>' => 'payment/<action>',
            ],*/
            "rules" => [
                #'' => 'site/index',
                #'<action>'=>'site/<action>',
                #'<action>'=>'user/<action>',
                #'<action>'=>'payment/<action>',

                //Site Controller
                "/" => "site/index",
                "about-us" => "site/about-us",
                "contact-us" => "site/contact-us",
                "terms-of-use" => "site/terms-of-use",
                "feedback" => "site/help-feedback",
                "logout" => "site/logout",
                "sign-up" => "site/sign-up",
                "subscriptions" => "site/subscriptions",

                "basic-details" => "site/basic-details",
                "education-occupation" => "site/education-occupation",
                "life-style" => "site/life-style",
                "about-family" => "site/about-family",
                "about-yourself" => "site/about-yourself",
                "verification" => "site/verification",
                "partner-preferences" => "site/partner-preferences",

                //User Controller
                "dashboard" => "user/dashboard",
                "my-profile" => "user/my-profile",
                "profile" => "user/profile",
                "photos" => "user/photos",
                "setting" => "user/setting",
                "subscription-history" => "user/subscription-history",
                "upgrade-plan" => "user/upgrade-plan",
                "upgrade-process" => "subscription/upgrade-process",

                "subscription-plan" => "user/subscription-plan",
                "subscription-plan-process" => "subscription/subscription-plan-process",

                "checkout" => "subscription/checkout-process",
                "plan-confirmation" => "subscription/plan-confirmation",

                //Payment Controller
                "payment" => "payment/payment",

                [
                    'pattern' => 'matrimony/education/<categories:.*>',
                    'route' => 'matrimony/education',
                    'encodeParams' => false,
                ],

                [
                    'pattern' => 'matrimony/city/<categories:.*>',
                    'route' => 'matrimony/city',
                    'encodeParams' => false,
                ],

                [
                    'pattern' => 'matrimony/occupation/<categories:.*>',
                    'route' => 'matrimony/occupation',
                    'encodeParams' => false,
                ],
                
                [
                    'pattern' => 'matrimony/marital-status/<categories:.*>',
                    'route' => 'matrimony/marital-status',
                    'encodeParams' => false,
                ],


                [
                    'pattern' => 'matrimony/physical-status/<categories:.*>',
                    'route' => 'matrimony/physical-status',
                    'encodeParams' => false,
                ],

                [
                    'pattern' => 'matrimony/caste/<categories:.*>',
                    'route' => 'matrimony/caste',
                    'encodeParams' => false,
                ],

                #"matrimony/" => "site/matrimony/",
                /*'<controller:\w+>/<id:\w+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\w+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                "matrimony/" => "matrimony/test",
                "matrimony/" => "matrimony/matrimony/",*/
               /* '<controller:(matrimony)>/<id:\d+>' => '<controller>/index',
                '<controller:(matrimony)>s' => '<controller>/test',
                'test' => 'matrimony/test',*/


                /*'<controller:\w+>/<action:\w+>/<id:\w+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',*/
                #"test/" => "site/test/",
            /*** #FInal
                '<controller:\w+>/<id:[\w-]+>' => '<controller>/view',
                '<controller:\w+>/<action:[\w-]+>/<id:[\w-]+>' => '<controller>/<action>',
                '<controller:\w+>/<action:[\w-]+>' => '<controller>/<action>',
            */
             
                /*'<alias:test>' => 'site/test/',
                '<alias:test><slug:\w+>' => 'site/test/',*/
                /*'<controller:\w+>/<slug:\w+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<slug:\w+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                '<action:\w+>/<slug:\w+>' => '<action>',*/


            ]
        ]

    ],
    'params' => $params,
];
