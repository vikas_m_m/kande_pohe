$(document).ready(function () {
    $(".pin").click(function () {
        $("p").slideToggle();
    });
});

$(document).ready(function () {
    (function () {
        [].slice.call(document.querySelectorAll('select.cs-select')).forEach(function (el) {
            new SelectFx(el);
        });
    })();
    jQuery(document).on('click', '[data-toggle*=modal]', function () {
        jQuery('[role*=dialog]').each(function () {
            switch (jQuery(this).css('display')) {
                case('block'):
                {
                    jQuery('#' + jQuery(this).attr('id')).modal('hide');
                    break;
                }
            }
        });
    });
    $(document).ready(function () {
        // Dirty fix for jumping scrollbar when modal opens
        $('#requestModal').on('show.bs.modal', function (e) {
            if ($(window).width() > 768) {
                $(".navbar-default").css("padding-right", "15px");
            }
        });

        $('#requestModal').on('hide.bs.modal', function (e) {
            if ($(window).width() > 768) {

                $(".navbar-default").css("padding-right", "0px");

            }

        });


    });


    function toggleChevron(e) {

        $(e.target)

            .prev('.panel-heading')

            .find("i.indicator")

            .toggleClass('fa fa-angle-double-up fa fa-angle-double-down');

    }

    $('#accordion').on('hidden.bs.collapse', toggleChevron);

    $('#accordion').on('shown.bs.collapse', toggleChevron);


    function init() {

        window.addEventListener('scroll', function (e) {

            var distanceY = window.pageYOffset || document.documentElement.scrollTop,

                shrinkOn = 100,

                header = document.querySelector("header");

            if (distanceY > shrinkOn) {

                classie.add(header, "smaller");

            } else {

                if (classie.has(header, "smaller")) {

                    classie.remove(header, "smaller");

                }

            }

        });

    }

    window.onload = init();


    $("#chatbox").click(function (e) {

        e.preventDefault();

        $('body').toggleClass('chat-move');

        $('.chatwe').toggleClass('chat-height');

    });


    $(function () {


        var $activate_selectator4 = $('#activate_selectator4');

        $activate_selectator4.click(function () {

            var $select4 = $('.select4');

            if ($select4.data('selectator') === undefined) {

                $select4.selectator({

                    showAllOptionsOnFocus: true

                });

                $activate_selectator4.val('destroy selectator');

            } else {

                $select4.selectator('destroy');

                $activate_selectator4.val('activate selectator');

            }

        });

        $activate_selectator4.trigger('click');


    });


    $(document).on('change', '.btn-file :file', function () {

        var input = $(this),

            numFiles = input.get(0).files ? input.get(0).files.length : 1,

            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');

        input.trigger('fileselect', [numFiles, label]);

    });


    $(document).ready(function () {

        $('.btn-file :file').on('fileselect', function (event, numFiles, label) {


            var input = $(this).parents('.input-group').find(':text'),

                log = numFiles > 1 ? numFiles + ' files selected' : label;


            if (input.length) {

                input.val(log);

            } else {

                if (log) alert(log);

            }


        });

    });


    $('head style[type="text/css"]');

    window.randomize = function () {

        $('.radial-progress').attr('data-progress', Math.floor(Math.random() * 100));

    }

    setTimeout(window.randomize, 200);

    $('.radial-progress').click(window.randomize);


// Salary range slider

    $(function () {

        $("#slider-range").slider({

            range: true,

            min: 0,

            max: 150,

            values: [0, 100],

            slide: function (event, ui) {

                $("#amount").val("" + ui.values[0]);

                $("#amount1").val("" + ui.values[1]);

            }

        });

        $("#amount").val("" + $("#slider-range").slider("values", 0));


        $("#amount1").val("" + $("#slider-range").slider("values", 1));

    });


// Age range slider

    $(function () {
        $("#slider-range-age").slider({
            range: true,
            min: 0,
            max: 100,
            values: [0, 100],
            slide: function (event, ui) {
                $("#age").val("" + ui.values[0]);
                $("#age1").val("" + ui.values[1]);
                /*console.log(ui.values[0]);
                 requestSearchFilter();*/
            },
            stop: function (event, ui) {
                requestSearchFilter();
            },
        });
        $("#age").val("" + $("#slider-range").slider("values", 0));
        $("#age1").val("" + $("#slider-range").slider("values", 1));
    });


// Height range slider
    $(function () {
        $("#slider-range-height").slider({
            range: true,
            min: 120,
            max: 204,
            values: [120, 204],
            slide: function (event, ui) {
                $("#height").val("" + ui.values[0]);

                $("#height1").val("" + ui.values[1]);
            },
            stop: function (event, ui) {
                requestSearchFilter();
            },
        });
        //alert( $( "#slider-range-height" ).slider( "values", 0 ));
        $("#height").val("" + $("#slider-range-height").slider("values", 0));
        $("#height1").val("" + $("#slider-range-height").slider("values", 1));
    });

// Weight range slider
    $(function () {
        $("#slider-range-weight").slider({
            range: true,
            min: 0,
            max: 200,
            values: [0, 200],
            slide: function (event, ui) {
                $("#weight").val("" + ui.values[0]);
                $("#weight1").val("" + ui.values[1]);
            },
            stop: function (event, ui) {
                requestSearchFilter();
            },

        });
        $("#weight").val("" + $("#slider-range").slider("values", 0));
        $("#weight1").val("" + $("#slider-range").slider("values", 1));
    });

    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
        $("#filter-toggle").click(function () {
            $(".open-div").toggle();
        });
        $("#ex18a").slider({min: 0, max: 10, value: 5, labelledby: 'ex18-label-1'});
        $("#ex18b").slider({min: 0, max: 10, value: [3, 6], labelledby: ['ex18-label-2a', 'ex18-label-2b']});
    });
    (function () {
        // trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
        if (!String.prototype.trim) {
            (function () {
                // Make sure we trim BOM and NBSP
                var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
                String.prototype.trim = function () {
                    return this.replace(rtrim, '');
                };
            })();
        }
        [].slice.call(document.querySelectorAll('input.input__field')).forEach(function (inputEl) {
            // in case the input is already filled..
            if (inputEl.value.trim() !== '') {
                classie.add(inputEl.parentNode, 'input--filled');
            }
            // events:
            inputEl.addEventListener('focus', onInputFocus);
            inputEl.addEventListener('blur', onInputBlur);
        });

        function onInputFocus(ev) {
            classie.add(ev.target.parentNode, 'input--filled');
        }
        function onInputBlur(ev) {
            if (ev.target.value.trim() === '') {
                classie.remove(ev.target.parentNode, 'input--filled');
            }
        }
    })();
});
var webJS = {
    map: {}

};

/**
 *
 * @type {{googleMapsInitCallback, clickroute, centerMap}}
 */
webJS.gmap = (function (window, document) {
    var markerType,
        map,
        mobile_breakpoint = 769,
        myTrip,
        coord,
        mapCenter,
        mapCity,
        clicked = false;

    function getPins() {
        //TODO delete TEST
        if ($('#map').length > 0) {
            myTrip = new Array();
            for (var i = 0; i < markerData.events.length; i++) {
                var lat = markerData.events[i].lat;
                var lng = markerData.events[i].lng;
                myTrip.push(new google.maps.LatLng(lat, lng));
            }
        }
    }
    /**
     * @param opts
     * @constructor
     */
    function CustomMarker(opts) {
        this.setValues(opts);
    }
    /**
     *
     * @param controlDiv
     * @param map
     * @constructor
     */
    function ZoomControl(controlDiv, map) {


        // Creating divs & styles for custom zoom control

        controlDiv.style.padding = '25px';


        // Set CSS for the control wrapper

        var controlWrapper = document.createElement('div');

        controlWrapper.style.backgroundColor = 'transparent';

        controlWrapper.style.borderStyle = 'solid';

        controlWrapper.style.borderColor = 'transparent';

        controlWrapper.style.borderWidth = '0px';

        controlWrapper.style.border = 'none';

        controlWrapper.style.cursor = 'pointer';

        controlWrapper.style.textAlign = 'center';

        controlWrapper.style.width = '44px';

        controlWrapper.style.height = '94px';

        controlDiv.appendChild(controlWrapper);


        // Set CSS for the zoomIn

        var zoomInButton = document.createElement('div');

        zoomInButton.className += "zoom-in-btn";

        zoomInButton.style.backgroundColor = 'transparent';

        zoomInButton.style.width = '44px';

        zoomInButton.style.height = '44px';

        zoomInButton.style.borderStyle = 'solid';

        zoomInButton.style.borderColor = 'white';

        zoomInButton.style.borderWidth = '2px';

        zoomInButton.style.borderRadius = '50%';

        zoomInButton.style.marginBottom = '6px';

        /* Change this to be the .png image you want to use */

        // zoomInButton.style.backgroundImage = 'url("http://placehold.it/32/00ff00")';

        controlWrapper.appendChild(zoomInButton);


        // Set CSS for the zoomOut

        var zoomOutButton = document.createElement('div');

        zoomOutButton.className += "zoom-out-btn";

        zoomOutButton.style.backgroundColor = 'transparent';

        zoomOutButton.style.width = '44px';

        zoomOutButton.style.height = '44px';

        zoomOutButton.style.borderStyle = 'solid';

        zoomOutButton.style.borderColor = 'white';

        zoomOutButton.style.borderWidth = '2px';

        zoomOutButton.style.borderRadius = '50%';

        zoomOutButton.style.marginTop = '4px';

        /* Change this to be the .png image you want to use */

        // zoomOutButton.style.backgroundImage = 'url("http://placehold.it/32/0000ff")';

        controlWrapper.appendChild(zoomOutButton);


        // Setup the click event listener - zoomIn

        google.maps.event.addDomListener(zoomInButton, 'click', function () {

            map.setZoom(map.getZoom() + 1);

        });


        // Setup the click event listener - zoomOut

        google.maps.event.addDomListener(zoomOutButton, 'click', function () {

            map.setZoom(map.getZoom() - 1);

        });


    }
    function googleMapsInitCallback() {
        if ($('#map').length === 0) {
            return;
        }
        CustomMarker.prototype = new google.maps.OverlayView();
        CustomMarker.prototype.draw = function () {
            var self = this;
            var div = this.div;
            if (!div) {
                div = this.div = $('' +
                '<div>' +
                '<div class="shadow"></div>' +
                '<div class="pin-wrap">' +
                '<div class="pulse"></div>' +
                '<div class="pin"><img src="images/image1.jpg"/></div>' +
                '</div>' +
                '</div>' +
                '')[0];
                this.pinWrap = this.div.getElementsByClassName('pin-wrap');
                this.pin = this.div.getElementsByClassName('pin');
                this.pinShadow = this.div.getElementsByClassName('shadow');
                div.style.position = 'absolute';
                div.style.cursor = 'pointer';
                var panes = this.getPanes();
                panes.overlayImage.appendChild(div);
                google.maps.event.addDomListener(div, "click", function (event) {
                    google.maps.event.trigger(self, "click", event);
                });
                google.maps.event.addDomListener(div, "mouseover", function (event) {
                    google.maps.event.trigger(self, "mouseover", event);
                });
                google.maps.event.addDomListener(div, "mouseout", function (event) {
                    google.maps.event.trigger(self, "mouseout", event);
                });
            }
            var point = this.getProjection().fromLatLngToDivPixel(this.position);
            if (point) {
                div.style.left = point.x + 'px';
                div.style.top = point.y + 'px';
            }
        };

        CustomMarker.prototype.animateDrop = function () {
            dynamics.stop(this.pinWrap);
            dynamics.css(this.pinWrap, {
                'transform': 'scaleY(2) translateY(-' + $('#map').outerHeight() + 'px)',
                'opacity': '1',
            });
            dynamics.animate(this.pinWrap, {
                translateY: 0,
                scaleY: 1.0,
            }, {
                type: dynamics.gravity,
                duration: 1800,
            });
            dynamics.stop(this.pin);
            dynamics.css(this.pin, {
                'transform': 'none',
            });
            dynamics.animate(this.pin, {
                scaleY: 0.8
            }, {
                type: dynamics.bounce,
                duration: 1800,
                bounciness: 600,
            })
            dynamics.stop(this.pinShadow);
            dynamics.css(this.pinShadow, {
                'transform': 'scale(0,0)',
            });
            dynamics.animate(this.pinShadow, {
                scale: 1,
            }, {
                type: dynamics.gravity,
                duration: 1800,
            });
        }
        CustomMarker.prototype.animateBounce = function () {
            dynamics.stop(this.pinWrap);
            dynamics.css(this.pinWrap, {
                'transform': 'none',
            });
            dynamics.animate(this.pinWrap, {
                translateY: -30
            }, {
                type: dynamics.forceWithGravity,
                bounciness: 0,
                duration: 500,
                delay: 150,
            });
            dynamics.stop(this.pin);
            dynamics.css(this.pin, {
                'transform': 'none',
            });
            dynamics.animate(this.pin, {
                scaleY: 0.8
            }, {
                type: dynamics.bounce,
                duration: 800,
                bounciness: 0,
            });
            dynamics.animate(this.pin, {
                scaleY: 0.8
            }, {
                type: dynamics.bounce,
                duration: 800,
                bounciness: 600,
                delay: 650,
            });
            dynamics.stop(this.pinShadow);

            dynamics.css(this.pinShadow, {

                'transform': 'none',

            });

            dynamics.animate(this.pinShadow, {

                scale: 0.6,

            }, {

                type: dynamics.forceWithGravity,

                bounciness: 0,

                duration: 500,

                delay: 150,

            });

        }


        CustomMarker.prototype.animateWobble = function () {

            dynamics.stop(this.pinWrap);

            dynamics.css(this.pinWrap, {

                'transform': 'none',

            });

            dynamics.animate(this.pinWrap, {

                rotateZ: -45,

            }, {

                type: dynamics.bounce,

                duration: 1800,

            });


            dynamics.stop(this.pin);

            dynamics.css(this.pin, {

                'transform': 'none',

            });

            dynamics.animate(this.pin, {

                scaleX: 0.8

            }, {

                type: dynamics.bounce,

                duration: 800,

                bounciness: 1800,

            });

        }
        gmap();
    }
    /**
     * @param callback
     */
    function isGoogleMapsLoaded(callback) {
        if (typeof window.google === 'object' && typeof window.google.maps === 'object') {
            callback();
        } else {
            setTimeout(function () {
                isGoogleMapsLoaded(callback);
            }, 250);
        }
    }
    /**
     *
     */
    function gmap() {
        mapCenter = $('#map').attr('data-center').split('|');
        mapCity = $('#map').attr('data-city');
        // Google Maps Scripts
        // map = null;
        // When the window has finished loading create our google map below
        // google.maps.event.addDomListener(window, 'load', initMap);
        isGoogleMapsLoaded(function () {
            initMap(mapCenter, mapCity);
        });
        /*google.maps.event.addDomListener(window, 'resize', function () {
         map.setCenter(new google.maps.LatLng(coord[0], coord[1]));
         if (window.innerWidth >= 980) {
         var offset = getMapCenterOffset('#map', 275, 240);
         googleMapRecenter(map, {lat: coord[0], lng: coord[1]}, 0, offset.y);
         map.setOptions({
         'draggable': true,
         zoomControl: false
         });
         } else {
         map.setOptions({
         'draggable': false,
         zoomControl: true
         });
         }
         });*/
    }
    /**
     *
     * @param mapCenter
     * @param mapCity
     */
    function initMap(mapCenter, mapCity) {
        // console.log(mapCenter);
        // console.log(mapCity);
        var isTouchDevice = !('ontouchstart' in document.documentElement);
        // Basic options for a simple Google Map
        // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
        var mapOptions = {
            // How zoomed in you want the map to start at (always required)
            zoom: 6,
            /*minZoom: 2,
             maxZoom: 14,*/
            // The latitude and longitude to center the map (always required)

            //center: new google.maps.LatLng(mapCenter[0], mapCenter[1]),
            center: new google.maps.LatLng(20.5937, 78.9629),
            // Disables the default Google Maps UI components
            disableDefaultUI: true,
            scrollwheel: false,
            draggable: true,
            gestureHandling: 'cooperative',
            // draggable: isTouchDevice,
            // zoomControl: window.innerWidth < 980,
            zoomControl: false,
            // How you would like to style the map.
            // This is where you would paste any style found on Snazzy Maps.
        };
        console.log(mapOptions);
        // Get the HTML DOM element that will contain your map
        // We are using a div with id="map" seen below in the <body>
        var mapElement = document.getElementById('map');
        // Create the Google Map using out element and options defined above
        map = new google.maps.Map(mapElement, mapOptions);
        // Create the DIV to hold the control and call the ZoomControl() constructor
        // passing in this DIV.
        var zoomControlDiv = document.createElement('div');
        var zoomControl = new ZoomControl(zoomControlDiv, map);
        zoomControlDiv.index = 1;
        map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(zoomControlDiv);

        setMarkers(map);
        if (mapCity) {
            //centerMap(mapCity);
        }
    }
    /**
     *
     * @param map
     */
    function setMarkers(map) {
        var shape = {
                coords: [1, 1, 1, 20, 18, 20, 18, 1],
                type: 'poly'
            },
            markerOriginal,
            animateTimer,
            infoboxOptions = {
                disableAutoPan: false,
                maxWidth: 0,
                pixelOffset: new google.maps.Size(0, 0),
                zIndex: null,
                boxStyle: {
                    // opacity: 0,
                    // background: 'rgba(255,255,255,0)',
                    //width: "320px",
                },
                closeBoxMargin: "0",
                closeBoxURL: "",
                infoBoxClearance: new google.maps.Size(10, 10),
                isHidden: false,
                pane: "floatPane",
                enableEventPropagation: false
            },
            ib = new InfoBox(infoboxOptions);
        var markers = [];
        for (var i = 0; i < markerData.events.length; i++) {
            var markerOne = markerData.events[i],
            // info window markup
                contentString = '<div class="info-window">' +
                    '<div class="info-back">' +
                        '<img src="' + markerOne.place_img + '"  class="info-window__img"/>' +
                        '<div class="info-box">' +
                                '<h3 class="info-window__heading"><span>' + markerOne.place_name + '</span></h3>' +
                                '<p class="info-window__text"><span>Age:&nbsp;' + markerOne.age + '&nbsp;</span>&nbsp;|&nbsp;<span>Height:&nbsp;' + markerOne.user_height + '</span></p>' +
                                '<p class="info-window__text"><span>Community:&nbsp;' + markerOne.community + '</span></p>' +
                                '<p class="info-window__text"><span>Occupation:&nbsp;' + markerOne.occupation + '</span></p>' +
                                '<p class="info-window__btn"><a href="" class="btn btn-interest">Send Interest</a><a href="" class="btn btn-love"><i class="fa fa-heart-o"></i></a></p>' +
                        '</div>'+
                    '</div>' +
                    '</div>',
                myLatLng = new google.maps.LatLng(markerOne.lat, markerOne.lng);
            /*markers[i] = new CustomMarker({
                position: myLatLng,
                map: map,
                shape: shape,
                name: markerOne.city_id,
                url: markerOne.event_link
            });*/

            var marker = new google.maps.Marker({
                position: myLatLng,
                //icon: 'https://localhost/kande_pohe_cr4/images/image1.jpg'
                //icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/info-i_maps.png'
            });
            markers.push(marker);
            // console.log(marker);
            markerOriginal = new google.maps.Marker({
                map: map,
                draggable: true,
                position: myLatLng,
                visible: false,

            });
            // mouseover event
            google.maps.event.addListener(markers[i], 'mouseover', (function (markerOriginal, contentString) {
                return function () {
                    if (markerOriginal.getAnimation() == null || typeof markerOriginal.getAnimation() === 'undefined') {
                        // Because of the google maps bug of moving cursor several times over and out of marker
                      //   causes bounce animation to break - we use small timer before triggering the bounce animation
                        clearTimeout(animateTimer);
                        ib.setContent(contentString);
                        ib.open(map, markerOriginal);
                        animateTimer = setTimeout(function () {
                                animateInfoBoxContent();
                            },
                            300);
                    }
                };
            })(markerOriginal, contentString));
            google.maps.event.addListener(markers[i], 'mouseout', function () {
                //ib.close();
                // If we already left marker, no need to bounce when timer is ready
                clearTimeout(animateTimer);
            });
        }

        var markerCluster = new MarkerClusterer(map, markers,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
    }
    /**
     * @param lat
     * @param long
     * @param bounds
     */
    function clickroute(lat, long, bounds) {
        var latLng = new google.maps.LatLng(lat, long); //Makes a latlng
        // console.log(lat,long);
        // console.log(map);
        map.panTo(latLng);
        // map.setZoom(14);
        map.fitBounds(bounds);
    }
    /**
     * @param $city
     */

    function centerMap($city) {
        //We do that to ensure to get a correct JSON
        var my_json = JSON.stringify(markerData.events);
        //We can use {'name': 'Lenovo Thinkpad 41A429ff8'} as criteria too
        var filtered_json = find_in_object(JSON.parse(my_json), {city_id: $city});
        function find_in_object(my_object, my_criteria) {
            return my_object.filter(function (obj) {
                return Object.keys(my_criteria).every(function (c) {
                    return obj[c] == my_criteria[c];
                });
            });
        }
        var myBounds = new google.maps.LatLngBounds();
        for (var i = 0; i < filtered_json.length; i++) {
            var lat = filtered_json[i].lat;
            var lng = filtered_json[i].lng;
            var ll = new google.maps.LatLng(lat, lng);
            myBounds.extend(ll);
        }
        //console.log(markerData.citiesLocation[$city]);
        webJS.gmap.clickroute(markerData.citiesLocation[$city].lat, markerData.citiesLocation[$city].long, myBounds);

    }
    function animateInfoBoxContent() {
        TweenMax.to('.info-back', 0.1, {autoAlpha: 1, x: '0%'});
    }
    /**
     * @param selector
     * @param boxWidth
     * @param boxHeight
     * @returns {{x: number, y: number}}
     */
    function getMapCenterOffset(selector, boxWidth, boxHeight) {
        var $mapContainer = $(selector);
        $mapContainerWidth = $mapContainer.width();
        $mapContainerHeight = $mapContainer.height();
        var offsetX = ($mapContainerWidth - boxWidth) / 2;
        var offsetY = -($mapContainerHeight - boxHeight) / 2 + 20;
        return {
            x: offsetX,
            y: offsetY
        }
    }

    /**
     * @param map
     * @param latlng
     * @param offsetx
     * @param offsety
     */
    function googleMapRecenter(map, latlng, offsetx, offsety) {
        var point1 = map.getProjection().fromLatLngToPoint(
            (latlng instanceof google.maps.LatLng) ? latlng : map.getCenter()
        );
        var point2 = new google.maps.Point(
            ( (typeof(offsetx) == 'number' ? offsetx : 0) / Math.pow(2, map.getZoom()) ) || 0,

            ( (typeof(offsety) == 'number' ? offsety : 0) / Math.pow(2, map.getZoom()) ) || 0
        );

        map.setCenter(map.getProjection().fromPointToLatLng(new google.maps.Point(
            point1.x - point2.x,

            point1.y + point2.y
        )));

    }


    return {

        googleMapsInitCallback: googleMapsInitCallback,

        clickroute: clickroute,

        centerMap: centerMap

    };


})(window, document);


$(function () {

    "use strict";

    webJS.gmap.googleMapsInitCallback();

});


// CLick to add class


$(".advanced-filter .panel-heading").click(function () {
    $('.advanced-filter').toggleClass("show");
});



