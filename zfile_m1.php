
<!-- Add Modal Here !-->
<div class="modal fade" id="addModal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body">
                <div class="panel-group" id="accordionSettings" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-primary">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordionSettings" href="#collapseChangePassword" aria-expanded="true" aria-controls="collapseOne">Add Record
                                </a>
                            </h4>
                        </div>
                        <div id="collapseChangePassword" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <form method="post" action="create" enctype="multipart/form-data">
                                <div class="panel-body">
                                    <fieldset class="form-group">
                                        <label>Facility Name</label>
                                        <input type="text" name="fname" class="form-control" placeholder="Enter Facility Name" required>
                                        <label>First Name <span class="aliasname">(Registering Individual)</span> </label>
                                        <input type="text" name="first_name" class="form-control" placeholder="Enter First Name" required>
                                        <label>Last Name <span class="aliasname">(Registering Individual)</span> </label>
                                        <input type="text" name="last_name" class="form-control" placeholder="Enter Last Name" required>
                                        <label>Address</label>
                                        <input type="text" name="address" class="form-control" placeholder="Enter Address" required autocomplete="off">
                                        <label>Zip</label>
                                        <input type="text" name="zip" class="form-control" maxlength="8" placeholder="Enter Zipcode (Ex:50XXXXXX)" required autocomplete="off">
                                        <label>Country</label>
                                        <input type="text" name="country" class="form-control" placeholder="Enter Country" required autocomplete="off">
                                        <label>Email ID</label>
                                        <input type="email" name="email" id="email" class="form-control email" placeholder="Enter Email Id" required autocomplete="off" onkeyup="get_verify(this.value);" required>
                                        <label class="error response_email" style="float: right;" id="response_email" ></label>
                                        <br/>


                                        <label>Phone No</label>
                                        <input type="tel" name="phone_no" id="phone_no" class="form-control" placeholder="Enter Phone Number" required autocomplete="off">

                                        <label>User Name</label>
                                        <input type="text" name="uname" class="form-control" placeholder="Enter User Name" required>
                                        <label>User Password</label>
                                        <input type="password" name="password" id="passwordv" pattern="[a-zA-Z0-9]{6,50}" class="form-control" placeholder="Enter An Alphanumeric Password & at Least 6 Characters Long. Ex: Vivo123" required title="Please Enter An Alphanumeric Password & at Least 6 Characters Long. Ex: Vivo123">
                                        <label class="error" style="float: right;" id="response_passwordv"></label>
                                        <input type="hidden" name="terms" value="Yes">
                                    </fieldset>
                                </div>
                                <div class="panel-footer">
                                    <input type="submit" class="btn btn-primary subbtn" value="Save" onkeyup="return get_verify('');">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Add Modal Ends !-->

