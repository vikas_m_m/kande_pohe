<?php
return [
    'adminEmail' => 'donotreply@kande-pohe.com',
    #'adminEmail' => 'kandepohetest@gmail.com',

    #URL Routes
    'userDashboard' => '/dashboard',
    'userMyProfile' => '/my-profile',
    'userMyProfileHome' => 'my-profile',
    'userMyPhotos' => '/photos',
    'userSetting' => '/setting',
    'userLogout' => '/logout',
'userSignUp' => '/sign-up',

    'PopUPLogin' => '/?ref=login',
    'PopUPSignUp' => '/?ref=signup',


    'registrationBasicDetails' => 'basic-details',
    'registrationEducationOccupation' => 'education-occupation',
    'registrationLifeStyle' => 'life-style',
    'registrationAboutFamily' => 'about-family',
    'registrationAboutYourself' => 'about-yourself',
    'userVerification' => '/verification',
    'registrationPartnerPreferences' => 'partner-preferences',
    'matrimony' => '/matrimony',
    'upgradePlan' => '/upgrade-plan',
    'upgradeProcess' => '/upgrade-process',
];

/*Yii::$app->params['userDashboard']*/
