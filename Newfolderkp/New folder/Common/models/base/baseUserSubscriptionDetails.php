<?php

namespace common\models\base;

use Yii;

/**
 * This is the model class for table "user_subscription_details".
 *
 * @property string $usd_id
 * @property string $usd_user_id
 * @property string $usd_subscription_start_date
 * @property string $usd_subscription_end_date
 * @property string $usd_status
 * @property string $usd_subscription_name
 * @property string $usd_short_name
 * @property double $usd_subscriptions_price
 * @property integer $usd_profile_duration
 * @property integer $usd_no_of_contacts
 * @property integer $no_of_pm
 * @property string $usd_privacy_settings
 * @property integer $usd_validity_of_package
 * @property string $customer_care_support
 * @property string $usd_dt_added
 */
class baseUserSubscriptionDetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_subscription_details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['usd_user_id'], 'required'],
            [['usd_user_id', 'usd_profile_duration', 'usd_no_of_contacts', 'usd_no_of_pm', 'usd_validity_of_package', 'usd_no_of_contacts_used', 'usd_no_of_pm_used'], 'integer'],
            [['usd_subscription_start_date', 'usd_subscription_end_date', 'usd_dt_added'], 'safe'],
            [['usd_status', 'usd_privacy_settings', 'usd_customer_care_support'], 'string'],
            [['usd_subscriptions_price'], 'number'],
            [['usd_subscription_name'], 'string', 'max' => 255],
            [['usd_short_name'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'usd_id' => 'User Subscription ID',
            'usd_user_id' => 'User ID',
            'usd_subscription_start_date' => 'Subscription Start Date',
            'usd_subscription_end_date' => 'Subscription End Date',
            'usd_status' => 'Status',
            'usd_subscription_name' => 'Subscription Name',
            'usd_short_name' => 'Short Name',
            'usd_subscriptions_price' => 'Subscriptions Price',
            'usd_profile_duration' => 'Profile Duration',
            'usd_no_of_contacts' => 'No Of Contacts',
            'usd_no_of_pm' => 'No Of Personalized Messaging',
            'usd_privacy_settings' => 'Privacy Settings',
            'usd_validity_of_package' => 'Validity Of Package',
            'usd_customer_care_support' => 'Customer Care Support',
            'usd_dt_added' => 'Date Added',
            'usd_no_of_contacts_used' => 'No Of Contacts Used',
            'usd_no_of_pm_used' => 'No Of Personalized Messaging Used',
        ];
    }
}
