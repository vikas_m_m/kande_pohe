<?php

namespace common\models\base;

use Yii;

/**
 * This is the model class for table "site_meta_tag".
 *
 * @property string $meta_tag_id
 * @property string $site_url
 * @property string $type
 * @property string $title
 * @property string $description
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keyword
 */
class baseSiteMetaTag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_meta_tag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_url', 'type', 'title', 'description', 'meta_title', 'meta_description', 'meta_keyword'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'meta_tag_id' => 'Meta Tag ID',
            'site_url' => 'Site Url',
            'type' => 'Type',
            'title' => 'Title',
            'description' => 'Description',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keyword' => 'Meta Keyword',
        ];
    }
}
