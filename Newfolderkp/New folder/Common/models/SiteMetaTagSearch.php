<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\SiteMetaTag;

/**
 * SiteMetaTagSearch represents the model behind the search form about `common\models\SiteMetaTag`.
 */
class SiteMetaTagSearch extends SiteMetaTag
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['meta_tag_id'], 'integer'],
            [['site_url', 'type', 'title', 'description', 'meta_title', 'meta_description', 'meta_keyword'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$type='')
    {

        if ($type != '') {
            $query = SiteMetaTag::find()->where(['type' => $type])->orderBy(['site_url' => SORT_ASC]); //For City List
        }else{
            $query = SiteMetaTag::find();
        }
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'meta_tag_id' => $this->meta_tag_id,
        ]);

        $query->andFilterWhere(['like', 'site_url', $this->site_url])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'meta_title', $this->meta_title])
            ->andFilterWhere(['like', 'meta_description', $this->meta_description])
            ->andFilterWhere(['like', 'meta_keyword', $this->meta_keyword]);

        return $dataProvider;
    }
}
