<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "subscriptions".
 *
 * @property integer $subscriptions_id
 * @property string $subscriptions_name
 * @property double $subscriptions_price
 * @property integer $profile_duration
 * @property integer $no_of_contacts
 * @property integer $no_of_pm
 * @property string $privacy_settings
 * @property integer $validity_of_package
 * @property string $customer_care_support
 */
class Subscriptions extends \common\models\base\baseSubscriptions
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscriptions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subscriptions_name'], 'required'],
            [['subscriptions_name', 'privacy_settings', 'customer_care_support','short_name'], 'string'],
            [['subscriptions_price'], 'number'],
            [['profile_duration', 'no_of_contacts', 'no_of_pm', 'validity_of_package'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'subscriptions_id' => 'Subscriptions ID',
            'subscriptions_name' => 'Subscriptions Name',
            'subscriptions_price' => 'Subscriptions Price',
            'profile_duration' => 'Profile Duration',
            'no_of_contacts' => 'No Of Contacts',
            'no_of_pm' => 'No Of Personalized Messaging',
            'privacy_settings' => 'Privacy Settings',
            'validity_of_package' => 'Validity Of Package',
            'customer_care_support' => 'Customer Care Support',
            'short_name' => 'Short Name',
        ];
    }

    public static function getSubscriptionPlanList()
    {
        return static::find()
            ->orderBy(['subscriptions_id' => SORT_ASC])
            ->all();
    }
    public static function getUpgradeSubscriptionPlanList()
    {
        #->where(['not in', 'short_name', 'PACKAGE_FREE'])
        return static::find()
            ->orderBy(['subscriptions_id' => SORT_ASC])
            ->all();
    }
    public static function getPlanTypeDetails($type)
    {
        return static::find()
            ->where(['=','short_name', $type])
            ->andWhere([' != ', 'short_name', 'PACKAGE_FREE'])
            ->orderBy(['subscriptions_id' => SORT_ASC])
            ->one();
    }
}
