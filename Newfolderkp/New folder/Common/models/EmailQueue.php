<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "email_queue".
 *
 * @property string $eq_id
 * @property string $eq_type
 * @property string $eq_subject
 * @property string $eq_to_email_id
 * @property string $eq_body
 * @property string $eq_to_name
 * @property string $eq_to_user_id
 * @property string $eq_send_status
 * @property string $eq_dt_added
 */
class EmailQueue extends \common\models\base\baseEmailQueue
{
    const STATUS_PENDING = 'PENDING';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'email_queue';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['eq_subject', 'eq_to_email_id', 'eq_body', 'eq_send_status'], 'string'],
            [['eq_to_user_id'], 'integer'],
            [['eq_dt_added'], 'safe'],
            [['eq_type', 'eq_to_name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'eq_id' => 'Eq ID',
            'eq_type' => 'Eq Type',
            'eq_subject' => 'Eq Subject',
            'eq_to_email_id' => 'Eq To Email ID',
            'eq_body' => 'Eq Body',
            'eq_to_name' => 'Eq To Name',
            'eq_to_user_id' => 'Eq To User ID',
            'eq_send_status' => 'Eq Send Status',
            'eq_dt_added' => 'Eq Dt Added',
        ];
    }


    public static function get_weekly_notification_new_joined($type,$limit=10, $order)
    {
            return static::find()->where(['eq_type' => $type])
                ->andWhere(['eq_send_status' => [self::STATUS_PENDING]])
                #->orderBy(['LastLoginTime' => SORT_DESC])
                ->orderBy(('eq_dt_added '.$order))
                ->limit($limit)->all();

    }
}
