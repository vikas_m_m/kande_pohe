<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "subscription_log".
 *
 * @property string $sub_log_id
 * @property string $sub_user_id
 * @property string $sub_log_type
 * @property string $sub_usd_id
 * @property string $sub_to_id
 * @property string $sub_date_time
 */
class SubscriptionLog extends \common\models\base\baseSubscriptionLog
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscription_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sub_user_id', 'sub_usd_id', 'sub_to_id'], 'integer'],
            [['sub_log_type'], 'string'],
            [['sub_usd_id', 'sub_to_id', 'sub_date_time'], 'required'],
            [['sub_date_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sub_log_id' => 'Sub Log ID',
            'sub_user_id' => 'Sub User ID',
            'sub_log_type' => 'Sub Log Type',
            'sub_usd_id' => 'Sub Usd ID',
            'sub_to_id' => 'Sub To ID',
            'sub_date_time' => 'Sub Date Time',
        ];
    }
    public static function checkLog($usd_id,$log_type,$user_id,$to_user_id)
    {
        return static::find()
            ->where(['=','sub_log_type', $log_type])
            ->andWhere(['= ', 'sub_usd_id', $usd_id])
            ->andWhere(['= ', 'sub_user_id', $user_id])
            ->andWhere(['= ', 'sub_to_id', $to_user_id])
            ->orderBy(['sub_log_id' => SORT_ASC])
            ->one();
    }
}
