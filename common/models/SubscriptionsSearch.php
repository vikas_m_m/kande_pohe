<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Subscriptions;

/**
 * SubscriptionsSearch represents the model behind the search form about `common\models\Subscriptions`.
 */
class SubscriptionsSearch extends Subscriptions
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subscriptions_id', 'profile_duration', 'no_of_contacts', 'no_of_pm', 'validity_of_package'], 'integer'],
            [['subscriptions_name', 'privacy_settings', 'customer_care_support'], 'safe'],
            [['subscriptions_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Subscriptions::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'subscriptions_id' => $this->subscriptions_id,
            'subscriptions_price' => $this->subscriptions_price,
            'profile_duration' => $this->profile_duration,
            'no_of_contacts' => $this->no_of_contacts,
            'no_of_pm' => $this->no_of_pm,
            'validity_of_package' => $this->validity_of_package,
        ]);

        $query->andFilterWhere(['like', 'subscriptions_name', $this->subscriptions_name])
            ->andFilterWhere(['like', 'privacy_settings', $this->privacy_settings])
            ->andFilterWhere(['like', 'customer_care_support', $this->customer_care_support]);

        return $dataProvider;
    }
}
