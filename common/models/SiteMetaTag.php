<?php

namespace common\models;

use Yii;
#use yii\behaviors\SluggableBehavior;
/**
 * This is the model class for table "site_meta_tag".
 *
 * @property string $meta_tag_id
 * @property string $site_url
 * @property string $type
 * @property string $title
 * @property string $description
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keyword
 */
class SiteMetaTag extends  \common\models\base\baseSiteMetaTag
{
    const CITY = 'City';
    const CASTE = 'Caste';
    const EDUCATION = 'Education';
    const OCCUPATION = 'Occupation';
    const PHYSICAL_STATUS = 'Physical Status';
    const MARITAL_STATUS = 'Marital Status';
    const OTHER = 'Other';

    const SCENARIO_ADD = 'ADD';
    const SCENARIO_UPDATE = 'Update';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_meta_tag';
    }
   /* public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'site_url',
                 'slugAttribute' => 'site_url',
            ],
        ];
    }*/
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_url', 'type', 'title','description'], 'required'],
            [['site_url', 'type', 'title', 'description', 'meta_title', 'meta_description', 'meta_keyword'], 'string'],


        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'meta_tag_id' => 'Meta Tag ID',
            'site_url' => 'Site Url',
            'type' => 'Type',
            'title' => 'Title',
            'description' => 'Description',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keyword' => 'Meta Keyword',
        ];
    }


    public function scenarios()
    {
        return [
            self::SCENARIO_ADD => ['site_url', 'type', 'title', 'description', 'meta_title', 'meta_description', 'meta_keyword'],
            self::SCENARIO_UPDATE  => ['site_url', 'type', 'title', 'description', 'meta_title', 'meta_description', 'meta_keyword'],

        ];

    }

    /*public function getList()
    {
        return static::find()->where(['type' => [self::CITY]])->orderBy(['title' => SORT_ASC])->all();
    }*/
}
