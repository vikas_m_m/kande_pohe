<?php
namespace common\models;

use common\components\CommonHelper;
use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginotpForm extends Model
{
    public $phone_number;
    public $otp;
    public $rememberMe = true;

    private $_user;
    const SCENARIO_GETOTP = 'getOTP';
    const SCENARIO_VERIFYOTP = 'verifyOTP';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            #[['email', 'password'], 'required'],
            ['phone_number', 'required', 'message' => 'Please enter your mobile phone number.'],
            #[['email'], 'email', 'message' => "Please enter valid email address."],
            //['password', 'required', 'message' => 'Please Enter your password.'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            ['otp', 'required'],
            // password is validated by validatePassword()
            //['password', 'validatePassword'],
        ];
    }
    public function scenarios()
    {
        return [
            self::SCENARIO_GETOTP => ['phone_number'],
            self::SCENARIO_VERIFYOTP => ['phone_number','otp']
        ];
    }
    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                #$this->addError($attribute, 'The email ID or password you entered is not correct. Try again!');
                $this->addError($attribute, 'Incorrect login credentials. Try again!');
            }
            else  if ($user ) {
                if (filter_var($this->phone_number, FILTER_VALIDATE_EMAIL)) {
                    if(!$user->checkEmailVerify($this->phone_number)) {
                        $this->addError($attribute, 'Your account either block or pending, Please contact to administrator for login');
                        //vikrant@backendbrains.com
                    }
                }
            }
        }
    }

    public function setOTP($password_otp){
        #echo $password_otp;
        $user = $this->getUser();
        #CommonHelper::pr($user );
        if ($user) {
            #$user->password_reset_token = $token;
            $user->login_otp = $password_otp;
            if (!$user->save()) {
                return false;
            }
        }
    }
    /*
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                #$this->addError($attribute, 'The email ID or password you entered is not correct. Try again!');
                $this->addError($attribute, 'Incorrect login credentials. Try again!');
            }
            else if (!$user || !$user->checkEmailVerify($this->email)) {
                $this->addError($attribute, 'Your account either block or panding, Please contact to administrator for login');
            }
        }
    }*/

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        #var_dump($this->email);exit;
        if ($this->_user === null) {
            #$this->_user = User::findByEmail($this->email);
            $this->_user = User::findByEmailORPhoneNumber($this->phone_number);
        }
        return $this->_user;
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }
}
