<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "subscription_log".
 *
 * @property string $sub_log_id
 * @property string $sub_user_id
 * @property string $sub_log_type
 * @property string $sub_usd_id
 * @property string $sub_to_id
 * @property string $sub_date_time
 */
class PersonalizedSmsHistory extends \common\models\base\basePersonalizedSmsHistory
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'personalized_sms_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sms_id', 'sms_user_id', 'sms_usd_id','sms_to_user_id'], 'integer'],
            [['sms_type','sms_content','sms_share_mobile_status'], 'string'],
            [['sms_user_id', 'sms_usd_id','sms_to_user_id', 'sms_content','sms_type'], 'required'],
            [['sms_dt_added'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sms_id' => 'SMS ID',
            'sms_user_id' => 'SMS User ID',
            'sms_to_user_id' => 'SMS To User ID',
            'sms_usd_id' => 'SMS Sub. ID',
            'sms_type' => 'SMS Type',
            'sms_content' => 'SMS Content',
            'sms_share_mobile_status' => 'Mobile Number Share Status',
            'sub_date_time' => 'Sub Date Time',
        ];
    }
}
