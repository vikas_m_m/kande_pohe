<?php
error_reporting(0);
#error_reporting(E_ALL);
ini_set('max_execution_time', 300);
ini_set('max_input_time', 300);
ini_set('upload_max_filesize', '32M');
//echo ini_get('upload_max_filesize'), ", " , ini_get('post_max_size');exit;
return [
    'components' => [
        /*'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=vcodeccr_kande',
            'username' => 'vcodeccr_m',
            'password' => 'vcodeccr_yii2kande',
            'charset' => 'utf8',
        ],*/
        'db' => [
            'class' => 'yii\db\Connection',
            #KP_LIVE
            /*'dsn' => 'mysql:host=localhost;dbname=kandepoh_kande_pohe',
            'username' => 'kandepoh_kp2019',
            'password' => '8*SAza)G2,PM',*/

            #KP_dev
            'dsn' => 'mysql:host=localhost;dbname=kandepoh_kp_2019_new',
            'username' => 'kandepoh_kp2019',
            'password' => '8*SAza)G2,PM',

            #127
            /*'dsn' => 'mysql:host=localhost;dbname=dev_kande_pohe_live',
            'username' => 'root',
            'password' => '',*/

            #216
            /*'dsn' => 'mysql:host=localhost;dbname=backeiiq_kpcr4',
            'username' => 'backeiiq_kpcr4',
            'password' => 'backeiiq_kpcr4',*/

            'charset' => 'utf8',
        ],
        'ReadHttpHeader' => [
            'class' => 'common\components\ReadHttpHeader'
        ],
        /*'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],*/
        'mailer' => [
         'class' => 'yii\swiftmailer\Mailer',
         'viewPath' => '@common/mail',
         //'useFileTransport' => false,
            'transport' => [
             'class' => 'Swift_SmtpTransport',
             'host' => 'smtpout.asia.secureserver.net',  // e.g. smtp.mandrillapp.com or smtp.gmail.com
             'username' => 'donotreply@kande-pohe.com',
             'password' => 'Ke#34$SA2Q',
             'port' => '465', // Port 25 is a very common port too
             'encryption' => 'SSL', // It is often used, check your provider or mail server specs
         ],
             /*'transport' => [
             'class' => 'Swift_SmtpTransport',
             'host' => 'smtp.gmail.com',  // e.g. smtp.mandrillapp.com or smtp.gmail.com
             'username' => 'kandepohetest@gmail.com',
             'password' => 'kp100100',
             'port' => '587', // Port 25 is a very common port too
             'encryption' => 'tls', // It is often used, check your provider or mail server specs
         ],*/

     ],
        'urlManager' => [
        'class' => 'yii\web\UrlManager',
        // Disable index.php
        'showScriptName' => false,
        // Disable r= routes
        'enablePrettyUrl' => true,
        'rules' => array(
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
        ),
        ],
    ],
];