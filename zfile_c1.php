</div>
<div class="row">
    <div class="col-md-12 text-right" style="
    margin-left: -4%;
">
        <span><strong>Estimated HbA1c:</strong></span> <span style="
    color: #f37c12;
"><strong><?php echo $average_blood_glucose; ?></strong></span>
    </div>
</div>
<script>
    $(document).ready(function(){
        localStorage.clear();
    });
</script>
<script type="text/javascript">

    $(document).ready(function(){

        $('#message_text').bind("cut copy paste",function(e) {
            e.preventDefault();
        });

        $("#message_text").keyup(function(){
            $("#count_message").text("Characters left: " + (140 - $(this).val().length));
        });
    });

    function send_message()
    {
        var patient_number = $("#phone_number").val();
        var message = $("#message_text").val();

        if(patient_number=="")
        {
            $("#phone_number").addClass("emailtxt");
            $("#message_text").removeClass("emailtxt");
            return false;
        }
        else if(message=="")
        {
            $("#phone_number").removeClass("emailtxt");
            $("#message_text").addClass("emailtxt");
            return false;
        }
        else
        {
            $.ajax({
                url: 'send_message_code',
                data: { patient_number:patient_number,message:message},
                type: 'POST',
                success: function(data)
                {
                    var data = $.parseJSON(data);
                    if(data.status=='send')
                    {
                        $("#success-alert_cp_message_failed").hide();
                        $("#success-alert_cp_message").show();
                        $("#message_text").val('');
                        $("#count_message").text("Characters left: 140");
                        setTimeout(function(){ $("#success-alert_cp_message").hide();}, 4000);

                    }
                    else
                    {
                        //$("#phone_number").addClass("emailtxt");
                        $("#success-alert_cp_message").hide();
                        $("#success-alert_cp_message_failed").show();
                        setTimeout(function(){ $("#success-alert_cp_message_failed").hide();}, 4000);
                    }

                }
            });

        }


    }

</script>
<style>
    .head-title{
        font-family: "Lucida Grande", "Lucida Sans Unicode", Arial, Helvetica, sans-serif; font-size:18px;
    }

    /* The container */
    .container-checkbox {
        display: block;
        position: relative;
        padding-left: 35px;
        margin-bottom: 12px;
        cursor: pointer;
        font-size: 14px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        font-weight: inherit;
    }

    /* Hide the browser's default checkbox */
    .container-checkbox input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;
    }

    /* Create a custom checkbox */
    .checkmark {
        position: absolute;
        top: 0;
        left: 0;
        height: 25px;
        width: 25px;
        background-color: #FFF;
        border:1px solid #4CAF50;
    }

    /* On mouse-over, add a grey background color */
    .container-checkbox:hover input ~ .checkmark {
        background-color: #4CAF50;
    }

    /* When the checkbox is checked, add a blue background */
    .container-checkbox input:checked ~ .checkmark {
        background-color: #FFF;

    }

    /* Create the checkmark/indicator (hidden when not checked) */
    .checkmark:after {
        content: "";
        position: absolute;
        display: none;
    }

    /* Show the checkmark when checked */
    .container-checkbox input:checked ~ .checkmark:after {
        display: block;
    }

    /* Style the checkmark/indicator */
    .container-checkbox .checkmark:after {
        left: 9px;
        top: 5px;
        width: 7px;
        height: 12px;
        border: solid #4CAF50;
        border-width: 0 3px 3px 0;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
    }
    .labtest-body{
        overflow-y: auto;
        max-height: 196px;
    }
</style>
<div class="row">

    <div class="col-md-3">
        <form action="mednotes" method="post" name="medication">
            <input type="text" value="<?php echo $_GET['id']; ?>" id="id" name="id" class="hidden form-control" required>
            <input type="hidden" name="android_device_ids"  id="android_device_ids" value="<?= trim($android_device_ids)?>" style="display:none;" >
            <input type="hidden" name="ios_device_ids"  id="ios_device_ids" value="<?= trim($ios_device_ids)?>" style="display:none;" >
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title head-title">Prescription Note:</h3>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <?php  #$MedeArray = explode(",",$notes[0]['Patient_Meds']);
                        $split_strings = preg_split('/[\n\,]+/', $notes[0]['Patient_Meds']);
                        $MedString = '';
                        foreach($split_strings as $k=>$v){
                            $MedString .= trim($v)."\n";
                        }
                        #echo $MedString;
                        ?>
                        <textarea style="resize: none; height:160px; width:270px; " cols="30" rows="6" name="meds" class="form-control text-area" placeholder="Write down a list of medications..."><?php echo stripslashes($MedString); ?></textarea>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-success btn btn-success col-md-4 col-md-offset-4 col-xs-12  text-center" value="Save">Send</button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-3">
        <?php
        $lab_test = array(
            "Comprehensive Metabolic Panel (CMP)",
            "Basic Metabolic Panel (BMP)",
            "Complete Blood Count(CBC)",
            "Hemoglobin A1C",
            "Insulin Levels",
            "C-Peptide",
            "Insulin Antibodies",
            "Urine Microalbumin",
            "TSH (Thyroid Stimulating Hormone)",
            "Free T4",
            "Triiodothyronine",
            "Thyroid Peroxidase Antibodies (TPO Antibodies)",
            "TSH Receptor Antibodies",
            "Cortisol Plasma",
            "Adrenocorticotropic Hormone (ACTH)",
            "Prolactin",
            "Insulin-Like Growth Hormone-1 (IGF-1)",
            "LH (Luteinizing Hormone)",
            "FSH (Follicle Stimulating Hormone)",
            "Estradiol",
            "Testosterone Total and Free",
            "DHEAs (Dehydroepiandosterone Sulfate)",
            "17-Hydroxy Progesterone",
            "Aldosterone",
            "Renin",
            "Metanephrine Plasma",
            "Parathyroid Hormone Intact (PTHi)",
            "Parathyroid Hormone related Peptide(PTHrP)",
            "Vitamin D 25 Hydroxy",
            "Vitamin D 1-25 Dihydroxy",
            "MRI Brain (Pituitary)",
            "CT Scan Abdomen (Adrenals and Ovaries)",
            "DEXA Scan (Bone Density)",
        );
        ?>
        <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/all.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>

        <script>
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                    increaseArea: '20%' // optional
                });
            });
        </script>-->
        <form action="labtests" method="post" name="lab_test">
            <input type="text" value="<?php echo $_GET['id']; ?>" id="id" name="id" class="hidden form-control" required>
            <input type="hidden" name="android_device_ids"  id="android_device_ids_1" value="<?= trim($android_device_ids)?>" style="display:none;" >
            <input type="hidden" id="ios_device_ids" name="ios_device_ids_1" value="<?= trim($ios_device_ids)?>" style="display:none;" >

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title head-title">Endocrinology Lab Tests</h3>
                </div>
                <div class="box-body labtest-body">
                    <?php
                    foreach($lab_test as $k=>$v){ ?>
                        <div class="form-group">
                            <label class="container-checkbox"> <?=$v?>
                                <input type="checkbox" name="lab_test_nam[]" id="lab_test_nam" value="<?=$v?>">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    <?php }
                    ?>
                    <div class="form-group">
                        <textarea style="resize: none; height:160px; width:270px; " cols="30" rows="6" name="lab_notes" class="form-control text-area" placeholder="Write down a Note..."></textarea>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-success btn btn-success col-md-4 col-md-offset-4 col-xs-12  text-center" value="Save">Send</button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title head-title">Notes:</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <textarea style="height:160px; width:900px; resize: none;" cols="30" rows="6" name="notes" class="form-control text-area" ><?php echo $notes[0]['Notes']; ?></textarea>
                </div>
                <input type="text" value="<?php echo $_GET['id']; ?>" id="id" name="id" class="hidden form-control" required>
            </div>
            <div class="box-footer">
                <p>&nbsp;</p>
            </div>
        </div>
    </div>
    <!--<div class="col-md-6">
        <h3 style='font-family: "Lucida Grande", "Lucida Sans Unicode", Arial, Helvetica, sans-serif; font-size:18px' >Notes:</h3>
        <div class="box box-body" style="height:180px; ">
            <p style='font-family: "Lucida Grande", "Lucida Sans Unicode", Arial, Helvetica, sans-serif' class="hidden"><?php /*echo $med[0]['Notes']; */?></p>

            <textarea style="height:160px; width:900px; resize: none;" cols="30" rows="6" name="notes" class="form-control text-area" ><?php /*echo $notes[0]['Notes']; */?></textarea>

            <input type="text" value="<?php /*echo $_GET['id']; */?>" id="id" name="id" class="hidden form-control" required>
            <p>&nbsp;</p>
        </div>
    </div>-->
</div>
<div class="row">
    <div class="col-md-6">
        <?php  if(!$_SESSION['isadmin']){?>
            <a href="../patient/view_patientdetail?Patient_Id=<?php echo $_GET['id'] ?>" class="btn btn-info  ">Encounter Notes </a>
            <a href="../schedule/index?Patient_Id=<?php echo $_GET['id'] ?>" class="btn btn-info " >Schedule Appointment</a>
        <?php }?>
    </div>
    <div class="col-md-6">
        <input type="email" id="receiver" value="<?php echo trim($PatientInfo['Patient_UID']); ?>@vivovitals.com" style="display:none;" >
        <input type="hidden" id="android_device_ids_111" value="<?= trim($android_device_ids)?>" style="display:none;" >
        <input type="hidden" id="ios_device_ids_1111" value="<?= trim($ios_device_ids)?>" style="display:none;" >
        <input type="hidden" id="femail" value="<?= $_SESSION['fEmail']?>" style="display:none;" >

        <!--<button class="btn btn-success delSing" onclick="videoCall()" id="videoCallBtn" disabled="disabled"><i class="fa fa-video-camera" ></i> Video Call</button>
        <button class="btn btn-success delSing" onclick="audioCall()" id="audioCallBtn" disabled="disabled"><i class="fa fa-phone-square"></i> Audio Call</button>
        <button class="btn btn-danger delSing"  onclick="endCall()" id="hangupBtn" disabled="disabled"><i class="fa fa-window-close-o"></i> Hangup</button>
        <a href="../daily/calling?id=<?php echo $_GET['id'] ?>" target="_blank" class="btn btn-warning " id="" ><i class="fa fa-phone-square"></i> Patient Call</a> -->
        <a href="javascript:void(0);" onclick="window.open('../daily/calling?id=<?php echo $_GET['id'] ?>','popUpWindow','height=500, width=800, left=100, top=100, resizable=yes, scrollbars=yes, toolbar=yes, menubar=no, location=no, directories=no, status=yes');" class="btn btn-warning " id="" ><i class="fa fa-phone-square"></i> Call Patient</a>

        <a href="../search_patient/patient_details?id=<?php echo $_GET['id'] ?>" class="btn btn-info  pull-right ">View/Edit Patient Details</a>
    </div>
</div>
<!--
<div class="row">
    <div class="col-md-9 text-right">
        <?php /* if(!$_SESSION['isadmin']){*/?>
            <a href="../patient/view_patientdetail?Patient_Id=<?php /*echo $_GET['id'] */?>" class="btn btn-info  pull-left ">Encounter Notes </a>
            <a href="../schedule/index?Patient_Id=<?php /*echo $_GET['id'] */?>" class="btn btn-info  pull-left " style="margin-left: 5px;">Schedule Appointment</a>
        <?php /*}*/?>
        <input type="email" id="receiver" value="<?/*= trim($PatientInfo['Email'])*/?>" style="display:none;" >
        <input type="hidden" id="android_device_ids" value="<?/*= trim($android_device_ids)*/?>" style="display:none;" >
        <input type="hidden" id="ios_device_ids" value="<?/*= trim($ios_device_ids)*/?>" style="display:none;" >
        <input type="hidden" id="femail" value="<?/*= $_SESSION['fEmail']*/?>" style="display:none;" >
        <button class="btn btn-success delSing" onclick="videoCall()" id="videoCallBtn" disabled="disabled"><i class="fa fa-video-camera" ></i> Video Call</button>
        <button class="btn btn-success delSing" onclick="audioCall()" id="audioCallBtn" disabled="disabled"><i class="fa fa-phone-square"></i> Audio Call</button>
        <button class="btn btn-danger delSing"  onclick="endCall()" id="hangupBtn" disabled="disabled"><i class="fa fa-window-close-o"></i> Hangup</button>
        <a href="../search_patient/patient_details?id=<?php /*echo $_GET['id'] */?>" class="btn btn-info  pull-right ">View/Edit Patient Details</a>
    </div>
</div>-->



<!--popup start-->
<div class="modal fade" id="MessageModal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>



            <div class="modal-body">
                <div class="panel-group" id="accordionSettings" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-primary">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordionSettings" href="#collapseChangePassword" aria-expanded="true" aria-controls="collapseOne">Text Message
                                </a>
                            </h4>
                        </div>
                        <div class="alert alert-success text-center" id="success-alert_cp_message"  style="display: none;">
                            Your Message Send Successfully.
                        </div>
                        <div class="alert alert-danger text-center" id="success-alert_cp_message_failed"  style="display: none;">
                            Something went wrong ! <br>Please check Mobile number.
                        </div>

                        <div id="collapseChangePassword" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <form method="post" >
                                <div class="panel-body">
                                    <fieldset class="form-group">
                                        <label>Phone Number</label>
                                        <input type="text" id="phone_number" name="phone_number" class="form-control" readonly value="<?= trim($PatientInfo['Cell_Number'])?>">
                                        <label>Message</label>
                                        <textarea  name="message_text" class="form-control" required id="message_text" maxlength="140" style="resize: none; height: 115px;"></textarea>
                                        <p id="count_message" style="text-align: right;color:red;">Characters left: 140</p>
                                    </fieldset>

                                </div>
                                <div class="panel-footer">
                                    <input type="button" onclick="send_message();" class="btn btn-primary subbtn" value="Send">
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
<!--popup End-->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
<!--<script src="../../js/sinch/sinch.min.js"></script>
<script src="../../js/sinch/novus-sinch.js"></script>

<script>
    $(document).ready(function () {
        <?php /*if($_SESSION['fEmail'] != '') { */?>
        initSinch({
            username: '<?/*=trim($_SESSION['fEmail'])*/?>',//'parmarvikrantr@gmail.com',
            password: '123456' //'<?/*=trim($_SESSION['fPWD'])*/?>'
        });
        <?php /*} */?>
        //createNewUser('<?/*=trim($_SESSION['fEmail'])*/?>','123456')
    });
    function audioCall() {
        console.log("Audio Call to " + $('#receiver').val());
        //pushNotify();
        callAudioSinch($('#receiver').val())
    }

    function videoCall() {
        console.log("Video Call to "+$('#receiver').val());
        //pushNotify();
        callVideoSinch($('#receiver').val())
    }

    function endCall() {
        hangupCall()
    }
    function callLog(dataLog){
        var facilityId =  '<?/*=trim($_SESSION['facilityId'])*/?>';
        var patientId =  '<?/*=$_GET['id']*/?>';
        //duration
        //endCause //endedTime //startedTime
        //console.log("========= IN  ===========");
        //console.log(dataLog);
        //console.log("========= OUT ===========");
        $.ajax({
            url: 'call_log',
            data: {dataLog:dataLog,facilityId:facilityId,patientId:patientId},
            type: 'POST',
            success: function(data)
            {

            }
        });
    }

    function pushNotify(){
        var ios_device_ids =  $('#ios_device_ids').val();
        var android_device_ids =  $('#android_device_ids').val();
        var facilityId = '<?/*=trim($_SESSION['fEmail'])*/?>';
        $.ajax({
            url: 'push_notify',
            data: {ios_device_ids:ios_device_ids,android_device_ids:android_device_ids,facilityId:facilityId},
            type: 'POST',
            success: function(data)
            {

            }
        });
    }
</script>-->