//var apiKey = 'bb6bb04b-fb93-4216-b50b-67faef5a69dd';
//var apiKey = '71bda475-9577-421b-bd3f-022b3d957e82';
//var apiKey = '9a693e34-d1d8-4d0b-87df-de36728610fa';
var apiKey = '2b9cdf29-ee0d-4ac5-8cc3-0da0dcf9900a';
var sinchAudioClient, sinchVideoClient, callAudioClient, callVideoClient, audioCallSinch, videoCallSinch;
var audioEnable = true
var videoEnable = true

var sinchAudioSessionName;
var sinchVideoSessionName;
var receiver;
var pushCalling = false;

/**
 * Initialize Sinch
 */
function initSinch(sinchUserObject) {
    localStorage.setItem('novus-sinch-user', JSON.stringify(sinchUserObject));
    initVideoCall();
    initAudioCall();
}

function hangupCall() {
    audioCallSinch && audioCallSinch.hangup();
    videoCallSinch && videoCallSinch.hangup();
// videoEnable = false;
    enableCallButtons();
}

function enableCallButtons() {
//console.log("Enable button")
//sinchAudioClient.stopActiveConnection()
//sinchVideoClient.stopActiveConnection()
    $('#hangupBtn').prop('disabled', true);
    $('#audioCallBtn').prop('disabled', false);
    $('#videoCallBtn').prop('disabled', false);
}

function disableCallButtons() {
    $('#hangupBtn').prop('disabled', false);
    $('#audioCallBtn').prop('disabled', true);
    $('#videoCallBtn').prop('disabled', true);
}

/**
 *
 * @param email
 * @description create sinch new user
 */
function createNewUser(email) {
// init
    var sinchClient = new SinchClient({
        applicationKey: apiKey,
        capabilities: {
            calling: true,
            video: true
        },
        startActiveConnection: true,
        onLogMessage: function (message) {
            console.log("=====================");
            console.log(message);
            console.log("=====================");
        }
    });

// create object
    var signUpObj = {};
    signUpObj.username = email;
    signUpObj.password = '123456';
    console.log('EMAIL => ' + email);
    console.log(signUpObj);//return false;
    localStorage.setItem('novus-sinch-user', JSON.stringify(signUpObj));
// remove local storage
    sinchAudioSessionName = 'sinchAudioSession-' + sinchClient.applicationKey;
    sinchVideoSessionName = 'sinchVideoSession-' + sinchClient.applicationKey;

    localStorage.removeItem(sinchAudioSessionName);
    localStorage.removeItem(sinchVideoSessionName);

    var sinchUser = JSON.parse(localStorage.getItem('novus-sinch-user'));
    console.log(sinchUser);
    console.log("-----------------------" + sinchUser);
    return false;
//Use Sinch SDK to create a new user
    sinchClient.newUser(sinchUser, function (ticket) {
//On success, start the client
        sinchClient.start(ticket, function () {
            var session = JSON.stringify(sinchClient.getSession());
            localStorage[sinchAudioSessionName] = session;
            localStorage[sinchVideoSessionName] = session;
            initAudioCall();
            initVideoCall();
            console.log(sinchClient);
        }).fail(handleError)
    }).fail(handleError);
}

/**
 * @description sinch terminate
 */
function logoutSinch() {
    sinchAudioClient.terminate();
    sinchVideoClient.terminate();
}

/**
 *
 * @param error
 * @description Error handler
 */
var handleError = function (error) {
    console.log(error.message);
};

var handleInitCall = function (error) {
    console.log("create new user");
    createNewUser();
};

// Audio functionality
/**
 *
 * @description init audio call
 */
function initAudioCall() {
    console.log("Audio calling: ", audioEnable)
    if (audioEnable) {
        console.log("in init audio")
        sinchAudioClient = new SinchClient({
            applicationKey: apiKey,
            capabilities: {
                calling: true,
                video: false
            },
            startActiveConnection: true,
            onLogMessage: function (message) {
                console.log("Audio: ", message);

                if (message.progress === 1) {
                    $('#audioCallBtn').prop('disabled', false);
                }
            }
        });
// Session
        sinchAudioSessionName = 'sinchAudioSession-' + sinchAudioClient.applicationKey
        var audioSessionObj = JSON.parse(localStorage[sinchAudioSessionName] || '{}');
        console.log("audioSessionObj", audioSessionObj);
        if (audioSessionObj.userId) {
            sinchAudioClient.start(audioSessionObj, function () {
                localStorage[sinchAudioSessionName] = JSON.stringify(sinchAudioClient.getSession());
            }).fail(handleError);
        } else {
            var sinchUser = JSON.parse(localStorage.getItem('novus-sinch-user'))
// sinchAudioClient.newUser(sinchUser, function(ticket) {
//     //On success, start the client
//     sinchAudioClient.start(ticket, function () {
//         localStorage[sinchAudioSessionName] = JSON.stringify(sinchAudioClient.getSession());
//     }).fail(handleError);
// }).fail(handleError);
            sinchAudioClient.start(sinchUser, function () {
                localStorage[sinchAudioSessionName] = JSON.stringify(sinchAudioClient.getSession());
            }).fail(function () {
                setTimeout(function () {
                    location.reload();
                }, 20000);
            });
        }

//
        callAudioClient = sinchAudioClient.getCallClient();
    }
}

/**
 *
 * @param receiverName
 * @description make audio call
 */
function callAudioSinch(receiverName) {
    if (audioEnable) {
        disableCallButtons()
        receiver = receiverName;
        audioCallSinch = callAudioClient.callUser(receiverName);
        audioCallSinch.addEventListener(audioCallListeners);
    }
}

/**
 *
 * @type {{onCallProgressing: audioCallListeners.onCallProgressing, onCallEstablished: audioCallListeners.onCallEstablished, onCallEnded: audioCallListeners.onCallEnded}}
 * @description audio call listener
 */
var audioCallListeners = {
    onCallProgressing: function (call) {
        console.log("Audio Ringing...");

        $('audio#ringbackAudio').prop("currentTime", 0);
        $('audio#ringbackAudio').trigger("play");

//Report call stats
        $('#callLog').html('<div id="stats">Ringing...</div>');
    },
    onCallEstablished: function (call) {
        console.log("Audio Call Start...")
        $('audio#incomingAudio').attr('src', call.incomingStreamURL);
        $('audio#ringbackAudio').trigger("pause");
        $('audio#ringtoneAudio').trigger("pause");

//Report call stats
        var callDetails = call.getDetails();
        $('#callLog').html('<div id="stats">Answered at: ' + (callDetails.establishedTime) + '</div>');
    },
    onCallEnded: function (call) {
        console.log("Audio Call Ended...");
        console.log(call.getEndCause());
        console.log(pushCalling);
        /* if (!pushCalling && call.getEndCause() == 'TIMEOUT'){
         console.log("Second Calling");
         $.ajax({
         type:'GET',
         url:'resetPushState',
         success:function(data){
         //setTimeout(function(){pushNotification();}, 10000);
         var count = 0;
         var isApiCalled = false;
         var timeId = setInterval(function(){
         console.log(count);

         $.getJSON('readPushState', function(result){
         console.log(result);

         if(result && result['state']) {
         if (result['state']== 'received'){
         if (isApiCalled)		return;
         clearInterval(timeId);
         console.log('ending');
         callAudioSinch(receiver);
         isApiCalled = true;
         //setTimeout(function(){callVideoSinch(receiver);}, 2000);
         }
         }
         });
         count++;
         if (count == 5){
         pushNotification();
         }
         if (count > 10){
         clearInterval(timeId);
         endAudioCalling(call);
         }
         }, 1000);
         }
         })

         pushCalling = true;
         return;
         } else { */
        endAudioCalling(call);
//		}
    }
};

function endAudioCalling(call) {
    pushCalling = false;

    $('audio#ringbackAudio').trigger("pause");
    $('audio#ringtoneAudio').trigger("pause");
    $('audio#incomingAudio').attr('src', '');

//Report call stats
    var callDetails = call.getDetails();

    var time = callDetails.duration;
// Hours, minutes and seconds
    var hrs = ~~(time / 3600);
    var mins = ~~((time % 3600) / 60);
    var secs = Math.round(time % 60);

// Output like "1:01" or "4:03:59" or "123:03:59"
    var ret = "";

    if (hrs > 0) {
        if (mins == 0) {
            ret += "" + hrs + ":" + (mins == 0 ? "00" : "");
        } else {
            ret += "" + hrs + ":" + (mins < 10 ? "0" : "");
        }

    } else {
        ret += "00:" + mins + ":" + (secs < 10 ? "0" : "");
    }

//ret += "" + mins + ":" + (secs < 10 ? "0" : "");
    ret += "" + secs;
//console.log(ret);

    if (call.error) {
//$('#callLog').html('<div id="stats">Call Ended : ' +callDetails.endedTime+ '<br>Duration (s): ' + callDetails.duration+'<br>End cause:'+call.getEndCause()+'Audio Call Failed : ' + call.error.message+ '</div>');
        $('#callLog').html('<div id="stats">Call Ended : ' + callDetails.endedTime + '<br>Duration: ' + ret + '<br>End cause:' + call.getEndCause() + 'Audio Call Failed : ' + call.error.message + '</div>');
    } else {
//$('#callLog').html('<div id="stats">Call Ended : ' +callDetails.endedTime+ '<br>Duration (s): ' + callDetails.duration+'<br>End cause:'+call.getEndCause()+'</div>');
        $('#callLog').html('<div id="stats">Call Ended : ' + callDetails.endedTime + '<br>Duration: ' + ret + '<br>End cause:' + call.getEndCause() + '</div>');
    }
//$('#callLog').html('<div id="stats">Call Ended : ' +callDetails.endedTime+ '<br>Duration: ' + ret +'<br>End cause:'+call.getEndCause()+'</div>');
//
    var callLodDetails = {
        callType: 'Audio',
        startedTime: callDetails.startedTime,
        endedTime: callDetails.endedTime,
        duration: callDetails.duration,
        getEndCause: call.getEndCause()
    };
    callLog(callLodDetails);
    enableCallButtons();

//
// callAudioClient && callAudioClient.terminate();
// sinchAudioClient.stopActiveConnection()
}


// Video functionality
/**
 *
 * @description init video call
 */
function initVideoCall() {
    console.log("Video calling: ", videoEnable)
    if (videoEnable) {
        console.log("in init Video")
        sinchVideoClient = new SinchClient({
            applicationKey: apiKey,
            capabilities: {
                calling: true,
                video: true
            },
            startActiveConnection: true,
            onLogMessage: function (message) {
                console.log("Video = > : " + message);

                if (message.progress === 1) {
                    $('#videoCallBtn').prop('disabled', false);
                }
            }
        });

        sinchVideoClient.startActiveConnection();

// Session

        sinchVideoSessionName = 'sinchVideoSession-' + sinchVideoClient.applicationKey
        var videoSessionObj = JSON.parse(localStorage[sinchVideoSessionName] || '{}');
        if (videoSessionObj.userId) {
            sinchVideoClient.start(videoSessionObj, function () {
                localStorage[sinchVideoSessionName] = JSON.stringify(sinchVideoClient.getSession());
            }).fail(handleError);
        } else {
            var sinchUser = JSON.parse(localStorage.getItem('novus-sinch-user'));
            sinchVideoClient.start(sinchUser, function () {
                localStorage[sinchVideoSessionName] = JSON.stringify(sinchVideoClient.getSession());
            }).fail(function () {
                sinchVideoClient.newUser(sinchUser, function (ticket) {
//On success, start the client
                    sinchVideoClient.start(ticket, function () {
                        localStorage[sinchVideoSessionName] = JSON.stringify(sinchVideoClient.getSession());
                    }).fail(handleError);
                }).fail(handleError);
            });
        }
        callVideoClient = sinchVideoClient.getCallClient();
    }
}
/**
 *
 * @param receiverName
 * @description make video call
 */
function callVideoSinch(receiverName) {
    if (videoEnable) {
        receiver = receiverName;
        disableCallButtons()
        videoCallSinch = callVideoClient.callUser(receiverName);
        videoCallSinch.addEventListener(videoCallListeners);
    }
}

function pushNotification() {
    var ios_device_ids = $('#ios_device_ids').val();
    var android_device_ids = $('#android_device_ids').val();
    console.log(android_device_ids);
    var facilityId = $('#femail').val();
//var facilityId = '<?php echo $this->session->userdata('fEmail');?>';
    $.ajax({
        url: 'push_notify',
        data: {ios_device_ids: ios_device_ids, android_device_ids: android_device_ids, facilityId: facilityId},
        type: 'POST',
        success: function (data) {
            console.log('aaaaa');
            console.log(data);
        }
    });
}

/**
 *
 * @type {{onCallProgressing: videoCallListeners.onCallProgressing, onCallEstablished: videoCallListeners.onCallEstablished, onCallEnded: videoCallListeners.onCallEnded}}
 * @description video call listener
 */
var videoCallListeners = {
    onCallProgressing: function (call) {
        console.log("Video Ringing...")
        $('audio#ringbackAudio').prop("currentTime", 0);
        $('audio#ringbackAudio').trigger("play");

//Report call stats
        $('#callLog').html('<div id="stats">Ringing...</div>');
    },
    onCallEstablished: function (call) {
        console.log("Video Call Start...")
        $('audio#ringbackAudio').trigger("pause");
        $('audio#ringtoneAudio').trigger("pause");
        console.log("====TESTING======");
        console.log(call);
        console.log("====TESTING END======");
        $('video#outgoingVideo').attr('src', call.outgoingStreamURL);
//$('video#outgoingVideo').attr('src', call.outgoingStream);
        $('video#incomingVideo').attr('src', call.incomingStreamURL);

//Report call stats
        var callDetails = call.getDetails();
        $('#callLog').html('<div id="stats">Answered at: ' + (callDetails.establishedTime) + '</div>');
    },
    onCallEnded: function (call) {


        console.log("Video Call Ended...");

        /* if (!pushCalling && call.getEndCause() == 'TIMEOUT'){
         $.ajax({
         type:'GET',
         url:'resetPushState',
         success:function(data){
         //setTimeout(function(){pushNotification();}, 10000);
         var count = 0;
         var isApiCalled = false;
         var timeId = setInterval(function(){
         console.log(count);

         $.getJSON('readPushState', function(result){
         console.log(result);

         if(result && result['state']) {
         if (result['state']== 'received'){
         if (isApiCalled)		return;
         clearInterval(timeId);
         console.log('ending');
         callVideoSinch(receiver);
         isApiCalled = true;
         //setTimeout(function(){callVideoSinch(receiver);}, 2000);
         }
         }
         });
         count++;
         if (count == 5){
         console.log("Send push notification.....");
         pushNotification();
         }
         if (count > 10){
         clearInterval(timeId);
         endCalling(call);
         }
         }, 1000);
         }
         })

         pushCalling = true;
         return;
         } else { */
        endCalling(call);
//		}
    }

}
function endCalling(call) {
    pushCalling = false;
    $('#ringbackAudio').trigger("pause");
    $('#ringtoneAudio').trigger("pause");

    $('#outgoingVideo').attr('src', '');
    $('#incomingVideo').attr('src', '');

//Report call stats
    var callDetails = call.getDetails();

    var time = callDetails.duration;
// Hours, minutes and seconds
    var hrs = ~~(time / 3600);
    var mins = ~~((time % 3600) / 60);
    var secs = Math.round(time % 60);

// Output like "1:01" or "4:03:59" or "123:03:59"
    var ret = "";

    if (hrs > 0) {
        ret += "" + hrs + ":" + (mins < 10 ? "0" : "");
    } else {
        ret += "00:" + mins + ":" + (secs < 10 ? "0" : "");
    }


    ret += "" + secs;

    if (call.error) {
//$('#callLog').html('<div id="stats">Call Ended : ' +callDetails.endedTime+ '<br>Duration (s): ' + callDetails.duration+'<br>End cause:'+call.getEndCause()+'Audio Call Failed : ' + call.error.message+ '</div>');
        $('#callLog').html('<div id="stats">Call Ended : ' + callDetails.endedTime + '<br>Duration : ' + ret + '<br>End cause:' + call.getEndCause() + 'Audio Call Failed : ' + call.error.message + '</div>');
    } else {
        $('#callLog').html('<div id="stats">Call Ended : ' + callDetails.endedTime + '<br>Duration : ' + ret + '<br>End cause:' + call.getEndCause() + '</div>');
    }
//
    var callLodDetails = {
        callType: 'Video',
        startedTime: callDetails.startedTime,
        endedTime: callDetails.endedTime,
        duration: callDetails.duration,
        getEndCause: call.getEndCause()
    };
    console.log("=====>");
    console.log(callLodDetails);
    callLog(callLodDetails);
    enableCallButtons();

//
// callVideoClient && callVideoClient.terminate();
}