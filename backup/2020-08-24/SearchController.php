<?php

namespace frontend\controllers;

use common\models\Mailbox;
use common\models\Tags;
use common\models\UserPhotos;
use common\models\UserRequest;
use common\models\UserRequestOp;
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use common\models\User;
use yii\widgets\ActiveForm;
use yii\web\Response;
use common\components\MailHelper;
use common\components\CommonHelper;
use common\components\MessageHelper;
use common\components\SmsHelper;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Site controller
 */
class SearchController extends Controller
{
    /**
     * @inheritdoc
     */
    /*public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }*/
    public $STATUS;
    public $MESSAGE;
    public $TITLE;

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],

        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->redirect(['search/basic-search']);
    }

    public function actionBasicSearch($ref = '',$e_id='')
    {
         $checkStep12= CommonHelper::checkVerification(12);
        if(!$checkStep12){
            $redirectSubscriptionUrl = Yii::$app->homeUrl . str_replace("/","",Yii::$app->params['selectPlan']);
            return Yii::$app->response->redirect($redirectSubscriptionUrl);
            #die($redirectSubscriptionUrl);
        }
        $request = Yii::$app->request;
        $session = Yii::$app->session;
        $params = $request->bodyParams;


        #return $this->redirect(Yii::$app->homeUrl);

        #CommonHelper::pr($params);exit;
        //http://localhost/KandePohe/search/basic-search?search-type=basic&profile-for=FEMALE&Community=1&sub-community=1&agerange=19&height=2
        $WHERE = '';
        if (Yii::$app->user->isGuest) {
            #TODO SET URL IN SESSION
            if($e_id !=''){
                #https://localhost/kande_pohe_cr4/search/basic-search?ref=recently_joined&e_id=cGFybWFydmlrcmFudHJAZ21haWwuY29t
                $email = base64_decode($e_id);
                $qstring = Yii::$app->homeUrl .'search/basic-search?ref='.$ref;//$_SERVER['REDIRECT_QUERY_STRING'];
               # die("==>".$qstring);
                $session->set('last_url', $qstring);
                $session->set('url_email', ($email));
            }
            return $this->redirect(Yii::$app->homeUrl . "?ref=login");
            exit;
            #$TempModel = new User();
        } else {
            $id = Yii::$app->user->identity->id;
            $TempModel = User::findOne($id);
            $WhereId = " AND user.id != " . $id;
        }
        if ($TempModel->load(Yii::$app->request->post())) {
            $Gender = $params['User']['Profile_for'];
            $Community = $params['User']['iCommunity_ID'];
            $CountryId = $params['User']['iCountryId'];
            $Gotra = $params['User']['iGotraID'];
            $Mangalik = $params['User']['Mangalik'];
            $SubCommunity = $params['User']['iSubCommunity_ID'];
            $HeightFrom = $params['User']['HeightFrom'];
            $HeightTo = $params['User']['HeightTo'];
            $ReligionID = $params['User']['iReligion_ID'];
            $MaritalStatusID = $params['User']['Marital_Status'];
            #$AgeFrom = $params['User']['AgeFrom'];
            #$AgeTo = $params['User']['AgeFrom'];
            /*if ($params['User']['AgeTo'] != '') {
                list($AgeFrom, $AgeTo) = explode("-", $params['User']['Agerange']);
            } else {*/
            $AgeFrom = $params['User']['AgeFrom'];
            $AgeTo = $params['User']['AgeTo'];
            //}
            $session->set('Profile_for', $Gender);
            $session->set('iCommunity_ID', $Community);
            $session->set('iCountryId', $CountryId);
            $session->set('Mangalik', $Mangalik);
            $session->set('iGotraID', $Gotra);
            $session->set('iSubCommunity_ID', $SubCommunity);
            $session->set('HeightFrom', $HeightFrom);
            $session->set('HeightTo', $HeightTo);
            $session->set('iReligion_ID', $ReligionID);
            $session->set('Marital_Status', $MaritalStatusID);
            $session->set('AgeFrom', $AgeFrom);
            $session->set('AgeTo', $AgeTo);
        }
        else {
            if ($ref != '') {
                $ReffArray = Yii::$app->params['ref'];
                if (array_key_exists($ref, $ReffArray)) {
                    if ($TempModel->Gender == 'MALE') {
                        $Gender = "FEMALE";
                    } else if ($TempModel->Gender == 'FEMALE') {
                        $Gender = "MALE";
                    }
                    $session->set('Profile_for', $Gender);
                    if ($Gender == '') {
                        if ($TempModel->Gender == 'MALE') {
                            $WHERE .= " AND user.Gender = 'FEMALE'";
                        } else if ($TempModel->Gender == 'FEMALE') {
                            $WHERE .= " AND user.Gender = 'MALE'";
                        }
                    }
                    $WHERE .= ($Gender != '') ? ' AND user.Gender = "' . $Gender . '" ' : '';
                    $Limit = Yii::$app->params['searchingLimit'];
                    $Offset = (Yii::$app->request->get('Offset') == 0) ? 0 : Yii::$app->request->get('Offset');
                    $Page = (Yii::$app->request->get('page') == 0 || Yii::$app->request->get('page') == '') ? 0 : Yii::$app->request->get('page');
                    if ($Page) {
                        $Page = $Page - 1;
                        $Offset = $Limit * $Page;
                    } else {
                        $Page = 0;
                        $Offset = 0;
                    }
                    if ($ref != 'recently_joined') {
                        $SearchStatus = 0;
                        $TotalRecords = count(UserRequestOp::getShortList($id, 0));
                        $ShortList = UserRequestOp::getShortList($id, $Offset, $Limit);
                        #CommonHelper::pr($Model);exit;
                        #$UserPhotoModel = new UserPhotos();
                        $Photos = array();
                        if (count($TotalRecords)) {
                            foreach ($ShortList as $Key => $Value) {
                                if ($Value->from_user_id == $id) {
                                    $Model[$Key] = $Value->toUserInfo;
                                    $UserTempId = $Value->to_user_id;
                                } else {
                                    $UserTempId = $Value->from_user_id;
                                    $Model[$Key] = $Value->fromUserInfo;
                                }
                                $Photos[$UserTempId] = $this->getPhotoList($UserTempId);
                            }
                        }
                    } else {
                        $SearchStatus = 1;
                        /*$TotalRecords = count(User::searchBasic($WHERE, 0));
                        $Model = User::searchBasic($WHERE, $Offset, $Limit);
                        #$Photos = array();
                        if (count($Model)) {
                            foreach ($Model as $SK => $SV) {
                                $Photos[$SV->id] = $this->getPhotoList($SV->id);
                            }
                        }*/
                    }
                } else {
                    return $this->render('searchlist',
                        [
                            'ErrorStatus' => 1,
                            'ErrorMessage' => Yii::$app->params['searchListInCorrectErrorMessage']
                        ]
                    );
                }
            } else {
                $Gender = $session->get('Profile_for');
                $Community = $session->get('iCommunity_ID');
                $CountryId = $session->get('iCountryId');
                $Gotra = $session->get('iGotraID');
                $Mangalik = $session->get('Mangalik');
                $SubCommunity = $session->get('iSubCommunity_ID');
                $HeightFrom = $session->get('HeightFrom');
                $HeightTo = $session->get('HeightTo');
                $ReligionID = $session->get('iReligion_ID');
                $MaritalStatusID = $session->get('Marital_Status');
                $AgeFrom = $session->get('AgeFrom');
                $AgeTo = $session->get('AgeTo');
            }
        }
        if ($ref == '') {

            $SearchStatus = 1;
            #$WHERE = '';
            if ($Gender == '') {
                if ($TempModel->Gender == 'MALE') {
                    $WHERE .= " AND user.Gender = 'FEMALE'";
                } else if ($TempModel->Gender == 'FEMALE') {
                    $WHERE .= " AND user.Gender = 'MALE'";
                }
            }
            $WHERE .= ($Gender != '') ? ' AND user.Gender = "' . $Gender . '" ' : '';

            $WHERE .= ($Community != '') ? ' AND user.iCommunity_ID = "' . $Community . '" ' : '';
            $WHERE .= ($CountryId != '') ? ' AND user.iCountryId = "' . $CountryId. '" ' : '';
            $WHERE .= ($Mangalik != '') ? ' AND user.Mangalik = "' . $Mangalik . '" ' : '';
            $WHERE .= ($Gotra != '') ? ' AND user.iGotraID = "' . $Gotra . '" ' : '';
            $WHERE .= ($SubCommunity != '') ? ' AND user.iSubCommunity_ID = "' . $SubCommunity . '" ' : '';
            #$WHERE .= ($HeightFrom != '') ? ' AND user.iHeightID = "' . $HeightFrom . '" ' : '';
            $WHERE .= ($ReligionID != '') ? ' AND user.iReligion_ID = "' . $ReligionID . '" ' : '';
            #$WHERE .= ($MaritalStatusID != '') ? ' AND user.Marital_Status = "' . $MaritalStatusID . '" ' : '';
            $WHERE .= ($MaritalStatusID != '') ? ' AND user.iMaritalStatusID = "' . $MaritalStatusID . '" ' : '';
            $WHERE .= ($AgeFrom != '') ? ' AND ( (user.Age >= "' . $AgeFrom . '") OR (TIMESTAMPDIFF(YEAR, user.DOB, CURDATE()) >= "' . $AgeFrom . '"))' : '';
            $WHERE .= ($AgeTo != '') ? ' AND ((user.Age <= "' . $AgeTo . '") OR (TIMESTAMPDIFF(YEAR, user.DOB, CURDATE()) <= "' . $AgeTo . '")) ' : '';

            $WHERE .= ($HeightFrom != '') ? ' AND master_heights.Centimeters >= "' . $HeightFrom . '" ' : '';
            $WHERE .= ($HeightTo != '') ? ' AND master_heights.Centimeters <= "' . $HeightTo . '" ' : '';

            $WHERE .= $WhereId;
            #echo $WHERE;exit;
            $Limit = Yii::$app->params['searchingLimit'];
            $Offset = (Yii::$app->request->get('Offset') == 0) ? 0 : Yii::$app->request->get('Offset');
            $Page = (Yii::$app->request->get('page') == 0 || Yii::$app->request->get('page') == '') ? 0 : Yii::$app->request->get('page');
            if ($Page) {
                $Page = $Page - 1;
                $Offset = $Limit * $Page;
            } else {
                $Page = 0;
                $Offset = 0;
            }
            $TotalRecords = 0;//count(User::searchBasic($WHERE, 0));
            $Model = array();//User::searchBasic($WHERE, $Offset, $Limit);
            $Photos = array();
            if (count($Model)) {
                foreach ($Model as $SK => $SV) {
                    $Photos[$SV->id] = $this->getPhotoList($SV->id);
                }
            }
            #$id = Yii::$app->user->identity->id;
            #$TempModel = ($id != null) ? User::findOne($id) : array();
            $TempModel->iCommunity_ID = $Community;
            $TempModel->iCountryId = $CountryId;
            $TempModel->iSubCommunity_ID = $SubCommunity;
            $TempModel->iReligion_ID = $ReligionID;
            $TempModel->Marital_Status = $MaritalStatusID;
            $TempModel->HeightFrom = $HeightFrom;
            $TempModel->HeightTo = $HeightTo;
            $TempModel->Profile_for = $Gender;
            $TempModel->AgeFrom = $AgeFrom;
            $TempModel->AgeTo = $AgeTo;
            $TempModel->Mangalik = $Mangalik;
            $TempModel->iGotraID = $Gotra;
        }
        #return $this->render('searchlist_2019_backup',
        $TagList = Tags::find()->orderBy(['Name' => SORT_ASC])->all();

        #$Gender = (Yii::$app->user->identity->Gender == 'MALE') ? 'FEMALE' : 'MALE';
        #$SubCommunityList = User::getSubCommunityID($Gender);
        #echo "<pre>";print_r($SubCommunityList );die("");
        return $this->render('searchlist',
            [
                'ErrorStatus' => 0,
                'SearchStatus' => $SearchStatus,
                'Model' => $Model,
                'TotalRecords' => $TotalRecords,
                'Photos' => $Photos,
                'Offset' => $Offset,
                'Limit' => $Limit,
                'Page' => $Page,
                'TempModel' => $TempModel,
                'TagList' => $TagList

            ]
        );
    }
    public function actionAjaxBasicSearch()
    {
        $request = Yii::$app->request;
        $session = Yii::$app->session;
        $params = $request->bodyParams;
        $WHERE = '';
        $WhereId = '';
        if (Yii::$app->user->isGuest) {
            return $this->redirect(Yii::$app->homeUrl . "?ref=login");
            exit;
            #$TempModel = new User();
        } else {
            $id = Yii::$app->user->identity->id;
            $TempModel = User::findOne($id);
            $WhereId = " AND user.id != " . $id;
        }
        $Gender = "";
        $Community = $params['CommunityIds'];
        $AgeFrom = $params['AgeFrom'];
        $AgeTo = $params['AgeTo'];
        $HeightFrom = ($params['HeightFrom']) ? $params['HeightFrom'] :120;
        $HeightTo = ($params['HeightTo']) ? $params['HeightTo'] : 204;
        $WeightFrom = $params['WeightFrom'];
        $WeightTo = $params['WeightTo'];
        $TagsIds = $params['TagsIds'];
     #   print_r($TagsIds);
        $MaritalStatusID = $params['MaritalStatusID'];
        $manglikStatus = $params['ManglikStatus'];
        $GotraIds = $params['GotraIds'];
        $educationFieldIds = $params['EducationFieldIds'];
        $occupationsIds = $params['OccupationsIds'];
        $PhysicalStatusIDs = $params['PhysicalStatusID'];
        $SkinToneIDs = $params['SkinToneID'];
        $BodyTypeIDs = $params['BodyTypeID'];
        $SmokeTypeIDs = $params['SmokeTypeID'];
        $DrinkTypeIDs = $params['DrinkTypeID'];
        $DietIDs = $params['DietID'];

        $Profile_id = $params['Profile_id'];
        $Keywords = explode(",",addslashes($params['search_by_keyword']));
        #CommonHelper::pr($Keywords);
        #die($Keywords );
        $SearchStatus = 1;
        #$WHERE = '';
        $WHERE .= $WhereId;
        if ($Gender == '') {
            if ($TempModel->Gender == 'MALE') {
                $WHERE .= " AND user.Gender = 'FEMALE'";
            } else if ($TempModel->Gender == 'FEMALE') {
                $WHERE .= " AND user.Gender = 'MALE'";
            }
        }
        $WHERE .= ($Gender != '') ? ' AND user.Gender = "' . $Gender . '" ' : '';
        #die("===".$Profile_id);
        if($Profile_id != ''){
            $WHERE .= ' AND user.Registration_Number= "' . $Profile_id . '" ';
        }else{
            $WHERE .= (count($Community) > 0) ? ' AND user.iCommunity_ID IN (' . implode(",",$Community). ') ' : '';
            #$WHERE .= ($AgeFrom != '') ? ' AND ( (user.Age >= "' . $AgeFrom . '") OR (TIMESTAMPDIFF(YEAR, user.DOB, CURDATE()) >= "' . $AgeFrom . '"))' : '';
            $WHERE .= ($AgeFrom != '') ? ' AND ((TIMESTAMPDIFF(YEAR, user.DOB, CURDATE()) >= ' . $AgeFrom . '))' : '';
            #$WHERE .= ($AgeTo != '') ? ' AND ((user.Age <= "' . $AgeTo . '") OR (TIMESTAMPDIFF(YEAR, user.DOB, CURDATE()) <= "' . $AgeTo . '")) ' : '';
            $WHERE .= ($AgeTo != '') ? ' AND ((TIMESTAMPDIFF(YEAR, user.DOB, CURDATE()) <= ' . $AgeTo . ')) ' : '';
            $WHERE .= (count($TagsIds) > 0) ? ' AND tag_id IN (' . implode(",",$TagsIds). ') ' : '';

            $WHERE .= ($HeightFrom != '') ? ' AND ((master_heights.Centimeters >= "' . $HeightFrom . '" ' : '';
            $WHERE .= ($HeightTo != '') ? ' AND master_heights.Centimeters <= "' . $HeightTo . '" ' : '';
            IF($HeightFrom !=''){
                $WHERE .= ') OR user.iHeightID = "" )' ;
            }

            #$WHERE .= ($WeightFrom != '') ? ' AND user.weight >= '.$WeightFrom.'' : '';
            #$WHERE .= ($WeightTo != '') ? ' AND user.weight <= '.$WeightTo.'' : '';
            $WHERE .= ($WeightFrom != '') ? ' AND ((user.weight >= ' . $WeightFrom . ' ' : '';
            $WHERE .= ($WeightTo != '') ? ' AND user.weight <= ' . $WeightTo . ' ' : '';
            IF($WeightFrom !=''){
                $WHERE .= ') OR user.weight = "" )' ;
            }
            $WHERE .= (count($MaritalStatusID) > 0) ? ' AND user.iMaritalStatusID IN (' . implode(",",$MaritalStatusID). ') ' : '';
            $WHERE .= ($manglikStatus != '' && $manglikStatus !='DoNotMatter') ? ' AND user.Mangalik = "' . $manglikStatus . '" ' : '';
            $WHERE .= (count($GotraIds) > 0) ? ' AND user.iGotraID IN (' . implode(",",$GotraIds). ') ' : '';
            $WHERE .= (count($educationFieldIds) > 0) ? ' AND user.iEducationFieldID IN (' . implode(",",$educationFieldIds). ') ' : '';
            $WHERE .= (count($occupationsIds) > 0) ? ' AND user.iWorkingAsID IN (' . implode(",",$occupationsIds). ') ' : '';
            $WHERE .= (count($PhysicalStatusIDs) > 0) ? ' AND user.vDisability IN (\'' . implode("','",$PhysicalStatusIDs). '\') ' : '';
            // "'" . implode ( "', '", $temp ) . "'"
            $WHERE .= (count($SkinToneIDs) > 0) ? ' AND user.vSkinTone IN (\'' . implode("','",$SkinToneIDs). '\') ' : '';
            $WHERE .= (count($BodyTypeIDs) > 0) ? ' AND user.vBodyType IN (\'' . implode("','",$BodyTypeIDs). '\') ' : '';
            $WHERE .= (count($SmokeTypeIDs) > 0) ? ' AND user.vSmoke IN (\'' . implode("','",$SmokeTypeIDs). '\') ' : '';
            $WHERE .= (count($DrinkTypeIDs) > 0) ? ' AND user.vDrink IN (\'' . implode("','",$DrinkTypeIDs). '\') ' : '';
            $WHERE .= (count($DietIDs) > 0) ? ' AND user.vDiet IN (\'' . implode("','",$DietIDs). '\') ' : '';
            if(count(array_filter($Keywords))>0){
                $WHERE .= ' AND (';
                foreach($Keywords as $k=>$v){
                    if($k > 0){
                        $WHERE .= 'OR ';
                    }
                    $WHERE .= ' user.vAreaName like "%'.$v.'%" OR cities.vCityName like "%'.$v.'%"  OR master_district.vName like "%'.$v.'%"   OR states.vStateName like "%'.$v.'%"  ';
                }
    $WHERE.= ')';
            }

        }

#        $WHERE .= $WhereId;
        #echo $WHERE;exit;
        $Limit = Yii::$app->params['searchingLimit'];
        $Offset = (Yii::$app->request->get('Offset') == 0) ? 0 : Yii::$app->request->get('Offset');
        //$Page = (Yii::$app->request->get('page') == 0 || Yii::$app->request->get('page') == '') ? 0 : Yii::$app->request->get('page');
        $Page = ($params['Page'] == 0 || $params['Page'] == '') ? 0 : $params['Page'];

        if ($Page) {
            $Page = $Page - 1;
            $Offset = $Limit * $Page;
        } else {
            $Page = 0;
            $Offset = 0;
        }
        $all_records = User::searchBasic($WHERE, 0);
        $TotalRecords = count($all_records);// count(User::searchBasic($WHERE, 0));
        $Model = User::searchBasic($WHERE, $Offset, $Limit);
        #CommonHelper::pr($Model);
        #die($WHERE);
        $Photos = array();
        if (count($Model)) {
            foreach ($Model as $SK => $SV) {
                $Photos[$SV->id] = $this->getPhotoList($SV->id);
            }
        }
        $map_records_array = array();
        if($TotalRecords>0){
            foreach($Model as $k=>$v){
                $pro_pic = CommonHelper::getPhotos('USER', $v->id, 'no-photo.jpg', 200);
                if (is_array($Photos[$v->id])) {
                    foreach ($Photos[$v->id] as $K1 => $V1) {
                        $Photo = Yii::$app->params['thumbnailPrefix'] . '200_' . $V1->File_Name;
                        if ($V1['Is_Profile_Photo'] == 'YES') {
                            $SELECTED = "active";
                            $Photo = '200' . $v->propic;
                            $Yes = 'Yes';
                            $pro_pic = CommonHelper::getPhotos('USER', $v->id, $Photo, 200, '', $Yes, CommonHelper::getVisiblePhoto($v->id, $V1['eStatus']));
                        }
                    }
                }
                #if(in_array($v->id,array(1255,1256,1259,1127))) {
                if(0) { //TODO DISABLE MAP VIEW
                    $temp = array();
                    $temp['id'] = $v->id;
                    $temp['registration_number'] = $v->Registration_Number;
                    $temp['city_id'] = $v->Registration_Number;
                    $temp['city_name'] = CommonHelper::setInputVal($v->cityName->vCityName, 'text') . ', ' . CommonHelper::setInputVal($v->countryName->vCountryName, 'text');
                    $temp['lat'] = $v->latitude;
                    $temp['lng'] = $v->longitude;
                    $temp['place_img'] = $pro_pic;
                    #$temp['place_name'] = $v->FullName;
                    $temp['place_name'] = '<a href="'.Yii::$app->homeUrl.'user/profile?uk='.$v->Registration_Number .'&source=profile_viewed_by" class="name" title="'.$v->Registration_Number .'">'.$v->FullName.'</a>';
                    $temp['age'] = CommonHelper::getAge($v->DOB) . " yrs";
                    $temp['user_height'] = CommonHelper::setInputVal($v->height->vName, 'text');
                    $temp['community'] = CommonHelper::setInputVal($v->communityName->vName, 'text');
                    $temp['occupation'] = CommonHelper::setInputVal($v->workingAsName->vWorkingAsName, 'text');
                    $temp['type'] = '';

                    $id= Yii::$app->user->identity->id;
                    $valueSI = \common\models\UserRequestOp::checkSendInterest($id, $v->id);
                    if (count($valueSI)) {
                        if ($id == $valueSI->from_user_id && $valueSI->profile_viewed_from_to == 'Yes') {
                            $ViewerId = $valueSI->to_user_id;
                        } else {
                            $ViewerId = $valueSI->from_user_id;
                        }
                    } else {
                        $ViewerId = $valueSI->id;
                    }
                    $UserInfoModel = \common\models\User::getUserInfroamtion($ViewerId);

                    if (count($valueSI) == 0 || ($id == $valueSI->from_user_id && $valueSI->send_request_status_from_to == 'No' && $valueSI->send_request_status_to_from == 'No') || ($id == $valueSI->to_user_id && $valueSI->send_request_status_to_from == 'No' && $valueSI->send_request_status_from_to == 'No')) {
                        #$temp['type'] =  'SI';
                        $temp['type'] = '<a href="javascript:void(0)"
                                           class="btn btn-interest sendinterestpopup"
                                           role="button"
                                           data-target="#sendInterest"
                                           data-toggle="modal"
                                           data-id="' . $v->id . '"
                                           data-name="' . $v->FullName . '"
                                           data-rgnumber="' . $v->Registration_Number . '"
                                           onclick="send_interest_map(\'' . $v->id . '\',\'' . $v->FullName . '\',\'' . $v->Registration_Number . '\')">Send Interest <i class="fa fa-heart-o"></i>
                                        </a>';
                    } else if (($id == $valueSI->from_user_id && $valueSI->send_request_status_from_to == 'Yes' && $valueSI->send_request_status_to_from != 'Yes') || ($id == $valueSI->to_user_id && $valueSI->send_request_status_to_from == 'Yes' && $valueSI->send_request_status_from_to != 'Yes')) {
                        $temp['type'] = '<a href="javascript:void(0)"
                                           class="btn btn-interest ci hashchange" role="button"
                                           data-target="#"
                                           data-toggle="modal"
                                           data-id="' . $v->id . '"
                                           data-name="' . $v->fullName . '"
                                           data-rgnumber="' . $v->Registration_Number . '"
                                           onclick="accept_decline_map(\'' . $v->id . '\',\'' . $v->FullName . '\',\'' . $v->Registration_Number . '\',\'Cancel Interest\')">Cancel Interest <i class="fa fa-close"></i> </a>';
                    } else if (($id == $valueSI->to_user_id && $valueSI->send_request_status_from_to == 'Yes' && $valueSI->send_request_status_to_from != 'Yes') || ($id == $valueSI->from_user_id && $valueSI->send_request_status_to_from == 'Yes' && $valueSI->send_request_status_from_to != 'Yes')) {
                            //Accept Decline
                        $temp['type'] = '<a href="javascript:void(0)"
                                           class="btn btn-interest accept_decline adbtn"
                                           role="button"
                                           data-target="#accept_decline"
                                           data-toggle="modal"
                                           data-id="' . $UserInfoModel->id . '"
                                           data-name="' . $UserInfoModel->fullName . '"
                                           data-rgnumber="' . $UserInfoModel->Registration_Number . '"
                                           data-type="Accept Interest"
                                           onclick="accept_decline_map(\'' . $UserInfoModel->id . '\',\'' . $UserInfoModel->fullName . '\',\'' . $UserInfoModel->Registration_Number . '\',\'Accept Interest\')"> Accept <i class="fa fa-check"></i>
                                        </a><br><br>
                                        <a href="javascript:void(0)"
                                           class="btn btn-interest accept_decline adbtn"
                                           role="button"
                                           data-target="#accept_decline"
                                           data-toggle="modal"
                                           data-id="' . $UserInfoModel->id . '"
                                           data-name="' . $UserInfoModel->fullName . '"
                                           data-rgnumber="' . $UserInfoModel->Registration_Number . '"
                                           data-type="Decline Interest"
                                           onclick="accept_decline_map(\'' . $UserInfoModel->id . '\',\'' . $UserInfoModel->fullName . '\',\'' . $UserInfoModel->Registration_Number . '\',\'Decline Interest\')"> Decline <i class="fa fa-close"></i>
                                        </a>';

                    } else if ($valueSI->send_request_status_from_to == 'Accepted' || $valueSI->send_request_status_to_from == 'Accepted') {
                        //Connected
                        $temp['type'] = '<a href="javascript:void(0)" class="btn btn-interest"
                                           role="button"
                                           data-target="#" data-toggle="modal"
                                           data-id="' . $UserInfoModel->id . '"
                                           data-name="' . $UserInfoModel->fullName . '"
                                           data-rgnumber="' . $UserInfoModel->Registration_Number . '"
                                           data-type="Connected">Connected <i class="fa fa-heart"></i> </a>';
                    } else if ($valueSI->send_request_status_from_to == 'Rejected' || $valueSI->send_request_status_to_from == 'Rejected') {
                        //rejected
                        $temp['type'] = ' <a href="javascript:void(0)" class="btn btn-interest"
                                           role="button"
                                           data-target="#" data-toggle="modal"
                                           data-id="' . $UserInfoModel->id . '"
                                           data-name="' . $UserInfoModel->fullName . '"
                                           data-rgnumber="' . $UserInfoModel->Registration_Number . '"
                                           data-type="Connected">Rejected <i class="fa fa-close"></i>
                                        </a>';
                    } else {
                        $temp['type'] = '<a href="javascript:void(0)"
                                           class="btn btn-interest isent"
                                           role="button" >Interest Sent <i class="fa fa-heart"></i></a>';
                    }
                    #$temp['type'] = '<a target="_blank" href="https://en.wikipedia.org/w/index.php?title=Uluru&amp;oldid=297882194" class="btn btn-interest ci hashchange" > CLick here</a>';
                    #$temp['type'] = '<button type="button" class="btn btn-info btn-lg image_links" data-toggle="modal" data-target="#myModal">Open Modal</button>';
                    $temp['html'] = '<div id="content"><div class="info-window">
                                        <div class="info-back">
                                            <img src="'.$pro_pic.'"  class="info-window__img"/>
                                                <div class="info-box">
                                                    <h3 class="info-window__heading">
                                                        <span>'.$temp['place_name'].'</span>
                                                    </h3>
                                                    <p class="info-window__text">
                                                        <span>Age:&nbsp; '.$temp['age'].' &nbsp;</span>&nbsp;|&nbsp;<span>Height:&nbsp;  '.$temp['user_height'].'  </span></p>
                                                    <p class="info-window__text"><span>Community:&nbsp; '.$temp['community'].'  </span></p>
                                                    <p class="info-window__text"><span>Occupation:&nbsp;  '.$temp['occupation'].'  </span></p>
                                                    <p class="info-window__btn map_btn_'.$v->id.'"> '.$temp['type'].'
                                                </div>
                                        </div>
                                     </div></div>';
                    $map_records_array[] = $temp;
                }
                //array_push($map_records_array,$temp);
            }
        }
        /*
        {
                "city_id": "mumbai1",
                "city_name": "Mumbai",
                "lat": "19.0760",
                "lng": "72.8790",
                "place_img":"images/image1.jpg",
                "place_name": "Vikas M",
                "age": "30",
                "user_height": "5'7",
                "community": "Maratha",
                "occupation": "Vikas Developer",
            },
        */
        $recordData = "";
        $sendToNext = "";
        $data = array("total_records"=> $TotalRecords, "records"=>$Model,"photos"=>$Photos,"limit"=>$Limit,"page"=>$Page,"Id"=>$id );
        $HtmlOutput = $this->actionRenderCall($data, '_search_list');
        $paginationHTML = $this->actionRenderCall($data, '_search_list_pagination');
       # CommonHelper::pr($map_records_array);
        /*$map_records_array1 = array(
                        array(
                            "city_id"=> "ahmedabad",
                            "city_name"=> "Ahmedabad",
                            "lat"=> "23.0225",
                            "lng"=> "72.5714",
                            "place_img"=>"images/image11.jpg",
                            "place_name"=> "Vikrant Parmar",
                            "age"=> "20",
                            "user_height"=> "5'7",
                            "community"=> "Gujarati",
                            "occupation"=> "Web Developer",
                        ),
                        array(
                            "city_id"=> "kolkata",
                            "city_name"=> "Kolkata",
                            "lat"=> "22.5726",
                            "lng"=> "88.3639",
                            "place_img"=>"images/image1.jpg",
                            "place_name"=> "Swapnil 11Kanherkar",
                            "age"=> "30",
                            "user_height"=> "5'7",
                            "community"=> "Maratha",
                            "occupation"=> "Web Designer",
                        ),
            array(
                            "city_id"=> "kolkata",
                            "city_name"=> "Kolkata",
                            "lat"=> "22.5726",
                            "lng"=> "88.3639",
                            "place_img"=>"images/image1.jpg",
                            "place_name"=> "Swapnil 11Kanherkar",
                            "age"=> "30",
                            "user_height"=> "5'7",
                            "community"=> "Maratha",
                            "occupation"=> "Web Designer",
            )
        );*/
       # CommonHelper::pr($map_records_array1);
        $array = array(
            'ErrorStatus' => 0,
            'SearchStatus' => $SearchStatus,
           // 'Model' => $Model,
            'TotalRecords' => $TotalRecords,
            //'Photos' => $Photos,
            'Offset' => $Offset,
            'Limit' => $Limit,
            'Page' => $Page,
           // 'TempModel' => $TempModel,
            'RecordData' => $HtmlOutput,
            'PaginationData' => $paginationHTML,
            //'stateDiv' => @$stateDiv,
            //'cityDiv' => @$cityDiv,
            'map_records_array' => (@$map_records_array),
        );
        #CommonHelper::pr($array);
        return json_encode($array);exit;
    }

    public
    function getPhotoList($Id)
    {
        $UserPhotoModel = new UserPhotos();
        #$PhotoList = $UserPhotoModel->findByUserId($Id);
        $PhotoList = $UserPhotoModel->userPhotoList($Id);
        if (count($PhotoList)) {
            $Photos = $PhotoList;
        } else {
            $Photos = CommonHelper::getUserDefaultPhoto();
        }
        return $Photos;
    }

    public
    function actionAdvancedSearch()
    {
        $request = Yii::$app->request;
        $session = Yii::$app->session;
        $params = $request->bodyParams;
        #CommonHelper::pr($params);exit;
        //http://localhost/KandePohe/search/basic-search?search-type=basic&profile-for=FEMALE&Community=1&sub-community=1&agerange=19&height=2
        if (Yii::$app->user->isGuest) {
            $TempModel = new User();
        } else {
            $id = Yii::$app->user->identity->id;
            $TempModel = User::findOne($id);
        }

        if ($TempModel->load(Yii::$app->request->post())) {
            $Gender = $params['User']['Profile_for'];
            $Community = $params['User']['iCommunity_ID'];
            $Mangalik = $params['User']['Mangalik'];
            $SubCommunity = $params['User']['iSubCommunity_ID'];
            $Height = $params['User']['iHeightID'];
            $ReligionID = $params['User']['iReligion_ID'];
            $MaritalStatusID = $params['User']['Marital_Status'];
            if ($params['User']['Agerange'] != '') {
                list($AgeFrom, $AgeTo) = explode("-", $params['User']['Agerange']);
            } else {

                $AgeFrom = $params['User']['AgeFrom'];
                $AgeTo = $params['User']['AgeTo'];
            }

            $session->set('Profile_for', $Gender);
            $session->set('iCommunity_ID', $Community);
            $session->set('Mangalik', $Mangalik);
            $session->set('iSubCommunity_ID', $SubCommunity);
            $session->set('iHeightID', $Height);
            $session->set('iReligion_ID', $ReligionID);
            $session->set('Marital_Status', $MaritalStatusID);
            $session->set('AgeFrom', $AgeFrom);
            $session->set('AgeTo', $AgeTo);
        } else {
            $Gender = $session->get('Profile_for');
            $Community = $session->get('iCommunity_ID');
            $Mangalik = $session->get('Mangalik');
            $SubCommunity = $session->get('iSubCommunity_ID');
            $Height = $session->get('iHeightID');
            $ReligionID = $session->get('iReligion_ID');
            $MaritalStatusID = $session->get('Marital_Status');
            $AgeFrom = $session->get('AgeFrom');
            $AgeTo = $session->get('AgeTo');
        }
        $WHERE = '';
        $WHERE .= ($Gender != '') ? ' AND user.Gender = "' . $Gender . '" ' : '';
        $WHERE .= ($Community != '') ? ' AND user.iCommunity_ID = "' . $Community . '" ' : '';
        $WHERE .= ($Mangalik != '') ? ' AND user.Mangalik = "' . $Mangalik . '" ' : '';
        $WHERE .= ($SubCommunity != '') ? ' AND user.iSubCommunity_ID = "' . $SubCommunity . '" ' : '';
        $WHERE .= ($Height != '') ? ' AND user.iHeightID = "' . $Height . '" ' : '';
        $WHERE .= ($ReligionID != '') ? ' AND user.iReligion_ID = "' . $ReligionID . '" ' : '';
        $WHERE .= ($MaritalStatusID != '') ? ' AND user.Marital_Status = "' . $MaritalStatusID . '" ' : '';
        $WHERE .= ($AgeFrom != '') ? ' AND user.Age >= "' . $AgeFrom . '" ' : '';
        $WHERE .= ($AgeTo != '') ? ' AND user.Age <= "' . $AgeTo . '" ' : '';
        $Limit = Yii::$app->params['searchingLimit'];
        $Offset = (Yii::$app->request->get('Offset') == 0) ? 0 : Yii::$app->request->get('Offset');
        $Page = (Yii::$app->request->get('page') == 0 || Yii::$app->request->get('page') == '') ? 0 : Yii::$app->request->get('page');
        if ($Page) {
            $Page = $Page - 1;
            $Offset = $Limit * $Page;
        } else {
            $Page = 0;
            $Offset = 0;
        }

        $TotalRecords = count(User::searchBasic($WHERE, 0));
        $Model = User::searchBasic($WHERE, $Offset, $Limit);

        $UserPhotoModel = new UserPhotos();
        $Photos = array();
        if (count($Model)) {
            foreach ($Model as $SK => $SV) {
                $PhotoList = $UserPhotoModel->findByUserId($SV->id);
                if (count($PhotoList)) {
                    $Photos[$SV->id] = $PhotoList;
                } else {
                    $Photos[$SV->id] = CommonHelper::getUserDefaultPhoto();
                }
            }
        }
        #$id = Yii::$app->user->identity->id;
        #$TempModel = ($id != null) ? User::findOne($id) : array();
        $TempModel->iCommunity_ID = $Community;
        $TempModel->Mangalik = $Mangalik;
        $TempModel->iSubCommunity_ID = $SubCommunity;
        $TempModel->iReligion_ID = $ReligionID;
        $TempModel->Marital_Status = $MaritalStatusID;
        $TempModel->iHeightID = $Height;
        $TempModel->Profile_for = $Gender;
        $TempModel->AgeFrom = $AgeFrom;
        $TempModel->AgeTo = $AgeTo;
        return $this->render('advancesearch',
            [
                'Model' => $Model,
                'TotalRecords' => $TotalRecords,
                'Photos' => $Photos,
                'Offset' => $Offset,
                'Limit' => $Limit,
                'Page' => $Page,
                'TempModel' => $TempModel

            ]
        );
    }

    function actionRenderCall($model, $view, $show = false, $popup = false, $flag = false, $temp = array())
    {
        return $this->renderAjax($view, [
            'model' => $model,
            'show' => $show,
            'popup' => $popup,
            'flag' => $flag,
            'temp' => $temp,
        ]);
    }

    public function actionShortList()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $Id = Yii::$app->user->identity->id;
        $Limit = Yii::$app->params['searchingLimit'];
        $Offset = (Yii::$app->request->get('Offset') == 0) ? 0 : Yii::$app->request->get('Offset');
        $Page = (Yii::$app->request->get('page') == 0 || Yii::$app->request->get('page') == '') ? 0 : Yii::$app->request->get('page');
        if ($Page) {
            $Page = $Page - 1;
            $Offset = $Limit * $Page;
        } else {
            $Page = 0;
            $Offset = 0;
        }
        $ShortList = UserRequestOp::getMyShortListed($Id, $Offset, $Limit);
        #$ShortList = UserRequestOp::getShortList($Id, $Offset, $Limit);
        foreach ($ShortList as $Key => $Value) {
            #CommonHelper::pr($Value);exit;
            if ($Value->from_user_id == $Id) {
                $ModelInfo[$Key] = $Value->toUserInfo;
            } else {
                $ModelInfo[$Key] = $Value->fromUserInfo;
            }
        }
        #CommonHelper::pr($ModelInfo);
        #CommonHelper::pr($ShortList);exit;
        return $this->render('shortlist', [
            'Model' => $ShortList,
            'Id' => $Id,
            'TotalRecords' => count($ShortList),
            'Offset' => $Offset,
            'Limit' => $Limit,
            'Page' => $Page,
        ]);
    }
}